﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bsn.GoldParser.Grammar;
using Test.XML;

namespace Test.JSON
{
    public class GoldTest : GoldParseTest
    {
        private readonly CompiledGrammar _grammar;

        public GoldTest() : base("Gold JSON")
        {
            // ReSharper disable AssignNullToNotNullAttribute
            _grammar = CompiledGrammar.Load(new BinaryReader(typeof(GoldTest).Assembly.GetManifestResourceStream("Test.JSON.JSON.egt")));
            // ReSharper restore AssignNullToNotNullAttribute
        }

        public override CompiledGrammar Grammar
        {
            get { return _grammar; }
        }
    }
}
