﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bsn.GoldParser.Grammar;
using NPEG;
using NPEG.GrammarInterpreter;
using Test.XML;

namespace Test.JSON
{
    public class NpegTest : ParseTest
    {
        private readonly AExpression _grammar;

        public NpegTest()
            : base("Npeg JSON")
        {
            _grammar = PEGrammar.Load(Resources.Npeg);
        }

        public override string ParseFile(string path)
        {
            return ParseString(File.ReadAllText(path));
        }

        public override string ParseString(string str)
        {
            using(StringInputIterator iterator = new StringInputIterator(str, Encoding.ASCII))
            {
                NpegParserVisitor visitor = new NpegParserVisitor(iterator);

                _grammar.Accept(visitor);

                if (!visitor.IsMatch)
                {
                    StringBuilder builder = new StringBuilder().AppendLine("Parse Failed");

                    foreach (string warning in visitor.Warnings.Select(w => string.Format("@{0} - {1}", w.Position, w.Message)))
                        builder.AppendLine(warning);

                    return builder.ToString();
                }
                
                return null;
            }
        }

        public override string ParseReader(TextReader reader)
        {
            return ParseString(reader.ReadToEnd());
        }
    }
}
