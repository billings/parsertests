/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

grammar JSON2;

json:   object
    |   array
    ;

object
    :   '{' (pair (',' pair)*)? '}'
    ;
    
pair:   STRING ':' value ;

array
    :   '[' (value (',' value)*)? ']'
    ;

value
    :   STRING
    |   NUMBER
    |   KEYWORD  // keywords
    |   object  // recursion
    |   array   // recursion
    ;

KEYWORD : 'true' | 'false' | 'null';

STRING :  '"' (ESC | ~["\\])* '"' ;

fragment ESC :   '\\' (["\\/bfnrt] | UNICODE) ;
fragment UNICODE : 'u' HEX HEX HEX HEX ;
fragment HEX : [0-9a-fA-F] ;

NUMBER
    :   '-'? INT ('.' INT)? EXP?
    ;
fragment INT :   '0' | [1-9] [0-9]* ; // no leading zeros
fragment EXP :   [Ee] [+\-]? INT ; // \- since - means "range" inside [...]

WS  :   [ \t\n\r]+ -> skip ;