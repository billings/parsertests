﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr4.Runtime;
using Antlr4.Runtime.Atn;
using JetBrains.Annotations;

namespace Test.JSON
{
    public class AntlrTest : ParseTest
    {
        private readonly StringBuilder _errorBuilder = new StringBuilder();
        private readonly JSONParser _parser = new JSONParser(null);

        public AntlrTest() : base("Antlr JSON")
        {
            _parser.RemoveErrorListeners();
            _parser.AddErrorListener(new ErrorListener(s => _errorBuilder.AppendLine(s)));
        }
        
        public override string ParseFile(string path)
        {
            return ParseString(File.ReadAllText(path));
        }

        public override string ParseString(string str)
        {
            AntlrInputStream input = new AntlrInputStream(str);

            Lexer lexer = new JSONLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);

            _errorBuilder.Clear();
            _parser.SetInputStream(tokens);
            _parser.json();

            if (_errorBuilder.Length <= 0)
                return null;
            
            string error = _errorBuilder.ToString();
            _errorBuilder.Clear();
            
            return error;
        }

        public override string ParseReader(TextReader reader)
        {
            return ParseString(reader.ReadToEnd());
        }
    }
}
