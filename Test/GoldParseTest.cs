﻿using bsn.GoldParser.Grammar;
using bsn.GoldParser.Parser;
using JetBrains.Annotations;
using System;
using System.Diagnostics;
using System.IO;

namespace Test
{
    public abstract class GoldParseTest : ParseTest
    {
        protected GoldParseTest([NotNull] string name) : base(name)
        {
        }

        [NotNull]
        public abstract CompiledGrammar Grammar { get; }

        public override string ParseFile(string path)
        {
            using (var fr = File.OpenRead(path))
            using (var reader = new StreamReader(fr, true))
                return ParseReader(reader);
        }

        public override string ParseString(string str)
        {
            using (var reader = new StringReader(str))
                return ParseReader(reader);
        }

        public override string ParseReader(TextReader reader)
        {
            var tokenizer = new Tokenizer(reader, Grammar);
            var processor = new LalrProcessor(tokenizer, true);
            ParseMessage parseMessage = processor.ParseAll();
            if (parseMessage == ParseMessage.Accept)
                return null;

            Token token = processor.CurrentToken;
            if (token == null)
                return string.Format("{0} - NO TOKEN!", parseMessage);

            int col = token.Position.Column;
            int line = token.Position.Line;
            return string.Format("{0}{1} at line {2}, column {3}.{0}{4}{0}",
                Environment.NewLine,
                parseMessage,
                line,
                col,
                token.Text);
        }
    }
}
