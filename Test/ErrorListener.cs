﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Antlr4.Runtime;
using JetBrains.Annotations;

namespace Test
{
    public class ErrorListener : BaseErrorListener
    {
        [NotNull] 
        public readonly Action<string> ErrorHandler;

        public ErrorListener([CanBeNull] Action<string> errorHandler)
        {
            ErrorHandler = errorHandler ?? (s => { throw new Exception(s); });
        }

        public override void SyntaxError([NotNull] IRecognizer recognizer, [NotNull] IToken offendingSymbol, int line, int charPositionInLine, [NotNull] string msg, [NotNull] RecognitionException e)
        {
            IList<string> stack = ((Parser)recognizer).GetRuleInvocationStack();

            StringBuilder builder = new StringBuilder()
                .AppendFormat("Error: {1}{0}  at Ln: {2} Col: {3}{0}", Environment.NewLine, msg, line, charPositionInLine);

            foreach (var frame in stack.Reverse())
                builder.AppendFormat("  at {1}{0}", Environment.NewLine, frame);

            ErrorHandler(builder.AppendLine().ToString());
        }
    }
}
