﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using NPEG;
using NPEG.ApplicationExceptions;

namespace Test
{
    public class StringInputIterator : IInputIterator, IDisposable
    {
        private readonly Stream _input;

        public StringInputIterator([NotNull] string input, [NotNull] Encoding encoding)
			: base(input.Length)
        {
            _input = new MemoryStream(encoding.GetBytes(input), false);
        }

        [NotNull]
        public override byte[] Text(int start, int end)
        {
            if (start > end)
                throw new IteratorUsageException("Index out of range. End must be >= than Start.  byte[] Text(int start, int end)");

            var buffer = new byte[end - start + 1];
            _input.Position = start;
            _input.Read(buffer, 0, end - start + 1);
            return buffer;
        }

        public override short Current()
		{
			if (Index >= Length) return -1;
			return GetByte(Index);
		}

		public override short Next()
		{
			if (Index >= Length) return -1;
			Index += 1;

			if (Index >= Length) return -1;
			return GetByte(Index);
		}

		public override short Previous()
		{
			if (Index <= 0)
			{
				return -1;
			}

			Index -= 1;

			return GetByte(Index);
		}


		private short GetByte(Int32 index)
		{
			var b = new Byte[1];
			_input.Position = index;
			_input.Read(b, 0, 1);
			return b[0];
		}

        public void Dispose()
        {
            _input.Dispose();
        }
    }
}
