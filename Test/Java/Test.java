/*      */ import org.lwjgl.opengl.GL11;
/*      */ 
/*      */ public class ble
/*      */ {
/*      */   public afx a;
/*      */   private ps d;
/*      */   private boolean e;
/*      */   private boolean f;
/*   26 */   public static boolean b = true;
/*   27 */   public boolean c = true;
/*   28 */   private boolean g = false;
/*      */   private double h;
/*      */   private double i;
/*      */   private double j;
/*      */   private double k;
/*      */   private double l;
/*      */   private double m;
/*      */   private boolean n;
/*      */   private boolean o;
/*      */   private final azd p;
/*      */   private int q;
/*      */   private int r;
/*      */   private int s;
/*      */   private int t;
/*      */   private int u;
/*      */   private int v;
/*      */   private boolean w;
/*      */   private float x;
/*      */   private float y;
/*      */   private float z;
/*      */   private float A;
/*      */   private float B;
/*      */   private float C;
/*      */   private float D;
/*      */   private float E;
/*      */   private float F;
/*      */   private float G;
/*      */   private float H;
/*      */   private float I;
/*      */   private float J;
/*      */   private float K;
/*      */   private float L;
/*      */   private float M;
/*      */   private float N;
/*      */   private float O;
/*      */   private float P;
/*      */   private float Q;
/*      */   private int R;
/*      */   private int S;
/*      */   private int T;
/*      */   private int U;
/*      */   private int V;
/*      */   private int W;
/*      */   private int X;
/*      */   private int Y;
/*      */   private int Z;
/*      */   private int aa;
/*      */   private int ab;
/*      */   private int ac;
/*      */   private int ad;
/*      */   private int ae;
/*      */   private int af;
/*      */   private int ag;
/*      */   private int ah;
/*      */   private int ai;
/*      */   private int aj;
/*      */   private int ak;
/*      */   private int al;
/*      */   private int am;
/*      */   private int an;
/*      */   private int ao;
/*      */   private float ap;
/*      */   private float aq;
/*      */   private float ar;
/*      */   private float as;
/*      */   private float at;
/*      */   private float au;
/*      */   private float av;
/*      */   private float aw;
/*      */   private float ax;
/*      */   private float ay;
/*      */   private float az;
/*      */   private float aA;
/*      */   
/*      */   public ble(afx paramafx)
/*      */   {
/*   41 */     this.a = paramafx;
/*   42 */     this.p = azd.A();
/*      */   }
/*      */   
/*      */   public ble()
/*      */   {
/*   46 */     this.p = azd.A();
/*      */   }
/*      */   
/*      */   public void a(ps paramps)
/*      */   {
/*   50 */     this.d = paramps;
/*      */   }
/*      */   
/*      */   public void a()
/*      */   {
/*   54 */     this.d = null;
/*      */   }
/*      */   
/*      */   public boolean b()
/*      */   {
/*   58 */     return this.d != null;
/*      */   }
/*      */   
/*      */   public void a(boolean paramBoolean)
/*      */   {
/*   62 */     this.g = paramBoolean;
/*      */   }
/*      */   
/*      */   public void b(boolean paramBoolean)
/*      */   {
/*   66 */     this.f = paramBoolean;
/*      */   }
/*      */   
/*      */   public void a(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6)
/*      */   {
/*   70 */     if (!this.n)
/*      */     {
/*   71 */       this.h = paramDouble1;
/*   72 */       this.i = paramDouble4;
/*   73 */       this.j = paramDouble2;
/*   74 */       this.k = paramDouble5;
/*   75 */       this.l = paramDouble3;
/*   76 */       this.m = paramDouble6;
/*   77 */       this.o = ((this.p.u.j >= 2) && ((this.h > 0.0D) || (this.i < 1.0D) || (this.j > 0.0D) || (this.k < 1.0D) || (this.l > 0.0D) || (this.m < 1.0D)));
/*      */     }
/*      */   }
/*      */   
/*      */   public void a(ahu paramahu)
/*      */   {
/*   82 */     if (!this.n)
/*      */     {
/*   83 */       this.h = paramahu.x();
/*   84 */       this.i = paramahu.y();
/*   85 */       this.j = paramahu.z();
/*   86 */       this.k = paramahu.A();
/*   87 */       this.l = paramahu.B();
/*   88 */       this.m = paramahu.C();
/*   89 */       this.o = ((this.p.u.j >= 2) && ((this.h > 0.0D) || (this.i < 1.0D) || (this.j > 0.0D) || (this.k < 1.0D) || (this.l > 0.0D) || (this.m < 1.0D)));
/*      */     }
/*      */   }
/*      */   
/*      */   public void b(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6)
/*      */   {
/*   94 */     this.h = paramDouble1;
/*   95 */     this.i = paramDouble4;
/*   96 */     this.j = paramDouble2;
/*   97 */     this.k = paramDouble5;
/*   98 */     this.l = paramDouble3;
/*   99 */     this.m = paramDouble6;
/*  100 */     this.n = true;
/*      */     
/*  102 */     this.o = ((this.p.u.j >= 2) && ((this.h > 0.0D) || (this.i < 1.0D) || (this.j > 0.0D) || (this.k < 1.0D) || (this.l > 0.0D) || (this.m < 1.0D)));
/*      */   }
/*      */   
/*      */   public void c()
/*      */   {
/*  106 */     this.n = false;
/*      */   }
/*      */   
/*      */   public void a(ahu paramahu, int paramInt1, int paramInt2, int paramInt3, ps paramps)
/*      */   {
/*  110 */     a(paramps);
/*  111 */     b(paramahu, paramInt1, paramInt2, paramInt3);
/*  112 */     a();
/*      */   }
/*      */   
/*      */   public void a(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  116 */     this.f = true;
/*  117 */     b(paramahu, paramInt1, paramInt2, paramInt3);
/*  118 */     this.f = false;
/*      */   }
/*      */   
/*      */   public boolean b(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  122 */     int i1 = paramahu.b();
/*  124 */     if (i1 == -1) {
/*  125 */       return false;
/*      */     }
/*  128 */     paramahu.a(this.a, paramInt1, paramInt2, paramInt3);
/*  129 */     a(paramahu);
/*  130 */     if (i1 == 0) {
/*  131 */       return q(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  132 */     if (i1 == 4) {
/*  133 */       return p(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  134 */     if (i1 == 31) {
/*  135 */       return r(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  136 */     if (i1 == 1) {
/*  137 */       return l(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  138 */     if (i1 == 40) {
/*  139 */       return a((aja)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  140 */     if (i1 == 2) {
/*  141 */       return c(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  142 */     if (i1 == 20) {
/*  143 */       return j(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  144 */     if (i1 == 11) {
/*  145 */       return a((ajl)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  146 */     if (i1 == 39) {
/*  147 */       return s(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  148 */     if (i1 == 5) {
/*  149 */       return h(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  150 */     if (i1 == 13) {
/*  151 */       return t(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  152 */     if (i1 == 9) {
/*  153 */       return a((ahq)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  154 */     if (i1 == 19) {
/*  155 */       return m(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  156 */     if (i1 == 23) {
/*  157 */       return o(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  158 */     if (i1 == 6) {
/*  159 */       return n(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  160 */     if (i1 == 3) {
/*  161 */       return a((ajn)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  162 */     if (i1 == 8) {
/*  163 */       return i(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  164 */     if (i1 == 7) {
/*  165 */       return u(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  166 */     if (i1 == 10) {
/*  167 */       return a((ame)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  168 */     if (i1 == 27) {
/*  169 */       return a((ajb)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  170 */     if (i1 == 32) {
/*  171 */       return a((amu)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  172 */     if (i1 == 12) {
/*  173 */       return e(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  174 */     if (i1 == 29) {
/*  175 */       return f(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  176 */     if (i1 == 30) {
/*  177 */       return g(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  178 */     if (i1 == 14) {
/*  179 */       return v(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  180 */     if (i1 == 15) {
/*  181 */       return a((alr)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  182 */     if (i1 == 36) {
/*  183 */       return a((aiv)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  184 */     if (i1 == 37) {
/*  185 */       return a((aio)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  186 */     if (i1 == 16) {
/*  187 */       return b(paramahu, paramInt1, paramInt2, paramInt3, false);
/*      */     }
/*  188 */     if (i1 == 17) {
/*  189 */       return c(paramahu, paramInt1, paramInt2, paramInt3, true);
/*      */     }
/*  190 */     if (i1 == 18) {
/*  191 */       return a((amm)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  192 */     if (i1 == 41) {
/*  193 */       return k(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  194 */     if (i1 == 21) {
/*  195 */       return a((ajm)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  196 */     if (i1 == 24) {
/*  197 */       return a((aii)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  198 */     if (i1 == 33) {
/*  199 */       return a((ajp)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  200 */     if (i1 == 35) {
/*  201 */       return a((ahn)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  202 */     if (i1 == 25) {
/*  203 */       return a((aic)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  204 */     if (i1 == 26) {
/*  205 */       return a((ajg)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  206 */     if (i1 == 28) {
/*  207 */       return a((ail)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  208 */     if (i1 == 34) {
/*  209 */       return a((ahs)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  210 */     if (i1 == 38) {
/*  211 */       return a((ajz)paramahu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  213 */     return false;
/*      */   }
/*      */   
/*      */   private boolean a(ajg paramajg, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  218 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/*      */     
/*  220 */     int i2 = i1 & 0x3;
/*  221 */     if (i2 == 0) {
/*  222 */       this.u = 3;
/*  223 */     } else if (i2 == 3) {
/*  224 */       this.u = 1;
/*  225 */     } else if (i2 == 1) {
/*  226 */       this.u = 2;
/*      */     }
/*  229 */     if (!ajg.b(i1))
/*      */     {
/*  230 */       a(0.0D, 0.0D, 0.0D, 1.0D, 0.8125D, 1.0D);
/*  231 */       q(paramajg, paramInt1, paramInt2, paramInt3);
/*      */       
/*  233 */       this.u = 0;
/*  234 */       return true;
/*      */     }
/*  237 */     this.f = true;
/*  238 */     a(0.0D, 0.0D, 0.0D, 1.0D, 0.8125D, 1.0D);
/*  239 */     q(paramajg, paramInt1, paramInt2, paramInt3);
/*  240 */     a(paramajg.e());
/*  241 */     a(0.25D, 0.8125D, 0.25D, 0.75D, 1.0D, 0.75D);
/*  242 */     q(paramajg, paramInt1, paramInt2, paramInt3);
/*  243 */     this.f = false;
/*  244 */     a();
/*      */     
/*  246 */     this.u = 0;
/*  247 */     return true;
/*      */   }
/*      */   
/*      */   private boolean v(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  252 */     blz localblz = blz.a;
/*      */     
/*  254 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/*  255 */     int i2 = aht.l(i1);
/*  256 */     boolean bool = aht.b(i1);
/*      */     
/*  258 */     float f1 = 0.5F;
/*  259 */     float f2 = 1.0F;
/*  260 */     float f3 = 0.8F;
/*  261 */     float f4 = 0.6F;
/*      */     
/*  263 */     float f5 = f2;
/*  264 */     float f6 = f2;
/*  265 */     float f7 = f2;
/*      */     
/*  267 */     float f8 = f1;
/*  268 */     float f9 = f3;
/*  269 */     float f10 = f4;
/*      */     
/*  271 */     float f11 = f1;
/*  272 */     float f12 = f3;
/*  273 */     float f13 = f4;
/*      */     
/*  275 */     float f14 = f1;
/*  276 */     float f15 = f3;
/*  277 */     float f16 = f4;
/*      */     
/*      */ 
/*  280 */     int i3 = paramahu.c(this.a, paramInt1, paramInt2, paramInt3);
/*      */     
/*      */ 
/*      */ 
/*  284 */     localblz.b(i3);
/*  285 */     localblz.b(f8, f11, f14);
/*      */     
/*  287 */     ps localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 0);
/*  288 */     double d1 = localps.c();
/*  289 */     double d2 = localps.d();
/*  290 */     double d3 = localps.e();
/*  291 */     double d4 = localps.f();
/*      */     
/*  293 */     double d5 = paramInt1 + this.h;
/*  294 */     double d6 = paramInt1 + this.i;
/*  295 */     double d7 = paramInt2 + this.j + 0.1875D;
/*  296 */     double d8 = paramInt3 + this.l;
/*  297 */     double d9 = paramInt3 + this.m;
/*      */     
/*  299 */     localblz.a(d5, d7, d9, d1, d4);
/*  300 */     localblz.a(d5, d7, d8, d1, d3);
/*  301 */     localblz.a(d6, d7, d8, d2, d3);
/*  302 */     localblz.a(d6, d7, d9, d2, d4);
/*      */     
/*      */ 
/*      */ 
/*  306 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3));
/*  307 */     localblz.b(f5, f6, f7);
/*      */     
/*  309 */     localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 1);
/*  310 */     d1 = localps.c();
/*  311 */     d2 = localps.d();
/*  312 */     d3 = localps.e();
/*  313 */     d4 = localps.f();
/*      */     
/*  315 */     d5 = d1;
/*  316 */     d6 = d2;
/*  317 */     d7 = d3;
/*  318 */     d8 = d3;
/*  319 */     d9 = d1;
/*  320 */     double d10 = d2;
/*  321 */     double d11 = d4;
/*  322 */     double d12 = d4;
/*  324 */     if (i2 == 0)
/*      */     {
/*  326 */       d6 = d1;
/*  327 */       d7 = d4;
/*  328 */       d9 = d2;
/*  329 */       d12 = d3;
/*      */     }
/*  330 */     else if (i2 == 2)
/*      */     {
/*  332 */       d5 = d2;
/*  333 */       d8 = d4;
/*  334 */       d10 = d1;
/*  335 */       d11 = d3;
/*      */     }
/*  336 */     else if (i2 == 3)
/*      */     {
/*  338 */       d5 = d2;
/*  339 */       d8 = d4;
/*  340 */       d10 = d1;
/*  341 */       d11 = d3;
/*  342 */       d6 = d1;
/*  343 */       d7 = d4;
/*  344 */       d9 = d2;
/*  345 */       d12 = d3;
/*      */     }
/*  348 */     double d13 = paramInt1 + this.h;
/*  349 */     double d14 = paramInt1 + this.i;
/*  350 */     double d15 = paramInt2 + this.k;
/*  351 */     double d16 = paramInt3 + this.l;
/*  352 */     double d17 = paramInt3 + this.m;
/*      */     
/*  354 */     localblz.a(d14, d15, d17, d9, d11);
/*  355 */     localblz.a(d14, d15, d16, d5, d7);
/*  356 */     localblz.a(d13, d15, d16, d6, d8);
/*  357 */     localblz.a(d13, d15, d17, d10, d12);
/*      */     
/*      */ 
/*  360 */     int i4 = p.d[i2];
/*  361 */     if (bool) {
/*  362 */       i4 = p.d[p.f[i2]];
/*      */     }
/*  365 */     int i5 = 4;
/*  366 */     switch (i2)
/*      */     {
/*      */     case 2: 
/*      */       break;
/*      */     case 0: 
/*  370 */       i5 = 5;
/*  371 */       break;
/*      */     case 3: 
/*  373 */       i5 = 2;
/*  374 */       break;
/*      */     case 1: 
/*  376 */       i5 = 3;
/*      */     }
/*  380 */     if ((i4 != 2) && ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2, paramInt3 - 1, 2))))
/*      */     {
/*  381 */       localblz.b(this.l > 0.0D ? i3 : paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1));
/*  382 */       localblz.b(f9, f12, f15);
/*  383 */       this.e = (i5 == 2);
/*  384 */       c(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 2));
/*      */     }
/*  387 */     if ((i4 != 3) && ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2, paramInt3 + 1, 3))))
/*      */     {
/*  388 */       localblz.b(this.m < 1.0D ? i3 : paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1));
/*  389 */       localblz.b(f9, f12, f15);
/*  390 */       this.e = (i5 == 3);
/*  391 */       d(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 3));
/*      */     }
/*  394 */     if ((i4 != 4) && ((this.f) || (paramahu.a(this.a, paramInt1 - 1, paramInt2, paramInt3, 4))))
/*      */     {
/*  395 */       localblz.b(this.l > 0.0D ? i3 : paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3));
/*  396 */       localblz.b(f10, f13, f16);
/*  397 */       this.e = (i5 == 4);
/*  398 */       e(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 4));
/*      */     }
/*  401 */     if ((i4 != 5) && ((this.f) || (paramahu.a(this.a, paramInt1 + 1, paramInt2, paramInt3, 5))))
/*      */     {
/*  402 */       localblz.b(this.m < 1.0D ? i3 : paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3));
/*  403 */       localblz.b(f10, f13, f16);
/*  404 */       this.e = (i5 == 5);
/*  405 */       f(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 5));
/*      */     }
/*  407 */     this.e = false;
/*  408 */     return true;
/*      */   }
/*      */   
/*      */   private boolean a(aic paramaic, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  414 */     a(0.4375D, 0.0D, 0.4375D, 0.5625D, 0.875D, 0.5625D);
/*  415 */     q(paramaic, paramInt1, paramInt2, paramInt3);
/*      */     
/*  417 */     a(paramaic.e());
/*      */     
/*      */ 
/*  420 */     this.f = true;
/*  421 */     a(0.5625D, 0.0D, 0.3125D, 0.9375D, 0.125D, 0.6875D);
/*  422 */     q(paramaic, paramInt1, paramInt2, paramInt3);
/*  423 */     a(0.125D, 0.0D, 0.0625D, 0.5D, 0.125D, 0.4375D);
/*  424 */     q(paramaic, paramInt1, paramInt2, paramInt3);
/*  425 */     a(0.125D, 0.0D, 0.5625D, 0.5D, 0.125D, 0.9375D);
/*  426 */     q(paramaic, paramInt1, paramInt2, paramInt3);
/*  427 */     this.f = false;
/*      */     
/*  429 */     a();
/*      */     
/*  431 */     blz localblz = blz.a;
/*      */     
/*  433 */     localblz.b(paramaic.c(this.a, paramInt1, paramInt2, paramInt3));
/*  434 */     int i1 = paramaic.d(this.a, paramInt1, paramInt2, paramInt3);
/*  435 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/*  436 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/*  437 */     float f3 = (i1 & 0xFF) / 255.0F;
/*  439 */     if (bll.a)
/*      */     {
/*  440 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/*  441 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/*  442 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/*  444 */       f1 = f4;
/*  445 */       f2 = f5;
/*  446 */       f3 = f6;
/*      */     }
/*  448 */     localblz.b(f1, f2, f3);
/*      */     
/*  450 */     ps localps = a(paramaic, 0, 0);
/*  452 */     if (b()) {
/*  452 */       localps = this.d;
/*      */     }
/*  453 */     double d1 = localps.e();
/*  454 */     double d2 = localps.f();
/*      */     
/*  456 */     int i2 = this.a.e(paramInt1, paramInt2, paramInt3);
/*  458 */     for (int i3 = 0; i3 < 3; i3++)
/*      */     {
/*  460 */       double d3 = i3 * 3.141592653589793D * 2.0D / 3.0D + 1.570796326794897D;
/*      */       
/*  462 */       double d4 = localps.a(8.0D);
/*  463 */       double d5 = localps.d();
/*  464 */       if ((i2 & 1 << i3) != 0) {
/*  465 */         d5 = localps.c();
/*      */       }
/*  468 */       double d6 = paramInt1 + 0.5D;
/*  469 */       double d7 = paramInt1 + 0.5D + Math.sin(d3) * 8.0D / 16.0D;
/*  470 */       double d8 = paramInt3 + 0.5D;
/*  471 */       double d9 = paramInt3 + 0.5D + Math.cos(d3) * 8.0D / 16.0D;
/*      */       
/*  473 */       localblz.a(d6, paramInt2 + 1, d8, d4, d1);
/*  474 */       localblz.a(d6, paramInt2 + 0, d8, d4, d2);
/*  475 */       localblz.a(d7, paramInt2 + 0, d9, d5, d2);
/*  476 */       localblz.a(d7, paramInt2 + 1, d9, d5, d1);
/*      */       
/*  478 */       localblz.a(d7, paramInt2 + 1, d9, d5, d1);
/*  479 */       localblz.a(d7, paramInt2 + 0, d9, d5, d2);
/*  480 */       localblz.a(d6, paramInt2 + 0, d8, d4, d2);
/*  481 */       localblz.a(d6, paramInt2 + 1, d8, d4, d1);
/*      */     }
/*  484 */     paramaic.g();
/*      */     
/*  486 */     return true;
/*      */   }
/*      */   
/*      */   private boolean a(aii paramaii, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  491 */     q(paramaii, paramInt1, paramInt2, paramInt3);
/*      */     
/*  493 */     blz localblz = blz.a;
/*      */     
/*  495 */     localblz.b(paramaii.c(this.a, paramInt1, paramInt2, paramInt3));
/*  496 */     int i1 = paramaii.d(this.a, paramInt1, paramInt2, paramInt3);
/*  497 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/*  498 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/*  499 */     float f3 = (i1 & 0xFF) / 255.0F;
/*  501 */     if (bll.a)
/*      */     {
/*  502 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/*  503 */       f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/*  504 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/*  506 */       f1 = f4;
/*  507 */       f2 = f5;
/*  508 */       f3 = f6;
/*      */     }
/*  510 */     localblz.b(f1, f2, f3);
/*      */     
/*      */ 
/*  513 */     ps localps1 = paramaii.h(2);
/*  514 */     float f5 = 0.125F;
/*  515 */     f(paramaii, paramInt1 - 1.0F + f5, paramInt2, paramInt3, localps1);
/*  516 */     e(paramaii, paramInt1 + 1.0F - f5, paramInt2, paramInt3, localps1);
/*  517 */     d(paramaii, paramInt1, paramInt2, paramInt3 - 1.0F + f5, localps1);
/*  518 */     c(paramaii, paramInt1, paramInt2, paramInt3 + 1.0F - f5, localps1);
/*      */     
/*  520 */     ps localps2 = aii.e("inner");
/*  521 */     b(paramaii, paramInt1, paramInt2 - 1.0F + 0.25F, paramInt3, localps2);
/*  522 */     a(paramaii, paramInt1, paramInt2 + 1.0F - 0.75F, paramInt3, localps2);
/*      */     
/*  524 */     int i2 = this.a.e(paramInt1, paramInt2, paramInt3);
/*  525 */     if (i2 > 0)
/*      */     {
/*  526 */       ps localps3 = aki.e("water_still");
/*      */       
/*  528 */       b(paramaii, paramInt1, paramInt2 - 1.0F + aii.c(i2), paramInt3, localps3);
/*      */     }
/*  531 */     return true;
/*      */   }
/*      */   
/*      */   private boolean a(ajp paramajp, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  536 */     q(paramajp, paramInt1, paramInt2, paramInt3);
/*      */     
/*  538 */     blz localblz = blz.a;
/*      */     
/*  540 */     localblz.b(paramajp.c(this.a, paramInt1, paramInt2, paramInt3));
/*  541 */     int i1 = paramajp.d(this.a, paramInt1, paramInt2, paramInt3);
/*  542 */     ps localps = a(paramajp, 0);
/*  543 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/*  544 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/*  545 */     float f3 = (i1 & 0xFF) / 255.0F;
/*  547 */     if (bll.a)
/*      */     {
/*  548 */       f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/*  549 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/*  550 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/*  552 */       f1 = f4;
/*  553 */       f2 = f5;
/*  554 */       f3 = f6;
/*      */     }
/*  556 */     localblz.b(f1, f2, f3);
/*      */     
/*      */ 
/*      */ 
/*  560 */     float f4 = 0.1865F;
/*  561 */     f(paramajp, paramInt1 - 0.5F + f4, paramInt2, paramInt3, localps);
/*  562 */     e(paramajp, paramInt1 + 0.5F - f4, paramInt2, paramInt3, localps);
/*  563 */     d(paramajp, paramInt1, paramInt2, paramInt3 - 0.5F + f4, localps);
/*  564 */     c(paramajp, paramInt1, paramInt2, paramInt3 + 0.5F - f4, localps);
/*      */     
/*  566 */     b(paramajp, paramInt1, paramInt2 - 0.5F + f4 + 0.1875F, paramInt3, b(ahz.d));
/*      */     
/*  568 */     and localand = this.a.o(paramInt1, paramInt2, paramInt3);
/*  569 */     if ((localand != null) && ((localand instanceof anr)))
/*      */     {
/*  570 */       abn localabn = ((anr)localand).a();
/*  571 */       int i2 = ((anr)localand).b();
/*  573 */       if ((localabn instanceof zt))
/*      */       {
/*  574 */         ahu localahu = ahu.a(localabn);
/*  575 */         int i3 = localahu.b();
/*      */         
/*  577 */         float f7 = 0.0F;
/*  578 */         float f8 = 4.0F;
/*  579 */         float f9 = 0.0F;
/*      */         
/*  581 */         localblz.d(f7 / 16.0F, f8 / 16.0F, f9 / 16.0F);
/*      */         
/*  583 */         i1 = localahu.d(this.a, paramInt1, paramInt2, paramInt3);
/*  584 */         if (i1 != 16777215)
/*      */         {
/*  585 */           f1 = (i1 >> 16 & 0xFF) / 255.0F;
/*  586 */           f2 = (i1 >> 8 & 0xFF) / 255.0F;
/*  587 */           f3 = (i1 & 0xFF) / 255.0F;
/*  588 */           localblz.b(f1, f2, f3);
/*      */         }
/*  591 */         if (i3 == 1)
/*      */         {
/*  592 */           a(a(localahu, 0, i2), paramInt1, paramInt2, paramInt3, 0.75F);
/*      */         }
/*  593 */         else if (i3 == 13)
/*      */         {
/*  595 */           this.f = true;
/*      */           
/*  597 */           float f10 = 0.125F;
/*  598 */           a(0.5F - f10, 0.0D, 0.5F - f10, 0.5F + f10, 0.25D, 0.5F + f10);
/*  599 */           q(localahu, paramInt1, paramInt2, paramInt3);
/*  600 */           a(0.5F - f10, 0.25D, 0.5F - f10, 0.5F + f10, 0.5D, 0.5F + f10);
/*  601 */           q(localahu, paramInt1, paramInt2, paramInt3);
/*  602 */           a(0.5F - f10, 0.5D, 0.5F - f10, 0.5F + f10, 0.75D, 0.5F + f10);
/*  603 */           q(localahu, paramInt1, paramInt2, paramInt3);
/*  604 */           this.f = false;
/*      */           
/*  606 */           a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/*      */         }
/*  609 */         localblz.d(-f7 / 16.0F, -f8 / 16.0F, -f9 / 16.0F);
/*      */       }
/*      */     }
/*  613 */     return true;
/*      */   }
/*      */   
/*      */   private boolean a(ahn paramahn, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  617 */     return a(paramahn, paramInt1, paramInt2, paramInt3, this.a.e(paramInt1, paramInt2, paramInt3));
/*      */   }
/*      */   
/*      */   public boolean a(ahn paramahn, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
/*      */   {
/*  622 */     blz localblz = blz.a;
/*      */     
/*  624 */     localblz.b(paramahn.c(this.a, paramInt1, paramInt2, paramInt3));
/*  625 */     int i1 = paramahn.d(this.a, paramInt1, paramInt2, paramInt3);
/*  626 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/*  627 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/*  628 */     float f3 = (i1 & 0xFF) / 255.0F;
/*  630 */     if (bll.a)
/*      */     {
/*  631 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/*  632 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/*  633 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/*  635 */       f1 = f4;
/*  636 */       f2 = f5;
/*  637 */       f3 = f6;
/*      */     }
/*  639 */     localblz.b(f1, f2, f3);
/*      */     
/*  641 */     return a(paramahn, paramInt1, paramInt2, paramInt3, paramInt4, false);
/*      */   }
/*      */   
/*      */   private boolean a(ahn paramahn, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
/*      */   {
/*  645 */     int i1 = paramBoolean ? 0 : paramInt4 & 0x3;
/*  646 */     boolean bool = false;
/*  647 */     float f1 = 0.0F;
/*  649 */     switch (i1)
/*      */     {
/*      */     case 2: 
/*  651 */       this.s = 1;
/*  652 */       this.t = 2;
/*  653 */       break;
/*      */     case 0: 
/*  655 */       this.s = 2;
/*  656 */       this.t = 1;
/*  657 */       this.u = 3;
/*  658 */       this.v = 3;
/*  659 */       break;
/*      */     case 1: 
/*  661 */       this.q = 1;
/*  662 */       this.r = 2;
/*  663 */       this.u = 2;
/*  664 */       this.v = 1;
/*  665 */       bool = true;
/*  666 */       break;
/*      */     case 3: 
/*  668 */       this.q = 2;
/*  669 */       this.r = 1;
/*  670 */       this.u = 1;
/*  671 */       this.v = 2;
/*  672 */       bool = true;
/*      */     }
/*  676 */     f1 = a(paramahn, paramInt1, paramInt2, paramInt3, 0, f1, 0.75F, 0.25F, 0.75F, bool, paramBoolean, paramInt4);
/*  677 */     f1 = a(paramahn, paramInt1, paramInt2, paramInt3, 1, f1, 0.5F, 0.0625F, 0.625F, bool, paramBoolean, paramInt4);
/*  678 */     f1 = a(paramahn, paramInt1, paramInt2, paramInt3, 2, f1, 0.25F, 0.3125F, 0.5F, bool, paramBoolean, paramInt4);
/*  679 */     f1 = a(paramahn, paramInt1, paramInt2, paramInt3, 3, f1, 0.625F, 0.375F, 1.0F, bool, paramBoolean, paramInt4);
/*      */     
/*  681 */     a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/*  682 */     this.q = 0;
/*  683 */     this.r = 0;
/*  684 */     this.s = 0;
/*  685 */     this.t = 0;
/*  686 */     this.u = 0;
/*  687 */     this.v = 0;
/*      */     
/*  689 */     return true;
/*      */   }
/*      */   
/*      */   private float a(ahn paramahn, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, boolean paramBoolean1, boolean paramBoolean2, int paramInt5)
/*      */   {
/*  693 */     if (paramBoolean1)
/*      */     {
/*  694 */       float f1 = paramFloat2;
/*  695 */       paramFloat2 = paramFloat4;
/*  696 */       paramFloat4 = f1;
/*      */     }
/*  699 */     paramFloat2 /= 2.0F;
/*  700 */     paramFloat4 /= 2.0F;
/*      */     
/*  702 */     paramahn.b = paramInt4;
/*  703 */     a(0.5F - paramFloat2, paramFloat1, 0.5F - paramFloat4, 0.5F + paramFloat2, paramFloat1 + paramFloat3, 0.5F + paramFloat4);
/*  705 */     if (paramBoolean2)
/*      */     {
/*  706 */       blz localblz = blz.a;
/*  707 */       localblz.b();
/*  708 */       localblz.c(0.0F, -1.0F, 0.0F);
/*  709 */       a(paramahn, 0.0D, 0.0D, 0.0D, a(paramahn, 0, paramInt5));
/*  710 */       localblz.a();
/*      */       
/*  712 */       localblz.b();
/*  713 */       localblz.c(0.0F, 1.0F, 0.0F);
/*  714 */       b(paramahn, 0.0D, 0.0D, 0.0D, a(paramahn, 1, paramInt5));
/*  715 */       localblz.a();
/*      */       
/*  717 */       localblz.b();
/*  718 */       localblz.c(0.0F, 0.0F, -1.0F);
/*  719 */       c(paramahn, 0.0D, 0.0D, 0.0D, a(paramahn, 2, paramInt5));
/*  720 */       localblz.a();
/*      */       
/*  722 */       localblz.b();
/*  723 */       localblz.c(0.0F, 0.0F, 1.0F);
/*  724 */       d(paramahn, 0.0D, 0.0D, 0.0D, a(paramahn, 3, paramInt5));
/*  725 */       localblz.a();
/*      */       
/*  727 */       localblz.b();
/*  728 */       localblz.c(-1.0F, 0.0F, 0.0F);
/*  729 */       e(paramahn, 0.0D, 0.0D, 0.0D, a(paramahn, 4, paramInt5));
/*  730 */       localblz.a();
/*      */       
/*  732 */       localblz.b();
/*  733 */       localblz.c(1.0F, 0.0F, 0.0F);
/*  734 */       f(paramahn, 0.0D, 0.0D, 0.0D, a(paramahn, 5, paramInt5));
/*  735 */       localblz.a();
/*      */     }
/*      */     else
/*      */     {
/*  737 */       q(paramahn, paramInt1, paramInt2, paramInt3);
/*      */     }
/*  740 */     return paramFloat1 + paramFloat3;
/*      */   }
/*      */   
/*      */   public boolean c(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  744 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/*      */     
/*  746 */     blz localblz = blz.a;
/*      */     
/*  748 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/*  749 */     localblz.b(1.0F, 1.0F, 1.0F);
/*      */     
/*  751 */     double d1 = 0.4000000059604645D;
/*  752 */     double d2 = 0.5D - d1;
/*  753 */     double d3 = 0.2000000029802322D;
/*  754 */     if (i1 == 1) {
/*  755 */       a(paramahu, paramInt1 - d2, paramInt2 + d3, paramInt3, -d1, 0.0D, 0);
/*  756 */     } else if (i1 == 2) {
/*  757 */       a(paramahu, paramInt1 + d2, paramInt2 + d3, paramInt3, d1, 0.0D, 0);
/*  758 */     } else if (i1 == 3) {
/*  759 */       a(paramahu, paramInt1, paramInt2 + d3, paramInt3 - d2, 0.0D, -d1, 0);
/*  760 */     } else if (i1 == 4) {
/*  761 */       a(paramahu, paramInt1, paramInt2 + d3, paramInt3 + d2, 0.0D, d1, 0);
/*      */     } else {
/*  763 */       a(paramahu, paramInt1, paramInt2, paramInt3, 0.0D, 0.0D, 0);
/*      */     }
/*  765 */     return true;
/*      */   }
/*      */   
/*      */   private boolean a(alr paramalr, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  769 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/*  770 */     int i2 = i1 & 0x3;
/*  771 */     int i3 = (i1 & 0xC) >> 2;
/*      */     
/*  773 */     blz localblz = blz.a;
/*      */     
/*  775 */     localblz.b(paramalr.c(this.a, paramInt1, paramInt2, paramInt3));
/*  776 */     localblz.b(1.0F, 1.0F, 1.0F);
/*      */     
/*  778 */     double d1 = -0.1875D;
/*  779 */     boolean bool = paramalr.g(this.a, paramInt1, paramInt2, paramInt3, i1);
/*  780 */     double d2 = 0.0D;
/*  781 */     double d3 = 0.0D;
/*  782 */     double d4 = 0.0D;
/*  783 */     double d5 = 0.0D;
/*  785 */     switch (i2)
/*      */     {
/*      */     case 0: 
/*  787 */       d5 = -0.3125D;
/*  788 */       d3 = alr.b[i3];
/*  789 */       break;
/*      */     case 2: 
/*  791 */       d5 = 0.3125D;
/*  792 */       d3 = -alr.b[i3];
/*  793 */       break;
/*      */     case 3: 
/*  795 */       d4 = -0.3125D;
/*  796 */       d2 = alr.b[i3];
/*  797 */       break;
/*      */     case 1: 
/*  799 */       d4 = 0.3125D;
/*  800 */       d2 = -alr.b[i3];
/*      */     }
/*  805 */     if (!bool)
/*      */     {
/*  806 */       a(paramalr, paramInt1 + d2, paramInt2 + d1, paramInt3 + d3, 0.0D, 0.0D, 0);
/*      */     }
/*      */     else
/*      */     {
/*  808 */       ps localps = b(ahz.h);
/*  809 */       a(localps);
/*      */       
/*  811 */       float f1 = 2.0F;
/*  812 */       float f2 = 14.0F;
/*  813 */       float f3 = 7.0F;
/*  814 */       float f4 = 9.0F;
/*  816 */       switch (i2)
/*      */       {
/*      */       case 0: 
/*      */       case 2: 
/*      */         break;
/*      */       case 1: 
/*      */       case 3: 
/*  822 */         f1 = 7.0F;
/*  823 */         f2 = 9.0F;
/*  824 */         f3 = 2.0F;
/*  825 */         f4 = 14.0F;
/*      */       }
/*  828 */       a(f1 / 16.0F + (float)d2, 0.125D, f3 / 16.0F + (float)d3, f2 / 16.0F + (float)d2, 0.25D, f4 / 16.0F + (float)d3);
/*  829 */       double d6 = localps.a(f1);
/*  830 */       double d7 = localps.b(f3);
/*  831 */       double d8 = localps.a(f2);
/*  832 */       double d9 = localps.b(f4);
/*  833 */       localblz.a(paramInt1 + f1 / 16.0F + d2, paramInt2 + 0.25F, paramInt3 + f3 / 16.0F + d3, d6, d7);
/*  834 */       localblz.a(paramInt1 + f1 / 16.0F + d2, paramInt2 + 0.25F, paramInt3 + f4 / 16.0F + d3, d6, d9);
/*  835 */       localblz.a(paramInt1 + f2 / 16.0F + d2, paramInt2 + 0.25F, paramInt3 + f4 / 16.0F + d3, d8, d9);
/*  836 */       localblz.a(paramInt1 + f2 / 16.0F + d2, paramInt2 + 0.25F, paramInt3 + f3 / 16.0F + d3, d8, d7);
/*  837 */       q(paramalr, paramInt1, paramInt2, paramInt3);
/*  838 */       a(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D);
/*  839 */       a();
/*      */     }
/*  842 */     localblz.b(paramalr.c(this.a, paramInt1, paramInt2, paramInt3));
/*  843 */     localblz.b(1.0F, 1.0F, 1.0F);
/*      */     
/*      */ 
/*  846 */     a(paramalr, paramInt1 + d4, paramInt2 + d1, paramInt3 + d5, 0.0D, 0.0D, 0);
/*      */     
/*      */ 
/*  849 */     a(paramalr, paramInt1, paramInt2, paramInt3);
/*      */     
/*  851 */     return true;
/*      */   }
/*      */   
/*      */   private boolean a(aio paramaio, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  855 */     blz localblz = blz.a;
/*      */     
/*  857 */     localblz.b(paramaio.c(this.a, paramInt1, paramInt2, paramInt3));
/*  858 */     localblz.b(1.0F, 1.0F, 1.0F);
/*      */     
/*  860 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/*  861 */     int i2 = i1 & 0x3;
/*  862 */     double d1 = 0.0D;
/*  863 */     double d2 = -0.1875D;
/*  864 */     double d3 = 0.0D;
/*  865 */     double d4 = 0.0D;
/*  866 */     double d5 = 0.0D;
/*      */     ps localps;
/*  869 */     if (paramaio.d(i1))
/*      */     {
/*  870 */       localps = ahz.aA.h(0);
/*      */     }
/*      */     else
/*      */     {
/*  872 */       d2 -= 0.1875D;
/*  873 */       localps = ahz.az.h(0);
/*      */     }
/*  876 */     switch (i2)
/*      */     {
/*      */     case 0: 
/*  878 */       d3 = -0.3125D;
/*  879 */       d5 = 1.0D;
/*  880 */       break;
/*      */     case 2: 
/*  882 */       d3 = 0.3125D;
/*  883 */       d5 = -1.0D;
/*  884 */       break;
/*      */     case 3: 
/*  886 */       d1 = -0.3125D;
/*  887 */       d4 = 1.0D;
/*  888 */       break;
/*      */     case 1: 
/*  890 */       d1 = 0.3125D;
/*  891 */       d4 = -1.0D;
/*      */     }
/*  896 */     a(paramaio, paramInt1 + 0.25D * d4 + 0.1875D * d5, paramInt2 - 0.1875F, paramInt3 + 0.25D * d5 + 0.1875D * d4, 0.0D, 0.0D, i1);
/*  897 */     a(paramaio, paramInt1 + 0.25D * d4 + -0.1875D * d5, paramInt2 - 0.1875F, paramInt3 + 0.25D * d5 + -0.1875D * d4, 0.0D, 0.0D, i1);
/*      */     
/*  899 */     a(localps);
/*  900 */     a(paramaio, paramInt1 + d1, paramInt2 + d2, paramInt3 + d3, 0.0D, 0.0D, i1);
/*  901 */     a();
/*      */     
/*  903 */     a(paramaio, paramInt1, paramInt2, paramInt3, i2);
/*      */     
/*  905 */     return true;
/*      */   }
/*      */   
/*      */   private boolean a(aiv paramaiv, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  909 */     blz localblz = blz.a;
/*      */     
/*  911 */     a(paramaiv, paramInt1, paramInt2, paramInt3, this.a.e(paramInt1, paramInt2, paramInt3) & 0x3);
/*      */     
/*  913 */     return true;
/*      */   }
/*      */   
/*      */   private void a(aiv paramaiv, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
/*      */   {
/*  918 */     q(paramaiv, paramInt1, paramInt2, paramInt3);
/*      */     
/*  920 */     blz localblz = blz.a;
/*      */     
/*  922 */     localblz.b(paramaiv.c(this.a, paramInt1, paramInt2, paramInt3));
/*  923 */     localblz.b(1.0F, 1.0F, 1.0F);
/*  924 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/*  925 */     ps localps = a(paramaiv, 1, i1);
/*  926 */     double d1 = localps.c();
/*  927 */     double d2 = localps.d();
/*  928 */     double d3 = localps.e();
/*  929 */     double d4 = localps.f();
/*      */     
/*  931 */     double d5 = 0.125D;
/*      */     
/*  933 */     double d6 = paramInt1 + 1;
/*  934 */     double d7 = paramInt1 + 1;
/*  935 */     double d8 = paramInt1 + 0;
/*  936 */     double d9 = paramInt1 + 0;
/*      */     
/*  938 */     double d10 = paramInt3 + 0;
/*  939 */     double d11 = paramInt3 + 1;
/*  940 */     double d12 = paramInt3 + 1;
/*  941 */     double d13 = paramInt3 + 0;
/*      */     
/*  943 */     double d14 = paramInt2 + d5;
/*  945 */     if (paramInt4 == 2)
/*      */     {
/*  947 */       d6 = d7 = paramInt1 + 0;
/*  948 */       d8 = d9 = paramInt1 + 1;
/*  949 */       d10 = d13 = paramInt3 + 1;
/*  950 */       d11 = d12 = paramInt3 + 0;
/*      */     }
/*  951 */     else if (paramInt4 == 3)
/*      */     {
/*  953 */       d6 = d9 = paramInt1 + 0;
/*  954 */       d7 = d8 = paramInt1 + 1;
/*  955 */       d10 = d11 = paramInt3 + 0;
/*  956 */       d12 = d13 = paramInt3 + 1;
/*      */     }
/*  957 */     else if (paramInt4 == 1)
/*      */     {
/*  959 */       d6 = d9 = paramInt1 + 1;
/*  960 */       d7 = d8 = paramInt1 + 0;
/*  961 */       d10 = d11 = paramInt3 + 1;
/*  962 */       d12 = d13 = paramInt3 + 0;
/*      */     }
/*  966 */     localblz.a(d9, d14, d13, d1, d3);
/*  967 */     localblz.a(d8, d14, d12, d1, d4);
/*  968 */     localblz.a(d7, d14, d11, d2, d4);
/*  969 */     localblz.a(d6, d14, d10, d2, d3);
/*      */   }
/*      */   
/*      */   public void d(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/*  982 */     this.f = true;
/*  983 */     b(paramahu, paramInt1, paramInt2, paramInt3, true);
/*  984 */     this.f = false;
/*      */   }
/*      */   
/*      */   private boolean b(ahu paramahu, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
/*      */   {
/*  989 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/*  990 */     int i2 = (paramBoolean) || ((i1 & 0x8) != 0) ? 1 : 0;
/*  991 */     int i3 = aob.b(i1);
/*      */     
/*  993 */     float f1 = 0.25F;
/*  995 */     if (i2 != 0)
/*      */     {
/*  996 */       switch (i3)
/*      */       {
/*      */       case 0: 
/*  998 */         this.q = 3;
/*  999 */         this.r = 3;
/* 1000 */         this.s = 3;
/* 1001 */         this.t = 3;
/* 1002 */         a(0.0D, 0.25D, 0.0D, 1.0D, 1.0D, 1.0D);
/* 1003 */         break;
/*      */       case 1: 
/* 1005 */         a(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D);
/* 1006 */         break;
/*      */       case 2: 
/* 1008 */         this.s = 1;
/* 1009 */         this.t = 2;
/* 1010 */         a(0.0D, 0.0D, 0.25D, 1.0D, 1.0D, 1.0D);
/* 1011 */         break;
/*      */       case 3: 
/* 1013 */         this.s = 2;
/* 1014 */         this.t = 1;
/* 1015 */         this.u = 3;
/* 1016 */         this.v = 3;
/* 1017 */         a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.75D);
/* 1018 */         break;
/*      */       case 4: 
/* 1020 */         this.q = 1;
/* 1021 */         this.r = 2;
/* 1022 */         this.u = 2;
/* 1023 */         this.v = 1;
/* 1024 */         a(0.25D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/* 1025 */         break;
/*      */       case 5: 
/* 1027 */         this.q = 2;
/* 1028 */         this.r = 1;
/* 1029 */         this.u = 1;
/* 1030 */         this.v = 2;
/* 1031 */         a(0.0D, 0.0D, 0.0D, 0.75D, 1.0D, 1.0D);
/*      */       }
/* 1035 */       ((aob)paramahu).b((float)this.h, (float)this.j, (float)this.l, (float)this.i, (float)this.k, (float)this.m);
/* 1036 */       q(paramahu, paramInt1, paramInt2, paramInt3);
/* 1037 */       this.q = 0;
/* 1038 */       this.r = 0;
/* 1039 */       this.s = 0;
/* 1040 */       this.t = 0;
/* 1041 */       this.u = 0;
/* 1042 */       this.v = 0;
/* 1043 */       a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/* 1044 */       ((aob)paramahu).b((float)this.h, (float)this.j, (float)this.l, (float)this.i, (float)this.k, (float)this.m);
/*      */     }
/*      */     else
/*      */     {
/* 1046 */       switch (i3)
/*      */       {
/*      */       case 0: 
/* 1048 */         this.q = 3;
/* 1049 */         this.r = 3;
/* 1050 */         this.s = 3;
/* 1051 */         this.t = 3;
/* 1052 */         break;
/*      */       case 1: 
/*      */         break;
/*      */       case 2: 
/* 1056 */         this.s = 1;
/* 1057 */         this.t = 2;
/* 1058 */         break;
/*      */       case 3: 
/* 1060 */         this.s = 2;
/* 1061 */         this.t = 1;
/* 1062 */         this.u = 3;
/* 1063 */         this.v = 3;
/* 1064 */         break;
/*      */       case 4: 
/* 1066 */         this.q = 1;
/* 1067 */         this.r = 2;
/* 1068 */         this.u = 2;
/* 1069 */         this.v = 1;
/* 1070 */         break;
/*      */       case 5: 
/* 1072 */         this.q = 2;
/* 1073 */         this.r = 1;
/* 1074 */         this.u = 1;
/* 1075 */         this.v = 2;
/*      */       }
/* 1078 */       q(paramahu, paramInt1, paramInt2, paramInt3);
/* 1079 */       this.q = 0;
/* 1080 */       this.r = 0;
/* 1081 */       this.s = 0;
/* 1082 */       this.t = 0;
/* 1083 */       this.u = 0;
/* 1084 */       this.v = 0;
/*      */     }
/* 1087 */     return true;
/*      */   }
/*      */   
/*      */   private void a(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, float paramFloat, double paramDouble7)
/*      */   {
/* 1091 */     ps localps = aob.e("piston_side");
/* 1092 */     if (b()) {
/* 1092 */       localps = this.d;
/*      */     }
/* 1094 */     blz localblz = blz.a;
/*      */     
/*      */ 
/* 1097 */     double d1 = localps.c();
/* 1098 */     double d2 = localps.e();
/* 1099 */     double d3 = localps.a(paramDouble7);
/* 1100 */     double d4 = localps.b(4.0D);
/*      */     
/* 1102 */     localblz.b(paramFloat, paramFloat, paramFloat);
/*      */     
/* 1104 */     localblz.a(paramDouble1, paramDouble4, paramDouble5, d3, d2);
/* 1105 */     localblz.a(paramDouble1, paramDouble3, paramDouble5, d1, d2);
/* 1106 */     localblz.a(paramDouble2, paramDouble3, paramDouble6, d1, d4);
/* 1107 */     localblz.a(paramDouble2, paramDouble4, paramDouble6, d3, d4);
/*      */   }
/*      */   
/*      */   private void b(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, float paramFloat, double paramDouble7)
/*      */   {
/* 1111 */     ps localps = aob.e("piston_side");
/* 1112 */     if (b()) {
/* 1112 */       localps = this.d;
/*      */     }
/* 1114 */     blz localblz = blz.a;
/*      */     
/*      */ 
/* 1117 */     double d1 = localps.c();
/* 1118 */     double d2 = localps.e();
/* 1119 */     double d3 = localps.a(paramDouble7);
/* 1120 */     double d4 = localps.b(4.0D);
/*      */     
/* 1122 */     localblz.b(paramFloat, paramFloat, paramFloat);
/*      */     
/* 1124 */     localblz.a(paramDouble1, paramDouble3, paramDouble6, d3, d2);
/* 1125 */     localblz.a(paramDouble1, paramDouble3, paramDouble5, d1, d2);
/* 1126 */     localblz.a(paramDouble2, paramDouble4, paramDouble5, d1, d4);
/* 1127 */     localblz.a(paramDouble2, paramDouble4, paramDouble6, d3, d4);
/*      */   }
/*      */   
/*      */   private void c(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, float paramFloat, double paramDouble7)
/*      */   {
/* 1131 */     ps localps = aob.e("piston_side");
/* 1132 */     if (b()) {
/* 1132 */       localps = this.d;
/*      */     }
/* 1134 */     blz localblz = blz.a;
/*      */     
/*      */ 
/* 1137 */     double d1 = localps.c();
/* 1138 */     double d2 = localps.e();
/* 1139 */     double d3 = localps.a(paramDouble7);
/* 1140 */     double d4 = localps.b(4.0D);
/*      */     
/* 1142 */     localblz.b(paramFloat, paramFloat, paramFloat);
/*      */     
/* 1144 */     localblz.a(paramDouble2, paramDouble3, paramDouble5, d3, d2);
/* 1145 */     localblz.a(paramDouble1, paramDouble3, paramDouble5, d1, d2);
/* 1146 */     localblz.a(paramDouble1, paramDouble4, paramDouble6, d1, d4);
/* 1147 */     localblz.a(paramDouble2, paramDouble4, paramDouble6, d3, d4);
/*      */   }
/*      */   
/*      */   public void a(ahu paramahu, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
/*      */   {
/* 1151 */     this.f = true;
/* 1152 */     c(paramahu, paramInt1, paramInt2, paramInt3, paramBoolean);
/* 1153 */     this.f = false;
/*      */   }
/*      */   
/*      */   private boolean c(ahu paramahu, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
/*      */   {
/* 1158 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 1159 */     int i2 = aoc.b(i1);
/*      */     
/* 1161 */     float f1 = 0.25F;
/* 1162 */     float f2 = 0.375F;
/* 1163 */     float f3 = 0.625F;
/* 1164 */     float f4 = paramBoolean ? 1.0F : 0.5F;
/* 1165 */     double d1 = paramBoolean ? 16.0D : 8.0D;
/* 1167 */     switch (i2)
/*      */     {
/*      */     case 0: 
/* 1169 */       this.q = 3;
/* 1170 */       this.r = 3;
/* 1171 */       this.s = 3;
/* 1172 */       this.t = 3;
/* 1173 */       a(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 1.0D);
/* 1174 */       q(paramahu, paramInt1, paramInt2, paramInt3);
/*      */       
/* 1176 */       a(paramInt1 + 0.375F, paramInt1 + 0.625F, paramInt2 + 0.25F, paramInt2 + 0.25F + f4, paramInt3 + 0.625F, paramInt3 + 0.625F, 0.8F, d1);
/* 1177 */       a(paramInt1 + 0.625F, paramInt1 + 0.375F, paramInt2 + 0.25F, paramInt2 + 0.25F + f4, paramInt3 + 0.375F, paramInt3 + 0.375F, 0.8F, d1);
/* 1178 */       a(paramInt1 + 0.375F, paramInt1 + 0.375F, paramInt2 + 0.25F, paramInt2 + 0.25F + f4, paramInt3 + 0.375F, paramInt3 + 0.625F, 0.6F, d1);
/* 1179 */       a(paramInt1 + 0.625F, paramInt1 + 0.625F, paramInt2 + 0.25F, paramInt2 + 0.25F + f4, paramInt3 + 0.625F, paramInt3 + 0.375F, 0.6F, d1);
/*      */       
/* 1181 */       break;
/*      */     case 1: 
/* 1183 */       a(0.0D, 0.75D, 0.0D, 1.0D, 1.0D, 1.0D);
/* 1184 */       q(paramahu, paramInt1, paramInt2, paramInt3);
/*      */       
/* 1186 */       a(paramInt1 + 0.375F, paramInt1 + 0.625F, paramInt2 - 0.25F + 1.0F - f4, paramInt2 - 0.25F + 1.0F, paramInt3 + 0.625F, paramInt3 + 0.625F, 0.8F, d1);
/* 1187 */       a(paramInt1 + 0.625F, paramInt1 + 0.375F, paramInt2 - 0.25F + 1.0F - f4, paramInt2 - 0.25F + 1.0F, paramInt3 + 0.375F, paramInt3 + 0.375F, 0.8F, d1);
/* 1188 */       a(paramInt1 + 0.375F, paramInt1 + 0.375F, paramInt2 - 0.25F + 1.0F - f4, paramInt2 - 0.25F + 1.0F, paramInt3 + 0.375F, paramInt3 + 0.625F, 0.6F, d1);
/* 1189 */       a(paramInt1 + 0.625F, paramInt1 + 0.625F, paramInt2 - 0.25F + 1.0F - f4, paramInt2 - 0.25F + 1.0F, paramInt3 + 0.625F, paramInt3 + 0.375F, 0.6F, d1);
/* 1190 */       break;
/*      */     case 2: 
/* 1192 */       this.s = 1;
/* 1193 */       this.t = 2;
/* 1194 */       a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.25D);
/* 1195 */       q(paramahu, paramInt1, paramInt2, paramInt3);
/*      */       
/* 1197 */       b(paramInt1 + 0.375F, paramInt1 + 0.375F, paramInt2 + 0.625F, paramInt2 + 0.375F, paramInt3 + 0.25F, paramInt3 + 0.25F + f4, 0.6F, d1);
/* 1198 */       b(paramInt1 + 0.625F, paramInt1 + 0.625F, paramInt2 + 0.375F, paramInt2 + 0.625F, paramInt3 + 0.25F, paramInt3 + 0.25F + f4, 0.6F, d1);
/* 1199 */       b(paramInt1 + 0.375F, paramInt1 + 0.625F, paramInt2 + 0.375F, paramInt2 + 0.375F, paramInt3 + 0.25F, paramInt3 + 0.25F + f4, 0.5F, d1);
/* 1200 */       b(paramInt1 + 0.625F, paramInt1 + 0.375F, paramInt2 + 0.625F, paramInt2 + 0.625F, paramInt3 + 0.25F, paramInt3 + 0.25F + f4, 1.0F, d1);
/* 1201 */       break;
/*      */     case 3: 
/* 1203 */       this.s = 2;
/* 1204 */       this.t = 1;
/* 1205 */       this.u = 3;
/* 1206 */       this.v = 3;
/* 1207 */       a(0.0D, 0.0D, 0.75D, 1.0D, 1.0D, 1.0D);
/* 1208 */       q(paramahu, paramInt1, paramInt2, paramInt3);
/*      */       
/* 1210 */       b(paramInt1 + 0.375F, paramInt1 + 0.375F, paramInt2 + 0.625F, paramInt2 + 0.375F, paramInt3 - 0.25F + 1.0F - f4, paramInt3 - 0.25F + 1.0F, 0.6F, d1);
/* 1211 */       b(paramInt1 + 0.625F, paramInt1 + 0.625F, paramInt2 + 0.375F, paramInt2 + 0.625F, paramInt3 - 0.25F + 1.0F - f4, paramInt3 - 0.25F + 1.0F, 0.6F, d1);
/* 1212 */       b(paramInt1 + 0.375F, paramInt1 + 0.625F, paramInt2 + 0.375F, paramInt2 + 0.375F, paramInt3 - 0.25F + 1.0F - f4, paramInt3 - 0.25F + 1.0F, 0.5F, d1);
/* 1213 */       b(paramInt1 + 0.625F, paramInt1 + 0.375F, paramInt2 + 0.625F, paramInt2 + 0.625F, paramInt3 - 0.25F + 1.0F - f4, paramInt3 - 0.25F + 1.0F, 1.0F, d1);
/* 1214 */       break;
/*      */     case 4: 
/* 1216 */       this.q = 1;
/* 1217 */       this.r = 2;
/* 1218 */       this.u = 2;
/* 1219 */       this.v = 1;
/* 1220 */       a(0.0D, 0.0D, 0.0D, 0.25D, 1.0D, 1.0D);
/* 1221 */       q(paramahu, paramInt1, paramInt2, paramInt3);
/*      */       
/* 1223 */       c(paramInt1 + 0.25F, paramInt1 + 0.25F + f4, paramInt2 + 0.375F, paramInt2 + 0.375F, paramInt3 + 0.625F, paramInt3 + 0.375F, 0.5F, d1);
/* 1224 */       c(paramInt1 + 0.25F, paramInt1 + 0.25F + f4, paramInt2 + 0.625F, paramInt2 + 0.625F, paramInt3 + 0.375F, paramInt3 + 0.625F, 1.0F, d1);
/* 1225 */       c(paramInt1 + 0.25F, paramInt1 + 0.25F + f4, paramInt2 + 0.375F, paramInt2 + 0.625F, paramInt3 + 0.375F, paramInt3 + 0.375F, 0.6F, d1);
/* 1226 */       c(paramInt1 + 0.25F, paramInt1 + 0.25F + f4, paramInt2 + 0.625F, paramInt2 + 0.375F, paramInt3 + 0.625F, paramInt3 + 0.625F, 0.6F, d1);
/* 1227 */       break;
/*      */     case 5: 
/* 1229 */       this.q = 2;
/* 1230 */       this.r = 1;
/* 1231 */       this.u = 1;
/* 1232 */       this.v = 2;
/* 1233 */       a(0.75D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/* 1234 */       q(paramahu, paramInt1, paramInt2, paramInt3);
/*      */       
/* 1236 */       c(paramInt1 - 0.25F + 1.0F - f4, paramInt1 - 0.25F + 1.0F, paramInt2 + 0.375F, paramInt2 + 0.375F, paramInt3 + 0.625F, paramInt3 + 0.375F, 0.5F, d1);
/* 1237 */       c(paramInt1 - 0.25F + 1.0F - f4, paramInt1 - 0.25F + 1.0F, paramInt2 + 0.625F, paramInt2 + 0.625F, paramInt3 + 0.375F, paramInt3 + 0.625F, 1.0F, d1);
/* 1238 */       c(paramInt1 - 0.25F + 1.0F - f4, paramInt1 - 0.25F + 1.0F, paramInt2 + 0.375F, paramInt2 + 0.625F, paramInt3 + 0.375F, paramInt3 + 0.375F, 0.6F, d1);
/* 1239 */       c(paramInt1 - 0.25F + 1.0F - f4, paramInt1 - 0.25F + 1.0F, paramInt2 + 0.625F, paramInt2 + 0.375F, paramInt3 + 0.625F, paramInt3 + 0.625F, 0.6F, d1);
/*      */     }
/* 1242 */     this.q = 0;
/* 1243 */     this.r = 0;
/* 1244 */     this.s = 0;
/* 1245 */     this.t = 0;
/* 1246 */     this.u = 0;
/* 1247 */     this.v = 0;
/* 1248 */     a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/*      */     
/* 1250 */     return true;
/*      */   }
/*      */   
/*      */   public boolean e(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 1254 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/*      */     
/* 1256 */     int i2 = i1 & 0x7;
/* 1257 */     int i3 = (i1 & 0x8) > 0 ? 1 : 0;
/*      */     
/* 1259 */     blz localblz = blz.a;
/*      */     
/* 1261 */     boolean bool = b();
/* 1262 */     if (!bool) {
/* 1262 */       a(b(ahz.e));
/*      */     }
/* 1263 */     float f1 = 0.25F;
/* 1264 */     float f2 = 0.1875F;
/* 1265 */     float f3 = 0.1875F;
/* 1266 */     if (i2 == 5) {
/* 1267 */       a(0.5F - f2, 0.0D, 0.5F - f1, 0.5F + f2, f3, 0.5F + f1);
/* 1268 */     } else if (i2 == 6) {
/* 1269 */       a(0.5F - f1, 0.0D, 0.5F - f2, 0.5F + f1, f3, 0.5F + f2);
/* 1270 */     } else if (i2 == 4) {
/* 1271 */       a(0.5F - f2, 0.5F - f1, 1.0F - f3, 0.5F + f2, 0.5F + f1, 1.0D);
/* 1272 */     } else if (i2 == 3) {
/* 1273 */       a(0.5F - f2, 0.5F - f1, 0.0D, 0.5F + f2, 0.5F + f1, f3);
/* 1274 */     } else if (i2 == 2) {
/* 1275 */       a(1.0F - f3, 0.5F - f1, 0.5F - f2, 1.0D, 0.5F + f1, 0.5F + f2);
/* 1276 */     } else if (i2 == 1) {
/* 1277 */       a(0.0D, 0.5F - f1, 0.5F - f2, f3, 0.5F + f1, 0.5F + f2);
/* 1278 */     } else if (i2 == 0) {
/* 1279 */       a(0.5F - f1, 1.0F - f3, 0.5F - f2, 0.5F + f1, 1.0D, 0.5F + f2);
/* 1280 */     } else if (i2 == 7) {
/* 1281 */       a(0.5F - f2, 1.0F - f3, 0.5F - f1, 0.5F + f2, 1.0D, 0.5F + f1);
/*      */     }
/* 1283 */     q(paramahu, paramInt1, paramInt2, paramInt3);
/* 1284 */     if (!bool) {
/* 1284 */       a();
/*      */     }
/* 1286 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/* 1287 */     localblz.b(1.0F, 1.0F, 1.0F);
/* 1288 */     ps localps = a(paramahu, 0);
/* 1290 */     if (b()) {
/* 1290 */       localps = this.d;
/*      */     }
/* 1291 */     double d1 = localps.c();
/* 1292 */     double d2 = localps.e();
/* 1293 */     double d3 = localps.d();
/* 1294 */     double d4 = localps.f();
/*      */     
/* 1296 */     ayk[] arrayOfayk = new ayk[8];
/* 1297 */     float f4 = 0.0625F;
/* 1298 */     float f5 = 0.0625F;
/* 1299 */     float f6 = 0.625F;
/* 1300 */     arrayOfayk[0] = this.a.U().a(-f4, 0.0D, -f5);
/* 1301 */     arrayOfayk[1] = this.a.U().a(f4, 0.0D, -f5);
/* 1302 */     arrayOfayk[2] = this.a.U().a(f4, 0.0D, f5);
/* 1303 */     arrayOfayk[3] = this.a.U().a(-f4, 0.0D, f5);
/* 1304 */     arrayOfayk[4] = this.a.U().a(-f4, f6, -f5);
/* 1305 */     arrayOfayk[5] = this.a.U().a(f4, f6, -f5);
/* 1306 */     arrayOfayk[6] = this.a.U().a(f4, f6, f5);
/* 1307 */     arrayOfayk[7] = this.a.U().a(-f4, f6, f5);
/* 1309 */     for (int i4 = 0; i4 < 8; i4++)
/*      */     {
/* 1310 */       if (i3 != 0)
/*      */       {
/* 1311 */         arrayOfayk[i4].e -= 0.0625D;
/* 1312 */         arrayOfayk[i4].a(0.6981317F);
/*      */       }
/*      */       else
/*      */       {
/* 1314 */         arrayOfayk[i4].e += 0.0625D;
/* 1315 */         arrayOfayk[i4].a(-0.6981317F);
/*      */       }
/* 1317 */       if ((i2 == 0) || (i2 == 7)) {
/* 1318 */         arrayOfayk[i4].c(3.141593F);
/*      */       }
/* 1320 */       if ((i2 == 6) || (i2 == 0)) {
/* 1321 */         arrayOfayk[i4].b(1.570796F);
/*      */       }
/* 1324 */       if ((i2 > 0) && (i2 < 5))
/*      */       {
/* 1325 */         arrayOfayk[i4].d -= 0.375D;
/* 1326 */         arrayOfayk[i4].a(1.570796F);
/* 1328 */         if (i2 == 4) {
/* 1328 */           arrayOfayk[i4].b(0.0F);
/*      */         }
/* 1329 */         if (i2 == 3) {
/* 1329 */           arrayOfayk[i4].b(3.141593F);
/*      */         }
/* 1330 */         if (i2 == 2) {
/* 1330 */           arrayOfayk[i4].b(1.570796F);
/*      */         }
/* 1331 */         if (i2 == 1) {
/* 1331 */           arrayOfayk[i4].b(-1.570796F);
/*      */         }
/* 1333 */         arrayOfayk[i4].c += paramInt1 + 0.5D;
/* 1334 */         arrayOfayk[i4].d += paramInt2 + 0.5F;
/* 1335 */         arrayOfayk[i4].e += paramInt3 + 0.5D;
/*      */       }
/* 1336 */       else if ((i2 == 0) || (i2 == 7))
/*      */       {
/* 1337 */         arrayOfayk[i4].c += paramInt1 + 0.5D;
/* 1338 */         arrayOfayk[i4].d += paramInt2 + 0.875F;
/* 1339 */         arrayOfayk[i4].e += paramInt3 + 0.5D;
/*      */       }
/*      */       else
/*      */       {
/* 1341 */         arrayOfayk[i4].c += paramInt1 + 0.5D;
/* 1342 */         arrayOfayk[i4].d += paramInt2 + 0.125F;
/* 1343 */         arrayOfayk[i4].e += paramInt3 + 0.5D;
/*      */       }
/*      */     }
/* 1347 */     ayk localayk1 = null;ayk localayk2 = null;ayk localayk3 = null;ayk localayk4 = null;
/* 1348 */     for (int i5 = 0; i5 < 6; i5++)
/*      */     {
/* 1349 */       if (i5 == 0)
/*      */       {
/* 1350 */         d1 = localps.a(7.0D);
/* 1351 */         d2 = localps.b(6.0D);
/* 1352 */         d3 = localps.a(9.0D);
/* 1353 */         d4 = localps.b(8.0D);
/*      */       }
/* 1354 */       else if (i5 == 2)
/*      */       {
/* 1355 */         d1 = localps.a(7.0D);
/* 1356 */         d2 = localps.b(6.0D);
/* 1357 */         d3 = localps.a(9.0D);
/* 1358 */         d4 = localps.f();
/*      */       }
/* 1360 */       if (i5 == 0)
/*      */       {
/* 1361 */         localayk1 = arrayOfayk[0];
/* 1362 */         localayk2 = arrayOfayk[1];
/* 1363 */         localayk3 = arrayOfayk[2];
/* 1364 */         localayk4 = arrayOfayk[3];
/*      */       }
/* 1365 */       else if (i5 == 1)
/*      */       {
/* 1366 */         localayk1 = arrayOfayk[7];
/* 1367 */         localayk2 = arrayOfayk[6];
/* 1368 */         localayk3 = arrayOfayk[5];
/* 1369 */         localayk4 = arrayOfayk[4];
/*      */       }
/* 1370 */       else if (i5 == 2)
/*      */       {
/* 1371 */         localayk1 = arrayOfayk[1];
/* 1372 */         localayk2 = arrayOfayk[0];
/* 1373 */         localayk3 = arrayOfayk[4];
/* 1374 */         localayk4 = arrayOfayk[5];
/*      */       }
/* 1375 */       else if (i5 == 3)
/*      */       {
/* 1376 */         localayk1 = arrayOfayk[2];
/* 1377 */         localayk2 = arrayOfayk[1];
/* 1378 */         localayk3 = arrayOfayk[5];
/* 1379 */         localayk4 = arrayOfayk[6];
/*      */       }
/* 1380 */       else if (i5 == 4)
/*      */       {
/* 1381 */         localayk1 = arrayOfayk[3];
/* 1382 */         localayk2 = arrayOfayk[2];
/* 1383 */         localayk3 = arrayOfayk[6];
/* 1384 */         localayk4 = arrayOfayk[7];
/*      */       }
/* 1385 */       else if (i5 == 5)
/*      */       {
/* 1386 */         localayk1 = arrayOfayk[0];
/* 1387 */         localayk2 = arrayOfayk[3];
/* 1388 */         localayk3 = arrayOfayk[7];
/* 1389 */         localayk4 = arrayOfayk[4];
/*      */       }
/* 1391 */       localblz.a(localayk1.c, localayk1.d, localayk1.e, d1, d4);
/* 1392 */       localblz.a(localayk2.c, localayk2.d, localayk2.e, d3, d4);
/* 1393 */       localblz.a(localayk3.c, localayk3.d, localayk3.e, d3, d2);
/* 1394 */       localblz.a(localayk4.c, localayk4.d, localayk4.e, d1, d2);
/*      */     }
/* 1396 */     return true;
/*      */   }
/*      */   
/*      */   public boolean f(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 1400 */     blz localblz = blz.a;
/* 1401 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 1402 */     int i2 = i1 & 0x3;
/* 1403 */     int i3 = (i1 & 0x4) == 4 ? 1 : 0;
/* 1404 */     int i4 = (i1 & 0x8) == 8 ? 1 : 0;
/* 1405 */     int i5 = !afn.a(this.a, paramInt1, paramInt2 - 1, paramInt3) ? 1 : 0;
/*      */     
/* 1407 */     boolean bool = b();
/* 1408 */     if (!bool) {
/* 1408 */       a(b(ahz.f));
/*      */     }
/* 1410 */     float f1 = 0.25F;
/* 1411 */     float f2 = 0.125F;
/* 1412 */     float f3 = 0.125F;
/*      */     
/* 1414 */     float f4 = 0.3F - f1;
/* 1415 */     float f5 = 0.3F + f1;
/* 1416 */     if (i2 == 2) {
/* 1417 */       a(0.5F - f2, f4, 1.0F - f3, 0.5F + f2, f5, 1.0D);
/* 1418 */     } else if (i2 == 0) {
/* 1419 */       a(0.5F - f2, f4, 0.0D, 0.5F + f2, f5, f3);
/* 1420 */     } else if (i2 == 1) {
/* 1421 */       a(1.0F - f3, f4, 0.5F - f2, 1.0D, f5, 0.5F + f2);
/* 1422 */     } else if (i2 == 3) {
/* 1423 */       a(0.0D, f4, 0.5F - f2, f3, f5, 0.5F + f2);
/*      */     }
/* 1426 */     q(paramahu, paramInt1, paramInt2, paramInt3);
/* 1427 */     if (!bool) {
/* 1427 */       a();
/*      */     }
/* 1429 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/* 1430 */     localblz.b(1.0F, 1.0F, 1.0F);
/* 1431 */     ps localps = a(paramahu, 0);
/* 1433 */     if (b()) {
/* 1433 */       localps = this.d;
/*      */     }
/* 1434 */     double d1 = localps.c();
/* 1435 */     double d2 = localps.e();
/* 1436 */     double d3 = localps.d();
/* 1437 */     double d4 = localps.f();
/*      */     
/* 1439 */     ayk[] arrayOfayk = new ayk[8];
/* 1440 */     float f6 = 0.046875F;
/* 1441 */     float f7 = 0.046875F;
/* 1442 */     float f8 = 0.3125F;
/* 1443 */     arrayOfayk[0] = this.a.U().a(-f6, 0.0D, -f7);
/* 1444 */     arrayOfayk[1] = this.a.U().a(f6, 0.0D, -f7);
/* 1445 */     arrayOfayk[2] = this.a.U().a(f6, 0.0D, f7);
/* 1446 */     arrayOfayk[3] = this.a.U().a(-f6, 0.0D, f7);
/* 1447 */     arrayOfayk[4] = this.a.U().a(-f6, f8, -f7);
/* 1448 */     arrayOfayk[5] = this.a.U().a(f6, f8, -f7);
/* 1449 */     arrayOfayk[6] = this.a.U().a(f6, f8, f7);
/* 1450 */     arrayOfayk[7] = this.a.U().a(-f6, f8, f7);
/* 1452 */     for (int i6 = 0; i6 < 8; i6++)
/*      */     {
/* 1453 */       arrayOfayk[i6].e += 0.0625D;
/* 1455 */       if (i4 != 0)
/*      */       {
/* 1456 */         arrayOfayk[i6].a(0.5235988F);
/* 1457 */         arrayOfayk[i6].d -= 0.4375D;
/*      */       }
/* 1458 */       else if (i3 != 0)
/*      */       {
/* 1459 */         arrayOfayk[i6].a(0.08726647F);
/* 1460 */         arrayOfayk[i6].d -= 0.4375D;
/*      */       }
/*      */       else
/*      */       {
/* 1462 */         arrayOfayk[i6].a(-0.6981317F);
/* 1463 */         arrayOfayk[i6].d -= 0.375D;
/*      */       }
/* 1466 */       arrayOfayk[i6].a(1.570796F);
/* 1468 */       if (i2 == 2) {
/* 1468 */         arrayOfayk[i6].b(0.0F);
/*      */       }
/* 1469 */       if (i2 == 0) {
/* 1469 */         arrayOfayk[i6].b(3.141593F);
/*      */       }
/* 1470 */       if (i2 == 1) {
/* 1470 */         arrayOfayk[i6].b(1.570796F);
/*      */       }
/* 1471 */       if (i2 == 3) {
/* 1471 */         arrayOfayk[i6].b(-1.570796F);
/*      */       }
/* 1473 */       arrayOfayk[i6].c += paramInt1 + 0.5D;
/* 1474 */       arrayOfayk[i6].d += paramInt2 + 0.3125F;
/* 1475 */       arrayOfayk[i6].e += paramInt3 + 0.5D;
/*      */     }
/* 1478 */     ayk localayk1 = null;ayk localayk2 = null;ayk localayk3 = null;ayk localayk4 = null;
/* 1479 */     int i7 = 7;
/* 1480 */     int i8 = 9;
/* 1481 */     int i9 = 9;
/* 1482 */     int i10 = 16;
/* 1484 */     for (int i11 = 0; i11 < 6; i11++)
/*      */     {
/* 1485 */       if (i11 == 0)
/*      */       {
/* 1486 */         localayk1 = arrayOfayk[0];
/* 1487 */         localayk2 = arrayOfayk[1];
/* 1488 */         localayk3 = arrayOfayk[2];
/* 1489 */         localayk4 = arrayOfayk[3];
/* 1490 */         d1 = localps.a(i7);
/* 1491 */         d2 = localps.b(i9);
/* 1492 */         d3 = localps.a(i8);
/* 1493 */         d4 = localps.b(i9 + 2);
/*      */       }
/* 1494 */       else if (i11 == 1)
/*      */       {
/* 1495 */         localayk1 = arrayOfayk[7];
/* 1496 */         localayk2 = arrayOfayk[6];
/* 1497 */         localayk3 = arrayOfayk[5];
/* 1498 */         localayk4 = arrayOfayk[4];
/*      */       }
/* 1499 */       else if (i11 == 2)
/*      */       {
/* 1500 */         localayk1 = arrayOfayk[1];
/* 1501 */         localayk2 = arrayOfayk[0];
/* 1502 */         localayk3 = arrayOfayk[4];
/* 1503 */         localayk4 = arrayOfayk[5];
/* 1504 */         d1 = localps.a(i7);
/* 1505 */         d2 = localps.b(i9);
/* 1506 */         d3 = localps.a(i8);
/* 1507 */         d4 = localps.b(i10);
/*      */       }
/* 1508 */       else if (i11 == 3)
/*      */       {
/* 1509 */         localayk1 = arrayOfayk[2];
/* 1510 */         localayk2 = arrayOfayk[1];
/* 1511 */         localayk3 = arrayOfayk[5];
/* 1512 */         localayk4 = arrayOfayk[6];
/*      */       }
/* 1513 */       else if (i11 == 4)
/*      */       {
/* 1514 */         localayk1 = arrayOfayk[3];
/* 1515 */         localayk2 = arrayOfayk[2];
/* 1516 */         localayk3 = arrayOfayk[6];
/* 1517 */         localayk4 = arrayOfayk[7];
/*      */       }
/* 1518 */       else if (i11 == 5)
/*      */       {
/* 1519 */         localayk1 = arrayOfayk[0];
/* 1520 */         localayk2 = arrayOfayk[3];
/* 1521 */         localayk3 = arrayOfayk[7];
/* 1522 */         localayk4 = arrayOfayk[4];
/*      */       }
/* 1524 */       localblz.a(localayk1.c, localayk1.d, localayk1.e, d1, d4);
/* 1525 */       localblz.a(localayk2.c, localayk2.d, localayk2.e, d3, d4);
/* 1526 */       localblz.a(localayk3.c, localayk3.d, localayk3.e, d3, d2);
/* 1527 */       localblz.a(localayk4.c, localayk4.d, localayk4.e, d1, d2);
/*      */     }
/* 1531 */     float f9 = 0.09375F;
/* 1532 */     float f10 = 0.09375F;
/* 1533 */     float f11 = 0.03125F;
/* 1534 */     arrayOfayk[0] = this.a.U().a(-f9, 0.0D, -f10);
/* 1535 */     arrayOfayk[1] = this.a.U().a(f9, 0.0D, -f10);
/* 1536 */     arrayOfayk[2] = this.a.U().a(f9, 0.0D, f10);
/* 1537 */     arrayOfayk[3] = this.a.U().a(-f9, 0.0D, f10);
/* 1538 */     arrayOfayk[4] = this.a.U().a(-f9, f11, -f10);
/* 1539 */     arrayOfayk[5] = this.a.U().a(f9, f11, -f10);
/* 1540 */     arrayOfayk[6] = this.a.U().a(f9, f11, f10);
/* 1541 */     arrayOfayk[7] = this.a.U().a(-f9, f11, f10);
/* 1543 */     for (int i12 = 0; i12 < 8; i12++)
/*      */     {
/* 1544 */       arrayOfayk[i12].e += 0.21875D;
/* 1546 */       if (i4 != 0)
/*      */       {
/* 1547 */         arrayOfayk[i12].d -= 0.09375D;
/* 1548 */         arrayOfayk[i12].e -= 0.1625D;
/* 1549 */         arrayOfayk[i12].a(0.0F);
/*      */       }
/* 1550 */       else if (i3 != 0)
/*      */       {
/* 1551 */         arrayOfayk[i12].d += 0.015625D;
/* 1552 */         arrayOfayk[i12].e -= 0.171875D;
/* 1553 */         arrayOfayk[i12].a(0.1745329F);
/*      */       }
/*      */       else
/*      */       {
/* 1555 */         arrayOfayk[i12].a(0.8726646F);
/*      */       }
/* 1558 */       if (i2 == 2) {
/* 1558 */         arrayOfayk[i12].b(0.0F);
/*      */       }
/* 1559 */       if (i2 == 0) {
/* 1559 */         arrayOfayk[i12].b(3.141593F);
/*      */       }
/* 1560 */       if (i2 == 1) {
/* 1560 */         arrayOfayk[i12].b(1.570796F);
/*      */       }
/* 1561 */       if (i2 == 3) {
/* 1561 */         arrayOfayk[i12].b(-1.570796F);
/*      */       }
/* 1563 */       arrayOfayk[i12].c += paramInt1 + 0.5D;
/* 1564 */       arrayOfayk[i12].d += paramInt2 + 0.3125F;
/* 1565 */       arrayOfayk[i12].e += paramInt3 + 0.5D;
/*      */     }
/* 1568 */     i12 = 5;
/* 1569 */     int i13 = 11;
/* 1570 */     int i14 = 3;
/* 1571 */     int i15 = 9;
/* 1573 */     for (int i16 = 0; i16 < 6; i16++)
/*      */     {
/* 1574 */       if (i16 == 0)
/*      */       {
/* 1575 */         localayk1 = arrayOfayk[0];
/* 1576 */         localayk2 = arrayOfayk[1];
/* 1577 */         localayk3 = arrayOfayk[2];
/* 1578 */         localayk4 = arrayOfayk[3];
/* 1579 */         d1 = localps.a(i12);
/* 1580 */         d2 = localps.b(i14);
/* 1581 */         d3 = localps.a(i13);
/* 1582 */         d4 = localps.b(i15);
/*      */       }
/* 1583 */       else if (i16 == 1)
/*      */       {
/* 1584 */         localayk1 = arrayOfayk[7];
/* 1585 */         localayk2 = arrayOfayk[6];
/* 1586 */         localayk3 = arrayOfayk[5];
/* 1587 */         localayk4 = arrayOfayk[4];
/*      */       }
/* 1588 */       else if (i16 == 2)
/*      */       {
/* 1589 */         localayk1 = arrayOfayk[1];
/* 1590 */         localayk2 = arrayOfayk[0];
/* 1591 */         localayk3 = arrayOfayk[4];
/* 1592 */         localayk4 = arrayOfayk[5];
/* 1593 */         d1 = localps.a(i12);
/* 1594 */         d2 = localps.b(i14);
/* 1595 */         d3 = localps.a(i13);
/* 1596 */         d4 = localps.b(i14 + 2);
/*      */       }
/* 1597 */       else if (i16 == 3)
/*      */       {
/* 1598 */         localayk1 = arrayOfayk[2];
/* 1599 */         localayk2 = arrayOfayk[1];
/* 1600 */         localayk3 = arrayOfayk[5];
/* 1601 */         localayk4 = arrayOfayk[6];
/*      */       }
/* 1602 */       else if (i16 == 4)
/*      */       {
/* 1603 */         localayk1 = arrayOfayk[3];
/* 1604 */         localayk2 = arrayOfayk[2];
/* 1605 */         localayk3 = arrayOfayk[6];
/* 1606 */         localayk4 = arrayOfayk[7];
/*      */       }
/* 1607 */       else if (i16 == 5)
/*      */       {
/* 1608 */         localayk1 = arrayOfayk[0];
/* 1609 */         localayk2 = arrayOfayk[3];
/* 1610 */         localayk3 = arrayOfayk[7];
/* 1611 */         localayk4 = arrayOfayk[4];
/*      */       }
/* 1613 */       localblz.a(localayk1.c, localayk1.d, localayk1.e, d1, d4);
/* 1614 */       localblz.a(localayk2.c, localayk2.d, localayk2.e, d3, d4);
/* 1615 */       localblz.a(localayk3.c, localayk3.d, localayk3.e, d3, d2);
/* 1616 */       localblz.a(localayk4.c, localayk4.d, localayk4.e, d1, d2);
/*      */     }
/* 1619 */     if (i3 != 0)
/*      */     {
/* 1620 */       double d5 = arrayOfayk[0].d;
/* 1621 */       float f12 = 0.03125F;
/* 1622 */       float f13 = 0.5F - f12 / 2.0F;
/* 1623 */       float f14 = f13 + f12;
/* 1624 */       double d6 = localps.c();
/* 1625 */       double d7 = localps.b(i3 != 0 ? 2.0D : 0.0D);
/* 1626 */       double d8 = localps.d();
/* 1627 */       double d9 = localps.b(i3 != 0 ? 4.0D : 2.0D);
/* 1628 */       double d10 = (i5 != 0 ? 3.5F : 1.5F) / 16.0D;
/*      */       
/* 1630 */       localblz.b(0.75F, 0.75F, 0.75F);
/* 1632 */       if (i2 == 2)
/*      */       {
/* 1633 */         localblz.a(paramInt1 + f13, paramInt2 + d10, paramInt3 + 0.25D, d6, d7);
/* 1634 */         localblz.a(paramInt1 + f14, paramInt2 + d10, paramInt3 + 0.25D, d6, d9);
/* 1635 */         localblz.a(paramInt1 + f14, paramInt2 + d10, paramInt3, d8, d9);
/* 1636 */         localblz.a(paramInt1 + f13, paramInt2 + d10, paramInt3, d8, d7);
/*      */         
/* 1638 */         localblz.a(paramInt1 + f13, d5, paramInt3 + 0.5D, d6, d7);
/* 1639 */         localblz.a(paramInt1 + f14, d5, paramInt3 + 0.5D, d6, d9);
/* 1640 */         localblz.a(paramInt1 + f14, paramInt2 + d10, paramInt3 + 0.25D, d8, d9);
/* 1641 */         localblz.a(paramInt1 + f13, paramInt2 + d10, paramInt3 + 0.25D, d8, d7);
/*      */       }
/* 1642 */       else if (i2 == 0)
/*      */       {
/* 1643 */         localblz.a(paramInt1 + f13, paramInt2 + d10, paramInt3 + 0.75D, d6, d7);
/* 1644 */         localblz.a(paramInt1 + f14, paramInt2 + d10, paramInt3 + 0.75D, d6, d9);
/* 1645 */         localblz.a(paramInt1 + f14, d5, paramInt3 + 0.5D, d8, d9);
/* 1646 */         localblz.a(paramInt1 + f13, d5, paramInt3 + 0.5D, d8, d7);
/*      */         
/* 1648 */         localblz.a(paramInt1 + f13, paramInt2 + d10, paramInt3 + 1, d6, d7);
/* 1649 */         localblz.a(paramInt1 + f14, paramInt2 + d10, paramInt3 + 1, d6, d9);
/* 1650 */         localblz.a(paramInt1 + f14, paramInt2 + d10, paramInt3 + 0.75D, d8, d9);
/* 1651 */         localblz.a(paramInt1 + f13, paramInt2 + d10, paramInt3 + 0.75D, d8, d7);
/*      */       }
/* 1652 */       else if (i2 == 1)
/*      */       {
/* 1653 */         localblz.a(paramInt1, paramInt2 + d10, paramInt3 + f14, d6, d9);
/* 1654 */         localblz.a(paramInt1 + 0.25D, paramInt2 + d10, paramInt3 + f14, d8, d9);
/* 1655 */         localblz.a(paramInt1 + 0.25D, paramInt2 + d10, paramInt3 + f13, d8, d7);
/* 1656 */         localblz.a(paramInt1, paramInt2 + d10, paramInt3 + f13, d6, d7);
/*      */         
/* 1658 */         localblz.a(paramInt1 + 0.25D, paramInt2 + d10, paramInt3 + f14, d6, d9);
/* 1659 */         localblz.a(paramInt1 + 0.5D, d5, paramInt3 + f14, d8, d9);
/* 1660 */         localblz.a(paramInt1 + 0.5D, d5, paramInt3 + f13, d8, d7);
/* 1661 */         localblz.a(paramInt1 + 0.25D, paramInt2 + d10, paramInt3 + f13, d6, d7);
/*      */       }
/*      */       else
/*      */       {
/* 1663 */         localblz.a(paramInt1 + 0.5D, d5, paramInt3 + f14, d6, d9);
/* 1664 */         localblz.a(paramInt1 + 0.75D, paramInt2 + d10, paramInt3 + f14, d8, d9);
/* 1665 */         localblz.a(paramInt1 + 0.75D, paramInt2 + d10, paramInt3 + f13, d8, d7);
/* 1666 */         localblz.a(paramInt1 + 0.5D, d5, paramInt3 + f13, d6, d7);
/*      */         
/* 1668 */         localblz.a(paramInt1 + 0.75D, paramInt2 + d10, paramInt3 + f14, d6, d9);
/* 1669 */         localblz.a(paramInt1 + 1, paramInt2 + d10, paramInt3 + f14, d8, d9);
/* 1670 */         localblz.a(paramInt1 + 1, paramInt2 + d10, paramInt3 + f13, d8, d7);
/* 1671 */         localblz.a(paramInt1 + 0.75D, paramInt2 + d10, paramInt3 + f13, d6, d7);
/*      */       }
/*      */     }
/* 1675 */     return true;
/*      */   }
/*      */   
/*      */   public boolean g(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 1679 */     blz localblz = blz.a;
/* 1680 */     ps localps = a(paramahu, 0);
/* 1681 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 1682 */     int i2 = (i1 & 0x4) == 4 ? 1 : 0;
/* 1683 */     int i3 = (i1 & 0x2) == 2 ? 1 : 0;
/* 1685 */     if (b()) {
/* 1685 */       localps = this.d;
/*      */     }
/* 1687 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/* 1688 */     localblz.b(1.0F, 1.0F, 1.0F);
/*      */     
/* 1690 */     double d1 = localps.c();
/* 1691 */     double d2 = localps.b(i2 != 0 ? 2.0D : 0.0D);
/* 1692 */     double d3 = localps.d();
/* 1693 */     double d4 = localps.b(i2 != 0 ? 4.0D : 2.0D);
/* 1694 */     double d5 = (i3 != 0 ? 3.5F : 1.5F) / 16.0D;
/*      */     
/* 1696 */     boolean bool1 = amr.a(this.a, paramInt1, paramInt2, paramInt3, i1, 1);
/* 1697 */     boolean bool2 = amr.a(this.a, paramInt1, paramInt2, paramInt3, i1, 3);
/* 1698 */     boolean bool3 = amr.a(this.a, paramInt1, paramInt2, paramInt3, i1, 2);
/* 1699 */     boolean bool4 = amr.a(this.a, paramInt1, paramInt2, paramInt3, i1, 0);
/*      */     
/* 1701 */     float f1 = 0.03125F;
/* 1702 */     float f2 = 0.5F - f1 / 2.0F;
/* 1703 */     float f3 = f2 + f1;
/* 1705 */     if ((!bool3) && (!bool2) && (!bool4) && (!bool1))
/*      */     {
/* 1706 */       bool3 = true;
/* 1707 */       bool4 = true;
/*      */     }
/* 1710 */     if (bool3)
/*      */     {
/* 1711 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.25D, d1, d2);
/* 1712 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.25D, d1, d4);
/* 1713 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3, d3, d4);
/* 1714 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3, d3, d2);
/*      */       
/* 1716 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3, d3, d2);
/* 1717 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3, d3, d4);
/* 1718 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.25D, d1, d4);
/* 1719 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.25D, d1, d2);
/*      */     }
/* 1721 */     if ((bool3) || ((bool4) && (!bool2) && (!bool1)))
/*      */     {
/* 1722 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.5D, d1, d2);
/* 1723 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.5D, d1, d4);
/* 1724 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.25D, d3, d4);
/* 1725 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.25D, d3, d2);
/*      */       
/* 1727 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.25D, d3, d2);
/* 1728 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.25D, d3, d4);
/* 1729 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.5D, d1, d4);
/* 1730 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.5D, d1, d2);
/*      */     }
/* 1732 */     if ((bool4) || ((bool3) && (!bool2) && (!bool1)))
/*      */     {
/* 1733 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.75D, d1, d2);
/* 1734 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.75D, d1, d4);
/* 1735 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.5D, d3, d4);
/* 1736 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.5D, d3, d2);
/*      */       
/* 1738 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.5D, d3, d2);
/* 1739 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.5D, d3, d4);
/* 1740 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.75D, d1, d4);
/* 1741 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.75D, d1, d2);
/*      */     }
/* 1743 */     if (bool4)
/*      */     {
/* 1744 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 1, d1, d2);
/* 1745 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 1, d1, d4);
/* 1746 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.75D, d3, d4);
/* 1747 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.75D, d3, d2);
/*      */       
/* 1749 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 0.75D, d3, d2);
/* 1750 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 0.75D, d3, d4);
/* 1751 */       localblz.a(paramInt1 + f3, paramInt2 + d5, paramInt3 + 1, d1, d4);
/* 1752 */       localblz.a(paramInt1 + f2, paramInt2 + d5, paramInt3 + 1, d1, d2);
/*      */     }
/* 1755 */     if (bool1)
/*      */     {
/* 1756 */       localblz.a(paramInt1, paramInt2 + d5, paramInt3 + f3, d1, d4);
/* 1757 */       localblz.a(paramInt1 + 0.25D, paramInt2 + d5, paramInt3 + f3, d3, d4);
/* 1758 */       localblz.a(paramInt1 + 0.25D, paramInt2 + d5, paramInt3 + f2, d3, d2);
/* 1759 */       localblz.a(paramInt1, paramInt2 + d5, paramInt3 + f2, d1, d2);
/*      */       
/* 1761 */       localblz.a(paramInt1, paramInt2 + d5, paramInt3 + f2, d1, d2);
/* 1762 */       localblz.a(paramInt1 + 0.25D, paramInt2 + d5, paramInt3 + f2, d3, d2);
/* 1763 */       localblz.a(paramInt1 + 0.25D, paramInt2 + d5, paramInt3 + f3, d3, d4);
/* 1764 */       localblz.a(paramInt1, paramInt2 + d5, paramInt3 + f3, d1, d4);
/*      */     }
/* 1766 */     if ((bool1) || ((bool2) && (!bool3) && (!bool4)))
/*      */     {
/* 1767 */       localblz.a(paramInt1 + 0.25D, paramInt2 + d5, paramInt3 + f3, d1, d4);
/* 1768 */       localblz.a(paramInt1 + 0.5D, paramInt2 + d5, paramInt3 + f3, d3, d4);
/* 1769 */       localblz.a(paramInt1 + 0.5D, paramInt2 + d5, paramInt3 + f2, d3, d2);
/* 1770 */       localblz.a(paramInt1 + 0.25D, paramInt2 + d5, paramInt3 + f2, d1, d2);
/*      */       
/* 1772 */       localblz.a(paramInt1 + 0.25D, paramInt2 + d5, paramInt3 + f2, d1, d2);
/* 1773 */       localblz.a(paramInt1 + 0.5D, paramInt2 + d5, paramInt3 + f2, d3, d2);
/* 1774 */       localblz.a(paramInt1 + 0.5D, paramInt2 + d5, paramInt3 + f3, d3, d4);
/* 1775 */       localblz.a(paramInt1 + 0.25D, paramInt2 + d5, paramInt3 + f3, d1, d4);
/*      */     }
/* 1777 */     if ((bool2) || ((bool1) && (!bool3) && (!bool4)))
/*      */     {
/* 1778 */       localblz.a(paramInt1 + 0.5D, paramInt2 + d5, paramInt3 + f3, d1, d4);
/* 1779 */       localblz.a(paramInt1 + 0.75D, paramInt2 + d5, paramInt3 + f3, d3, d4);
/* 1780 */       localblz.a(paramInt1 + 0.75D, paramInt2 + d5, paramInt3 + f2, d3, d2);
/* 1781 */       localblz.a(paramInt1 + 0.5D, paramInt2 + d5, paramInt3 + f2, d1, d2);
/*      */       
/* 1783 */       localblz.a(paramInt1 + 0.5D, paramInt2 + d5, paramInt3 + f2, d1, d2);
/* 1784 */       localblz.a(paramInt1 + 0.75D, paramInt2 + d5, paramInt3 + f2, d3, d2);
/* 1785 */       localblz.a(paramInt1 + 0.75D, paramInt2 + d5, paramInt3 + f3, d3, d4);
/* 1786 */       localblz.a(paramInt1 + 0.5D, paramInt2 + d5, paramInt3 + f3, d1, d4);
/*      */     }
/* 1788 */     if (bool2)
/*      */     {
/* 1789 */       localblz.a(paramInt1 + 0.75D, paramInt2 + d5, paramInt3 + f3, d1, d4);
/* 1790 */       localblz.a(paramInt1 + 1, paramInt2 + d5, paramInt3 + f3, d3, d4);
/* 1791 */       localblz.a(paramInt1 + 1, paramInt2 + d5, paramInt3 + f2, d3, d2);
/* 1792 */       localblz.a(paramInt1 + 0.75D, paramInt2 + d5, paramInt3 + f2, d1, d2);
/*      */       
/* 1794 */       localblz.a(paramInt1 + 0.75D, paramInt2 + d5, paramInt3 + f2, d1, d2);
/* 1795 */       localblz.a(paramInt1 + 1, paramInt2 + d5, paramInt3 + f2, d3, d2);
/* 1796 */       localblz.a(paramInt1 + 1, paramInt2 + d5, paramInt3 + f3, d3, d4);
/* 1797 */       localblz.a(paramInt1 + 0.75D, paramInt2 + d5, paramInt3 + f3, d1, d4);
/*      */     }
/* 1800 */     return true;
/*      */   }
/*      */   
/*      */   public boolean a(ajn paramajn, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 1804 */     blz localblz = blz.a;
/* 1805 */     ps localps1 = paramajn.c(0);
/* 1806 */     ps localps2 = paramajn.c(1);
/* 1807 */     ps localps3 = localps1;
/* 1809 */     if (b()) {
/* 1809 */       localps3 = this.d;
/*      */     }
/* 1811 */     localblz.b(1.0F, 1.0F, 1.0F);
/* 1812 */     localblz.b(paramajn.c(this.a, paramInt1, paramInt2, paramInt3));
/*      */     
/* 1814 */     double d1 = localps3.c();
/* 1815 */     double d2 = localps3.e();
/* 1816 */     double d3 = localps3.d();
/* 1817 */     double d4 = localps3.f();
/* 1818 */     float f1 = 1.4F;
/*      */     double d6;
/*      */     double d7;
/*      */     double d8;
/*      */     double d9;
/*      */     double d10;
/*      */     double d11;
/*      */     double d12;
/* 1821 */     if ((afn.a(this.a, paramInt1, paramInt2 - 1, paramInt3)) || (ahz.ab.e(this.a, paramInt1, paramInt2 - 1, paramInt3)))
/*      */     {
/* 1822 */       double d5 = paramInt1 + 0.5D + 0.2D;
/* 1823 */       d6 = paramInt1 + 0.5D - 0.2D;
/* 1824 */       d7 = paramInt3 + 0.5D + 0.2D;
/* 1825 */       d8 = paramInt3 + 0.5D - 0.2D;
/*      */       
/* 1827 */       d9 = paramInt1 + 0.5D - 0.3D;
/* 1828 */       d10 = paramInt1 + 0.5D + 0.3D;
/* 1829 */       d11 = paramInt3 + 0.5D - 0.3D;
/* 1830 */       d12 = paramInt3 + 0.5D + 0.3D;
/*      */       
/* 1832 */       localblz.a(d9, paramInt2 + f1, paramInt3 + 1, d3, d2);
/* 1833 */       localblz.a(d5, paramInt2 + 0, paramInt3 + 1, d3, d4);
/* 1834 */       localblz.a(d5, paramInt2 + 0, paramInt3 + 0, d1, d4);
/* 1835 */       localblz.a(d9, paramInt2 + f1, paramInt3 + 0, d1, d2);
/*      */       
/* 1837 */       localblz.a(d10, paramInt2 + f1, paramInt3 + 0, d3, d2);
/* 1838 */       localblz.a(d6, paramInt2 + 0, paramInt3 + 0, d3, d4);
/* 1839 */       localblz.a(d6, paramInt2 + 0, paramInt3 + 1, d1, d4);
/* 1840 */       localblz.a(d10, paramInt2 + f1, paramInt3 + 1, d1, d2);
/*      */       
/* 1842 */       localps3 = localps2;
/* 1843 */       d1 = localps3.c();
/* 1844 */       d2 = localps3.e();
/* 1845 */       d3 = localps3.d();
/* 1846 */       d4 = localps3.f();
/*      */       
/* 1848 */       localblz.a(paramInt1 + 1, paramInt2 + f1, d12, d3, d2);
/* 1849 */       localblz.a(paramInt1 + 1, paramInt2 + 0, d8, d3, d4);
/* 1850 */       localblz.a(paramInt1 + 0, paramInt2 + 0, d8, d1, d4);
/* 1851 */       localblz.a(paramInt1 + 0, paramInt2 + f1, d12, d1, d2);
/*      */       
/* 1853 */       localblz.a(paramInt1 + 0, paramInt2 + f1, d11, d3, d2);
/* 1854 */       localblz.a(paramInt1 + 0, paramInt2 + 0, d7, d3, d4);
/* 1855 */       localblz.a(paramInt1 + 1, paramInt2 + 0, d7, d1, d4);
/* 1856 */       localblz.a(paramInt1 + 1, paramInt2 + f1, d11, d1, d2);
/*      */       
/* 1858 */       d5 = paramInt1 + 0.5D - 0.5D;
/* 1859 */       d6 = paramInt1 + 0.5D + 0.5D;
/* 1860 */       d7 = paramInt3 + 0.5D - 0.5D;
/* 1861 */       d8 = paramInt3 + 0.5D + 0.5D;
/*      */       
/* 1863 */       d9 = paramInt1 + 0.5D - 0.4D;
/* 1864 */       d10 = paramInt1 + 0.5D + 0.4D;
/* 1865 */       d11 = paramInt3 + 0.5D - 0.4D;
/* 1866 */       d12 = paramInt3 + 0.5D + 0.4D;
/*      */       
/* 1868 */       localblz.a(d9, paramInt2 + f1, paramInt3 + 0, d1, d2);
/* 1869 */       localblz.a(d5, paramInt2 + 0, paramInt3 + 0, d1, d4);
/* 1870 */       localblz.a(d5, paramInt2 + 0, paramInt3 + 1, d3, d4);
/* 1871 */       localblz.a(d9, paramInt2 + f1, paramInt3 + 1, d3, d2);
/*      */       
/* 1873 */       localblz.a(d10, paramInt2 + f1, paramInt3 + 1, d1, d2);
/* 1874 */       localblz.a(d6, paramInt2 + 0, paramInt3 + 1, d1, d4);
/* 1875 */       localblz.a(d6, paramInt2 + 0, paramInt3 + 0, d3, d4);
/* 1876 */       localblz.a(d10, paramInt2 + f1, paramInt3 + 0, d3, d2);
/*      */       
/* 1878 */       localps3 = localps1;
/* 1879 */       d1 = localps3.c();
/* 1880 */       d2 = localps3.e();
/* 1881 */       d3 = localps3.d();
/* 1882 */       d4 = localps3.f();
/*      */       
/* 1884 */       localblz.a(paramInt1 + 0, paramInt2 + f1, d12, d1, d2);
/* 1885 */       localblz.a(paramInt1 + 0, paramInt2 + 0, d8, d1, d4);
/* 1886 */       localblz.a(paramInt1 + 1, paramInt2 + 0, d8, d3, d4);
/* 1887 */       localblz.a(paramInt1 + 1, paramInt2 + f1, d12, d3, d2);
/*      */       
/* 1889 */       localblz.a(paramInt1 + 1, paramInt2 + f1, d11, d1, d2);
/* 1890 */       localblz.a(paramInt1 + 1, paramInt2 + 0, d7, d1, d4);
/* 1891 */       localblz.a(paramInt1 + 0, paramInt2 + 0, d7, d3, d4);
/* 1892 */       localblz.a(paramInt1 + 0, paramInt2 + f1, d11, d3, d2);
/*      */     }
/*      */     else
/*      */     {
/* 1894 */       float f2 = 0.2F;
/* 1895 */       float f3 = 0.0625F;
/* 1896 */       if ((paramInt1 + paramInt2 + paramInt3 & 0x1) == 1)
/*      */       {
/* 1897 */         localps3 = localps2;
/* 1898 */         d1 = localps3.c();
/* 1899 */         d2 = localps3.e();
/* 1900 */         d3 = localps3.d();
/* 1901 */         d4 = localps3.f();
/*      */       }
/* 1903 */       if ((paramInt1 / 2 + paramInt2 / 2 + paramInt3 / 2 & 0x1) == 1)
/*      */       {
/* 1904 */         d6 = d3;
/* 1905 */         d3 = d1;
/* 1906 */         d1 = d6;
/*      */       }
/* 1908 */       if (ahz.ab.e(this.a, paramInt1 - 1, paramInt2, paramInt3))
/*      */       {
/* 1909 */         localblz.a(paramInt1 + f2, paramInt2 + f1 + f3, paramInt3 + 1, d3, d2);
/* 1910 */         localblz.a(paramInt1 + 0, paramInt2 + 0 + f3, paramInt3 + 1, d3, d4);
/* 1911 */         localblz.a(paramInt1 + 0, paramInt2 + 0 + f3, paramInt3 + 0, d1, d4);
/* 1912 */         localblz.a(paramInt1 + f2, paramInt2 + f1 + f3, paramInt3 + 0, d1, d2);
/*      */         
/* 1914 */         localblz.a(paramInt1 + f2, paramInt2 + f1 + f3, paramInt3 + 0, d1, d2);
/* 1915 */         localblz.a(paramInt1 + 0, paramInt2 + 0 + f3, paramInt3 + 0, d1, d4);
/* 1916 */         localblz.a(paramInt1 + 0, paramInt2 + 0 + f3, paramInt3 + 1, d3, d4);
/* 1917 */         localblz.a(paramInt1 + f2, paramInt2 + f1 + f3, paramInt3 + 1, d3, d2);
/*      */       }
/* 1919 */       if (ahz.ab.e(this.a, paramInt1 + 1, paramInt2, paramInt3))
/*      */       {
/* 1920 */         localblz.a(paramInt1 + 1 - f2, paramInt2 + f1 + f3, paramInt3 + 0, d1, d2);
/* 1921 */         localblz.a(paramInt1 + 1 - 0, paramInt2 + 0 + f3, paramInt3 + 0, d1, d4);
/* 1922 */         localblz.a(paramInt1 + 1 - 0, paramInt2 + 0 + f3, paramInt3 + 1, d3, d4);
/* 1923 */         localblz.a(paramInt1 + 1 - f2, paramInt2 + f1 + f3, paramInt3 + 1, d3, d2);
/*      */         
/* 1925 */         localblz.a(paramInt1 + 1 - f2, paramInt2 + f1 + f3, paramInt3 + 1, d3, d2);
/* 1926 */         localblz.a(paramInt1 + 1 - 0, paramInt2 + 0 + f3, paramInt3 + 1, d3, d4);
/* 1927 */         localblz.a(paramInt1 + 1 - 0, paramInt2 + 0 + f3, paramInt3 + 0, d1, d4);
/* 1928 */         localblz.a(paramInt1 + 1 - f2, paramInt2 + f1 + f3, paramInt3 + 0, d1, d2);
/*      */       }
/* 1930 */       if (ahz.ab.e(this.a, paramInt1, paramInt2, paramInt3 - 1))
/*      */       {
/* 1931 */         localblz.a(paramInt1 + 0, paramInt2 + f1 + f3, paramInt3 + f2, d3, d2);
/* 1932 */         localblz.a(paramInt1 + 0, paramInt2 + 0 + f3, paramInt3 + 0, d3, d4);
/* 1933 */         localblz.a(paramInt1 + 1, paramInt2 + 0 + f3, paramInt3 + 0, d1, d4);
/* 1934 */         localblz.a(paramInt1 + 1, paramInt2 + f1 + f3, paramInt3 + f2, d1, d2);
/*      */         
/* 1936 */         localblz.a(paramInt1 + 1, paramInt2 + f1 + f3, paramInt3 + f2, d1, d2);
/* 1937 */         localblz.a(paramInt1 + 1, paramInt2 + 0 + f3, paramInt3 + 0, d1, d4);
/* 1938 */         localblz.a(paramInt1 + 0, paramInt2 + 0 + f3, paramInt3 + 0, d3, d4);
/* 1939 */         localblz.a(paramInt1 + 0, paramInt2 + f1 + f3, paramInt3 + f2, d3, d2);
/*      */       }
/* 1941 */       if (ahz.ab.e(this.a, paramInt1, paramInt2, paramInt3 + 1))
/*      */       {
/* 1942 */         localblz.a(paramInt1 + 1, paramInt2 + f1 + f3, paramInt3 + 1 - f2, d1, d2);
/* 1943 */         localblz.a(paramInt1 + 1, paramInt2 + 0 + f3, paramInt3 + 1 - 0, d1, d4);
/* 1944 */         localblz.a(paramInt1 + 0, paramInt2 + 0 + f3, paramInt3 + 1 - 0, d3, d4);
/* 1945 */         localblz.a(paramInt1 + 0, paramInt2 + f1 + f3, paramInt3 + 1 - f2, d3, d2);
/*      */         
/* 1947 */         localblz.a(paramInt1 + 0, paramInt2 + f1 + f3, paramInt3 + 1 - f2, d3, d2);
/* 1948 */         localblz.a(paramInt1 + 0, paramInt2 + 0 + f3, paramInt3 + 1 - 0, d3, d4);
/* 1949 */         localblz.a(paramInt1 + 1, paramInt2 + 0 + f3, paramInt3 + 1 - 0, d1, d4);
/* 1950 */         localblz.a(paramInt1 + 1, paramInt2 + f1 + f3, paramInt3 + 1 - f2, d1, d2);
/*      */       }
/* 1952 */       if (ahz.ab.e(this.a, paramInt1, paramInt2 + 1, paramInt3))
/*      */       {
/* 1953 */         d6 = paramInt1 + 0.5D + 0.5D;
/* 1954 */         d7 = paramInt1 + 0.5D - 0.5D;
/* 1955 */         d8 = paramInt3 + 0.5D + 0.5D;
/* 1956 */         d9 = paramInt3 + 0.5D - 0.5D;
/*      */         
/* 1958 */         d10 = paramInt1 + 0.5D - 0.5D;
/* 1959 */         d11 = paramInt1 + 0.5D + 0.5D;
/* 1960 */         d12 = paramInt3 + 0.5D - 0.5D;
/* 1961 */         double d13 = paramInt3 + 0.5D + 0.5D;
/*      */         
/* 1963 */         localps3 = localps1;
/* 1964 */         d1 = localps3.c();
/* 1965 */         d2 = localps3.e();
/* 1966 */         d3 = localps3.d();
/* 1967 */         d4 = localps3.f();
/*      */         
/* 1969 */         paramInt2++;
/* 1970 */         f1 = -0.2F;
/* 1972 */         if ((paramInt1 + paramInt2 + paramInt3 & 0x1) == 0)
/*      */         {
/* 1974 */           localblz.a(d10, paramInt2 + f1, paramInt3 + 0, d3, d2);
/* 1975 */           localblz.a(d6, paramInt2 + 0, paramInt3 + 0, d3, d4);
/* 1976 */           localblz.a(d6, paramInt2 + 0, paramInt3 + 1, d1, d4);
/* 1977 */           localblz.a(d10, paramInt2 + f1, paramInt3 + 1, d1, d2);
/*      */           
/* 1979 */           localps3 = localps2;
/* 1980 */           d1 = localps3.c();
/* 1981 */           d2 = localps3.e();
/* 1982 */           d3 = localps3.d();
/* 1983 */           d4 = localps3.f();
/*      */           
/* 1985 */           localblz.a(d11, paramInt2 + f1, paramInt3 + 1, d3, d2);
/* 1986 */           localblz.a(d7, paramInt2 + 0, paramInt3 + 1, d3, d4);
/* 1987 */           localblz.a(d7, paramInt2 + 0, paramInt3 + 0, d1, d4);
/* 1988 */           localblz.a(d11, paramInt2 + f1, paramInt3 + 0, d1, d2);
/*      */         }
/*      */         else
/*      */         {
/* 1992 */           localblz.a(paramInt1 + 0, paramInt2 + f1, d13, d3, d2);
/* 1993 */           localblz.a(paramInt1 + 0, paramInt2 + 0, d9, d3, d4);
/* 1994 */           localblz.a(paramInt1 + 1, paramInt2 + 0, d9, d1, d4);
/* 1995 */           localblz.a(paramInt1 + 1, paramInt2 + f1, d13, d1, d2);
/*      */           
/* 1997 */           localps3 = localps2;
/* 1998 */           d1 = localps3.c();
/* 1999 */           d2 = localps3.e();
/* 2000 */           d3 = localps3.d();
/* 2001 */           d4 = localps3.f();
/*      */           
/* 2003 */           localblz.a(paramInt1 + 1, paramInt2 + f1, d12, d3, d2);
/* 2004 */           localblz.a(paramInt1 + 1, paramInt2 + 0, d8, d3, d4);
/* 2005 */           localblz.a(paramInt1 + 0, paramInt2 + 0, d8, d1, d4);
/* 2006 */           localblz.a(paramInt1 + 0, paramInt2 + f1, d12, d1, d2);
/*      */         }
/*      */       }
/*      */     }
/* 2011 */     return true;
/*      */   }
/*      */   
/*      */   public boolean h(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 2015 */     blz localblz = blz.a;
/*      */     
/* 2017 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 2018 */     ps localps1 = alm.e("cross");
/* 2019 */     ps localps2 = alm.e("line");
/* 2020 */     ps localps3 = alm.e("cross_overlay");
/* 2021 */     ps localps4 = alm.e("line_overlay");
/*      */     
/* 2023 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/*      */     
/* 2025 */     float f1 = i1 / 15.0F;
/* 2026 */     float f2 = f1 * 0.6F + 0.4F;
/* 2027 */     if (i1 == 0) {
/* 2027 */       f2 = 0.3F;
/*      */     }
/* 2029 */     float f3 = f1 * f1 * 0.7F - 0.5F;
/* 2030 */     float f4 = f1 * f1 * 0.6F - 0.7F;
/* 2031 */     if (f3 < 0.0F) {
/* 2031 */       f3 = 0.0F;
/*      */     }
/* 2032 */     if (f4 < 0.0F) {
/* 2032 */       f4 = 0.0F;
/*      */     }
/* 2034 */     localblz.b(f2, f3, f4);
/*      */     
/* 2036 */     double d1 = 0.015625D;
/* 2037 */     double d2 = 0.015625D;
/*      */     
/* 2039 */     int i2 = (alm.f(this.a, paramInt1 - 1, paramInt2, paramInt3, 1)) || ((!this.a.a(paramInt1 - 1, paramInt2, paramInt3).q()) && (alm.f(this.a, paramInt1 - 1, paramInt2 - 1, paramInt3, -1))) ? 1 : 0;
/*      */     
/* 2041 */     int i3 = (alm.f(this.a, paramInt1 + 1, paramInt2, paramInt3, 3)) || ((!this.a.a(paramInt1 + 1, paramInt2, paramInt3).q()) && (alm.f(this.a, paramInt1 + 1, paramInt2 - 1, paramInt3, -1))) ? 1 : 0;
/*      */     
/* 2043 */     int i4 = (alm.f(this.a, paramInt1, paramInt2, paramInt3 - 1, 2)) || ((!this.a.a(paramInt1, paramInt2, paramInt3 - 1).q()) && (alm.f(this.a, paramInt1, paramInt2 - 1, paramInt3 - 1, -1))) ? 1 : 0;
/*      */     
/* 2045 */     int i5 = (alm.f(this.a, paramInt1, paramInt2, paramInt3 + 1, 0)) || ((!this.a.a(paramInt1, paramInt2, paramInt3 + 1).q()) && (alm.f(this.a, paramInt1, paramInt2 - 1, paramInt3 + 1, -1))) ? 1 : 0;
/* 2047 */     if (!this.a.a(paramInt1, paramInt2 + 1, paramInt3).q())
/*      */     {
/* 2048 */       if ((this.a.a(paramInt1 - 1, paramInt2, paramInt3).q()) && (alm.f(this.a, paramInt1 - 1, paramInt2 + 1, paramInt3, -1))) {
/* 2048 */         i2 = 1;
/*      */       }
/* 2049 */       if ((this.a.a(paramInt1 + 1, paramInt2, paramInt3).q()) && (alm.f(this.a, paramInt1 + 1, paramInt2 + 1, paramInt3, -1))) {
/* 2049 */         i3 = 1;
/*      */       }
/* 2050 */       if ((this.a.a(paramInt1, paramInt2, paramInt3 - 1).q()) && (alm.f(this.a, paramInt1, paramInt2 + 1, paramInt3 - 1, -1))) {
/* 2050 */         i4 = 1;
/*      */       }
/* 2051 */       if ((this.a.a(paramInt1, paramInt2, paramInt3 + 1).q()) && (alm.f(this.a, paramInt1, paramInt2 + 1, paramInt3 + 1, -1))) {
/* 2051 */         i5 = 1;
/*      */       }
/*      */     }
/* 2054 */     float f5 = paramInt1 + 0;
/* 2055 */     float f6 = paramInt1 + 1;
/* 2056 */     float f7 = paramInt3 + 0;
/* 2057 */     float f8 = paramInt3 + 1;
/*      */     
/* 2059 */     int i6 = 0;
/* 2060 */     if (((i2 != 0) || (i3 != 0)) && (i4 == 0) && (i5 == 0)) {
/* 2060 */       i6 = 1;
/*      */     }
/* 2061 */     if (((i4 != 0) || (i5 != 0)) && (i3 == 0) && (i2 == 0)) {
/* 2061 */       i6 = 2;
/*      */     }
/* 2063 */     if (i6 == 0)
/*      */     {
/* 2065 */       int i7 = 0;
/* 2066 */       int i8 = 0;
/* 2067 */       int i9 = 16;
/* 2068 */       int i10 = 16;
/*      */       
/* 2070 */       int i11 = 5;
/* 2071 */       if (i2 == 0) {
/* 2071 */         f5 += 0.3125F;
/*      */       }
/* 2072 */       if (i2 == 0) {
/* 2072 */         i7 += 5;
/*      */       }
/* 2073 */       if (i3 == 0) {
/* 2073 */         f6 -= 0.3125F;
/*      */       }
/* 2074 */       if (i3 == 0) {
/* 2074 */         i9 -= 5;
/*      */       }
/* 2075 */       if (i4 == 0) {
/* 2075 */         f7 += 0.3125F;
/*      */       }
/* 2076 */       if (i4 == 0) {
/* 2076 */         i8 += 5;
/*      */       }
/* 2077 */       if (i5 == 0) {
/* 2077 */         f8 -= 0.3125F;
/*      */       }
/* 2078 */       if (i5 == 0) {
/* 2078 */         i10 -= 5;
/*      */       }
/* 2080 */       localblz.a(f6, paramInt2 + 0.015625D, f8, localps1.a(i9), localps1.b(i10));
/* 2081 */       localblz.a(f6, paramInt2 + 0.015625D, f7, localps1.a(i9), localps1.b(i8));
/* 2082 */       localblz.a(f5, paramInt2 + 0.015625D, f7, localps1.a(i7), localps1.b(i8));
/* 2083 */       localblz.a(f5, paramInt2 + 0.015625D, f8, localps1.a(i7), localps1.b(i10));
/*      */       
/* 2085 */       localblz.b(1.0F, 1.0F, 1.0F);
/* 2086 */       localblz.a(f6, paramInt2 + 0.015625D, f8, localps3.a(i9), localps3.b(i10));
/* 2087 */       localblz.a(f6, paramInt2 + 0.015625D, f7, localps3.a(i9), localps3.b(i8));
/* 2088 */       localblz.a(f5, paramInt2 + 0.015625D, f7, localps3.a(i7), localps3.b(i8));
/* 2089 */       localblz.a(f5, paramInt2 + 0.015625D, f8, localps3.a(i7), localps3.b(i10));
/*      */     }
/* 2090 */     else if (i6 == 1)
/*      */     {
/* 2091 */       localblz.a(f6, paramInt2 + 0.015625D, f8, localps2.d(), localps2.f());
/* 2092 */       localblz.a(f6, paramInt2 + 0.015625D, f7, localps2.d(), localps2.e());
/* 2093 */       localblz.a(f5, paramInt2 + 0.015625D, f7, localps2.c(), localps2.e());
/* 2094 */       localblz.a(f5, paramInt2 + 0.015625D, f8, localps2.c(), localps2.f());
/*      */       
/* 2096 */       localblz.b(1.0F, 1.0F, 1.0F);
/* 2097 */       localblz.a(f6, paramInt2 + 0.015625D, f8, localps4.d(), localps4.f());
/* 2098 */       localblz.a(f6, paramInt2 + 0.015625D, f7, localps4.d(), localps4.e());
/* 2099 */       localblz.a(f5, paramInt2 + 0.015625D, f7, localps4.c(), localps4.e());
/* 2100 */       localblz.a(f5, paramInt2 + 0.015625D, f8, localps4.c(), localps4.f());
/*      */     }
/*      */     else
/*      */     {
/* 2102 */       localblz.a(f6, paramInt2 + 0.015625D, f8, localps2.d(), localps2.f());
/* 2103 */       localblz.a(f6, paramInt2 + 0.015625D, f7, localps2.c(), localps2.f());
/* 2104 */       localblz.a(f5, paramInt2 + 0.015625D, f7, localps2.c(), localps2.e());
/* 2105 */       localblz.a(f5, paramInt2 + 0.015625D, f8, localps2.d(), localps2.e());
/*      */       
/* 2107 */       localblz.b(1.0F, 1.0F, 1.0F);
/* 2108 */       localblz.a(f6, paramInt2 + 0.015625D, f8, localps4.d(), localps4.f());
/* 2109 */       localblz.a(f6, paramInt2 + 0.015625D, f7, localps4.c(), localps4.f());
/* 2110 */       localblz.a(f5, paramInt2 + 0.015625D, f7, localps4.c(), localps4.e());
/* 2111 */       localblz.a(f5, paramInt2 + 0.015625D, f8, localps4.d(), localps4.e());
/*      */     }
/* 2114 */     if (!this.a.a(paramInt1, paramInt2 + 1, paramInt3).q())
/*      */     {
/* 2115 */       float f9 = 0.021875F;
/* 2117 */       if ((this.a.a(paramInt1 - 1, paramInt2, paramInt3).q()) && (this.a.a(paramInt1 - 1, paramInt2 + 1, paramInt3) == ahz.af))
/*      */       {
/* 2118 */         localblz.b(f2, f3, f4);
/* 2119 */         localblz.a(paramInt1 + 0.015625D, paramInt2 + 1 + 0.021875F, paramInt3 + 1, localps2.d(), localps2.e());
/* 2120 */         localblz.a(paramInt1 + 0.015625D, paramInt2 + 0, paramInt3 + 1, localps2.c(), localps2.e());
/* 2121 */         localblz.a(paramInt1 + 0.015625D, paramInt2 + 0, paramInt3 + 0, localps2.c(), localps2.f());
/* 2122 */         localblz.a(paramInt1 + 0.015625D, paramInt2 + 1 + 0.021875F, paramInt3 + 0, localps2.d(), localps2.f());
/*      */         
/* 2124 */         localblz.b(1.0F, 1.0F, 1.0F);
/* 2125 */         localblz.a(paramInt1 + 0.015625D, paramInt2 + 1 + 0.021875F, paramInt3 + 1, localps4.d(), localps4.e());
/* 2126 */         localblz.a(paramInt1 + 0.015625D, paramInt2 + 0, paramInt3 + 1, localps4.c(), localps4.e());
/* 2127 */         localblz.a(paramInt1 + 0.015625D, paramInt2 + 0, paramInt3 + 0, localps4.c(), localps4.f());
/* 2128 */         localblz.a(paramInt1 + 0.015625D, paramInt2 + 1 + 0.021875F, paramInt3 + 0, localps4.d(), localps4.f());
/*      */       }
/* 2130 */       if ((this.a.a(paramInt1 + 1, paramInt2, paramInt3).q()) && (this.a.a(paramInt1 + 1, paramInt2 + 1, paramInt3) == ahz.af))
/*      */       {
/* 2131 */         localblz.b(f2, f3, f4);
/* 2132 */         localblz.a(paramInt1 + 1 - 0.015625D, paramInt2 + 0, paramInt3 + 1, localps2.c(), localps2.f());
/* 2133 */         localblz.a(paramInt1 + 1 - 0.015625D, paramInt2 + 1 + 0.021875F, paramInt3 + 1, localps2.d(), localps2.f());
/* 2134 */         localblz.a(paramInt1 + 1 - 0.015625D, paramInt2 + 1 + 0.021875F, paramInt3 + 0, localps2.d(), localps2.e());
/* 2135 */         localblz.a(paramInt1 + 1 - 0.015625D, paramInt2 + 0, paramInt3 + 0, localps2.c(), localps2.e());
/*      */         
/* 2137 */         localblz.b(1.0F, 1.0F, 1.0F);
/* 2138 */         localblz.a(paramInt1 + 1 - 0.015625D, paramInt2 + 0, paramInt3 + 1, localps4.c(), localps4.f());
/* 2139 */         localblz.a(paramInt1 + 1 - 0.015625D, paramInt2 + 1 + 0.021875F, paramInt3 + 1, localps4.d(), localps4.f());
/* 2140 */         localblz.a(paramInt1 + 1 - 0.015625D, paramInt2 + 1 + 0.021875F, paramInt3 + 0, localps4.d(), localps4.e());
/* 2141 */         localblz.a(paramInt1 + 1 - 0.015625D, paramInt2 + 0, paramInt3 + 0, localps4.c(), localps4.e());
/*      */       }
/* 2143 */       if ((this.a.a(paramInt1, paramInt2, paramInt3 - 1).q()) && (this.a.a(paramInt1, paramInt2 + 1, paramInt3 - 1) == ahz.af))
/*      */       {
/* 2144 */         localblz.b(f2, f3, f4);
/* 2145 */         localblz.a(paramInt1 + 1, paramInt2 + 0, paramInt3 + 0.015625D, localps2.c(), localps2.f());
/* 2146 */         localblz.a(paramInt1 + 1, paramInt2 + 1 + 0.021875F, paramInt3 + 0.015625D, localps2.d(), localps2.f());
/* 2147 */         localblz.a(paramInt1 + 0, paramInt2 + 1 + 0.021875F, paramInt3 + 0.015625D, localps2.d(), localps2.e());
/* 2148 */         localblz.a(paramInt1 + 0, paramInt2 + 0, paramInt3 + 0.015625D, localps2.c(), localps2.e());
/*      */         
/* 2150 */         localblz.b(1.0F, 1.0F, 1.0F);
/* 2151 */         localblz.a(paramInt1 + 1, paramInt2 + 0, paramInt3 + 0.015625D, localps4.c(), localps4.f());
/* 2152 */         localblz.a(paramInt1 + 1, paramInt2 + 1 + 0.021875F, paramInt3 + 0.015625D, localps4.d(), localps4.f());
/* 2153 */         localblz.a(paramInt1 + 0, paramInt2 + 1 + 0.021875F, paramInt3 + 0.015625D, localps4.d(), localps4.e());
/* 2154 */         localblz.a(paramInt1 + 0, paramInt2 + 0, paramInt3 + 0.015625D, localps4.c(), localps4.e());
/*      */       }
/* 2156 */       if ((this.a.a(paramInt1, paramInt2, paramInt3 + 1).q()) && (this.a.a(paramInt1, paramInt2 + 1, paramInt3 + 1) == ahz.af))
/*      */       {
/* 2157 */         localblz.b(f2, f3, f4);
/* 2158 */         localblz.a(paramInt1 + 1, paramInt2 + 1 + 0.021875F, paramInt3 + 1 - 0.015625D, localps2.d(), localps2.e());
/* 2159 */         localblz.a(paramInt1 + 1, paramInt2 + 0, paramInt3 + 1 - 0.015625D, localps2.c(), localps2.e());
/* 2160 */         localblz.a(paramInt1 + 0, paramInt2 + 0, paramInt3 + 1 - 0.015625D, localps2.c(), localps2.f());
/* 2161 */         localblz.a(paramInt1 + 0, paramInt2 + 1 + 0.021875F, paramInt3 + 1 - 0.015625D, localps2.d(), localps2.f());
/*      */         
/* 2163 */         localblz.b(1.0F, 1.0F, 1.0F);
/* 2164 */         localblz.a(paramInt1 + 1, paramInt2 + 1 + 0.021875F, paramInt3 + 1 - 0.015625D, localps4.d(), localps4.e());
/* 2165 */         localblz.a(paramInt1 + 1, paramInt2 + 0, paramInt3 + 1 - 0.015625D, localps4.c(), localps4.e());
/* 2166 */         localblz.a(paramInt1 + 0, paramInt2 + 0, paramInt3 + 1 - 0.015625D, localps4.c(), localps4.f());
/* 2167 */         localblz.a(paramInt1 + 0, paramInt2 + 1 + 0.021875F, paramInt3 + 1 - 0.015625D, localps4.d(), localps4.f());
/*      */       }
/*      */     }
/* 2171 */     return true;
/*      */   }
/*      */   
/*      */   public boolean a(ahq paramahq, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 2175 */     blz localblz = blz.a;
/* 2176 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/*      */     
/* 2178 */     ps localps = a(paramahq, 0, i1);
/* 2179 */     if (b()) {
/* 2179 */       localps = this.d;
/*      */     }
/* 2181 */     if (paramahq.e()) {
/* 2182 */       i1 &= 0x7;
/*      */     }
/* 2185 */     localblz.b(paramahq.c(this.a, paramInt1, paramInt2, paramInt3));
/* 2186 */     localblz.b(1.0F, 1.0F, 1.0F);
/*      */     
/* 2188 */     double d1 = localps.c();
/* 2189 */     double d2 = localps.e();
/* 2190 */     double d3 = localps.d();
/* 2191 */     double d4 = localps.f();
/*      */     
/* 2193 */     double d5 = 0.0625D;
/*      */     
/* 2195 */     double d6 = paramInt1 + 1;
/* 2196 */     double d7 = paramInt1 + 1;
/* 2197 */     double d8 = paramInt1 + 0;
/* 2198 */     double d9 = paramInt1 + 0;
/*      */     
/* 2200 */     double d10 = paramInt3 + 0;
/* 2201 */     double d11 = paramInt3 + 1;
/* 2202 */     double d12 = paramInt3 + 1;
/* 2203 */     double d13 = paramInt3 + 0;
/*      */     
/* 2205 */     double d14 = paramInt2 + d5;
/* 2206 */     double d15 = paramInt2 + d5;
/* 2207 */     double d16 = paramInt2 + d5;
/* 2208 */     double d17 = paramInt2 + d5;
/* 2210 */     if ((i1 == 1) || (i1 == 2) || (i1 == 3) || (i1 == 7))
/*      */     {
/* 2211 */       d6 = d9 = paramInt1 + 1;
/* 2212 */       d7 = d8 = paramInt1 + 0;
/* 2213 */       d10 = d11 = paramInt3 + 1;
/* 2214 */       d12 = d13 = paramInt3 + 0;
/*      */     }
/* 2215 */     else if (i1 == 8)
/*      */     {
/* 2216 */       d6 = d7 = paramInt1 + 0;
/* 2217 */       d8 = d9 = paramInt1 + 1;
/* 2218 */       d10 = d13 = paramInt3 + 1;
/* 2219 */       d11 = d12 = paramInt3 + 0;
/*      */     }
/* 2220 */     else if (i1 == 9)
/*      */     {
/* 2221 */       d6 = d9 = paramInt1 + 0;
/* 2222 */       d7 = d8 = paramInt1 + 1;
/* 2223 */       d10 = d11 = paramInt3 + 0;
/* 2224 */       d12 = d13 = paramInt3 + 1;
/*      */     }
/* 2227 */     if ((i1 == 2) || (i1 == 4))
/*      */     {
/* 2228 */       d14 += 1.0D;
/* 2229 */       d17 += 1.0D;
/*      */     }
/* 2230 */     else if ((i1 == 3) || (i1 == 5))
/*      */     {
/* 2231 */       d15 += 1.0D;
/* 2232 */       d16 += 1.0D;
/*      */     }
/* 2235 */     localblz.a(d6, d14, d10, d3, d2);
/* 2236 */     localblz.a(d7, d15, d11, d3, d4);
/* 2237 */     localblz.a(d8, d16, d12, d1, d4);
/* 2238 */     localblz.a(d9, d17, d13, d1, d2);
/*      */     
/* 2240 */     localblz.a(d9, d17, d13, d1, d2);
/* 2241 */     localblz.a(d8, d16, d12, d1, d4);
/* 2242 */     localblz.a(d7, d15, d11, d3, d4);
/* 2243 */     localblz.a(d6, d14, d10, d3, d2);
/*      */     
/* 2245 */     return true;
/*      */   }
/*      */   
/*      */   public boolean i(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 2249 */     blz localblz = blz.a;
/*      */     
/* 2251 */     ps localps = a(paramahu, 0);
/* 2253 */     if (b()) {
/* 2253 */       localps = this.d;
/*      */     }
/* 2256 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/* 2257 */     localblz.b(1.0F, 1.0F, 1.0F);
/*      */     
/*      */ 
/* 2260 */     double d1 = localps.c();
/* 2261 */     double d2 = localps.e();
/* 2262 */     double d3 = localps.d();
/* 2263 */     double d4 = localps.f();
/*      */     
/* 2265 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/*      */     
/* 2267 */     double d5 = 0.0D;
/* 2268 */     double d6 = 0.0500000007450581D;
/* 2269 */     if (i1 == 5)
/*      */     {
/* 2270 */       localblz.a(paramInt1 + d6, paramInt2 + 1 + d5, paramInt3 + 1 + d5, d1, d2);
/* 2271 */       localblz.a(paramInt1 + d6, paramInt2 + 0 - d5, paramInt3 + 1 + d5, d1, d4);
/* 2272 */       localblz.a(paramInt1 + d6, paramInt2 + 0 - d5, paramInt3 + 0 - d5, d3, d4);
/* 2273 */       localblz.a(paramInt1 + d6, paramInt2 + 1 + d5, paramInt3 + 0 - d5, d3, d2);
/*      */     }
/* 2275 */     if (i1 == 4)
/*      */     {
/* 2276 */       localblz.a(paramInt1 + 1 - d6, paramInt2 + 0 - d5, paramInt3 + 1 + d5, d3, d4);
/* 2277 */       localblz.a(paramInt1 + 1 - d6, paramInt2 + 1 + d5, paramInt3 + 1 + d5, d3, d2);
/* 2278 */       localblz.a(paramInt1 + 1 - d6, paramInt2 + 1 + d5, paramInt3 + 0 - d5, d1, d2);
/* 2279 */       localblz.a(paramInt1 + 1 - d6, paramInt2 + 0 - d5, paramInt3 + 0 - d5, d1, d4);
/*      */     }
/* 2281 */     if (i1 == 3)
/*      */     {
/* 2282 */       localblz.a(paramInt1 + 1 + d5, paramInt2 + 0 - d5, paramInt3 + d6, d3, d4);
/* 2283 */       localblz.a(paramInt1 + 1 + d5, paramInt2 + 1 + d5, paramInt3 + d6, d3, d2);
/* 2284 */       localblz.a(paramInt1 + 0 - d5, paramInt2 + 1 + d5, paramInt3 + d6, d1, d2);
/* 2285 */       localblz.a(paramInt1 + 0 - d5, paramInt2 + 0 - d5, paramInt3 + d6, d1, d4);
/*      */     }
/* 2287 */     if (i1 == 2)
/*      */     {
/* 2288 */       localblz.a(paramInt1 + 1 + d5, paramInt2 + 1 + d5, paramInt3 + 1 - d6, d1, d2);
/* 2289 */       localblz.a(paramInt1 + 1 + d5, paramInt2 + 0 - d5, paramInt3 + 1 - d6, d1, d4);
/* 2290 */       localblz.a(paramInt1 + 0 - d5, paramInt2 + 0 - d5, paramInt3 + 1 - d6, d3, d4);
/* 2291 */       localblz.a(paramInt1 + 0 - d5, paramInt2 + 1 + d5, paramInt3 + 1 - d6, d3, d2);
/*      */     }
/* 2294 */     return true;
/*      */   }
/*      */   
/*      */   public boolean j(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 2298 */     blz localblz = blz.a;
/*      */     
/* 2300 */     ps localps = a(paramahu, 0);
/* 2302 */     if (b()) {
/* 2302 */       localps = this.d;
/*      */     }
/* 2304 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/*      */     
/* 2306 */     int i1 = paramahu.d(this.a, paramInt1, paramInt2, paramInt3);
/* 2307 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/* 2308 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/* 2309 */     float f3 = (i1 & 0xFF) / 255.0F;
/*      */     
/* 2311 */     localblz.b(f1, f2, f3);
/*      */     
/*      */ 
/* 2314 */     double d1 = localps.c();
/* 2315 */     double d2 = localps.e();
/* 2316 */     double d3 = localps.d();
/* 2317 */     double d4 = localps.f();
/*      */     
/* 2319 */     double d5 = 0.0500000007450581D;
/* 2320 */     int i2 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 2322 */     if ((i2 & 0x2) != 0)
/*      */     {
/* 2323 */       localblz.a(paramInt1 + d5, paramInt2 + 1, paramInt3 + 1, d1, d2);
/* 2324 */       localblz.a(paramInt1 + d5, paramInt2 + 0, paramInt3 + 1, d1, d4);
/* 2325 */       localblz.a(paramInt1 + d5, paramInt2 + 0, paramInt3 + 0, d3, d4);
/* 2326 */       localblz.a(paramInt1 + d5, paramInt2 + 1, paramInt3 + 0, d3, d2);
/*      */       
/* 2328 */       localblz.a(paramInt1 + d5, paramInt2 + 1, paramInt3 + 0, d3, d2);
/* 2329 */       localblz.a(paramInt1 + d5, paramInt2 + 0, paramInt3 + 0, d3, d4);
/* 2330 */       localblz.a(paramInt1 + d5, paramInt2 + 0, paramInt3 + 1, d1, d4);
/* 2331 */       localblz.a(paramInt1 + d5, paramInt2 + 1, paramInt3 + 1, d1, d2);
/*      */     }
/* 2333 */     if ((i2 & 0x8) != 0)
/*      */     {
/* 2334 */       localblz.a(paramInt1 + 1 - d5, paramInt2 + 0, paramInt3 + 1, d3, d4);
/* 2335 */       localblz.a(paramInt1 + 1 - d5, paramInt2 + 1, paramInt3 + 1, d3, d2);
/* 2336 */       localblz.a(paramInt1 + 1 - d5, paramInt2 + 1, paramInt3 + 0, d1, d2);
/* 2337 */       localblz.a(paramInt1 + 1 - d5, paramInt2 + 0, paramInt3 + 0, d1, d4);
/*      */       
/* 2339 */       localblz.a(paramInt1 + 1 - d5, paramInt2 + 0, paramInt3 + 0, d1, d4);
/* 2340 */       localblz.a(paramInt1 + 1 - d5, paramInt2 + 1, paramInt3 + 0, d1, d2);
/* 2341 */       localblz.a(paramInt1 + 1 - d5, paramInt2 + 1, paramInt3 + 1, d3, d2);
/* 2342 */       localblz.a(paramInt1 + 1 - d5, paramInt2 + 0, paramInt3 + 1, d3, d4);
/*      */     }
/* 2344 */     if ((i2 & 0x4) != 0)
/*      */     {
/* 2345 */       localblz.a(paramInt1 + 1, paramInt2 + 0, paramInt3 + d5, d3, d4);
/* 2346 */       localblz.a(paramInt1 + 1, paramInt2 + 1, paramInt3 + d5, d3, d2);
/* 2347 */       localblz.a(paramInt1 + 0, paramInt2 + 1, paramInt3 + d5, d1, d2);
/* 2348 */       localblz.a(paramInt1 + 0, paramInt2 + 0, paramInt3 + d5, d1, d4);
/*      */       
/* 2350 */       localblz.a(paramInt1 + 0, paramInt2 + 0, paramInt3 + d5, d1, d4);
/* 2351 */       localblz.a(paramInt1 + 0, paramInt2 + 1, paramInt3 + d5, d1, d2);
/* 2352 */       localblz.a(paramInt1 + 1, paramInt2 + 1, paramInt3 + d5, d3, d2);
/* 2353 */       localblz.a(paramInt1 + 1, paramInt2 + 0, paramInt3 + d5, d3, d4);
/*      */     }
/* 2355 */     if ((i2 & 0x1) != 0)
/*      */     {
/* 2356 */       localblz.a(paramInt1 + 1, paramInt2 + 1, paramInt3 + 1 - d5, d1, d2);
/* 2357 */       localblz.a(paramInt1 + 1, paramInt2 + 0, paramInt3 + 1 - d5, d1, d4);
/* 2358 */       localblz.a(paramInt1 + 0, paramInt2 + 0, paramInt3 + 1 - d5, d3, d4);
/* 2359 */       localblz.a(paramInt1 + 0, paramInt2 + 1, paramInt3 + 1 - d5, d3, d2);
/*      */       
/* 2361 */       localblz.a(paramInt1 + 0, paramInt2 + 1, paramInt3 + 1 - d5, d3, d2);
/* 2362 */       localblz.a(paramInt1 + 0, paramInt2 + 0, paramInt3 + 1 - d5, d3, d4);
/* 2363 */       localblz.a(paramInt1 + 1, paramInt2 + 0, paramInt3 + 1 - d5, d1, d4);
/* 2364 */       localblz.a(paramInt1 + 1, paramInt2 + 1, paramInt3 + 1 - d5, d1, d2);
/*      */     }
/* 2366 */     if (this.a.a(paramInt1, paramInt2 + 1, paramInt3).q())
/*      */     {
/* 2367 */       localblz.a(paramInt1 + 1, paramInt2 + 1 - d5, paramInt3 + 0, d1, d2);
/* 2368 */       localblz.a(paramInt1 + 1, paramInt2 + 1 - d5, paramInt3 + 1, d1, d4);
/* 2369 */       localblz.a(paramInt1 + 0, paramInt2 + 1 - d5, paramInt3 + 1, d3, d4);
/* 2370 */       localblz.a(paramInt1 + 0, paramInt2 + 1 - d5, paramInt3 + 0, d3, d2);
/*      */     }
/* 2373 */     return true;
/*      */   }
/*      */   
/*      */   public boolean k(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 2377 */     int i1 = this.a.Q();
/* 2378 */     blz localblz = blz.a;
/*      */     
/* 2380 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/* 2381 */     int i2 = paramahu.d(this.a, paramInt1, paramInt2, paramInt3);
/* 2382 */     float f1 = (i2 >> 16 & 0xFF) / 255.0F;
/* 2383 */     float f2 = (i2 >> 8 & 0xFF) / 255.0F;
/* 2384 */     float f3 = (i2 & 0xFF) / 255.0F;
/* 2386 */     if (bll.a)
/*      */     {
/* 2387 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/* 2388 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/* 2389 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/* 2391 */       f1 = f4;
/* 2392 */       f2 = f5;
/* 2393 */       f3 = f6;
/*      */     }
/* 2395 */     localblz.b(f1, f2, f3);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 2400 */     boolean bool1 = paramahu instanceof amd;
/*      */     ps localps1;
/*      */     ps localps2;
/* 2401 */     if (b())
/*      */     {
/* 2402 */       localps1 = this.d;
/* 2403 */       localps2 = this.d;
/*      */     }
/*      */     else
/*      */     {
/* 2405 */       int i3 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 2406 */       localps1 = a(paramahu, 0, i3);
/* 2407 */       localps2 = bool1 ? ((amd)paramahu).b(i3) : ((amm)paramahu).e();
/*      */     }
/* 2410 */     double d1 = localps1.c();
/* 2411 */     double d2 = localps1.a(7.0D);
/* 2412 */     double d3 = localps1.a(9.0D);
/* 2413 */     double d4 = localps1.d();
/* 2414 */     double d5 = localps1.e();
/* 2415 */     double d6 = localps1.f();
/*      */     
/* 2417 */     double d7 = localps2.a(7.0D);
/* 2418 */     double d8 = localps2.a(9.0D);
/* 2419 */     double d9 = localps2.e();
/* 2420 */     double d10 = localps2.f();
/* 2421 */     double d11 = localps2.b(7.0D);
/* 2422 */     double d12 = localps2.b(9.0D);
/*      */     
/* 2424 */     double d13 = paramInt1;
/* 2425 */     double d14 = paramInt1 + 1;
/* 2426 */     double d15 = paramInt3;
/* 2427 */     double d16 = paramInt3 + 1;
/* 2428 */     double d17 = paramInt1 + 0.5D - 0.0625D;
/* 2429 */     double d18 = paramInt1 + 0.5D + 0.0625D;
/* 2430 */     double d19 = paramInt3 + 0.5D - 0.0625D;
/* 2431 */     double d20 = paramInt3 + 0.5D + 0.0625D;
/*      */     
/* 2433 */     boolean bool2 = bool1 ? ((amd)paramahu).a(this.a.a(paramInt1, paramInt2, paramInt3 - 1)) : ((amm)paramahu).a(this.a.a(paramInt1, paramInt2, paramInt3 - 1));
/* 2434 */     boolean bool3 = bool1 ? ((amd)paramahu).a(this.a.a(paramInt1, paramInt2, paramInt3 + 1)) : ((amm)paramahu).a(this.a.a(paramInt1, paramInt2, paramInt3 + 1));
/* 2435 */     boolean bool4 = bool1 ? ((amd)paramahu).a(this.a.a(paramInt1 - 1, paramInt2, paramInt3)) : ((amm)paramahu).a(this.a.a(paramInt1 - 1, paramInt2, paramInt3));
/* 2436 */     boolean bool5 = bool1 ? ((amd)paramahu).a(this.a.a(paramInt1 + 1, paramInt2, paramInt3)) : ((amm)paramahu).a(this.a.a(paramInt1 + 1, paramInt2, paramInt3));
/*      */     
/* 2438 */     double d21 = 0.001D;
/* 2439 */     double d22 = 0.999D;
/* 2440 */     double d23 = 0.001D;
/*      */     
/* 2442 */     int i4 = (!bool2) && (!bool3) && (!bool4) && (!bool5) ? 1 : 0;
/* 2444 */     if ((bool4) || (i4 != 0))
/*      */     {
/* 2445 */       if ((bool4) && (bool5))
/*      */       {
/* 2446 */         if (!bool2)
/*      */         {
/* 2447 */           localblz.a(d14, paramInt2 + 0.999D, d19, d4, d5);
/* 2448 */           localblz.a(d14, paramInt2 + 0.001D, d19, d4, d6);
/* 2449 */           localblz.a(d13, paramInt2 + 0.001D, d19, d1, d6);
/* 2450 */           localblz.a(d13, paramInt2 + 0.999D, d19, d1, d5);
/*      */         }
/*      */         else
/*      */         {
/* 2452 */           localblz.a(d17, paramInt2 + 0.999D, d19, d2, d5);
/* 2453 */           localblz.a(d17, paramInt2 + 0.001D, d19, d2, d6);
/* 2454 */           localblz.a(d13, paramInt2 + 0.001D, d19, d1, d6);
/* 2455 */           localblz.a(d13, paramInt2 + 0.999D, d19, d1, d5);
/*      */           
/* 2457 */           localblz.a(d14, paramInt2 + 0.999D, d19, d4, d5);
/* 2458 */           localblz.a(d14, paramInt2 + 0.001D, d19, d4, d6);
/* 2459 */           localblz.a(d18, paramInt2 + 0.001D, d19, d3, d6);
/* 2460 */           localblz.a(d18, paramInt2 + 0.999D, d19, d3, d5);
/*      */         }
/* 2462 */         if (!bool3)
/*      */         {
/* 2463 */           localblz.a(d13, paramInt2 + 0.999D, d20, d1, d5);
/* 2464 */           localblz.a(d13, paramInt2 + 0.001D, d20, d1, d6);
/* 2465 */           localblz.a(d14, paramInt2 + 0.001D, d20, d4, d6);
/* 2466 */           localblz.a(d14, paramInt2 + 0.999D, d20, d4, d5);
/*      */         }
/*      */         else
/*      */         {
/* 2468 */           localblz.a(d13, paramInt2 + 0.999D, d20, d1, d5);
/* 2469 */           localblz.a(d13, paramInt2 + 0.001D, d20, d1, d6);
/* 2470 */           localblz.a(d17, paramInt2 + 0.001D, d20, d2, d6);
/* 2471 */           localblz.a(d17, paramInt2 + 0.999D, d20, d2, d5);
/*      */           
/* 2473 */           localblz.a(d18, paramInt2 + 0.999D, d20, d3, d5);
/* 2474 */           localblz.a(d18, paramInt2 + 0.001D, d20, d3, d6);
/* 2475 */           localblz.a(d14, paramInt2 + 0.001D, d20, d4, d6);
/* 2476 */           localblz.a(d14, paramInt2 + 0.999D, d20, d4, d5);
/*      */         }
/* 2479 */         localblz.a(d13, paramInt2 + 0.999D, d20, d8, d9);
/* 2480 */         localblz.a(d14, paramInt2 + 0.999D, d20, d8, d10);
/* 2481 */         localblz.a(d14, paramInt2 + 0.999D, d19, d7, d10);
/* 2482 */         localblz.a(d13, paramInt2 + 0.999D, d19, d7, d9);
/*      */         
/* 2484 */         localblz.a(d14, paramInt2 + 0.001D, d20, d7, d10);
/* 2485 */         localblz.a(d13, paramInt2 + 0.001D, d20, d7, d9);
/* 2486 */         localblz.a(d13, paramInt2 + 0.001D, d19, d8, d9);
/* 2487 */         localblz.a(d14, paramInt2 + 0.001D, d19, d8, d10);
/*      */       }
/*      */       else
/*      */       {
/* 2489 */         if ((!bool2) && (i4 == 0))
/*      */         {
/* 2490 */           localblz.a(d18, paramInt2 + 0.999D, d19, d3, d5);
/* 2491 */           localblz.a(d18, paramInt2 + 0.001D, d19, d3, d6);
/* 2492 */           localblz.a(d13, paramInt2 + 0.001D, d19, d1, d6);
/* 2493 */           localblz.a(d13, paramInt2 + 0.999D, d19, d1, d5);
/*      */         }
/*      */         else
/*      */         {
/* 2495 */           localblz.a(d17, paramInt2 + 0.999D, d19, d2, d5);
/* 2496 */           localblz.a(d17, paramInt2 + 0.001D, d19, d2, d6);
/* 2497 */           localblz.a(d13, paramInt2 + 0.001D, d19, d1, d6);
/* 2498 */           localblz.a(d13, paramInt2 + 0.999D, d19, d1, d5);
/*      */         }
/* 2500 */         if ((!bool3) && (i4 == 0))
/*      */         {
/* 2501 */           localblz.a(d13, paramInt2 + 0.999D, d20, d1, d5);
/* 2502 */           localblz.a(d13, paramInt2 + 0.001D, d20, d1, d6);
/* 2503 */           localblz.a(d18, paramInt2 + 0.001D, d20, d3, d6);
/* 2504 */           localblz.a(d18, paramInt2 + 0.999D, d20, d3, d5);
/*      */         }
/*      */         else
/*      */         {
/* 2506 */           localblz.a(d13, paramInt2 + 0.999D, d20, d1, d5);
/* 2507 */           localblz.a(d13, paramInt2 + 0.001D, d20, d1, d6);
/* 2508 */           localblz.a(d17, paramInt2 + 0.001D, d20, d2, d6);
/* 2509 */           localblz.a(d17, paramInt2 + 0.999D, d20, d2, d5);
/*      */         }
/* 2512 */         localblz.a(d13, paramInt2 + 0.999D, d20, d8, d9);
/* 2513 */         localblz.a(d17, paramInt2 + 0.999D, d20, d8, d11);
/* 2514 */         localblz.a(d17, paramInt2 + 0.999D, d19, d7, d11);
/* 2515 */         localblz.a(d13, paramInt2 + 0.999D, d19, d7, d9);
/*      */         
/* 2517 */         localblz.a(d17, paramInt2 + 0.001D, d20, d7, d11);
/* 2518 */         localblz.a(d13, paramInt2 + 0.001D, d20, d7, d9);
/* 2519 */         localblz.a(d13, paramInt2 + 0.001D, d19, d8, d9);
/* 2520 */         localblz.a(d17, paramInt2 + 0.001D, d19, d8, d11);
/*      */       }
/*      */     }
/* 2522 */     else if ((!bool2) && (!bool3))
/*      */     {
/* 2523 */       localblz.a(d17, paramInt2 + 0.999D, d19, d2, d5);
/* 2524 */       localblz.a(d17, paramInt2 + 0.001D, d19, d2, d6);
/* 2525 */       localblz.a(d17, paramInt2 + 0.001D, d20, d3, d6);
/* 2526 */       localblz.a(d17, paramInt2 + 0.999D, d20, d3, d5);
/*      */     }
/* 2529 */     if (((bool5) || (i4 != 0)) && (!bool4))
/*      */     {
/* 2530 */       if ((!bool3) && (i4 == 0))
/*      */       {
/* 2531 */         localblz.a(d17, paramInt2 + 0.999D, d20, d2, d5);
/* 2532 */         localblz.a(d17, paramInt2 + 0.001D, d20, d2, d6);
/* 2533 */         localblz.a(d14, paramInt2 + 0.001D, d20, d4, d6);
/* 2534 */         localblz.a(d14, paramInt2 + 0.999D, d20, d4, d5);
/*      */       }
/*      */       else
/*      */       {
/* 2536 */         localblz.a(d18, paramInt2 + 0.999D, d20, d3, d5);
/* 2537 */         localblz.a(d18, paramInt2 + 0.001D, d20, d3, d6);
/* 2538 */         localblz.a(d14, paramInt2 + 0.001D, d20, d4, d6);
/* 2539 */         localblz.a(d14, paramInt2 + 0.999D, d20, d4, d5);
/*      */       }
/* 2541 */       if ((!bool2) && (i4 == 0))
/*      */       {
/* 2542 */         localblz.a(d14, paramInt2 + 0.999D, d19, d4, d5);
/* 2543 */         localblz.a(d14, paramInt2 + 0.001D, d19, d4, d6);
/* 2544 */         localblz.a(d17, paramInt2 + 0.001D, d19, d2, d6);
/* 2545 */         localblz.a(d17, paramInt2 + 0.999D, d19, d2, d5);
/*      */       }
/*      */       else
/*      */       {
/* 2547 */         localblz.a(d14, paramInt2 + 0.999D, d19, d4, d5);
/* 2548 */         localblz.a(d14, paramInt2 + 0.001D, d19, d4, d6);
/* 2549 */         localblz.a(d18, paramInt2 + 0.001D, d19, d3, d6);
/* 2550 */         localblz.a(d18, paramInt2 + 0.999D, d19, d3, d5);
/*      */       }
/* 2553 */       localblz.a(d18, paramInt2 + 0.999D, d20, d8, d12);
/* 2554 */       localblz.a(d14, paramInt2 + 0.999D, d20, d8, d9);
/* 2555 */       localblz.a(d14, paramInt2 + 0.999D, d19, d7, d9);
/* 2556 */       localblz.a(d18, paramInt2 + 0.999D, d19, d7, d12);
/*      */       
/* 2558 */       localblz.a(d14, paramInt2 + 0.001D, d20, d7, d10);
/* 2559 */       localblz.a(d18, paramInt2 + 0.001D, d20, d7, d12);
/* 2560 */       localblz.a(d18, paramInt2 + 0.001D, d19, d8, d12);
/* 2561 */       localblz.a(d14, paramInt2 + 0.001D, d19, d8, d10);
/*      */     }
/* 2562 */     else if ((!bool5) && (!bool2) && (!bool3))
/*      */     {
/* 2563 */       localblz.a(d18, paramInt2 + 0.999D, d20, d2, d5);
/* 2564 */       localblz.a(d18, paramInt2 + 0.001D, d20, d2, d6);
/* 2565 */       localblz.a(d18, paramInt2 + 0.001D, d19, d3, d6);
/* 2566 */       localblz.a(d18, paramInt2 + 0.999D, d19, d3, d5);
/*      */     }
/* 2569 */     if ((bool2) || (i4 != 0))
/*      */     {
/* 2570 */       if ((bool2) && (bool3))
/*      */       {
/* 2571 */         if (!bool4)
/*      */         {
/* 2572 */           localblz.a(d17, paramInt2 + 0.999D, d15, d1, d5);
/* 2573 */           localblz.a(d17, paramInt2 + 0.001D, d15, d1, d6);
/* 2574 */           localblz.a(d17, paramInt2 + 0.001D, d16, d4, d6);
/* 2575 */           localblz.a(d17, paramInt2 + 0.999D, d16, d4, d5);
/*      */         }
/*      */         else
/*      */         {
/* 2577 */           localblz.a(d17, paramInt2 + 0.999D, d15, d1, d5);
/* 2578 */           localblz.a(d17, paramInt2 + 0.001D, d15, d1, d6);
/* 2579 */           localblz.a(d17, paramInt2 + 0.001D, d19, d2, d6);
/* 2580 */           localblz.a(d17, paramInt2 + 0.999D, d19, d2, d5);
/*      */           
/* 2582 */           localblz.a(d17, paramInt2 + 0.999D, d20, d3, d5);
/* 2583 */           localblz.a(d17, paramInt2 + 0.001D, d20, d3, d6);
/* 2584 */           localblz.a(d17, paramInt2 + 0.001D, d16, d4, d6);
/* 2585 */           localblz.a(d17, paramInt2 + 0.999D, d16, d4, d5);
/*      */         }
/* 2587 */         if (!bool5)
/*      */         {
/* 2588 */           localblz.a(d18, paramInt2 + 0.999D, d16, d4, d5);
/* 2589 */           localblz.a(d18, paramInt2 + 0.001D, d16, d4, d6);
/* 2590 */           localblz.a(d18, paramInt2 + 0.001D, d15, d1, d6);
/* 2591 */           localblz.a(d18, paramInt2 + 0.999D, d15, d1, d5);
/*      */         }
/*      */         else
/*      */         {
/* 2593 */           localblz.a(d18, paramInt2 + 0.999D, d19, d2, d5);
/* 2594 */           localblz.a(d18, paramInt2 + 0.001D, d19, d2, d6);
/* 2595 */           localblz.a(d18, paramInt2 + 0.001D, d15, d1, d6);
/* 2596 */           localblz.a(d18, paramInt2 + 0.999D, d15, d1, d5);
/*      */           
/* 2598 */           localblz.a(d18, paramInt2 + 0.999D, d16, d4, d5);
/* 2599 */           localblz.a(d18, paramInt2 + 0.001D, d16, d4, d6);
/* 2600 */           localblz.a(d18, paramInt2 + 0.001D, d20, d3, d6);
/* 2601 */           localblz.a(d18, paramInt2 + 0.999D, d20, d3, d5);
/*      */         }
/* 2604 */         localblz.a(d18, paramInt2 + 0.999D, d15, d8, d9);
/* 2605 */         localblz.a(d17, paramInt2 + 0.999D, d15, d7, d9);
/* 2606 */         localblz.a(d17, paramInt2 + 0.999D, d16, d7, d10);
/* 2607 */         localblz.a(d18, paramInt2 + 0.999D, d16, d8, d10);
/*      */         
/* 2609 */         localblz.a(d17, paramInt2 + 0.001D, d15, d7, d9);
/* 2610 */         localblz.a(d18, paramInt2 + 0.001D, d15, d8, d9);
/* 2611 */         localblz.a(d18, paramInt2 + 0.001D, d16, d8, d10);
/* 2612 */         localblz.a(d17, paramInt2 + 0.001D, d16, d7, d10);
/*      */       }
/*      */       else
/*      */       {
/* 2614 */         if ((!bool4) && (i4 == 0))
/*      */         {
/* 2615 */           localblz.a(d17, paramInt2 + 0.999D, d15, d1, d5);
/* 2616 */           localblz.a(d17, paramInt2 + 0.001D, d15, d1, d6);
/* 2617 */           localblz.a(d17, paramInt2 + 0.001D, d20, d3, d6);
/* 2618 */           localblz.a(d17, paramInt2 + 0.999D, d20, d3, d5);
/*      */         }
/*      */         else
/*      */         {
/* 2620 */           localblz.a(d17, paramInt2 + 0.999D, d15, d1, d5);
/* 2621 */           localblz.a(d17, paramInt2 + 0.001D, d15, d1, d6);
/* 2622 */           localblz.a(d17, paramInt2 + 0.001D, d19, d2, d6);
/* 2623 */           localblz.a(d17, paramInt2 + 0.999D, d19, d2, d5);
/*      */         }
/* 2625 */         if ((!bool5) && (i4 == 0))
/*      */         {
/* 2626 */           localblz.a(d18, paramInt2 + 0.999D, d20, d3, d5);
/* 2627 */           localblz.a(d18, paramInt2 + 0.001D, d20, d3, d6);
/* 2628 */           localblz.a(d18, paramInt2 + 0.001D, d15, d1, d6);
/* 2629 */           localblz.a(d18, paramInt2 + 0.999D, d15, d1, d5);
/*      */         }
/*      */         else
/*      */         {
/* 2631 */           localblz.a(d18, paramInt2 + 0.999D, d19, d2, d5);
/* 2632 */           localblz.a(d18, paramInt2 + 0.001D, d19, d2, d6);
/* 2633 */           localblz.a(d18, paramInt2 + 0.001D, d15, d1, d6);
/* 2634 */           localblz.a(d18, paramInt2 + 0.999D, d15, d1, d5);
/*      */         }
/* 2637 */         localblz.a(d18, paramInt2 + 0.999D, d15, d8, d9);
/* 2638 */         localblz.a(d17, paramInt2 + 0.999D, d15, d7, d9);
/* 2639 */         localblz.a(d17, paramInt2 + 0.999D, d19, d7, d11);
/* 2640 */         localblz.a(d18, paramInt2 + 0.999D, d19, d8, d11);
/*      */         
/* 2642 */         localblz.a(d17, paramInt2 + 0.001D, d15, d7, d9);
/* 2643 */         localblz.a(d18, paramInt2 + 0.001D, d15, d8, d9);
/* 2644 */         localblz.a(d18, paramInt2 + 0.001D, d19, d8, d11);
/* 2645 */         localblz.a(d17, paramInt2 + 0.001D, d19, d7, d11);
/*      */       }
/*      */     }
/* 2647 */     else if ((!bool5) && (!bool4))
/*      */     {
/* 2648 */       localblz.a(d18, paramInt2 + 0.999D, d19, d3, d5);
/* 2649 */       localblz.a(d18, paramInt2 + 0.001D, d19, d3, d6);
/* 2650 */       localblz.a(d17, paramInt2 + 0.001D, d19, d2, d6);
/* 2651 */       localblz.a(d17, paramInt2 + 0.999D, d19, d2, d5);
/*      */     }
/* 2654 */     if (((bool3) || (i4 != 0)) && (!bool2))
/*      */     {
/* 2655 */       if ((!bool4) && (i4 == 0))
/*      */       {
/* 2656 */         localblz.a(d17, paramInt2 + 0.999D, d19, d2, d5);
/* 2657 */         localblz.a(d17, paramInt2 + 0.001D, d19, d2, d6);
/* 2658 */         localblz.a(d17, paramInt2 + 0.001D, d16, d4, d6);
/* 2659 */         localblz.a(d17, paramInt2 + 0.999D, d16, d4, d5);
/*      */       }
/*      */       else
/*      */       {
/* 2661 */         localblz.a(d17, paramInt2 + 0.999D, d20, d3, d5);
/* 2662 */         localblz.a(d17, paramInt2 + 0.001D, d20, d3, d6);
/* 2663 */         localblz.a(d17, paramInt2 + 0.001D, d16, d4, d6);
/* 2664 */         localblz.a(d17, paramInt2 + 0.999D, d16, d4, d5);
/*      */       }
/* 2666 */       if ((!bool5) && (i4 == 0))
/*      */       {
/* 2667 */         localblz.a(d18, paramInt2 + 0.999D, d16, d4, d5);
/* 2668 */         localblz.a(d18, paramInt2 + 0.001D, d16, d4, d6);
/* 2669 */         localblz.a(d18, paramInt2 + 0.001D, d19, d2, d6);
/* 2670 */         localblz.a(d18, paramInt2 + 0.999D, d19, d2, d5);
/*      */       }
/*      */       else
/*      */       {
/* 2672 */         localblz.a(d18, paramInt2 + 0.999D, d16, d4, d5);
/* 2673 */         localblz.a(d18, paramInt2 + 0.001D, d16, d4, d6);
/* 2674 */         localblz.a(d18, paramInt2 + 0.001D, d20, d3, d6);
/* 2675 */         localblz.a(d18, paramInt2 + 0.999D, d20, d3, d5);
/*      */       }
/* 2678 */       localblz.a(d18, paramInt2 + 0.999D, d20, d8, d12);
/* 2679 */       localblz.a(d17, paramInt2 + 0.999D, d20, d7, d12);
/* 2680 */       localblz.a(d17, paramInt2 + 0.999D, d16, d7, d10);
/* 2681 */       localblz.a(d18, paramInt2 + 0.999D, d16, d8, d10);
/*      */       
/* 2683 */       localblz.a(d17, paramInt2 + 0.001D, d20, d7, d12);
/* 2684 */       localblz.a(d18, paramInt2 + 0.001D, d20, d8, d12);
/* 2685 */       localblz.a(d18, paramInt2 + 0.001D, d16, d8, d10);
/* 2686 */       localblz.a(d17, paramInt2 + 0.001D, d16, d7, d10);
/*      */     }
/* 2687 */     else if ((!bool3) && (!bool5) && (!bool4))
/*      */     {
/* 2688 */       localblz.a(d17, paramInt2 + 0.999D, d20, d2, d5);
/* 2689 */       localblz.a(d17, paramInt2 + 0.001D, d20, d2, d6);
/* 2690 */       localblz.a(d18, paramInt2 + 0.001D, d20, d3, d6);
/* 2691 */       localblz.a(d18, paramInt2 + 0.999D, d20, d3, d5);
/*      */     }
/* 2694 */     localblz.a(d18, paramInt2 + 0.999D, d19, d8, d11);
/* 2695 */     localblz.a(d17, paramInt2 + 0.999D, d19, d7, d11);
/* 2696 */     localblz.a(d17, paramInt2 + 0.999D, d20, d7, d12);
/* 2697 */     localblz.a(d18, paramInt2 + 0.999D, d20, d8, d12);
/*      */     
/* 2699 */     localblz.a(d17, paramInt2 + 0.001D, d19, d7, d11);
/* 2700 */     localblz.a(d18, paramInt2 + 0.001D, d19, d8, d11);
/* 2701 */     localblz.a(d18, paramInt2 + 0.001D, d20, d8, d12);
/* 2702 */     localblz.a(d17, paramInt2 + 0.001D, d20, d7, d12);
/* 2704 */     if (i4 != 0)
/*      */     {
/* 2705 */       localblz.a(d13, paramInt2 + 0.999D, d19, d2, d5);
/* 2706 */       localblz.a(d13, paramInt2 + 0.001D, d19, d2, d6);
/* 2707 */       localblz.a(d13, paramInt2 + 0.001D, d20, d3, d6);
/* 2708 */       localblz.a(d13, paramInt2 + 0.999D, d20, d3, d5);
/*      */       
/* 2710 */       localblz.a(d14, paramInt2 + 0.999D, d20, d2, d5);
/* 2711 */       localblz.a(d14, paramInt2 + 0.001D, d20, d2, d6);
/* 2712 */       localblz.a(d14, paramInt2 + 0.001D, d19, d3, d6);
/* 2713 */       localblz.a(d14, paramInt2 + 0.999D, d19, d3, d5);
/*      */       
/* 2715 */       localblz.a(d18, paramInt2 + 0.999D, d15, d3, d5);
/* 2716 */       localblz.a(d18, paramInt2 + 0.001D, d15, d3, d6);
/* 2717 */       localblz.a(d17, paramInt2 + 0.001D, d15, d2, d6);
/* 2718 */       localblz.a(d17, paramInt2 + 0.999D, d15, d2, d5);
/*      */       
/* 2720 */       localblz.a(d17, paramInt2 + 0.999D, d16, d2, d5);
/* 2721 */       localblz.a(d17, paramInt2 + 0.001D, d16, d2, d6);
/* 2722 */       localblz.a(d18, paramInt2 + 0.001D, d16, d3, d6);
/* 2723 */       localblz.a(d18, paramInt2 + 0.999D, d16, d3, d5);
/*      */     }
/* 2725 */     return true;
/*      */   }
/*      */   
/*      */   public boolean a(amm paramamm, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 2729 */     int i1 = this.a.Q();
/* 2730 */     blz localblz = blz.a;
/*      */     
/* 2732 */     localblz.b(paramamm.c(this.a, paramInt1, paramInt2, paramInt3));
/* 2733 */     int i2 = paramamm.d(this.a, paramInt1, paramInt2, paramInt3);
/* 2734 */     float f1 = (i2 >> 16 & 0xFF) / 255.0F;
/* 2735 */     float f2 = (i2 >> 8 & 0xFF) / 255.0F;
/* 2736 */     float f3 = (i2 & 0xFF) / 255.0F;
/* 2738 */     if (bll.a)
/*      */     {
/* 2739 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/* 2740 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/* 2741 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/* 2743 */       f1 = f4;
/* 2744 */       f2 = f5;
/* 2745 */       f3 = f6;
/*      */     }
/* 2747 */     localblz.b(f1, f2, f3);
/*      */     ps localps1;
/*      */     ps localps2;
/* 2752 */     if (b())
/*      */     {
/* 2753 */       localps1 = this.d;
/* 2754 */       localps2 = this.d;
/*      */     }
/*      */     else
/*      */     {
/* 2756 */       int i3 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 2757 */       localps1 = a(paramamm, 0, i3);
/* 2758 */       localps2 = paramamm.e();
/*      */     }
/* 2761 */     double d1 = localps1.c();
/* 2762 */     double d2 = localps1.a(8.0D);
/* 2763 */     double d3 = localps1.d();
/* 2764 */     double d4 = localps1.e();
/* 2765 */     double d5 = localps1.f();
/*      */     
/* 2767 */     double d6 = localps2.a(7.0D);
/* 2768 */     double d7 = localps2.a(9.0D);
/* 2769 */     double d8 = localps2.e();
/* 2770 */     double d9 = localps2.b(8.0D);
/* 2771 */     double d10 = localps2.f();
/*      */     
/* 2773 */     double d11 = paramInt1;
/* 2774 */     double d12 = paramInt1 + 0.5D;
/* 2775 */     double d13 = paramInt1 + 1;
/* 2776 */     double d14 = paramInt3;
/* 2777 */     double d15 = paramInt3 + 0.5D;
/* 2778 */     double d16 = paramInt3 + 1;
/* 2779 */     double d17 = paramInt1 + 0.5D - 0.0625D;
/* 2780 */     double d18 = paramInt1 + 0.5D + 0.0625D;
/* 2781 */     double d19 = paramInt3 + 0.5D - 0.0625D;
/* 2782 */     double d20 = paramInt3 + 0.5D + 0.0625D;
/*      */     
/* 2784 */     boolean bool1 = paramamm.a(this.a.a(paramInt1, paramInt2, paramInt3 - 1));
/* 2785 */     boolean bool2 = paramamm.a(this.a.a(paramInt1, paramInt2, paramInt3 + 1));
/* 2786 */     boolean bool3 = paramamm.a(this.a.a(paramInt1 - 1, paramInt2, paramInt3));
/* 2787 */     boolean bool4 = paramamm.a(this.a.a(paramInt1 + 1, paramInt2, paramInt3));
/*      */     
/* 2789 */     boolean bool5 = paramamm.a(this.a, paramInt1, paramInt2 + 1, paramInt3, 1);
/* 2790 */     boolean bool6 = paramamm.a(this.a, paramInt1, paramInt2 - 1, paramInt3, 0);
/*      */     
/* 2792 */     double d21 = 0.01D;
/* 2793 */     double d22 = 0.005D;
/* 2795 */     if (((bool3) && (bool4)) || ((!bool3) && (!bool4) && (!bool1) && (!bool2)))
/*      */     {
/* 2796 */       localblz.a(d11, paramInt2 + 1, d15, d1, d4);
/* 2797 */       localblz.a(d11, paramInt2 + 0, d15, d1, d5);
/* 2798 */       localblz.a(d13, paramInt2 + 0, d15, d3, d5);
/* 2799 */       localblz.a(d13, paramInt2 + 1, d15, d3, d4);
/*      */       
/* 2801 */       localblz.a(d13, paramInt2 + 1, d15, d1, d4);
/* 2802 */       localblz.a(d13, paramInt2 + 0, d15, d1, d5);
/* 2803 */       localblz.a(d11, paramInt2 + 0, d15, d3, d5);
/* 2804 */       localblz.a(d11, paramInt2 + 1, d15, d3, d4);
/* 2806 */       if (bool5)
/*      */       {
/* 2808 */         localblz.a(d11, paramInt2 + 1 + 0.01D, d20, d7, d10);
/* 2809 */         localblz.a(d13, paramInt2 + 1 + 0.01D, d20, d7, d8);
/* 2810 */         localblz.a(d13, paramInt2 + 1 + 0.01D, d19, d6, d8);
/* 2811 */         localblz.a(d11, paramInt2 + 1 + 0.01D, d19, d6, d10);
/*      */         
/* 2813 */         localblz.a(d13, paramInt2 + 1 + 0.01D, d20, d7, d10);
/* 2814 */         localblz.a(d11, paramInt2 + 1 + 0.01D, d20, d7, d8);
/* 2815 */         localblz.a(d11, paramInt2 + 1 + 0.01D, d19, d6, d8);
/* 2816 */         localblz.a(d13, paramInt2 + 1 + 0.01D, d19, d6, d10);
/*      */       }
/*      */       else
/*      */       {
/* 2818 */         if ((paramInt2 < i1 - 1) && (this.a.c(paramInt1 - 1, paramInt2 + 1, paramInt3)))
/*      */         {
/* 2819 */           localblz.a(d11, paramInt2 + 1 + 0.01D, d20, d7, d9);
/* 2820 */           localblz.a(d12, paramInt2 + 1 + 0.01D, d20, d7, d10);
/* 2821 */           localblz.a(d12, paramInt2 + 1 + 0.01D, d19, d6, d10);
/* 2822 */           localblz.a(d11, paramInt2 + 1 + 0.01D, d19, d6, d9);
/*      */           
/* 2824 */           localblz.a(d12, paramInt2 + 1 + 0.01D, d20, d7, d9);
/* 2825 */           localblz.a(d11, paramInt2 + 1 + 0.01D, d20, d7, d10);
/* 2826 */           localblz.a(d11, paramInt2 + 1 + 0.01D, d19, d6, d10);
/* 2827 */           localblz.a(d12, paramInt2 + 1 + 0.01D, d19, d6, d9);
/*      */         }
/* 2829 */         if ((paramInt2 < i1 - 1) && (this.a.c(paramInt1 + 1, paramInt2 + 1, paramInt3)))
/*      */         {
/* 2830 */           localblz.a(d12, paramInt2 + 1 + 0.01D, d20, d7, d8);
/* 2831 */           localblz.a(d13, paramInt2 + 1 + 0.01D, d20, d7, d9);
/* 2832 */           localblz.a(d13, paramInt2 + 1 + 0.01D, d19, d6, d9);
/* 2833 */           localblz.a(d12, paramInt2 + 1 + 0.01D, d19, d6, d8);
/*      */           
/* 2835 */           localblz.a(d13, paramInt2 + 1 + 0.01D, d20, d7, d8);
/* 2836 */           localblz.a(d12, paramInt2 + 1 + 0.01D, d20, d7, d9);
/* 2837 */           localblz.a(d12, paramInt2 + 1 + 0.01D, d19, d6, d9);
/* 2838 */           localblz.a(d13, paramInt2 + 1 + 0.01D, d19, d6, d8);
/*      */         }
/*      */       }
/* 2841 */       if (bool6)
/*      */       {
/* 2843 */         localblz.a(d11, paramInt2 - 0.01D, d20, d7, d10);
/* 2844 */         localblz.a(d13, paramInt2 - 0.01D, d20, d7, d8);
/* 2845 */         localblz.a(d13, paramInt2 - 0.01D, d19, d6, d8);
/* 2846 */         localblz.a(d11, paramInt2 - 0.01D, d19, d6, d10);
/*      */         
/* 2848 */         localblz.a(d13, paramInt2 - 0.01D, d20, d7, d10);
/* 2849 */         localblz.a(d11, paramInt2 - 0.01D, d20, d7, d8);
/* 2850 */         localblz.a(d11, paramInt2 - 0.01D, d19, d6, d8);
/* 2851 */         localblz.a(d13, paramInt2 - 0.01D, d19, d6, d10);
/*      */       }
/*      */       else
/*      */       {
/* 2853 */         if ((paramInt2 > 1) && (this.a.c(paramInt1 - 1, paramInt2 - 1, paramInt3)))
/*      */         {
/* 2854 */           localblz.a(d11, paramInt2 - 0.01D, d20, d7, d9);
/* 2855 */           localblz.a(d12, paramInt2 - 0.01D, d20, d7, d10);
/* 2856 */           localblz.a(d12, paramInt2 - 0.01D, d19, d6, d10);
/* 2857 */           localblz.a(d11, paramInt2 - 0.01D, d19, d6, d9);
/*      */           
/* 2859 */           localblz.a(d12, paramInt2 - 0.01D, d20, d7, d9);
/* 2860 */           localblz.a(d11, paramInt2 - 0.01D, d20, d7, d10);
/* 2861 */           localblz.a(d11, paramInt2 - 0.01D, d19, d6, d10);
/* 2862 */           localblz.a(d12, paramInt2 - 0.01D, d19, d6, d9);
/*      */         }
/* 2864 */         if ((paramInt2 > 1) && (this.a.c(paramInt1 + 1, paramInt2 - 1, paramInt3)))
/*      */         {
/* 2865 */           localblz.a(d12, paramInt2 - 0.01D, d20, d7, d8);
/* 2866 */           localblz.a(d13, paramInt2 - 0.01D, d20, d7, d9);
/* 2867 */           localblz.a(d13, paramInt2 - 0.01D, d19, d6, d9);
/* 2868 */           localblz.a(d12, paramInt2 - 0.01D, d19, d6, d8);
/*      */           
/* 2870 */           localblz.a(d13, paramInt2 - 0.01D, d20, d7, d8);
/* 2871 */           localblz.a(d12, paramInt2 - 0.01D, d20, d7, d9);
/* 2872 */           localblz.a(d12, paramInt2 - 0.01D, d19, d6, d9);
/* 2873 */           localblz.a(d13, paramInt2 - 0.01D, d19, d6, d8);
/*      */         }
/*      */       }
/*      */     }
/* 2877 */     else if ((bool3) && (!bool4))
/*      */     {
/* 2879 */       localblz.a(d11, paramInt2 + 1, d15, d1, d4);
/* 2880 */       localblz.a(d11, paramInt2 + 0, d15, d1, d5);
/* 2881 */       localblz.a(d12, paramInt2 + 0, d15, d2, d5);
/* 2882 */       localblz.a(d12, paramInt2 + 1, d15, d2, d4);
/*      */       
/* 2884 */       localblz.a(d12, paramInt2 + 1, d15, d1, d4);
/* 2885 */       localblz.a(d12, paramInt2 + 0, d15, d1, d5);
/* 2886 */       localblz.a(d11, paramInt2 + 0, d15, d2, d5);
/* 2887 */       localblz.a(d11, paramInt2 + 1, d15, d2, d4);
/* 2890 */       if ((!bool2) && (!bool1))
/*      */       {
/* 2891 */         localblz.a(d12, paramInt2 + 1, d20, d6, d8);
/* 2892 */         localblz.a(d12, paramInt2 + 0, d20, d6, d10);
/* 2893 */         localblz.a(d12, paramInt2 + 0, d19, d7, d10);
/* 2894 */         localblz.a(d12, paramInt2 + 1, d19, d7, d8);
/*      */         
/* 2896 */         localblz.a(d12, paramInt2 + 1, d19, d6, d8);
/* 2897 */         localblz.a(d12, paramInt2 + 0, d19, d6, d10);
/* 2898 */         localblz.a(d12, paramInt2 + 0, d20, d7, d10);
/* 2899 */         localblz.a(d12, paramInt2 + 1, d20, d7, d8);
/*      */       }
/* 2902 */       if ((bool5) || ((paramInt2 < i1 - 1) && (this.a.c(paramInt1 - 1, paramInt2 + 1, paramInt3))))
/*      */       {
/* 2904 */         localblz.a(d11, paramInt2 + 1 + 0.01D, d20, d7, d9);
/* 2905 */         localblz.a(d12, paramInt2 + 1 + 0.01D, d20, d7, d10);
/* 2906 */         localblz.a(d12, paramInt2 + 1 + 0.01D, d19, d6, d10);
/* 2907 */         localblz.a(d11, paramInt2 + 1 + 0.01D, d19, d6, d9);
/*      */         
/* 2909 */         localblz.a(d12, paramInt2 + 1 + 0.01D, d20, d7, d9);
/* 2910 */         localblz.a(d11, paramInt2 + 1 + 0.01D, d20, d7, d10);
/* 2911 */         localblz.a(d11, paramInt2 + 1 + 0.01D, d19, d6, d10);
/* 2912 */         localblz.a(d12, paramInt2 + 1 + 0.01D, d19, d6, d9);
/*      */       }
/* 2914 */       if ((bool6) || ((paramInt2 > 1) && (this.a.c(paramInt1 - 1, paramInt2 - 1, paramInt3))))
/*      */       {
/* 2916 */         localblz.a(d11, paramInt2 - 0.01D, d20, d7, d9);
/* 2917 */         localblz.a(d12, paramInt2 - 0.01D, d20, d7, d10);
/* 2918 */         localblz.a(d12, paramInt2 - 0.01D, d19, d6, d10);
/* 2919 */         localblz.a(d11, paramInt2 - 0.01D, d19, d6, d9);
/*      */         
/* 2921 */         localblz.a(d12, paramInt2 - 0.01D, d20, d7, d9);
/* 2922 */         localblz.a(d11, paramInt2 - 0.01D, d20, d7, d10);
/* 2923 */         localblz.a(d11, paramInt2 - 0.01D, d19, d6, d10);
/* 2924 */         localblz.a(d12, paramInt2 - 0.01D, d19, d6, d9);
/*      */       }
/*      */     }
/* 2927 */     else if ((!bool3) && (bool4))
/*      */     {
/* 2929 */       localblz.a(d12, paramInt2 + 1, d15, d2, d4);
/* 2930 */       localblz.a(d12, paramInt2 + 0, d15, d2, d5);
/* 2931 */       localblz.a(d13, paramInt2 + 0, d15, d3, d5);
/* 2932 */       localblz.a(d13, paramInt2 + 1, d15, d3, d4);
/*      */       
/* 2934 */       localblz.a(d13, paramInt2 + 1, d15, d2, d4);
/* 2935 */       localblz.a(d13, paramInt2 + 0, d15, d2, d5);
/* 2936 */       localblz.a(d12, paramInt2 + 0, d15, d3, d5);
/* 2937 */       localblz.a(d12, paramInt2 + 1, d15, d3, d4);
/* 2940 */       if ((!bool2) && (!bool1))
/*      */       {
/* 2941 */         localblz.a(d12, paramInt2 + 1, d19, d6, d8);
/* 2942 */         localblz.a(d12, paramInt2 + 0, d19, d6, d10);
/* 2943 */         localblz.a(d12, paramInt2 + 0, d20, d7, d10);
/* 2944 */         localblz.a(d12, paramInt2 + 1, d20, d7, d8);
/*      */         
/* 2946 */         localblz.a(d12, paramInt2 + 1, d20, d6, d8);
/* 2947 */         localblz.a(d12, paramInt2 + 0, d20, d6, d10);
/* 2948 */         localblz.a(d12, paramInt2 + 0, d19, d7, d10);
/* 2949 */         localblz.a(d12, paramInt2 + 1, d19, d7, d8);
/*      */       }
/* 2952 */       if ((bool5) || ((paramInt2 < i1 - 1) && (this.a.c(paramInt1 + 1, paramInt2 + 1, paramInt3))))
/*      */       {
/* 2954 */         localblz.a(d12, paramInt2 + 1 + 0.01D, d20, d7, d8);
/* 2955 */         localblz.a(d13, paramInt2 + 1 + 0.01D, d20, d7, d9);
/* 2956 */         localblz.a(d13, paramInt2 + 1 + 0.01D, d19, d6, d9);
/* 2957 */         localblz.a(d12, paramInt2 + 1 + 0.01D, d19, d6, d8);
/*      */         
/* 2959 */         localblz.a(d13, paramInt2 + 1 + 0.01D, d20, d7, d8);
/* 2960 */         localblz.a(d12, paramInt2 + 1 + 0.01D, d20, d7, d9);
/* 2961 */         localblz.a(d12, paramInt2 + 1 + 0.01D, d19, d6, d9);
/* 2962 */         localblz.a(d13, paramInt2 + 1 + 0.01D, d19, d6, d8);
/*      */       }
/* 2964 */       if ((bool6) || ((paramInt2 > 1) && (this.a.c(paramInt1 + 1, paramInt2 - 1, paramInt3))))
/*      */       {
/* 2966 */         localblz.a(d12, paramInt2 - 0.01D, d20, d7, d8);
/* 2967 */         localblz.a(d13, paramInt2 - 0.01D, d20, d7, d9);
/* 2968 */         localblz.a(d13, paramInt2 - 0.01D, d19, d6, d9);
/* 2969 */         localblz.a(d12, paramInt2 - 0.01D, d19, d6, d8);
/*      */         
/* 2971 */         localblz.a(d13, paramInt2 - 0.01D, d20, d7, d8);
/* 2972 */         localblz.a(d12, paramInt2 - 0.01D, d20, d7, d9);
/* 2973 */         localblz.a(d12, paramInt2 - 0.01D, d19, d6, d9);
/* 2974 */         localblz.a(d13, paramInt2 - 0.01D, d19, d6, d8);
/*      */       }
/*      */     }
/* 2979 */     if (((bool1) && (bool2)) || ((!bool3) && (!bool4) && (!bool1) && (!bool2)))
/*      */     {
/* 2981 */       localblz.a(d12, paramInt2 + 1, d16, d1, d4);
/* 2982 */       localblz.a(d12, paramInt2 + 0, d16, d1, d5);
/* 2983 */       localblz.a(d12, paramInt2 + 0, d14, d3, d5);
/* 2984 */       localblz.a(d12, paramInt2 + 1, d14, d3, d4);
/*      */       
/* 2986 */       localblz.a(d12, paramInt2 + 1, d14, d1, d4);
/* 2987 */       localblz.a(d12, paramInt2 + 0, d14, d1, d5);
/* 2988 */       localblz.a(d12, paramInt2 + 0, d16, d3, d5);
/* 2989 */       localblz.a(d12, paramInt2 + 1, d16, d3, d4);
/* 2991 */       if (bool5)
/*      */       {
/* 2993 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d16, d7, d10);
/* 2994 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d14, d7, d8);
/* 2995 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d14, d6, d8);
/* 2996 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d16, d6, d10);
/*      */         
/* 2998 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d14, d7, d10);
/* 2999 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d16, d7, d8);
/* 3000 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d16, d6, d8);
/* 3001 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d14, d6, d10);
/*      */       }
/*      */       else
/*      */       {
/* 3003 */         if ((paramInt2 < i1 - 1) && (this.a.c(paramInt1, paramInt2 + 1, paramInt3 - 1)))
/*      */         {
/* 3004 */           localblz.a(d17, paramInt2 + 1 + 0.005D, d14, d7, d8);
/* 3005 */           localblz.a(d17, paramInt2 + 1 + 0.005D, d15, d7, d9);
/* 3006 */           localblz.a(d18, paramInt2 + 1 + 0.005D, d15, d6, d9);
/* 3007 */           localblz.a(d18, paramInt2 + 1 + 0.005D, d14, d6, d8);
/*      */           
/* 3009 */           localblz.a(d17, paramInt2 + 1 + 0.005D, d15, d7, d8);
/* 3010 */           localblz.a(d17, paramInt2 + 1 + 0.005D, d14, d7, d9);
/* 3011 */           localblz.a(d18, paramInt2 + 1 + 0.005D, d14, d6, d9);
/* 3012 */           localblz.a(d18, paramInt2 + 1 + 0.005D, d15, d6, d8);
/*      */         }
/* 3014 */         if ((paramInt2 < i1 - 1) && (this.a.c(paramInt1, paramInt2 + 1, paramInt3 + 1)))
/*      */         {
/* 3015 */           localblz.a(d17, paramInt2 + 1 + 0.005D, d15, d6, d9);
/* 3016 */           localblz.a(d17, paramInt2 + 1 + 0.005D, d16, d6, d10);
/* 3017 */           localblz.a(d18, paramInt2 + 1 + 0.005D, d16, d7, d10);
/* 3018 */           localblz.a(d18, paramInt2 + 1 + 0.005D, d15, d7, d9);
/*      */           
/* 3020 */           localblz.a(d17, paramInt2 + 1 + 0.005D, d16, d6, d9);
/* 3021 */           localblz.a(d17, paramInt2 + 1 + 0.005D, d15, d6, d10);
/* 3022 */           localblz.a(d18, paramInt2 + 1 + 0.005D, d15, d7, d10);
/* 3023 */           localblz.a(d18, paramInt2 + 1 + 0.005D, d16, d7, d9);
/*      */         }
/*      */       }
/* 3026 */       if (bool6)
/*      */       {
/* 3028 */         localblz.a(d18, paramInt2 - 0.005D, d16, d7, d10);
/* 3029 */         localblz.a(d18, paramInt2 - 0.005D, d14, d7, d8);
/* 3030 */         localblz.a(d17, paramInt2 - 0.005D, d14, d6, d8);
/* 3031 */         localblz.a(d17, paramInt2 - 0.005D, d16, d6, d10);
/*      */         
/* 3033 */         localblz.a(d18, paramInt2 - 0.005D, d14, d7, d10);
/* 3034 */         localblz.a(d18, paramInt2 - 0.005D, d16, d7, d8);
/* 3035 */         localblz.a(d17, paramInt2 - 0.005D, d16, d6, d8);
/* 3036 */         localblz.a(d17, paramInt2 - 0.005D, d14, d6, d10);
/*      */       }
/*      */       else
/*      */       {
/* 3038 */         if ((paramInt2 > 1) && (this.a.c(paramInt1, paramInt2 - 1, paramInt3 - 1)))
/*      */         {
/* 3040 */           localblz.a(d17, paramInt2 - 0.005D, d14, d7, d8);
/* 3041 */           localblz.a(d17, paramInt2 - 0.005D, d15, d7, d9);
/* 3042 */           localblz.a(d18, paramInt2 - 0.005D, d15, d6, d9);
/* 3043 */           localblz.a(d18, paramInt2 - 0.005D, d14, d6, d8);
/*      */           
/* 3045 */           localblz.a(d17, paramInt2 - 0.005D, d15, d7, d8);
/* 3046 */           localblz.a(d17, paramInt2 - 0.005D, d14, d7, d9);
/* 3047 */           localblz.a(d18, paramInt2 - 0.005D, d14, d6, d9);
/* 3048 */           localblz.a(d18, paramInt2 - 0.005D, d15, d6, d8);
/*      */         }
/* 3050 */         if ((paramInt2 > 1) && (this.a.c(paramInt1, paramInt2 - 1, paramInt3 + 1)))
/*      */         {
/* 3052 */           localblz.a(d17, paramInt2 - 0.005D, d15, d6, d9);
/* 3053 */           localblz.a(d17, paramInt2 - 0.005D, d16, d6, d10);
/* 3054 */           localblz.a(d18, paramInt2 - 0.005D, d16, d7, d10);
/* 3055 */           localblz.a(d18, paramInt2 - 0.005D, d15, d7, d9);
/*      */           
/* 3057 */           localblz.a(d17, paramInt2 - 0.005D, d16, d6, d9);
/* 3058 */           localblz.a(d17, paramInt2 - 0.005D, d15, d6, d10);
/* 3059 */           localblz.a(d18, paramInt2 - 0.005D, d15, d7, d10);
/* 3060 */           localblz.a(d18, paramInt2 - 0.005D, d16, d7, d9);
/*      */         }
/*      */       }
/*      */     }
/* 3064 */     else if ((bool1) && (!bool2))
/*      */     {
/* 3066 */       localblz.a(d12, paramInt2 + 1, d14, d1, d4);
/* 3067 */       localblz.a(d12, paramInt2 + 0, d14, d1, d5);
/* 3068 */       localblz.a(d12, paramInt2 + 0, d15, d2, d5);
/* 3069 */       localblz.a(d12, paramInt2 + 1, d15, d2, d4);
/*      */       
/* 3071 */       localblz.a(d12, paramInt2 + 1, d15, d1, d4);
/* 3072 */       localblz.a(d12, paramInt2 + 0, d15, d1, d5);
/* 3073 */       localblz.a(d12, paramInt2 + 0, d14, d2, d5);
/* 3074 */       localblz.a(d12, paramInt2 + 1, d14, d2, d4);
/* 3077 */       if ((!bool4) && (!bool3))
/*      */       {
/* 3078 */         localblz.a(d17, paramInt2 + 1, d15, d6, d8);
/* 3079 */         localblz.a(d17, paramInt2 + 0, d15, d6, d10);
/* 3080 */         localblz.a(d18, paramInt2 + 0, d15, d7, d10);
/* 3081 */         localblz.a(d18, paramInt2 + 1, d15, d7, d8);
/*      */         
/* 3083 */         localblz.a(d18, paramInt2 + 1, d15, d6, d8);
/* 3084 */         localblz.a(d18, paramInt2 + 0, d15, d6, d10);
/* 3085 */         localblz.a(d17, paramInt2 + 0, d15, d7, d10);
/* 3086 */         localblz.a(d17, paramInt2 + 1, d15, d7, d8);
/*      */       }
/* 3089 */       if ((bool5) || ((paramInt2 < i1 - 1) && (this.a.c(paramInt1, paramInt2 + 1, paramInt3 - 1))))
/*      */       {
/* 3091 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d14, d7, d8);
/* 3092 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d15, d7, d9);
/* 3093 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d15, d6, d9);
/* 3094 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d14, d6, d8);
/*      */         
/* 3096 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d15, d7, d8);
/* 3097 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d14, d7, d9);
/* 3098 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d14, d6, d9);
/* 3099 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d15, d6, d8);
/*      */       }
/* 3102 */       if ((bool6) || ((paramInt2 > 1) && (this.a.c(paramInt1, paramInt2 - 1, paramInt3 - 1))))
/*      */       {
/* 3104 */         localblz.a(d17, paramInt2 - 0.005D, d14, d7, d8);
/* 3105 */         localblz.a(d17, paramInt2 - 0.005D, d15, d7, d9);
/* 3106 */         localblz.a(d18, paramInt2 - 0.005D, d15, d6, d9);
/* 3107 */         localblz.a(d18, paramInt2 - 0.005D, d14, d6, d8);
/*      */         
/* 3109 */         localblz.a(d17, paramInt2 - 0.005D, d15, d7, d8);
/* 3110 */         localblz.a(d17, paramInt2 - 0.005D, d14, d7, d9);
/* 3111 */         localblz.a(d18, paramInt2 - 0.005D, d14, d6, d9);
/* 3112 */         localblz.a(d18, paramInt2 - 0.005D, d15, d6, d8);
/*      */       }
/*      */     }
/* 3115 */     else if ((!bool1) && (bool2))
/*      */     {
/* 3117 */       localblz.a(d12, paramInt2 + 1, d15, d2, d4);
/* 3118 */       localblz.a(d12, paramInt2 + 0, d15, d2, d5);
/* 3119 */       localblz.a(d12, paramInt2 + 0, d16, d3, d5);
/* 3120 */       localblz.a(d12, paramInt2 + 1, d16, d3, d4);
/*      */       
/* 3122 */       localblz.a(d12, paramInt2 + 1, d16, d2, d4);
/* 3123 */       localblz.a(d12, paramInt2 + 0, d16, d2, d5);
/* 3124 */       localblz.a(d12, paramInt2 + 0, d15, d3, d5);
/* 3125 */       localblz.a(d12, paramInt2 + 1, d15, d3, d4);
/* 3128 */       if ((!bool4) && (!bool3))
/*      */       {
/* 3129 */         localblz.a(d18, paramInt2 + 1, d15, d6, d8);
/* 3130 */         localblz.a(d18, paramInt2 + 0, d15, d6, d10);
/* 3131 */         localblz.a(d17, paramInt2 + 0, d15, d7, d10);
/* 3132 */         localblz.a(d17, paramInt2 + 1, d15, d7, d8);
/*      */         
/* 3134 */         localblz.a(d17, paramInt2 + 1, d15, d6, d8);
/* 3135 */         localblz.a(d17, paramInt2 + 0, d15, d6, d10);
/* 3136 */         localblz.a(d18, paramInt2 + 0, d15, d7, d10);
/* 3137 */         localblz.a(d18, paramInt2 + 1, d15, d7, d8);
/*      */       }
/* 3140 */       if ((bool5) || ((paramInt2 < i1 - 1) && (this.a.c(paramInt1, paramInt2 + 1, paramInt3 + 1))))
/*      */       {
/* 3142 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d15, d6, d9);
/* 3143 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d16, d6, d10);
/* 3144 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d16, d7, d10);
/* 3145 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d15, d7, d9);
/*      */         
/* 3147 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d16, d6, d9);
/* 3148 */         localblz.a(d17, paramInt2 + 1 + 0.005D, d15, d6, d10);
/* 3149 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d15, d7, d10);
/* 3150 */         localblz.a(d18, paramInt2 + 1 + 0.005D, d16, d7, d9);
/*      */       }
/* 3152 */       if ((bool6) || ((paramInt2 > 1) && (this.a.c(paramInt1, paramInt2 - 1, paramInt3 + 1))))
/*      */       {
/* 3154 */         localblz.a(d17, paramInt2 - 0.005D, d15, d6, d9);
/* 3155 */         localblz.a(d17, paramInt2 - 0.005D, d16, d6, d10);
/* 3156 */         localblz.a(d18, paramInt2 - 0.005D, d16, d7, d10);
/* 3157 */         localblz.a(d18, paramInt2 - 0.005D, d15, d7, d9);
/*      */         
/* 3159 */         localblz.a(d17, paramInt2 - 0.005D, d16, d6, d9);
/* 3160 */         localblz.a(d17, paramInt2 - 0.005D, d15, d6, d10);
/* 3161 */         localblz.a(d18, paramInt2 - 0.005D, d15, d7, d10);
/* 3162 */         localblz.a(d18, paramInt2 - 0.005D, d16, d7, d9);
/*      */       }
/*      */     }
/* 3166 */     return true;
/*      */   }
/*      */   
/*      */   public boolean l(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 3170 */     blz localblz = blz.a;
/*      */     
/* 3172 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/* 3173 */     int i1 = paramahu.d(this.a, paramInt1, paramInt2, paramInt3);
/* 3174 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/* 3175 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/* 3176 */     float f3 = (i1 & 0xFF) / 255.0F;
/* 3178 */     if (bll.a)
/*      */     {
/* 3179 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/* 3180 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/* 3181 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/* 3183 */       f1 = f4;
/* 3184 */       f2 = f5;
/* 3185 */       f3 = f6;
/*      */     }
/* 3187 */     localblz.b(f1, f2, f3);
/*      */     
/* 3189 */     double d1 = paramInt1;
/* 3190 */     double d2 = paramInt2;
/* 3191 */     double d3 = paramInt3;
/*      */     long l1;
/* 3193 */     if (paramahu == ahz.H)
/*      */     {
/* 3194 */       l1 = paramInt1 * 3129871 ^ paramInt3 * 116129781L ^ paramInt2;
/* 3195 */       l1 = l1 * l1 * 42317861L + l1 * 11L;
/*      */       
/* 3197 */       d1 += ((float)(l1 >> 16 & 0xF) / 15.0F - 0.5D) * 0.5D;
/* 3198 */       d2 += ((float)(l1 >> 20 & 0xF) / 15.0F - 1.0D) * 0.2D;
/* 3199 */       d3 += ((float)(l1 >> 24 & 0xF) / 15.0F - 0.5D) * 0.5D;
/*      */     }
/* 3200 */     else if ((paramahu == ahz.O) || (paramahu == ahz.N))
/*      */     {
/* 3201 */       l1 = paramInt1 * 3129871 ^ paramInt3 * 116129781L ^ paramInt2;
/* 3202 */       l1 = l1 * l1 * 42317861L + l1 * 11L;
/*      */       
/* 3204 */       d1 += ((float)(l1 >> 16 & 0xF) / 15.0F - 0.5D) * 0.3D;
/* 3205 */       d3 += ((float)(l1 >> 24 & 0xF) / 15.0F - 0.5D) * 0.3D;
/*      */     }
/* 3208 */     ps localps = a(paramahu, 0, this.a.e(paramInt1, paramInt2, paramInt3));
/* 3209 */     a(localps, d1, d2, d3, 1.0F);
/* 3210 */     return true;
/*      */   }
/*      */   
/*      */   public boolean a(aja paramaja, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 3214 */     blz localblz = blz.a;
/*      */     
/* 3216 */     localblz.b(paramaja.c(this.a, paramInt1, paramInt2, paramInt3));
/* 3217 */     int i1 = paramaja.d(this.a, paramInt1, paramInt2, paramInt3);
/* 3218 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/* 3219 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/* 3220 */     float f3 = (i1 & 0xFF) / 255.0F;
/* 3222 */     if (bll.a)
/*      */     {
/* 3223 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/* 3224 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/* 3225 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/* 3227 */       f1 = f4;
/* 3228 */       f2 = f5;
/* 3229 */       f3 = f6;
/*      */     }
/* 3231 */     localblz.b(f1, f2, f3);
/*      */     
/* 3233 */     long l1 = paramInt1 * 3129871 ^ paramInt3 * 116129781L;
/* 3234 */     l1 = l1 * l1 * 42317861L + l1 * 11L;
/*      */     
/* 3236 */     double d1 = paramInt1;
/* 3237 */     double d2 = paramInt2;
/* 3238 */     double d3 = paramInt3;
/*      */     
/* 3240 */     d1 += ((float)(l1 >> 16 & 0xF) / 15.0F - 0.5D) * 0.3D;
/* 3241 */     d3 += ((float)(l1 >> 24 & 0xF) / 15.0F - 0.5D) * 0.3D;
/*      */     
/*      */ 
/* 3244 */     int i2 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 3245 */     int i3 = 0;
/* 3246 */     boolean bool = aja.c(i2);
/* 3247 */     if (bool)
/*      */     {
/* 3248 */       if (this.a.a(paramInt1, paramInt2 - 1, paramInt3) != paramaja) {
/* 3249 */         return false;
/*      */       }
/* 3251 */       i3 = aja.d(this.a.e(paramInt1, paramInt2 - 1, paramInt3));
/*      */     }
/*      */     else
/*      */     {
/* 3253 */       i3 = aja.d(i2);
/*      */     }
/* 3255 */     ps localps1 = paramaja.a(bool, i3);
/*      */     
/* 3257 */     a(localps1, d1, d2, d3, 1.0F);
/* 3259 */     if ((bool) && (i3 == 0))
/*      */     {
/* 3261 */       ps localps2 = paramaja.b[0];
/*      */       
/* 3263 */       double d4 = Math.cos(l1 * 0.8D) * 3.141592653589793D * 0.1D;
/* 3264 */       double d5 = Math.cos(d4);
/* 3265 */       double d6 = Math.sin(d4);
/*      */       
/* 3267 */       double d7 = localps2.c();
/* 3268 */       double d8 = localps2.e();
/* 3269 */       double d9 = localps2.d();
/* 3270 */       double d10 = localps2.f();
/*      */       
/*      */ 
/* 3273 */       double d11 = 0.3D;
/* 3274 */       double d12 = -0.05D;
/* 3275 */       double d13 = 0.5D + 0.3D * d5 - 0.5D * d6;
/* 3276 */       double d14 = 0.5D + 0.5D * d5 + 0.3D * d6;
/* 3277 */       double d15 = 0.5D + 0.3D * d5 + 0.5D * d6;
/* 3278 */       double d16 = 0.5D + -0.5D * d5 + 0.3D * d6;
/* 3279 */       double d17 = 0.5D + -0.05D * d5 + 0.5D * d6;
/* 3280 */       double d18 = 0.5D + -0.5D * d5 + -0.05D * d6;
/* 3281 */       double d19 = 0.5D + -0.05D * d5 - 0.5D * d6;
/* 3282 */       double d20 = 0.5D + 0.5D * d5 + -0.05D * d6;
/*      */       
/*      */ 
/*      */ 
/* 3286 */       localblz.a(d1 + d17, d2 + 1.0D, d3 + d18, d7, d10);
/* 3287 */       localblz.a(d1 + d19, d2 + 1.0D, d3 + d20, d9, d10);
/* 3288 */       localblz.a(d1 + d13, d2 + 0.0D, d3 + d14, d9, d8);
/* 3289 */       localblz.a(d1 + d15, d2 + 0.0D, d3 + d16, d7, d8);
/*      */       
/* 3291 */       ps localps3 = paramaja.b[1];
/*      */       
/* 3293 */       d7 = localps3.c();
/* 3294 */       d8 = localps3.e();
/* 3295 */       d9 = localps3.d();
/* 3296 */       d10 = localps3.f();
/*      */       
/* 3298 */       localblz.a(d1 + d19, d2 + 1.0D, d3 + d20, d7, d10);
/* 3299 */       localblz.a(d1 + d17, d2 + 1.0D, d3 + d18, d9, d10);
/* 3300 */       localblz.a(d1 + d15, d2 + 0.0D, d3 + d16, d9, d8);
/* 3301 */       localblz.a(d1 + d13, d2 + 0.0D, d3 + d14, d7, d8);
/*      */     }
/* 3304 */     return true;
/*      */   }
/*      */   
/*      */   public boolean m(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 3308 */     amg localamg = (amg)paramahu;
/* 3309 */     blz localblz = blz.a;
/*      */     
/* 3311 */     localblz.b(localamg.c(this.a, paramInt1, paramInt2, paramInt3));
/* 3312 */     int i1 = localamg.d(this.a, paramInt1, paramInt2, paramInt3);
/* 3313 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/* 3314 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/* 3315 */     float f3 = (i1 & 0xFF) / 255.0F;
/* 3317 */     if (bll.a)
/*      */     {
/* 3318 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/* 3319 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/* 3320 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/* 3322 */       f1 = f4;
/* 3323 */       f2 = f5;
/* 3324 */       f3 = f6;
/*      */     }
/* 3326 */     localblz.b(f1, f2, f3);
/*      */     
/* 3328 */     localamg.a(this.a, paramInt1, paramInt2, paramInt3);
/* 3329 */     int i2 = localamg.e(this.a, paramInt1, paramInt2, paramInt3);
/* 3330 */     if (i2 < 0)
/*      */     {
/* 3331 */       a(localamg, this.a.e(paramInt1, paramInt2, paramInt3), this.k, paramInt1, paramInt2 - 0.0625F, paramInt3);
/*      */     }
/*      */     else
/*      */     {
/* 3333 */       a(localamg, this.a.e(paramInt1, paramInt2, paramInt3), 0.5D, paramInt1, paramInt2 - 0.0625F, paramInt3);
/* 3334 */       a(localamg, this.a.e(paramInt1, paramInt2, paramInt3), i2, this.k, paramInt1, paramInt2 - 0.0625F, paramInt3);
/*      */     }
/* 3336 */     return true;
/*      */   }
/*      */   
/*      */   public boolean n(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 3340 */     blz localblz = blz.a;
/*      */     
/* 3342 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/* 3343 */     localblz.b(1.0F, 1.0F, 1.0F);
/*      */     
/* 3345 */     a(paramahu, this.a.e(paramInt1, paramInt2, paramInt3), paramInt1, paramInt2 - 0.0625F, paramInt3);
/* 3346 */     return true;
/*      */   }
/*      */   
/*      */   public void a(ahu paramahu, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, int paramInt)
/*      */   {
/* 3350 */     blz localblz = blz.a;
/* 3351 */     ps localps = a(paramahu, 0, paramInt);
/* 3353 */     if (b()) {
/* 3353 */       localps = this.d;
/*      */     }
/* 3354 */     double d1 = localps.c();
/* 3355 */     double d2 = localps.e();
/* 3356 */     double d3 = localps.d();
/* 3357 */     double d4 = localps.f();
/*      */     
/* 3359 */     double d5 = localps.a(7.0D);
/* 3360 */     double d6 = localps.b(6.0D);
/* 3361 */     double d7 = localps.a(9.0D);
/* 3362 */     double d8 = localps.b(8.0D);
/*      */     
/* 3364 */     double d9 = localps.a(7.0D);
/* 3365 */     double d10 = localps.b(13.0D);
/* 3366 */     double d11 = localps.a(9.0D);
/* 3367 */     double d12 = localps.b(15.0D);
/*      */     
/* 3369 */     paramDouble1 += 0.5D;
/* 3370 */     paramDouble3 += 0.5D;
/*      */     
/* 3372 */     double d13 = paramDouble1 - 0.5D;
/* 3373 */     double d14 = paramDouble1 + 0.5D;
/* 3374 */     double d15 = paramDouble3 - 0.5D;
/* 3375 */     double d16 = paramDouble3 + 0.5D;
/* 3376 */     double d17 = 0.0625D;
/*      */     
/* 3378 */     double d18 = 0.625D;
/* 3379 */     localblz.a(paramDouble1 + paramDouble4 * (1.0D - d18) - d17, paramDouble2 + d18, paramDouble3 + paramDouble5 * (1.0D - d18) - d17, d5, d6);
/* 3380 */     localblz.a(paramDouble1 + paramDouble4 * (1.0D - d18) - d17, paramDouble2 + d18, paramDouble3 + paramDouble5 * (1.0D - d18) + d17, d5, d8);
/* 3381 */     localblz.a(paramDouble1 + paramDouble4 * (1.0D - d18) + d17, paramDouble2 + d18, paramDouble3 + paramDouble5 * (1.0D - d18) + d17, d7, d8);
/* 3382 */     localblz.a(paramDouble1 + paramDouble4 * (1.0D - d18) + d17, paramDouble2 + d18, paramDouble3 + paramDouble5 * (1.0D - d18) - d17, d7, d6);
/*      */     
/* 3384 */     localblz.a(paramDouble1 + d17 + paramDouble4, paramDouble2, paramDouble3 - d17 + paramDouble5, d11, d10);
/* 3385 */     localblz.a(paramDouble1 + d17 + paramDouble4, paramDouble2, paramDouble3 + d17 + paramDouble5, d11, d12);
/* 3386 */     localblz.a(paramDouble1 - d17 + paramDouble4, paramDouble2, paramDouble3 + d17 + paramDouble5, d9, d12);
/* 3387 */     localblz.a(paramDouble1 - d17 + paramDouble4, paramDouble2, paramDouble3 - d17 + paramDouble5, d9, d10);
/*      */     
/* 3389 */     localblz.a(paramDouble1 - d17, paramDouble2 + 1.0D, d15, d1, d2);
/* 3390 */     localblz.a(paramDouble1 - d17 + paramDouble4, paramDouble2 + 0.0D, d15 + paramDouble5, d1, d4);
/* 3391 */     localblz.a(paramDouble1 - d17 + paramDouble4, paramDouble2 + 0.0D, d16 + paramDouble5, d3, d4);
/* 3392 */     localblz.a(paramDouble1 - d17, paramDouble2 + 1.0D, d16, d3, d2);
/*      */     
/* 3394 */     localblz.a(paramDouble1 + d17, paramDouble2 + 1.0D, d16, d1, d2);
/* 3395 */     localblz.a(paramDouble1 + paramDouble4 + d17, paramDouble2 + 0.0D, d16 + paramDouble5, d1, d4);
/* 3396 */     localblz.a(paramDouble1 + paramDouble4 + d17, paramDouble2 + 0.0D, d15 + paramDouble5, d3, d4);
/* 3397 */     localblz.a(paramDouble1 + d17, paramDouble2 + 1.0D, d15, d3, d2);
/*      */     
/* 3399 */     localblz.a(d13, paramDouble2 + 1.0D, paramDouble3 + d17, d1, d2);
/* 3400 */     localblz.a(d13 + paramDouble4, paramDouble2 + 0.0D, paramDouble3 + d17 + paramDouble5, d1, d4);
/* 3401 */     localblz.a(d14 + paramDouble4, paramDouble2 + 0.0D, paramDouble3 + d17 + paramDouble5, d3, d4);
/* 3402 */     localblz.a(d14, paramDouble2 + 1.0D, paramDouble3 + d17, d3, d2);
/*      */     
/* 3404 */     localblz.a(d14, paramDouble2 + 1.0D, paramDouble3 - d17, d1, d2);
/* 3405 */     localblz.a(d14 + paramDouble4, paramDouble2 + 0.0D, paramDouble3 - d17 + paramDouble5, d1, d4);
/* 3406 */     localblz.a(d13 + paramDouble4, paramDouble2 + 0.0D, paramDouble3 - d17 + paramDouble5, d3, d4);
/* 3407 */     localblz.a(d13, paramDouble2 + 1.0D, paramDouble3 - d17, d3, d2);
/*      */   }
/*      */   
/*      */   public void a(ps paramps, double paramDouble1, double paramDouble2, double paramDouble3, float paramFloat)
/*      */   {
/* 3411 */     blz localblz = blz.a;
/* 3413 */     if (b()) {
/* 3413 */       paramps = this.d;
/*      */     }
/* 3414 */     double d1 = paramps.c();
/* 3415 */     double d2 = paramps.e();
/* 3416 */     double d3 = paramps.d();
/* 3417 */     double d4 = paramps.f();
/*      */     
/* 3419 */     double d5 = 0.45D * paramFloat;
/* 3420 */     double d6 = paramDouble1 + 0.5D - d5;
/* 3421 */     double d7 = paramDouble1 + 0.5D + d5;
/* 3422 */     double d8 = paramDouble3 + 0.5D - d5;
/* 3423 */     double d9 = paramDouble3 + 0.5D + d5;
/*      */     
/* 3425 */     localblz.a(d6, paramDouble2 + paramFloat, d8, d1, d2);
/* 3426 */     localblz.a(d6, paramDouble2 + 0.0D, d8, d1, d4);
/* 3427 */     localblz.a(d7, paramDouble2 + 0.0D, d9, d3, d4);
/* 3428 */     localblz.a(d7, paramDouble2 + paramFloat, d9, d3, d2);
/*      */     
/* 3430 */     localblz.a(d7, paramDouble2 + paramFloat, d9, d1, d2);
/* 3431 */     localblz.a(d7, paramDouble2 + 0.0D, d9, d1, d4);
/* 3432 */     localblz.a(d6, paramDouble2 + 0.0D, d8, d3, d4);
/* 3433 */     localblz.a(d6, paramDouble2 + paramFloat, d8, d3, d2);
/*      */     
/* 3435 */     localblz.a(d6, paramDouble2 + paramFloat, d9, d1, d2);
/* 3436 */     localblz.a(d6, paramDouble2 + 0.0D, d9, d1, d4);
/* 3437 */     localblz.a(d7, paramDouble2 + 0.0D, d8, d3, d4);
/* 3438 */     localblz.a(d7, paramDouble2 + paramFloat, d8, d3, d2);
/*      */     
/* 3440 */     localblz.a(d7, paramDouble2 + paramFloat, d8, d1, d2);
/* 3441 */     localblz.a(d7, paramDouble2 + 0.0D, d8, d1, d4);
/* 3442 */     localblz.a(d6, paramDouble2 + 0.0D, d9, d3, d4);
/* 3443 */     localblz.a(d6, paramDouble2 + paramFloat, d9, d3, d2);
/*      */   }
/*      */   
/*      */   public void a(ahu paramahu, int paramInt, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
/*      */   {
/* 3447 */     blz localblz = blz.a;
/*      */     
/* 3449 */     ps localps = a(paramahu, 0, paramInt);
/* 3451 */     if (b()) {
/* 3451 */       localps = this.d;
/*      */     }
/* 3452 */     double d1 = localps.c();
/* 3453 */     double d2 = localps.e();
/* 3454 */     double d3 = localps.d();
/* 3455 */     double d4 = localps.b(paramDouble1 * 16.0D);
/*      */     
/* 3457 */     double d5 = paramDouble2 + 0.5D - 0.449999988079071D;
/* 3458 */     double d6 = paramDouble2 + 0.5D + 0.449999988079071D;
/* 3459 */     double d7 = paramDouble4 + 0.5D - 0.449999988079071D;
/* 3460 */     double d8 = paramDouble4 + 0.5D + 0.449999988079071D;
/*      */     
/* 3462 */     localblz.a(d5, paramDouble3 + paramDouble1, d7, d1, d2);
/* 3463 */     localblz.a(d5, paramDouble3 + 0.0D, d7, d1, d4);
/* 3464 */     localblz.a(d6, paramDouble3 + 0.0D, d8, d3, d4);
/* 3465 */     localblz.a(d6, paramDouble3 + paramDouble1, d8, d3, d2);
/*      */     
/* 3467 */     localblz.a(d6, paramDouble3 + paramDouble1, d8, d3, d2);
/* 3468 */     localblz.a(d6, paramDouble3 + 0.0D, d8, d3, d4);
/* 3469 */     localblz.a(d5, paramDouble3 + 0.0D, d7, d1, d4);
/* 3470 */     localblz.a(d5, paramDouble3 + paramDouble1, d7, d1, d2);
/*      */     
/* 3472 */     localblz.a(d5, paramDouble3 + paramDouble1, d8, d1, d2);
/* 3473 */     localblz.a(d5, paramDouble3 + 0.0D, d8, d1, d4);
/* 3474 */     localblz.a(d6, paramDouble3 + 0.0D, d7, d3, d4);
/* 3475 */     localblz.a(d6, paramDouble3 + paramDouble1, d7, d3, d2);
/*      */     
/* 3477 */     localblz.a(d6, paramDouble3 + paramDouble1, d7, d3, d2);
/* 3478 */     localblz.a(d6, paramDouble3 + 0.0D, d7, d3, d4);
/* 3479 */     localblz.a(d5, paramDouble3 + 0.0D, d8, d1, d4);
/* 3480 */     localblz.a(d5, paramDouble3 + paramDouble1, d8, d1, d2);
/*      */   }
/*      */   
/*      */   public boolean o(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 3484 */     blz localblz = blz.a;
/*      */     
/* 3486 */     ps localps = a(paramahu, 1);
/* 3488 */     if (b()) {
/* 3488 */       localps = this.d;
/*      */     }
/* 3489 */     float f1 = 0.015625F;
/*      */     
/* 3491 */     double d1 = localps.c();
/* 3492 */     double d2 = localps.e();
/* 3493 */     double d3 = localps.d();
/* 3494 */     double d4 = localps.f();
/*      */     
/* 3496 */     long l1 = paramInt1 * 3129871 ^ paramInt3 * 116129781L ^ paramInt2;
/* 3497 */     l1 = l1 * l1 * 42317861L + l1 * 11L;
/*      */     
/* 3499 */     int i1 = (int)(l1 >> 16 & 0x3);
/*      */     
/* 3501 */     localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/*      */     
/* 3503 */     float f2 = paramInt1 + 0.5F;
/* 3504 */     float f3 = paramInt3 + 0.5F;
/* 3505 */     float f4 = (i1 & 0x1) * 0.5F * (1 - i1 / 2 % 2 * 2);
/* 3506 */     float f5 = (i1 + 1 & 0x1) * 0.5F * (1 - (i1 + 1) / 2 % 2 * 2);
/*      */     
/* 3508 */     localblz.c(paramahu.D());
/* 3509 */     localblz.a(f2 + f4 - f5, paramInt2 + f1, f3 + f4 + f5, d1, d2);
/* 3510 */     localblz.a(f2 + f4 + f5, paramInt2 + f1, f3 - f4 + f5, d3, d2);
/* 3511 */     localblz.a(f2 - f4 + f5, paramInt2 + f1, f3 - f4 - f5, d3, d4);
/* 3512 */     localblz.a(f2 - f4 - f5, paramInt2 + f1, f3 + f4 - f5, d1, d4);
/*      */     
/* 3514 */     localblz.c((paramahu.D() & 0xFEFEFE) >> 1);
/* 3515 */     localblz.a(f2 - f4 - f5, paramInt2 + f1, f3 + f4 - f5, d1, d4);
/* 3516 */     localblz.a(f2 - f4 + f5, paramInt2 + f1, f3 - f4 - f5, d3, d4);
/* 3517 */     localblz.a(f2 + f4 + f5, paramInt2 + f1, f3 - f4 + f5, d3, d2);
/* 3518 */     localblz.a(f2 + f4 - f5, paramInt2 + f1, f3 + f4 + f5, d1, d2);
/*      */     
/* 3520 */     return true;
/*      */   }
/*      */   
/*      */   public void a(amg paramamg, int paramInt1, int paramInt2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
/*      */   {
/* 3524 */     blz localblz = blz.a;
/*      */     
/* 3526 */     ps localps = paramamg.i();
/* 3528 */     if (b()) {
/* 3528 */       localps = this.d;
/*      */     }
/* 3529 */     double d1 = localps.c();
/* 3530 */     double d2 = localps.e();
/* 3531 */     double d3 = localps.d();
/* 3532 */     double d4 = localps.f();
/*      */     
/* 3534 */     double d5 = paramDouble2 + 0.5D - 0.5D;
/* 3535 */     double d6 = paramDouble2 + 0.5D + 0.5D;
/* 3536 */     double d7 = paramDouble4 + 0.5D - 0.5D;
/* 3537 */     double d8 = paramDouble4 + 0.5D + 0.5D;
/*      */     
/* 3539 */     double d9 = paramDouble2 + 0.5D;
/* 3540 */     double d10 = paramDouble4 + 0.5D;
/* 3542 */     if ((paramInt2 + 1) / 2 % 2 == 1)
/*      */     {
/* 3543 */       double d11 = d3;
/* 3544 */       d3 = d1;
/* 3545 */       d1 = d11;
/*      */     }
/* 3548 */     if (paramInt2 < 2)
/*      */     {
/* 3549 */       localblz.a(d5, paramDouble3 + paramDouble1, d10, d1, d2);
/* 3550 */       localblz.a(d5, paramDouble3 + 0.0D, d10, d1, d4);
/* 3551 */       localblz.a(d6, paramDouble3 + 0.0D, d10, d3, d4);
/* 3552 */       localblz.a(d6, paramDouble3 + paramDouble1, d10, d3, d2);
/*      */       
/* 3554 */       localblz.a(d6, paramDouble3 + paramDouble1, d10, d3, d2);
/* 3555 */       localblz.a(d6, paramDouble3 + 0.0D, d10, d3, d4);
/* 3556 */       localblz.a(d5, paramDouble3 + 0.0D, d10, d1, d4);
/* 3557 */       localblz.a(d5, paramDouble3 + paramDouble1, d10, d1, d2);
/*      */     }
/*      */     else
/*      */     {
/* 3560 */       localblz.a(d9, paramDouble3 + paramDouble1, d8, d1, d2);
/* 3561 */       localblz.a(d9, paramDouble3 + 0.0D, d8, d1, d4);
/* 3562 */       localblz.a(d9, paramDouble3 + 0.0D, d7, d3, d4);
/* 3563 */       localblz.a(d9, paramDouble3 + paramDouble1, d7, d3, d2);
/*      */       
/* 3565 */       localblz.a(d9, paramDouble3 + paramDouble1, d7, d3, d2);
/* 3566 */       localblz.a(d9, paramDouble3 + 0.0D, d7, d3, d4);
/* 3567 */       localblz.a(d9, paramDouble3 + 0.0D, d8, d1, d4);
/* 3568 */       localblz.a(d9, paramDouble3 + paramDouble1, d8, d1, d2);
/*      */     }
/*      */   }
/*      */   
/*      */   public void a(ahu paramahu, int paramInt, double paramDouble1, double paramDouble2, double paramDouble3)
/*      */   {
/* 3573 */     blz localblz = blz.a;
/*      */     
/* 3575 */     ps localps = a(paramahu, 0, paramInt);
/* 3577 */     if (b()) {
/* 3577 */       localps = this.d;
/*      */     }
/* 3578 */     double d1 = localps.c();
/* 3579 */     double d2 = localps.e();
/* 3580 */     double d3 = localps.d();
/* 3581 */     double d4 = localps.f();
/*      */     
/* 3583 */     double d5 = paramDouble1 + 0.5D - 0.25D;
/* 3584 */     double d6 = paramDouble1 + 0.5D + 0.25D;
/* 3585 */     double d7 = paramDouble3 + 0.5D - 0.5D;
/* 3586 */     double d8 = paramDouble3 + 0.5D + 0.5D;
/*      */     
/* 3588 */     localblz.a(d5, paramDouble2 + 1.0D, d7, d1, d2);
/* 3589 */     localblz.a(d5, paramDouble2 + 0.0D, d7, d1, d4);
/* 3590 */     localblz.a(d5, paramDouble2 + 0.0D, d8, d3, d4);
/* 3591 */     localblz.a(d5, paramDouble2 + 1.0D, d8, d3, d2);
/*      */     
/* 3593 */     localblz.a(d5, paramDouble2 + 1.0D, d8, d1, d2);
/* 3594 */     localblz.a(d5, paramDouble2 + 0.0D, d8, d1, d4);
/* 3595 */     localblz.a(d5, paramDouble2 + 0.0D, d7, d3, d4);
/* 3596 */     localblz.a(d5, paramDouble2 + 1.0D, d7, d3, d2);
/*      */     
/* 3598 */     localblz.a(d6, paramDouble2 + 1.0D, d8, d1, d2);
/* 3599 */     localblz.a(d6, paramDouble2 + 0.0D, d8, d1, d4);
/* 3600 */     localblz.a(d6, paramDouble2 + 0.0D, d7, d3, d4);
/* 3601 */     localblz.a(d6, paramDouble2 + 1.0D, d7, d3, d2);
/*      */     
/* 3603 */     localblz.a(d6, paramDouble2 + 1.0D, d7, d1, d2);
/* 3604 */     localblz.a(d6, paramDouble2 + 0.0D, d7, d1, d4);
/* 3605 */     localblz.a(d6, paramDouble2 + 0.0D, d8, d3, d4);
/* 3606 */     localblz.a(d6, paramDouble2 + 1.0D, d8, d3, d2);
/*      */     
/* 3608 */     d5 = paramDouble1 + 0.5D - 0.5D;
/* 3609 */     d6 = paramDouble1 + 0.5D + 0.5D;
/* 3610 */     d7 = paramDouble3 + 0.5D - 0.25D;
/* 3611 */     d8 = paramDouble3 + 0.5D + 0.25D;
/*      */     
/* 3613 */     localblz.a(d5, paramDouble2 + 1.0D, d7, d1, d2);
/* 3614 */     localblz.a(d5, paramDouble2 + 0.0D, d7, d1, d4);
/* 3615 */     localblz.a(d6, paramDouble2 + 0.0D, d7, d3, d4);
/* 3616 */     localblz.a(d6, paramDouble2 + 1.0D, d7, d3, d2);
/*      */     
/* 3618 */     localblz.a(d6, paramDouble2 + 1.0D, d7, d1, d2);
/* 3619 */     localblz.a(d6, paramDouble2 + 0.0D, d7, d1, d4);
/* 3620 */     localblz.a(d5, paramDouble2 + 0.0D, d7, d3, d4);
/* 3621 */     localblz.a(d5, paramDouble2 + 1.0D, d7, d3, d2);
/*      */     
/* 3623 */     localblz.a(d6, paramDouble2 + 1.0D, d8, d1, d2);
/* 3624 */     localblz.a(d6, paramDouble2 + 0.0D, d8, d1, d4);
/* 3625 */     localblz.a(d5, paramDouble2 + 0.0D, d8, d3, d4);
/* 3626 */     localblz.a(d5, paramDouble2 + 1.0D, d8, d3, d2);
/*      */     
/* 3628 */     localblz.a(d5, paramDouble2 + 1.0D, d8, d1, d2);
/* 3629 */     localblz.a(d5, paramDouble2 + 0.0D, d8, d1, d4);
/* 3630 */     localblz.a(d6, paramDouble2 + 0.0D, d8, d3, d4);
/* 3631 */     localblz.a(d6, paramDouble2 + 1.0D, d8, d3, d2);
/*      */   }
/*      */   
/*      */   public boolean p(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 3636 */     blz localblz = blz.a;
/*      */     
/* 3638 */     int i1 = paramahu.d(this.a, paramInt1, paramInt2, paramInt3);
/* 3639 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/* 3640 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/* 3641 */     float f3 = (i1 & 0xFF) / 255.0F;
/* 3642 */     boolean bool1 = paramahu.a(this.a, paramInt1, paramInt2 + 1, paramInt3, 1);
/* 3643 */     boolean bool2 = paramahu.a(this.a, paramInt1, paramInt2 - 1, paramInt3, 0);
/* 3644 */     boolean[] arrayOfBoolean = new boolean[4];
/* 3645 */     arrayOfBoolean[0] = paramahu.a(this.a, paramInt1, paramInt2, paramInt3 - 1, 2);
/* 3646 */     arrayOfBoolean[1] = paramahu.a(this.a, paramInt1, paramInt2, paramInt3 + 1, 3);
/* 3647 */     arrayOfBoolean[2] = paramahu.a(this.a, paramInt1 - 1, paramInt2, paramInt3, 4);
/* 3648 */     arrayOfBoolean[3] = paramahu.a(this.a, paramInt1 + 1, paramInt2, paramInt3, 5);
/* 3650 */     if ((!bool1) && (!bool2) && (arrayOfBoolean[0] == 0) && (arrayOfBoolean[1] == 0) && (arrayOfBoolean[2] == 0) && (arrayOfBoolean[3] == 0)) {
/* 3650 */       return false;
/*      */     }
/* 3652 */     boolean bool3 = false;
/* 3653 */     float f4 = 0.5F;
/* 3654 */     float f5 = 1.0F;
/* 3655 */     float f6 = 0.8F;
/* 3656 */     float f7 = 0.6F;
/*      */     
/* 3658 */     double d1 = 0.0D;
/* 3659 */     double d2 = 1.0D;
/*      */     
/* 3661 */     avf localavf = paramahu.o();
/* 3662 */     int i2 = this.a.e(paramInt1, paramInt2, paramInt3);
/*      */     
/* 3664 */     double d3 = a(paramInt1, paramInt2, paramInt3, localavf);
/* 3665 */     double d4 = a(paramInt1, paramInt2, paramInt3 + 1, localavf);
/* 3666 */     double d5 = a(paramInt1 + 1, paramInt2, paramInt3 + 1, localavf);
/* 3667 */     double d6 = a(paramInt1 + 1, paramInt2, paramInt3, localavf);
/*      */     
/* 3669 */     double d7 = 0.001000000047497451D;
/*      */     float f10;
/*      */     float f11;
/*      */     float f12;
/* 3670 */     if ((this.f) || (bool1))
/*      */     {
/* 3671 */       bool3 = true;
/* 3672 */       ps localps1 = a(paramahu, 1, i2);
/* 3673 */       float f8 = (float)aki.a(this.a, paramInt1, paramInt2, paramInt3, localavf);
/* 3674 */       if (f8 > -999.0F) {
/* 3675 */         localps1 = a(paramahu, 2, i2);
/*      */       }
/* 3678 */       d3 -= d7;
/* 3679 */       d4 -= d7;
/* 3680 */       d5 -= d7;
/* 3681 */       d6 -= d7;
/*      */       double d8;
/*      */       double d15;
/*      */       double d9;
/*      */       double d17;
/*      */       double d11;
/*      */       double d19;
/*      */       double d13;
/*      */       double d21;
/* 3686 */       if (f8 < -999.0F)
/*      */       {
/* 3687 */         d8 = localps1.a(0.0D);
/* 3688 */         d15 = localps1.b(0.0D);
/* 3689 */         d9 = d8;
/* 3690 */         d17 = localps1.b(16.0D);
/* 3691 */         d11 = localps1.a(16.0D);
/* 3692 */         d19 = d17;
/* 3693 */         d13 = d11;
/* 3694 */         d21 = d15;
/*      */       }
/*      */       else
/*      */       {
/* 3696 */         f10 = ou.a(f8) * 0.25F;
/* 3697 */         f11 = ou.b(f8) * 0.25F;
/* 3698 */         f12 = 8.0F;
/* 3699 */         d8 = localps1.a(8.0F + (-f11 - f10) * 16.0F);
/* 3700 */         d15 = localps1.b(8.0F + (-f11 + f10) * 16.0F);
/* 3701 */         d9 = localps1.a(8.0F + (-f11 + f10) * 16.0F);
/* 3702 */         d17 = localps1.b(8.0F + (f11 + f10) * 16.0F);
/* 3703 */         d11 = localps1.a(8.0F + (f11 + f10) * 16.0F);
/* 3704 */         d19 = localps1.b(8.0F + (f11 - f10) * 16.0F);
/* 3705 */         d13 = localps1.a(8.0F + (f11 - f10) * 16.0F);
/* 3706 */         d21 = localps1.b(8.0F + (-f11 - f10) * 16.0F);
/*      */       }
/* 3709 */       localblz.b(paramahu.c(this.a, paramInt1, paramInt2, paramInt3));
/* 3710 */       localblz.b(f5 * f1, f5 * f2, f5 * f3);
/* 3711 */       localblz.a(paramInt1 + 0, paramInt2 + d3, paramInt3 + 0, d8, d15);
/* 3712 */       localblz.a(paramInt1 + 0, paramInt2 + d4, paramInt3 + 1, d9, d17);
/* 3713 */       localblz.a(paramInt1 + 1, paramInt2 + d5, paramInt3 + 1, d11, d19);
/* 3714 */       localblz.a(paramInt1 + 1, paramInt2 + d6, paramInt3 + 0, d13, d21);
/*      */     }
/* 3717 */     if ((this.f) || (bool2))
/*      */     {
/* 3718 */       localblz.b(paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3));
/* 3719 */       localblz.b(f4, f4, f4);
/* 3720 */       a(paramahu, paramInt1, paramInt2 + d7, paramInt3, a(paramahu, 0));
/* 3721 */       bool3 = true;
/*      */     }
/* 3724 */     for (int i3 = 0; i3 < 4; i3++)
/*      */     {
/* 3725 */       int i4 = paramInt1;
/* 3726 */       int i5 = paramInt2;
/* 3727 */       int i6 = paramInt3;
/* 3729 */       if (i3 == 0) {
/* 3729 */         i6--;
/*      */       }
/* 3730 */       if (i3 == 1) {
/* 3730 */         i6++;
/*      */       }
/* 3731 */       if (i3 == 2) {
/* 3731 */         i4--;
/*      */       }
/* 3732 */       if (i3 == 3) {
/* 3732 */         i4++;
/*      */       }
/* 3734 */       ps localps2 = a(paramahu, i3 + 2, i2);
/* 3736 */       if ((this.f) || (arrayOfBoolean[i3] != 0))
/*      */       {
/*      */         double d10;
/*      */         double d12;
/*      */         double d14;
/*      */         double d18;
/*      */         double d16;
/*      */         double d20;
/* 3740 */         if (i3 == 0)
/*      */         {
/* 3741 */           d10 = d3;
/* 3742 */           d12 = d6;
/* 3743 */           d14 = paramInt1;
/* 3744 */           d18 = paramInt1 + 1;
/* 3745 */           d16 = paramInt3 + d7;
/* 3746 */           d20 = paramInt3 + d7;
/*      */         }
/* 3747 */         else if (i3 == 1)
/*      */         {
/* 3748 */           d10 = d5;
/* 3749 */           d12 = d4;
/* 3750 */           d14 = paramInt1 + 1;
/* 3751 */           d18 = paramInt1;
/* 3752 */           d16 = paramInt3 + 1 - d7;
/* 3753 */           d20 = paramInt3 + 1 - d7;
/*      */         }
/* 3754 */         else if (i3 == 2)
/*      */         {
/* 3755 */           d10 = d4;
/* 3756 */           d12 = d3;
/* 3757 */           d14 = paramInt1 + d7;
/* 3758 */           d18 = paramInt1 + d7;
/* 3759 */           d16 = paramInt3 + 1;
/* 3760 */           d20 = paramInt3;
/*      */         }
/*      */         else
/*      */         {
/* 3762 */           d10 = d6;
/* 3763 */           d12 = d5;
/* 3764 */           d14 = paramInt1 + 1 - d7;
/* 3765 */           d18 = paramInt1 + 1 - d7;
/* 3766 */           d16 = paramInt3;
/* 3767 */           d20 = paramInt3 + 1;
/*      */         }
/* 3770 */         bool3 = true;
/* 3771 */         float f9 = localps2.a(0.0D);
/* 3772 */         f10 = localps2.a(8.0D);
/*      */         
/* 3774 */         f11 = localps2.b((1.0D - d10) * 16.0D * 0.5D);
/* 3775 */         f12 = localps2.b((1.0D - d12) * 16.0D * 0.5D);
/* 3776 */         float f13 = localps2.b(8.0D);
/*      */         
/* 3778 */         localblz.b(paramahu.c(this.a, i4, i5, i6));
/*      */         
/* 3780 */         float f14 = 1.0F;
/* 3781 */         f14 *= (i3 < 2 ? f6 : f7);
/*      */         
/* 3783 */         localblz.b(f5 * f14 * f1, f5 * f14 * f2, f5 * f14 * f3);
/* 3784 */         localblz.a(d14, paramInt2 + d10, d16, f9, f11);
/* 3785 */         localblz.a(d18, paramInt2 + d12, d20, f10, f12);
/* 3786 */         localblz.a(d18, paramInt2 + 0, d20, f10, f13);
/* 3787 */         localblz.a(d14, paramInt2 + 0, d16, f9, f13);
/*      */       }
/*      */     }
/* 3792 */     this.j = d1;
/* 3793 */     this.k = d2;
/*      */     
/* 3795 */     return bool3;
/*      */   }
/*      */   
/*      */   private float a(int paramInt1, int paramInt2, int paramInt3, avf paramavf)
/*      */   {
/* 3799 */     int i1 = 0;
/* 3800 */     float f1 = 0.0F;
/* 3801 */     for (int i2 = 0; i2 < 4; i2++)
/*      */     {
/* 3802 */       int i3 = paramInt1 - (i2 & 0x1);
/* 3803 */       int i4 = paramInt2;
/* 3804 */       int i5 = paramInt3 - (i2 >> 1 & 0x1);
/* 3805 */       if (this.a.a(i3, i4 + 1, i5).o() == paramavf) {
/* 3806 */         return 1.0F;
/*      */       }
/* 3808 */       avf localavf = this.a.a(i3, i4, i5).o();
/* 3809 */       if (localavf == paramavf)
/*      */       {
/* 3810 */         int i6 = this.a.e(i3, i4, i5);
/* 3811 */         if ((i6 >= 8) || (i6 == 0))
/*      */         {
/* 3812 */           f1 += aki.b(i6) * 10.0F;
/* 3813 */           i1 += 10;
/*      */         }
/* 3815 */         f1 += aki.b(i6);
/* 3816 */         i1++;
/*      */       }
/* 3817 */       else if (!localavf.a())
/*      */       {
/* 3818 */         f1 += 1.0F;
/* 3819 */         i1++;
/*      */       }
/*      */     }
/* 3822 */     return 1.0F - f1 / i1;
/*      */   }
/*      */   
/*      */   public void a(ahu paramahu, afn paramafn, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
/*      */   {
/* 3826 */     float f1 = 0.5F;
/* 3827 */     float f2 = 1.0F;
/* 3828 */     float f3 = 0.8F;
/* 3829 */     float f4 = 0.6F;
/*      */     
/* 3831 */     blz localblz = blz.a;
/* 3832 */     localblz.b();
/*      */     
/* 3834 */     localblz.b(paramahu.c(paramafn, paramInt1, paramInt2, paramInt3));
/*      */     
/* 3836 */     localblz.b(f1, f1, f1);
/* 3837 */     a(paramahu, -0.5D, -0.5D, -0.5D, a(paramahu, 0, paramInt4));
/*      */     
/* 3839 */     localblz.b(f2, f2, f2);
/* 3840 */     b(paramahu, -0.5D, -0.5D, -0.5D, a(paramahu, 1, paramInt4));
/*      */     
/* 3842 */     localblz.b(f3, f3, f3);
/* 3843 */     c(paramahu, -0.5D, -0.5D, -0.5D, a(paramahu, 2, paramInt4));
/*      */     
/* 3845 */     localblz.b(f3, f3, f3);
/* 3846 */     d(paramahu, -0.5D, -0.5D, -0.5D, a(paramahu, 3, paramInt4));
/*      */     
/* 3848 */     localblz.b(f4, f4, f4);
/* 3849 */     e(paramahu, -0.5D, -0.5D, -0.5D, a(paramahu, 4, paramInt4));
/*      */     
/* 3851 */     localblz.b(f4, f4, f4);
/* 3852 */     f(paramahu, -0.5D, -0.5D, -0.5D, a(paramahu, 5, paramInt4));
/* 3853 */     localblz.a();
/*      */   }
/*      */   
/*      */   public boolean q(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 3857 */     int i1 = paramahu.d(this.a, paramInt1, paramInt2, paramInt3);
/* 3858 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/* 3859 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/* 3860 */     float f3 = (i1 & 0xFF) / 255.0F;
/* 3862 */     if (bll.a)
/*      */     {
/* 3863 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/* 3864 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/* 3865 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/* 3867 */       f1 = f4;
/* 3868 */       f2 = f5;
/* 3869 */       f3 = f6;
/*      */     }
/* 3872 */     if ((azd.x()) && (paramahu.m() == 0))
/*      */     {
/* 3873 */       if (this.o) {
/* 3874 */         return b(paramahu, paramInt1, paramInt2, paramInt3, f1, f2, f3);
/*      */       }
/* 3876 */       return a(paramahu, paramInt1, paramInt2, paramInt3, f1, f2, f3);
/*      */     }
/* 3879 */     return d(paramahu, paramInt1, paramInt2, paramInt3, f1, f2, f3);
/*      */   }
/*      */   
/*      */   public boolean r(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 3884 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 3885 */     int i2 = i1 & 0xC;
/* 3887 */     if (i2 == 4)
/*      */     {
/* 3888 */       this.q = 1;
/* 3889 */       this.r = 1;
/* 3890 */       this.u = 1;
/* 3891 */       this.v = 1;
/*      */     }
/* 3892 */     else if (i2 == 8)
/*      */     {
/* 3893 */       this.s = 1;
/* 3894 */       this.t = 1;
/*      */     }
/* 3897 */     boolean bool = q(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     
/* 3899 */     this.s = 0;
/* 3900 */     this.q = 0;
/* 3901 */     this.r = 0;
/* 3902 */     this.t = 0;
/* 3903 */     this.u = 0;
/* 3904 */     this.v = 0;
/*      */     
/* 3906 */     return bool;
/*      */   }
/*      */   
/*      */   public boolean s(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 3910 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 3912 */     if (i1 == 3)
/*      */     {
/* 3913 */       this.q = 1;
/* 3914 */       this.r = 1;
/* 3915 */       this.u = 1;
/* 3916 */       this.v = 1;
/*      */     }
/* 3917 */     else if (i1 == 4)
/*      */     {
/* 3918 */       this.s = 1;
/* 3919 */       this.t = 1;
/*      */     }
/* 3922 */     boolean bool = q(paramahu, paramInt1, paramInt2, paramInt3);
/*      */     
/* 3924 */     this.s = 0;
/* 3925 */     this.q = 0;
/* 3926 */     this.r = 0;
/* 3927 */     this.t = 0;
/* 3928 */     this.u = 0;
/* 3929 */     this.v = 0;
/*      */     
/* 3931 */     return bool;
/*      */   }
/*      */   
/*      */   public boolean a(ahu paramahu, int paramInt1, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, float paramFloat3)
/*      */   {
/* 3949 */     this.w = true;
/* 3950 */     boolean bool1 = false;
/* 3951 */     float f1 = 0.0F;
/* 3952 */     float f2 = 0.0F;
/* 3953 */     float f3 = 0.0F;
/* 3954 */     float f4 = 0.0F;
/*      */     
/* 3956 */     int i1 = 1;
/*      */     
/* 3958 */     int i2 = paramahu.c(this.a, paramInt1, paramInt2, paramInt3);
/*      */     
/* 3960 */     blz localblz = blz.a;
/* 3961 */     localblz.b(983055);
/* 3963 */     if (b(paramahu).g().equals("grass_top")) {
/* 3963 */       i1 = 0;
/* 3964 */     } else if (b()) {
/* 3964 */       i1 = 0;
/*      */     }
/*      */     boolean bool2;
/*      */     boolean bool3;
/*      */     boolean bool4;
/*      */     boolean bool5;
/*      */     int i3;
/*      */     float f5;
/* 3966 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2 - 1, paramInt3, 0)))
/*      */     {
/* 3967 */       if (this.j <= 0.0D) {
/* 3967 */         paramInt2--;
/*      */       }
/* 3969 */       this.S = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3);
/* 3970 */       this.U = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1);
/* 3971 */       this.V = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1);
/* 3972 */       this.X = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3);
/*      */       
/* 3974 */       this.y = this.a.a(paramInt1 - 1, paramInt2, paramInt3).I();
/* 3975 */       this.A = this.a.a(paramInt1, paramInt2, paramInt3 - 1).I();
/* 3976 */       this.B = this.a.a(paramInt1, paramInt2, paramInt3 + 1).I();
/* 3977 */       this.D = this.a.a(paramInt1 + 1, paramInt2, paramInt3).I();
/*      */       
/* 3979 */       bool2 = this.a.a(paramInt1 + 1, paramInt2 - 1, paramInt3).l();
/* 3980 */       bool3 = this.a.a(paramInt1 - 1, paramInt2 - 1, paramInt3).l();
/* 3981 */       bool4 = this.a.a(paramInt1, paramInt2 - 1, paramInt3 + 1).l();
/* 3982 */       bool5 = this.a.a(paramInt1, paramInt2 - 1, paramInt3 - 1).l();
/* 3984 */       if ((bool5) || (bool3))
/*      */       {
/* 3985 */         this.x = this.a.a(paramInt1 - 1, paramInt2, paramInt3 - 1).I();
/* 3986 */         this.R = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 3988 */         this.x = this.y;
/* 3989 */         this.R = this.S;
/*      */       }
/* 3991 */       if ((bool4) || (bool3))
/*      */       {
/* 3992 */         this.z = this.a.a(paramInt1 - 1, paramInt2, paramInt3 + 1).I();
/* 3993 */         this.T = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 3995 */         this.z = this.y;
/* 3996 */         this.T = this.S;
/*      */       }
/* 3998 */       if ((bool5) || (bool2))
/*      */       {
/* 3999 */         this.C = this.a.a(paramInt1 + 1, paramInt2, paramInt3 - 1).I();
/* 4000 */         this.W = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4002 */         this.C = this.D;
/* 4003 */         this.W = this.X;
/*      */       }
/* 4005 */       if ((bool4) || (bool2))
/*      */       {
/* 4006 */         this.E = this.a.a(paramInt1 + 1, paramInt2, paramInt3 + 1).I();
/* 4007 */         this.Y = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4009 */         this.E = this.D;
/* 4010 */         this.Y = this.X;
/*      */       }
/* 4012 */       if (this.j <= 0.0D) {
/* 4012 */         paramInt2++;
/*      */       }
/* 4014 */       i3 = i2;
/* 4015 */       if ((this.j <= 0.0D) || (!this.a.a(paramInt1, paramInt2 - 1, paramInt3).c())) {
/* 4015 */         i3 = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3);
/*      */       }
/* 4016 */       f5 = this.a.a(paramInt1, paramInt2 - 1, paramInt3).I();
/*      */       
/* 4018 */       f1 = (this.z + this.y + this.B + f5) / 4.0F;
/* 4019 */       f4 = (this.B + f5 + this.E + this.D) / 4.0F;
/* 4020 */       f3 = (f5 + this.A + this.D + this.C) / 4.0F;
/* 4021 */       f2 = (this.y + this.x + f5 + this.A) / 4.0F;
/*      */       
/* 4023 */       this.al = a(this.T, this.S, this.V, i3);
/* 4024 */       this.ao = a(this.V, this.Y, this.X, i3);
/* 4025 */       this.an = a(this.U, this.X, this.W, i3);
/* 4026 */       this.am = a(this.S, this.R, this.U, i3);
/* 4028 */       if (i1 != 0)
/*      */       {
/* 4029 */         this.ap = (this.aq = this.ar = this.as = paramFloat1 * 0.5F);
/* 4030 */         this.at = (this.au = this.av = this.aw = paramFloat2 * 0.5F);
/* 4031 */         this.ax = (this.ay = this.az = this.aA = paramFloat3 * 0.5F);
/*      */       }
/*      */       else
/*      */       {
/* 4033 */         this.ap = (this.aq = this.ar = this.as = 0.5F);
/* 4034 */         this.at = (this.au = this.av = this.aw = 0.5F);
/* 4035 */         this.ax = (this.ay = this.az = this.aA = 0.5F);
/*      */       }
/* 4037 */       this.ap *= f1;
/* 4038 */       this.at *= f1;
/* 4039 */       this.ax *= f1;
/* 4040 */       this.aq *= f2;
/* 4041 */       this.au *= f2;
/* 4042 */       this.ay *= f2;
/* 4043 */       this.ar *= f3;
/* 4044 */       this.av *= f3;
/* 4045 */       this.az *= f3;
/* 4046 */       this.as *= f4;
/* 4047 */       this.aw *= f4;
/* 4048 */       this.aA *= f4;
/*      */       
/* 4050 */       a(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 0));
/* 4051 */       bool1 = true;
/*      */     }
/* 4053 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2 + 1, paramInt3, 1)))
/*      */     {
/* 4054 */       if (this.k >= 1.0D) {
/* 4054 */         paramInt2++;
/*      */       }
/* 4056 */       this.aa = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3);
/* 4057 */       this.ae = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3);
/* 4058 */       this.ac = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1);
/* 4059 */       this.af = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1);
/*      */       
/* 4061 */       this.G = this.a.a(paramInt1 - 1, paramInt2, paramInt3).I();
/* 4062 */       this.K = this.a.a(paramInt1 + 1, paramInt2, paramInt3).I();
/* 4063 */       this.I = this.a.a(paramInt1, paramInt2, paramInt3 - 1).I();
/* 4064 */       this.L = this.a.a(paramInt1, paramInt2, paramInt3 + 1).I();
/*      */       
/* 4066 */       bool2 = this.a.a(paramInt1 + 1, paramInt2 + 1, paramInt3).l();
/* 4067 */       bool3 = this.a.a(paramInt1 - 1, paramInt2 + 1, paramInt3).l();
/* 4068 */       bool4 = this.a.a(paramInt1, paramInt2 + 1, paramInt3 + 1).l();
/* 4069 */       bool5 = this.a.a(paramInt1, paramInt2 + 1, paramInt3 - 1).l();
/* 4072 */       if ((bool5) || (bool3))
/*      */       {
/* 4073 */         this.F = this.a.a(paramInt1 - 1, paramInt2, paramInt3 - 1).I();
/* 4074 */         this.Z = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4076 */         this.F = this.G;
/* 4077 */         this.Z = this.aa;
/*      */       }
/* 4079 */       if ((bool5) || (bool2))
/*      */       {
/* 4080 */         this.J = this.a.a(paramInt1 + 1, paramInt2, paramInt3 - 1).I();
/* 4081 */         this.ad = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4083 */         this.J = this.K;
/* 4084 */         this.ad = this.ae;
/*      */       }
/* 4086 */       if ((bool4) || (bool3))
/*      */       {
/* 4087 */         this.H = this.a.a(paramInt1 - 1, paramInt2, paramInt3 + 1).I();
/* 4088 */         this.ab = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4090 */         this.H = this.G;
/* 4091 */         this.ab = this.aa;
/*      */       }
/* 4093 */       if ((bool4) || (bool2))
/*      */       {
/* 4094 */         this.M = this.a.a(paramInt1 + 1, paramInt2, paramInt3 + 1).I();
/* 4095 */         this.ag = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4097 */         this.M = this.K;
/* 4098 */         this.ag = this.ae;
/*      */       }
/* 4100 */       if (this.k >= 1.0D) {
/* 4100 */         paramInt2--;
/*      */       }
/* 4102 */       i3 = i2;
/* 4103 */       if ((this.k >= 1.0D) || (!this.a.a(paramInt1, paramInt2 + 1, paramInt3).c())) {
/* 4103 */         i3 = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3);
/*      */       }
/* 4104 */       f5 = this.a.a(paramInt1, paramInt2 + 1, paramInt3).I();
/*      */       
/* 4106 */       f4 = (this.H + this.G + this.L + f5) / 4.0F;
/* 4107 */       f1 = (this.L + f5 + this.M + this.K) / 4.0F;
/* 4108 */       f2 = (f5 + this.I + this.K + this.J) / 4.0F;
/* 4109 */       f3 = (this.G + this.F + f5 + this.I) / 4.0F;
/*      */       
/*      */ 
/* 4112 */       this.ao = a(this.ab, this.aa, this.af, i3);
/* 4113 */       this.al = a(this.af, this.ag, this.ae, i3);
/* 4114 */       this.am = a(this.ac, this.ae, this.ad, i3);
/* 4115 */       this.an = a(this.aa, this.Z, this.ac, i3);
/*      */       
/* 4117 */       this.ap = (this.aq = this.ar = this.as = paramFloat1);
/* 4118 */       this.at = (this.au = this.av = this.aw = paramFloat2);
/* 4119 */       this.ax = (this.ay = this.az = this.aA = paramFloat3);
/* 4120 */       this.ap *= f1;
/* 4121 */       this.at *= f1;
/* 4122 */       this.ax *= f1;
/* 4123 */       this.aq *= f2;
/* 4124 */       this.au *= f2;
/* 4125 */       this.ay *= f2;
/* 4126 */       this.ar *= f3;
/* 4127 */       this.av *= f3;
/* 4128 */       this.az *= f3;
/* 4129 */       this.as *= f4;
/* 4130 */       this.aw *= f4;
/* 4131 */       this.aA *= f4;
/* 4132 */       b(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 1));
/* 4133 */       bool1 = true;
/*      */     }
/*      */     ps localps;
/* 4136 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2, paramInt3 - 1, 2)))
/*      */     {
/* 4137 */       if (this.l <= 0.0D) {
/* 4137 */         paramInt3--;
/*      */       }
/* 4138 */       this.N = this.a.a(paramInt1 - 1, paramInt2, paramInt3).I();
/* 4139 */       this.A = this.a.a(paramInt1, paramInt2 - 1, paramInt3).I();
/* 4140 */       this.I = this.a.a(paramInt1, paramInt2 + 1, paramInt3).I();
/* 4141 */       this.O = this.a.a(paramInt1 + 1, paramInt2, paramInt3).I();
/*      */       
/* 4143 */       this.ah = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3);
/* 4144 */       this.U = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3);
/* 4145 */       this.ac = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3);
/* 4146 */       this.ai = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3);
/*      */       
/* 4148 */       bool2 = this.a.a(paramInt1 + 1, paramInt2, paramInt3 - 1).l();
/* 4149 */       bool3 = this.a.a(paramInt1 - 1, paramInt2, paramInt3 - 1).l();
/* 4150 */       bool4 = this.a.a(paramInt1, paramInt2 + 1, paramInt3 - 1).l();
/* 4151 */       bool5 = this.a.a(paramInt1, paramInt2 - 1, paramInt3 - 1).l();
/* 4153 */       if ((bool3) || (bool5))
/*      */       {
/* 4154 */         this.x = this.a.a(paramInt1 - 1, paramInt2 - 1, paramInt3).I();
/* 4155 */         this.R = paramahu.c(this.a, paramInt1 - 1, paramInt2 - 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4157 */         this.x = this.N;
/* 4158 */         this.R = this.ah;
/*      */       }
/* 4160 */       if ((bool3) || (bool4))
/*      */       {
/* 4161 */         this.F = this.a.a(paramInt1 - 1, paramInt2 + 1, paramInt3).I();
/* 4162 */         this.Z = paramahu.c(this.a, paramInt1 - 1, paramInt2 + 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4164 */         this.F = this.N;
/* 4165 */         this.Z = this.ah;
/*      */       }
/* 4167 */       if ((bool2) || (bool5))
/*      */       {
/* 4168 */         this.C = this.a.a(paramInt1 + 1, paramInt2 - 1, paramInt3).I();
/* 4169 */         this.W = paramahu.c(this.a, paramInt1 + 1, paramInt2 - 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4171 */         this.C = this.O;
/* 4172 */         this.W = this.ai;
/*      */       }
/* 4174 */       if ((bool2) || (bool4))
/*      */       {
/* 4175 */         this.J = this.a.a(paramInt1 + 1, paramInt2 + 1, paramInt3).I();
/* 4176 */         this.ad = paramahu.c(this.a, paramInt1 + 1, paramInt2 + 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4178 */         this.J = this.O;
/* 4179 */         this.ad = this.ai;
/*      */       }
/* 4181 */       if (this.l <= 0.0D) {
/* 4181 */         paramInt3++;
/*      */       }
/* 4182 */       i3 = i2;
/* 4183 */       if ((this.l <= 0.0D) || (!this.a.a(paramInt1, paramInt2, paramInt3 - 1).c())) {
/* 4183 */         i3 = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1);
/*      */       }
/* 4184 */       f5 = this.a.a(paramInt1, paramInt2, paramInt3 - 1).I();
/*      */       
/*      */ 
/* 4187 */       f1 = (this.N + this.F + f5 + this.I) / 4.0F;
/* 4188 */       f2 = (f5 + this.I + this.O + this.J) / 4.0F;
/* 4189 */       f3 = (this.A + f5 + this.C + this.O) / 4.0F;
/* 4190 */       f4 = (this.x + this.N + this.A + f5) / 4.0F;
/*      */       
/* 4192 */       this.al = a(this.ah, this.Z, this.ac, i3);
/* 4193 */       this.am = a(this.ac, this.ai, this.ad, i3);
/* 4194 */       this.an = a(this.U, this.W, this.ai, i3);
/* 4195 */       this.ao = a(this.R, this.ah, this.U, i3);
/* 4198 */       if (i1 != 0)
/*      */       {
/* 4199 */         this.ap = (this.aq = this.ar = this.as = paramFloat1 * 0.8F);
/* 4200 */         this.at = (this.au = this.av = this.aw = paramFloat2 * 0.8F);
/* 4201 */         this.ax = (this.ay = this.az = this.aA = paramFloat3 * 0.8F);
/*      */       }
/*      */       else
/*      */       {
/* 4203 */         this.ap = (this.aq = this.ar = this.as = 0.8F);
/* 4204 */         this.at = (this.au = this.av = this.aw = 0.8F);
/* 4205 */         this.ax = (this.ay = this.az = this.aA = 0.8F);
/*      */       }
/* 4207 */       this.ap *= f1;
/* 4208 */       this.at *= f1;
/* 4209 */       this.ax *= f1;
/* 4210 */       this.aq *= f2;
/* 4211 */       this.au *= f2;
/* 4212 */       this.ay *= f2;
/* 4213 */       this.ar *= f3;
/* 4214 */       this.av *= f3;
/* 4215 */       this.az *= f3;
/* 4216 */       this.as *= f4;
/* 4217 */       this.aw *= f4;
/* 4218 */       this.aA *= f4;
/*      */       
/* 4220 */       localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 2);
/* 4221 */       c(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 4223 */       if ((b) && (localps.g().equals("grass_side")) && (!b()))
/*      */       {
/* 4224 */         this.ap *= paramFloat1;
/* 4225 */         this.aq *= paramFloat1;
/* 4226 */         this.ar *= paramFloat1;
/* 4227 */         this.as *= paramFloat1;
/* 4228 */         this.at *= paramFloat2;
/* 4229 */         this.au *= paramFloat2;
/* 4230 */         this.av *= paramFloat2;
/* 4231 */         this.aw *= paramFloat2;
/* 4232 */         this.ax *= paramFloat3;
/* 4233 */         this.ay *= paramFloat3;
/* 4234 */         this.az *= paramFloat3;
/* 4235 */         this.aA *= paramFloat3;
/* 4236 */         c(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 4239 */       bool1 = true;
/*      */     }
/* 4241 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2, paramInt3 + 1, 3)))
/*      */     {
/* 4242 */       if (this.m >= 1.0D) {
/* 4242 */         paramInt3++;
/*      */       }
/* 4244 */       this.P = this.a.a(paramInt1 - 1, paramInt2, paramInt3).I();
/* 4245 */       this.Q = this.a.a(paramInt1 + 1, paramInt2, paramInt3).I();
/* 4246 */       this.B = this.a.a(paramInt1, paramInt2 - 1, paramInt3).I();
/* 4247 */       this.L = this.a.a(paramInt1, paramInt2 + 1, paramInt3).I();
/*      */       
/* 4249 */       this.aj = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3);
/* 4250 */       this.ak = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3);
/* 4251 */       this.V = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3);
/* 4252 */       this.af = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3);
/*      */       
/* 4254 */       bool2 = this.a.a(paramInt1 + 1, paramInt2, paramInt3 + 1).l();
/* 4255 */       bool3 = this.a.a(paramInt1 - 1, paramInt2, paramInt3 + 1).l();
/* 4256 */       bool4 = this.a.a(paramInt1, paramInt2 + 1, paramInt3 + 1).l();
/* 4257 */       bool5 = this.a.a(paramInt1, paramInt2 - 1, paramInt3 + 1).l();
/* 4259 */       if ((bool3) || (bool5))
/*      */       {
/* 4260 */         this.z = this.a.a(paramInt1 - 1, paramInt2 - 1, paramInt3).I();
/* 4261 */         this.T = paramahu.c(this.a, paramInt1 - 1, paramInt2 - 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4263 */         this.z = this.P;
/* 4264 */         this.T = this.aj;
/*      */       }
/* 4266 */       if ((bool3) || (bool4))
/*      */       {
/* 4267 */         this.H = this.a.a(paramInt1 - 1, paramInt2 + 1, paramInt3).I();
/* 4268 */         this.ab = paramahu.c(this.a, paramInt1 - 1, paramInt2 + 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4270 */         this.H = this.P;
/* 4271 */         this.ab = this.aj;
/*      */       }
/* 4273 */       if ((bool2) || (bool5))
/*      */       {
/* 4274 */         this.E = this.a.a(paramInt1 + 1, paramInt2 - 1, paramInt3).I();
/* 4275 */         this.Y = paramahu.c(this.a, paramInt1 + 1, paramInt2 - 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4277 */         this.E = this.Q;
/* 4278 */         this.Y = this.ak;
/*      */       }
/* 4280 */       if ((bool2) || (bool4))
/*      */       {
/* 4281 */         this.M = this.a.a(paramInt1 + 1, paramInt2 + 1, paramInt3).I();
/* 4282 */         this.ag = paramahu.c(this.a, paramInt1 + 1, paramInt2 + 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4284 */         this.M = this.Q;
/* 4285 */         this.ag = this.ak;
/*      */       }
/* 4287 */       if (this.m >= 1.0D) {
/* 4287 */         paramInt3--;
/*      */       }
/* 4288 */       i3 = i2;
/* 4289 */       if ((this.m >= 1.0D) || (!this.a.a(paramInt1, paramInt2, paramInt3 + 1).c())) {
/* 4289 */         i3 = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1);
/*      */       }
/* 4290 */       f5 = this.a.a(paramInt1, paramInt2, paramInt3 + 1).I();
/*      */       
/*      */ 
/* 4293 */       f1 = (this.P + this.H + f5 + this.L) / 4.0F;
/* 4294 */       f4 = (f5 + this.L + this.Q + this.M) / 4.0F;
/* 4295 */       f3 = (this.B + f5 + this.E + this.Q) / 4.0F;
/* 4296 */       f2 = (this.z + this.P + this.B + f5) / 4.0F;
/*      */       
/* 4298 */       this.al = a(this.aj, this.ab, this.af, i3);
/* 4299 */       this.ao = a(this.af, this.ak, this.ag, i3);
/* 4300 */       this.an = a(this.V, this.Y, this.ak, i3);
/* 4301 */       this.am = a(this.T, this.aj, this.V, i3);
/* 4303 */       if (i1 != 0)
/*      */       {
/* 4304 */         this.ap = (this.aq = this.ar = this.as = paramFloat1 * 0.8F);
/* 4305 */         this.at = (this.au = this.av = this.aw = paramFloat2 * 0.8F);
/* 4306 */         this.ax = (this.ay = this.az = this.aA = paramFloat3 * 0.8F);
/*      */       }
/*      */       else
/*      */       {
/* 4308 */         this.ap = (this.aq = this.ar = this.as = 0.8F);
/* 4309 */         this.at = (this.au = this.av = this.aw = 0.8F);
/* 4310 */         this.ax = (this.ay = this.az = this.aA = 0.8F);
/*      */       }
/* 4312 */       this.ap *= f1;
/* 4313 */       this.at *= f1;
/* 4314 */       this.ax *= f1;
/* 4315 */       this.aq *= f2;
/* 4316 */       this.au *= f2;
/* 4317 */       this.ay *= f2;
/* 4318 */       this.ar *= f3;
/* 4319 */       this.av *= f3;
/* 4320 */       this.az *= f3;
/* 4321 */       this.as *= f4;
/* 4322 */       this.aw *= f4;
/* 4323 */       this.aA *= f4;
/* 4324 */       localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 3);
/* 4325 */       d(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 3));
/* 4326 */       if ((b) && (localps.g().equals("grass_side")) && (!b()))
/*      */       {
/* 4327 */         this.ap *= paramFloat1;
/* 4328 */         this.aq *= paramFloat1;
/* 4329 */         this.ar *= paramFloat1;
/* 4330 */         this.as *= paramFloat1;
/* 4331 */         this.at *= paramFloat2;
/* 4332 */         this.au *= paramFloat2;
/* 4333 */         this.av *= paramFloat2;
/* 4334 */         this.aw *= paramFloat2;
/* 4335 */         this.ax *= paramFloat3;
/* 4336 */         this.ay *= paramFloat3;
/* 4337 */         this.az *= paramFloat3;
/* 4338 */         this.aA *= paramFloat3;
/* 4339 */         d(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 4341 */       bool1 = true;
/*      */     }
/* 4343 */     if ((this.f) || (paramahu.a(this.a, paramInt1 - 1, paramInt2, paramInt3, 4)))
/*      */     {
/* 4344 */       if (this.h <= 0.0D) {
/* 4344 */         paramInt1--;
/*      */       }
/* 4345 */       this.y = this.a.a(paramInt1, paramInt2 - 1, paramInt3).I();
/* 4346 */       this.N = this.a.a(paramInt1, paramInt2, paramInt3 - 1).I();
/* 4347 */       this.P = this.a.a(paramInt1, paramInt2, paramInt3 + 1).I();
/* 4348 */       this.G = this.a.a(paramInt1, paramInt2 + 1, paramInt3).I();
/*      */       
/* 4350 */       this.S = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3);
/* 4351 */       this.ah = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1);
/* 4352 */       this.aj = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1);
/* 4353 */       this.aa = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3);
/*      */       
/* 4355 */       bool2 = this.a.a(paramInt1 - 1, paramInt2 + 1, paramInt3).l();
/* 4356 */       bool3 = this.a.a(paramInt1 - 1, paramInt2 - 1, paramInt3).l();
/* 4357 */       bool4 = this.a.a(paramInt1 - 1, paramInt2, paramInt3 - 1).l();
/* 4358 */       bool5 = this.a.a(paramInt1 - 1, paramInt2, paramInt3 + 1).l();
/* 4360 */       if ((bool4) || (bool3))
/*      */       {
/* 4361 */         this.x = this.a.a(paramInt1, paramInt2 - 1, paramInt3 - 1).I();
/* 4362 */         this.R = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4364 */         this.x = this.N;
/* 4365 */         this.R = this.ah;
/*      */       }
/* 4367 */       if ((bool5) || (bool3))
/*      */       {
/* 4368 */         this.z = this.a.a(paramInt1, paramInt2 - 1, paramInt3 + 1).I();
/* 4369 */         this.T = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4371 */         this.z = this.P;
/* 4372 */         this.T = this.aj;
/*      */       }
/* 4374 */       if ((bool4) || (bool2))
/*      */       {
/* 4375 */         this.F = this.a.a(paramInt1, paramInt2 + 1, paramInt3 - 1).I();
/* 4376 */         this.Z = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4378 */         this.F = this.N;
/* 4379 */         this.Z = this.ah;
/*      */       }
/* 4381 */       if ((bool5) || (bool2))
/*      */       {
/* 4382 */         this.H = this.a.a(paramInt1, paramInt2 + 1, paramInt3 + 1).I();
/* 4383 */         this.ab = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4385 */         this.H = this.P;
/* 4386 */         this.ab = this.aj;
/*      */       }
/* 4388 */       if (this.h <= 0.0D) {
/* 4388 */         paramInt1++;
/*      */       }
/* 4389 */       i3 = i2;
/* 4390 */       if ((this.h <= 0.0D) || (!this.a.a(paramInt1 - 1, paramInt2, paramInt3).c())) {
/* 4390 */         i3 = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3);
/*      */       }
/* 4391 */       f5 = this.a.a(paramInt1 - 1, paramInt2, paramInt3).I();
/*      */       
/*      */ 
/* 4394 */       f4 = (this.y + this.z + f5 + this.P) / 4.0F;
/* 4395 */       f1 = (f5 + this.P + this.G + this.H) / 4.0F;
/* 4396 */       f2 = (this.N + f5 + this.F + this.G) / 4.0F;
/* 4397 */       f3 = (this.x + this.y + this.N + f5) / 4.0F;
/*      */       
/* 4399 */       this.ao = a(this.S, this.T, this.aj, i3);
/* 4400 */       this.al = a(this.aj, this.aa, this.ab, i3);
/* 4401 */       this.am = a(this.ah, this.Z, this.aa, i3);
/* 4402 */       this.an = a(this.R, this.S, this.ah, i3);
/* 4405 */       if (i1 != 0)
/*      */       {
/* 4406 */         this.ap = (this.aq = this.ar = this.as = paramFloat1 * 0.6F);
/* 4407 */         this.at = (this.au = this.av = this.aw = paramFloat2 * 0.6F);
/* 4408 */         this.ax = (this.ay = this.az = this.aA = paramFloat3 * 0.6F);
/*      */       }
/*      */       else
/*      */       {
/* 4410 */         this.ap = (this.aq = this.ar = this.as = 0.6F);
/* 4411 */         this.at = (this.au = this.av = this.aw = 0.6F);
/* 4412 */         this.ax = (this.ay = this.az = this.aA = 0.6F);
/*      */       }
/* 4414 */       this.ap *= f1;
/* 4415 */       this.at *= f1;
/* 4416 */       this.ax *= f1;
/* 4417 */       this.aq *= f2;
/* 4418 */       this.au *= f2;
/* 4419 */       this.ay *= f2;
/* 4420 */       this.ar *= f3;
/* 4421 */       this.av *= f3;
/* 4422 */       this.az *= f3;
/* 4423 */       this.as *= f4;
/* 4424 */       this.aw *= f4;
/* 4425 */       this.aA *= f4;
/* 4426 */       localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 4);
/* 4427 */       e(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 4428 */       if ((b) && (localps.g().equals("grass_side")) && (!b()))
/*      */       {
/* 4429 */         this.ap *= paramFloat1;
/* 4430 */         this.aq *= paramFloat1;
/* 4431 */         this.ar *= paramFloat1;
/* 4432 */         this.as *= paramFloat1;
/* 4433 */         this.at *= paramFloat2;
/* 4434 */         this.au *= paramFloat2;
/* 4435 */         this.av *= paramFloat2;
/* 4436 */         this.aw *= paramFloat2;
/* 4437 */         this.ax *= paramFloat3;
/* 4438 */         this.ay *= paramFloat3;
/* 4439 */         this.az *= paramFloat3;
/* 4440 */         this.aA *= paramFloat3;
/* 4441 */         e(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 4443 */       bool1 = true;
/*      */     }
/* 4445 */     if ((this.f) || (paramahu.a(this.a, paramInt1 + 1, paramInt2, paramInt3, 5)))
/*      */     {
/* 4446 */       if (this.i >= 1.0D) {
/* 4446 */         paramInt1++;
/*      */       }
/* 4447 */       this.D = this.a.a(paramInt1, paramInt2 - 1, paramInt3).I();
/* 4448 */       this.O = this.a.a(paramInt1, paramInt2, paramInt3 - 1).I();
/* 4449 */       this.Q = this.a.a(paramInt1, paramInt2, paramInt3 + 1).I();
/* 4450 */       this.K = this.a.a(paramInt1, paramInt2 + 1, paramInt3).I();
/*      */       
/* 4452 */       this.X = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3);
/* 4453 */       this.ai = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1);
/* 4454 */       this.ak = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1);
/* 4455 */       this.ae = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3);
/*      */       
/* 4457 */       bool2 = this.a.a(paramInt1 + 1, paramInt2 + 1, paramInt3).l();
/* 4458 */       bool3 = this.a.a(paramInt1 + 1, paramInt2 - 1, paramInt3).l();
/* 4459 */       bool4 = this.a.a(paramInt1 + 1, paramInt2, paramInt3 + 1).l();
/* 4460 */       bool5 = this.a.a(paramInt1 + 1, paramInt2, paramInt3 - 1).l();
/* 4463 */       if ((bool3) || (bool5))
/*      */       {
/* 4464 */         this.C = this.a.a(paramInt1, paramInt2 - 1, paramInt3 - 1).I();
/* 4465 */         this.W = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4467 */         this.C = this.O;
/* 4468 */         this.W = this.ai;
/*      */       }
/* 4470 */       if ((bool3) || (bool4))
/*      */       {
/* 4471 */         this.E = this.a.a(paramInt1, paramInt2 - 1, paramInt3 + 1).I();
/* 4472 */         this.Y = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4474 */         this.E = this.Q;
/* 4475 */         this.Y = this.ak;
/*      */       }
/* 4477 */       if ((bool2) || (bool5))
/*      */       {
/* 4478 */         this.J = this.a.a(paramInt1, paramInt2 + 1, paramInt3 - 1).I();
/* 4479 */         this.ad = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4481 */         this.J = this.O;
/* 4482 */         this.ad = this.ai;
/*      */       }
/* 4484 */       if ((bool2) || (bool4))
/*      */       {
/* 4485 */         this.M = this.a.a(paramInt1, paramInt2 + 1, paramInt3 + 1).I();
/* 4486 */         this.ag = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4488 */         this.M = this.Q;
/* 4489 */         this.ag = this.ak;
/*      */       }
/* 4491 */       if (this.i >= 1.0D) {
/* 4491 */         paramInt1--;
/*      */       }
/* 4492 */       i3 = i2;
/* 4493 */       if ((this.i >= 1.0D) || (!this.a.a(paramInt1 + 1, paramInt2, paramInt3).c())) {
/* 4493 */         i3 = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3);
/*      */       }
/* 4494 */       f5 = this.a.a(paramInt1 + 1, paramInt2, paramInt3).I();
/*      */       
/*      */ 
/* 4497 */       f1 = (this.D + this.E + f5 + this.Q) / 4.0F;
/* 4498 */       f2 = (this.C + this.D + this.O + f5) / 4.0F;
/* 4499 */       f3 = (this.O + f5 + this.J + this.K) / 4.0F;
/* 4500 */       f4 = (f5 + this.Q + this.K + this.M) / 4.0F;
/*      */       
/* 4502 */       this.al = a(this.X, this.Y, this.ak, i3);
/* 4503 */       this.ao = a(this.ak, this.ae, this.ag, i3);
/* 4504 */       this.an = a(this.ai, this.ad, this.ae, i3);
/* 4505 */       this.am = a(this.W, this.X, this.ai, i3);
/* 4507 */       if (i1 != 0)
/*      */       {
/* 4508 */         this.ap = (this.aq = this.ar = this.as = paramFloat1 * 0.6F);
/* 4509 */         this.at = (this.au = this.av = this.aw = paramFloat2 * 0.6F);
/* 4510 */         this.ax = (this.ay = this.az = this.aA = paramFloat3 * 0.6F);
/*      */       }
/*      */       else
/*      */       {
/* 4512 */         this.ap = (this.aq = this.ar = this.as = 0.6F);
/* 4513 */         this.at = (this.au = this.av = this.aw = 0.6F);
/* 4514 */         this.ax = (this.ay = this.az = this.aA = 0.6F);
/*      */       }
/* 4516 */       this.ap *= f1;
/* 4517 */       this.at *= f1;
/* 4518 */       this.ax *= f1;
/* 4519 */       this.aq *= f2;
/* 4520 */       this.au *= f2;
/* 4521 */       this.ay *= f2;
/* 4522 */       this.ar *= f3;
/* 4523 */       this.av *= f3;
/* 4524 */       this.az *= f3;
/* 4525 */       this.as *= f4;
/* 4526 */       this.aw *= f4;
/* 4527 */       this.aA *= f4;
/*      */       
/* 4529 */       localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 5);
/* 4530 */       f(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 4531 */       if ((b) && (localps.g().equals("grass_side")) && (!b()))
/*      */       {
/* 4532 */         this.ap *= paramFloat1;
/* 4533 */         this.aq *= paramFloat1;
/* 4534 */         this.ar *= paramFloat1;
/* 4535 */         this.as *= paramFloat1;
/* 4536 */         this.at *= paramFloat2;
/* 4537 */         this.au *= paramFloat2;
/* 4538 */         this.av *= paramFloat2;
/* 4539 */         this.aw *= paramFloat2;
/* 4540 */         this.ax *= paramFloat3;
/* 4541 */         this.ay *= paramFloat3;
/* 4542 */         this.az *= paramFloat3;
/* 4543 */         this.aA *= paramFloat3;
/* 4544 */         f(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 4546 */       bool1 = true;
/*      */     }
/* 4548 */     this.w = false;
/* 4549 */     return bool1;
/*      */   }
/*      */   
/*      */   public boolean b(ahu paramahu, int paramInt1, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, float paramFloat3)
/*      */   {
/* 4553 */     this.w = true;
/* 4554 */     boolean bool1 = false;
/* 4555 */     float f1 = 0.0F;
/* 4556 */     float f2 = 0.0F;
/* 4557 */     float f3 = 0.0F;
/* 4558 */     float f4 = 0.0F;
/*      */     
/* 4560 */     int i1 = 1;
/*      */     
/* 4562 */     int i2 = paramahu.c(this.a, paramInt1, paramInt2, paramInt3);
/*      */     
/* 4564 */     blz localblz = blz.a;
/* 4565 */     localblz.b(983055);
/* 4567 */     if (b(paramahu).g().equals("grass_top")) {
/* 4567 */       i1 = 0;
/* 4568 */     } else if (b()) {
/* 4568 */       i1 = 0;
/*      */     }
/*      */     boolean bool2;
/*      */     boolean bool3;
/*      */     boolean bool4;
/*      */     boolean bool5;
/*      */     int i3;
/*      */     float f5;
/* 4570 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2 - 1, paramInt3, 0)))
/*      */     {
/* 4571 */       if (this.j <= 0.0D) {
/* 4571 */         paramInt2--;
/*      */       }
/* 4573 */       this.S = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3);
/* 4574 */       this.U = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1);
/* 4575 */       this.V = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1);
/* 4576 */       this.X = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3);
/*      */       
/* 4578 */       this.y = this.a.a(paramInt1 - 1, paramInt2, paramInt3).I();
/* 4579 */       this.A = this.a.a(paramInt1, paramInt2, paramInt3 - 1).I();
/* 4580 */       this.B = this.a.a(paramInt1, paramInt2, paramInt3 + 1).I();
/* 4581 */       this.D = this.a.a(paramInt1 + 1, paramInt2, paramInt3).I();
/*      */       
/* 4583 */       bool2 = this.a.a(paramInt1 + 1, paramInt2 - 1, paramInt3).l();
/* 4584 */       bool3 = this.a.a(paramInt1 - 1, paramInt2 - 1, paramInt3).l();
/* 4585 */       bool4 = this.a.a(paramInt1, paramInt2 - 1, paramInt3 + 1).l();
/* 4586 */       bool5 = this.a.a(paramInt1, paramInt2 - 1, paramInt3 - 1).l();
/* 4588 */       if ((bool5) || (bool3))
/*      */       {
/* 4589 */         this.x = this.a.a(paramInt1 - 1, paramInt2, paramInt3 - 1).I();
/* 4590 */         this.R = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4592 */         this.x = this.y;
/* 4593 */         this.R = this.S;
/*      */       }
/* 4595 */       if ((bool4) || (bool3))
/*      */       {
/* 4596 */         this.z = this.a.a(paramInt1 - 1, paramInt2, paramInt3 + 1).I();
/* 4597 */         this.T = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4599 */         this.z = this.y;
/* 4600 */         this.T = this.S;
/*      */       }
/* 4602 */       if ((bool5) || (bool2))
/*      */       {
/* 4603 */         this.C = this.a.a(paramInt1 + 1, paramInt2, paramInt3 - 1).I();
/* 4604 */         this.W = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4606 */         this.C = this.D;
/* 4607 */         this.W = this.X;
/*      */       }
/* 4609 */       if ((bool4) || (bool2))
/*      */       {
/* 4610 */         this.E = this.a.a(paramInt1 + 1, paramInt2, paramInt3 + 1).I();
/* 4611 */         this.Y = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4613 */         this.E = this.D;
/* 4614 */         this.Y = this.X;
/*      */       }
/* 4616 */       if (this.j <= 0.0D) {
/* 4616 */         paramInt2++;
/*      */       }
/* 4618 */       i3 = i2;
/* 4619 */       if ((this.j <= 0.0D) || (!this.a.a(paramInt1, paramInt2 - 1, paramInt3).c())) {
/* 4619 */         i3 = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3);
/*      */       }
/* 4620 */       f5 = this.a.a(paramInt1, paramInt2 - 1, paramInt3).I();
/*      */       
/* 4622 */       f1 = (this.z + this.y + this.B + f5) / 4.0F;
/* 4623 */       f4 = (this.B + f5 + this.E + this.D) / 4.0F;
/* 4624 */       f3 = (f5 + this.A + this.D + this.C) / 4.0F;
/* 4625 */       f2 = (this.y + this.x + f5 + this.A) / 4.0F;
/*      */       
/* 4627 */       this.al = a(this.T, this.S, this.V, i3);
/* 4628 */       this.ao = a(this.V, this.Y, this.X, i3);
/* 4629 */       this.an = a(this.U, this.X, this.W, i3);
/* 4630 */       this.am = a(this.S, this.R, this.U, i3);
/* 4632 */       if (i1 != 0)
/*      */       {
/* 4633 */         this.ap = (this.aq = this.ar = this.as = paramFloat1 * 0.5F);
/* 4634 */         this.at = (this.au = this.av = this.aw = paramFloat2 * 0.5F);
/* 4635 */         this.ax = (this.ay = this.az = this.aA = paramFloat3 * 0.5F);
/*      */       }
/*      */       else
/*      */       {
/* 4637 */         this.ap = (this.aq = this.ar = this.as = 0.5F);
/* 4638 */         this.at = (this.au = this.av = this.aw = 0.5F);
/* 4639 */         this.ax = (this.ay = this.az = this.aA = 0.5F);
/*      */       }
/* 4641 */       this.ap *= f1;
/* 4642 */       this.at *= f1;
/* 4643 */       this.ax *= f1;
/* 4644 */       this.aq *= f2;
/* 4645 */       this.au *= f2;
/* 4646 */       this.ay *= f2;
/* 4647 */       this.ar *= f3;
/* 4648 */       this.av *= f3;
/* 4649 */       this.az *= f3;
/* 4650 */       this.as *= f4;
/* 4651 */       this.aw *= f4;
/* 4652 */       this.aA *= f4;
/*      */       
/* 4654 */       a(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 0));
/* 4655 */       bool1 = true;
/*      */     }
/* 4657 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2 + 1, paramInt3, 1)))
/*      */     {
/* 4658 */       if (this.k >= 1.0D) {
/* 4658 */         paramInt2++;
/*      */       }
/* 4660 */       this.aa = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3);
/* 4661 */       this.ae = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3);
/* 4662 */       this.ac = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1);
/* 4663 */       this.af = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1);
/*      */       
/* 4665 */       this.G = this.a.a(paramInt1 - 1, paramInt2, paramInt3).I();
/* 4666 */       this.K = this.a.a(paramInt1 + 1, paramInt2, paramInt3).I();
/* 4667 */       this.I = this.a.a(paramInt1, paramInt2, paramInt3 - 1).I();
/* 4668 */       this.L = this.a.a(paramInt1, paramInt2, paramInt3 + 1).I();
/*      */       
/* 4670 */       bool2 = this.a.a(paramInt1 + 1, paramInt2 + 1, paramInt3).l();
/* 4671 */       bool3 = this.a.a(paramInt1 - 1, paramInt2 + 1, paramInt3).l();
/* 4672 */       bool4 = this.a.a(paramInt1, paramInt2 + 1, paramInt3 + 1).l();
/* 4673 */       bool5 = this.a.a(paramInt1, paramInt2 + 1, paramInt3 - 1).l();
/* 4676 */       if ((bool5) || (bool3))
/*      */       {
/* 4677 */         this.F = this.a.a(paramInt1 - 1, paramInt2, paramInt3 - 1).I();
/* 4678 */         this.Z = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4680 */         this.F = this.G;
/* 4681 */         this.Z = this.aa;
/*      */       }
/* 4683 */       if ((bool5) || (bool2))
/*      */       {
/* 4684 */         this.J = this.a.a(paramInt1 + 1, paramInt2, paramInt3 - 1).I();
/* 4685 */         this.ad = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4687 */         this.J = this.K;
/* 4688 */         this.ad = this.ae;
/*      */       }
/* 4690 */       if ((bool4) || (bool3))
/*      */       {
/* 4691 */         this.H = this.a.a(paramInt1 - 1, paramInt2, paramInt3 + 1).I();
/* 4692 */         this.ab = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4694 */         this.H = this.G;
/* 4695 */         this.ab = this.aa;
/*      */       }
/* 4697 */       if ((bool4) || (bool2))
/*      */       {
/* 4698 */         this.M = this.a.a(paramInt1 + 1, paramInt2, paramInt3 + 1).I();
/* 4699 */         this.ag = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4701 */         this.M = this.K;
/* 4702 */         this.ag = this.ae;
/*      */       }
/* 4704 */       if (this.k >= 1.0D) {
/* 4704 */         paramInt2--;
/*      */       }
/* 4706 */       i3 = i2;
/* 4707 */       if ((this.k >= 1.0D) || (!this.a.a(paramInt1, paramInt2 + 1, paramInt3).c())) {
/* 4707 */         i3 = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3);
/*      */       }
/* 4708 */       f5 = this.a.a(paramInt1, paramInt2 + 1, paramInt3).I();
/*      */       
/* 4710 */       f4 = (this.H + this.G + this.L + f5) / 4.0F;
/* 4711 */       f1 = (this.L + f5 + this.M + this.K) / 4.0F;
/* 4712 */       f2 = (f5 + this.I + this.K + this.J) / 4.0F;
/* 4713 */       f3 = (this.G + this.F + f5 + this.I) / 4.0F;
/*      */       
/*      */ 
/* 4716 */       this.ao = a(this.ab, this.aa, this.af, i3);
/* 4717 */       this.al = a(this.af, this.ag, this.ae, i3);
/* 4718 */       this.am = a(this.ac, this.ae, this.ad, i3);
/* 4719 */       this.an = a(this.aa, this.Z, this.ac, i3);
/*      */       
/* 4721 */       this.ap = (this.aq = this.ar = this.as = paramFloat1);
/* 4722 */       this.at = (this.au = this.av = this.aw = paramFloat2);
/* 4723 */       this.ax = (this.ay = this.az = this.aA = paramFloat3);
/* 4724 */       this.ap *= f1;
/* 4725 */       this.at *= f1;
/* 4726 */       this.ax *= f1;
/* 4727 */       this.aq *= f2;
/* 4728 */       this.au *= f2;
/* 4729 */       this.ay *= f2;
/* 4730 */       this.ar *= f3;
/* 4731 */       this.av *= f3;
/* 4732 */       this.az *= f3;
/* 4733 */       this.as *= f4;
/* 4734 */       this.aw *= f4;
/* 4735 */       this.aA *= f4;
/* 4736 */       b(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 1));
/* 4737 */       bool1 = true;
/*      */     }
/*      */     float f10;
/*      */     float f11;
/*      */     float f12;
/*      */     int i4;
/*      */     int i5;
/*      */     int i6;
/*      */     int i7;
/* 4740 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2, paramInt3 - 1, 2)))
/*      */     {
/* 4741 */       if (this.l <= 0.0D) {
/* 4741 */         paramInt3--;
/*      */       }
/* 4742 */       this.N = this.a.a(paramInt1 - 1, paramInt2, paramInt3).I();
/* 4743 */       this.A = this.a.a(paramInt1, paramInt2 - 1, paramInt3).I();
/* 4744 */       this.I = this.a.a(paramInt1, paramInt2 + 1, paramInt3).I();
/* 4745 */       this.O = this.a.a(paramInt1 + 1, paramInt2, paramInt3).I();
/*      */       
/* 4747 */       this.ah = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3);
/* 4748 */       this.U = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3);
/* 4749 */       this.ac = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3);
/* 4750 */       this.ai = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3);
/*      */       
/* 4752 */       bool2 = this.a.a(paramInt1 + 1, paramInt2, paramInt3 - 1).l();
/* 4753 */       bool3 = this.a.a(paramInt1 - 1, paramInt2, paramInt3 - 1).l();
/* 4754 */       bool4 = this.a.a(paramInt1, paramInt2 + 1, paramInt3 - 1).l();
/* 4755 */       bool5 = this.a.a(paramInt1, paramInt2 - 1, paramInt3 - 1).l();
/* 4757 */       if ((bool3) || (bool5))
/*      */       {
/* 4758 */         this.x = this.a.a(paramInt1 - 1, paramInt2 - 1, paramInt3).I();
/* 4759 */         this.R = paramahu.c(this.a, paramInt1 - 1, paramInt2 - 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4761 */         this.x = this.N;
/* 4762 */         this.R = this.ah;
/*      */       }
/* 4764 */       if ((bool3) || (bool4))
/*      */       {
/* 4765 */         this.F = this.a.a(paramInt1 - 1, paramInt2 + 1, paramInt3).I();
/* 4766 */         this.Z = paramahu.c(this.a, paramInt1 - 1, paramInt2 + 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4768 */         this.F = this.N;
/* 4769 */         this.Z = this.ah;
/*      */       }
/* 4771 */       if ((bool2) || (bool5))
/*      */       {
/* 4772 */         this.C = this.a.a(paramInt1 + 1, paramInt2 - 1, paramInt3).I();
/* 4773 */         this.W = paramahu.c(this.a, paramInt1 + 1, paramInt2 - 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4775 */         this.C = this.O;
/* 4776 */         this.W = this.ai;
/*      */       }
/* 4778 */       if ((bool2) || (bool4))
/*      */       {
/* 4779 */         this.J = this.a.a(paramInt1 + 1, paramInt2 + 1, paramInt3).I();
/* 4780 */         this.ad = paramahu.c(this.a, paramInt1 + 1, paramInt2 + 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4782 */         this.J = this.O;
/* 4783 */         this.ad = this.ai;
/*      */       }
/* 4785 */       if (this.l <= 0.0D) {
/* 4785 */         paramInt3++;
/*      */       }
/* 4786 */       i3 = i2;
/* 4787 */       if ((this.l <= 0.0D) || (!this.a.a(paramInt1, paramInt2, paramInt3 - 1).c())) {
/* 4787 */         i3 = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1);
/*      */       }
/* 4788 */       f5 = this.a.a(paramInt1, paramInt2, paramInt3 - 1).I();
/*      */       
/*      */ 
/* 4791 */       float f6 = (this.N + this.F + f5 + this.I) / 4.0F;
/* 4792 */       f10 = (f5 + this.I + this.O + this.J) / 4.0F;
/* 4793 */       f11 = (this.A + f5 + this.C + this.O) / 4.0F;
/* 4794 */       f12 = (this.x + this.N + this.A + f5) / 4.0F;
/* 4795 */       f1 = (float)(f6 * this.k * (1.0D - this.h) + f10 * this.k * this.h + f11 * (1.0D - this.k) * this.h + f12 * (1.0D - this.k) * (1.0D - this.h));
/*      */       
/* 4797 */       f2 = (float)(f6 * this.k * (1.0D - this.i) + f10 * this.k * this.i + f11 * (1.0D - this.k) * this.i + f12 * (1.0D - this.k) * (1.0D - this.i));
/*      */       
/* 4799 */       f3 = (float)(f6 * this.j * (1.0D - this.i) + f10 * this.j * this.i + f11 * (1.0D - this.j) * this.i + f12 * (1.0D - this.j) * (1.0D - this.i));
/*      */       
/* 4801 */       f4 = (float)(f6 * this.j * (1.0D - this.h) + f10 * this.j * this.h + f11 * (1.0D - this.j) * this.h + f12 * (1.0D - this.j) * (1.0D - this.h));
/*      */       
/*      */ 
/* 4804 */       i4 = a(this.ah, this.Z, this.ac, i3);
/* 4805 */       i5 = a(this.ac, this.ai, this.ad, i3);
/* 4806 */       i6 = a(this.U, this.W, this.ai, i3);
/* 4807 */       i7 = a(this.R, this.ah, this.U, i3);
/* 4808 */       this.al = a(i4, i5, i6, i7, this.k * (1.0D - this.h), this.k * this.h, (1.0D - this.k) * this.h, (1.0D - this.k) * (1.0D - this.h));
/* 4809 */       this.am = a(i4, i5, i6, i7, this.k * (1.0D - this.i), this.k * this.i, (1.0D - this.k) * this.i, (1.0D - this.k) * (1.0D - this.i));
/* 4810 */       this.an = a(i4, i5, i6, i7, this.j * (1.0D - this.i), this.j * this.i, (1.0D - this.j) * this.i, (1.0D - this.j) * (1.0D - this.i));
/* 4811 */       this.ao = a(i4, i5, i6, i7, this.j * (1.0D - this.h), this.j * this.h, (1.0D - this.j) * this.h, (1.0D - this.j) * (1.0D - this.h));
/* 4814 */       if (i1 != 0)
/*      */       {
/* 4815 */         this.ap = (this.aq = this.ar = this.as = paramFloat1 * 0.8F);
/* 4816 */         this.at = (this.au = this.av = this.aw = paramFloat2 * 0.8F);
/* 4817 */         this.ax = (this.ay = this.az = this.aA = paramFloat3 * 0.8F);
/*      */       }
/*      */       else
/*      */       {
/* 4819 */         this.ap = (this.aq = this.ar = this.as = 0.8F);
/* 4820 */         this.at = (this.au = this.av = this.aw = 0.8F);
/* 4821 */         this.ax = (this.ay = this.az = this.aA = 0.8F);
/*      */       }
/* 4823 */       this.ap *= f1;
/* 4824 */       this.at *= f1;
/* 4825 */       this.ax *= f1;
/* 4826 */       this.aq *= f2;
/* 4827 */       this.au *= f2;
/* 4828 */       this.ay *= f2;
/* 4829 */       this.ar *= f3;
/* 4830 */       this.av *= f3;
/* 4831 */       this.az *= f3;
/* 4832 */       this.as *= f4;
/* 4833 */       this.aw *= f4;
/* 4834 */       this.aA *= f4;
/*      */       
/* 4836 */       ps localps1 = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 2);
/* 4837 */       c(paramahu, paramInt1, paramInt2, paramInt3, localps1);
/* 4839 */       if ((b) && (localps1.g().equals("grass_side")) && (!b()))
/*      */       {
/* 4840 */         this.ap *= paramFloat1;
/* 4841 */         this.aq *= paramFloat1;
/* 4842 */         this.ar *= paramFloat1;
/* 4843 */         this.as *= paramFloat1;
/* 4844 */         this.at *= paramFloat2;
/* 4845 */         this.au *= paramFloat2;
/* 4846 */         this.av *= paramFloat2;
/* 4847 */         this.aw *= paramFloat2;
/* 4848 */         this.ax *= paramFloat3;
/* 4849 */         this.ay *= paramFloat3;
/* 4850 */         this.az *= paramFloat3;
/* 4851 */         this.aA *= paramFloat3;
/* 4852 */         c(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 4855 */       bool1 = true;
/*      */     }
/* 4857 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2, paramInt3 + 1, 3)))
/*      */     {
/* 4858 */       if (this.m >= 1.0D) {
/* 4858 */         paramInt3++;
/*      */       }
/* 4860 */       this.P = this.a.a(paramInt1 - 1, paramInt2, paramInt3).I();
/* 4861 */       this.Q = this.a.a(paramInt1 + 1, paramInt2, paramInt3).I();
/* 4862 */       this.B = this.a.a(paramInt1, paramInt2 - 1, paramInt3).I();
/* 4863 */       this.L = this.a.a(paramInt1, paramInt2 + 1, paramInt3).I();
/*      */       
/* 4865 */       this.aj = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3);
/* 4866 */       this.ak = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3);
/* 4867 */       this.V = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3);
/* 4868 */       this.af = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3);
/*      */       
/* 4870 */       bool2 = this.a.a(paramInt1 + 1, paramInt2, paramInt3 + 1).l();
/* 4871 */       bool3 = this.a.a(paramInt1 - 1, paramInt2, paramInt3 + 1).l();
/* 4872 */       bool4 = this.a.a(paramInt1, paramInt2 + 1, paramInt3 + 1).l();
/* 4873 */       bool5 = this.a.a(paramInt1, paramInt2 - 1, paramInt3 + 1).l();
/* 4875 */       if ((bool3) || (bool5))
/*      */       {
/* 4876 */         this.z = this.a.a(paramInt1 - 1, paramInt2 - 1, paramInt3).I();
/* 4877 */         this.T = paramahu.c(this.a, paramInt1 - 1, paramInt2 - 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4879 */         this.z = this.P;
/* 4880 */         this.T = this.aj;
/*      */       }
/* 4882 */       if ((bool3) || (bool4))
/*      */       {
/* 4883 */         this.H = this.a.a(paramInt1 - 1, paramInt2 + 1, paramInt3).I();
/* 4884 */         this.ab = paramahu.c(this.a, paramInt1 - 1, paramInt2 + 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4886 */         this.H = this.P;
/* 4887 */         this.ab = this.aj;
/*      */       }
/* 4889 */       if ((bool2) || (bool5))
/*      */       {
/* 4890 */         this.E = this.a.a(paramInt1 + 1, paramInt2 - 1, paramInt3).I();
/* 4891 */         this.Y = paramahu.c(this.a, paramInt1 + 1, paramInt2 - 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4893 */         this.E = this.Q;
/* 4894 */         this.Y = this.ak;
/*      */       }
/* 4896 */       if ((bool2) || (bool4))
/*      */       {
/* 4897 */         this.M = this.a.a(paramInt1 + 1, paramInt2 + 1, paramInt3).I();
/* 4898 */         this.ag = paramahu.c(this.a, paramInt1 + 1, paramInt2 + 1, paramInt3);
/*      */       }
/*      */       else
/*      */       {
/* 4900 */         this.M = this.Q;
/* 4901 */         this.ag = this.ak;
/*      */       }
/* 4903 */       if (this.m >= 1.0D) {
/* 4903 */         paramInt3--;
/*      */       }
/* 4904 */       i3 = i2;
/* 4905 */       if ((this.m >= 1.0D) || (!this.a.a(paramInt1, paramInt2, paramInt3 + 1).c())) {
/* 4905 */         i3 = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1);
/*      */       }
/* 4906 */       f5 = this.a.a(paramInt1, paramInt2, paramInt3 + 1).I();
/*      */       
/*      */ 
/* 4909 */       float f7 = (this.P + this.H + f5 + this.L) / 4.0F;
/* 4910 */       f10 = (f5 + this.L + this.Q + this.M) / 4.0F;
/* 4911 */       f11 = (this.B + f5 + this.E + this.Q) / 4.0F;
/* 4912 */       f12 = (this.z + this.P + this.B + f5) / 4.0F;
/* 4913 */       f1 = (float)(f7 * this.k * (1.0D - this.h) + f10 * this.k * this.h + f11 * (1.0D - this.k) * this.h + f12 * (1.0D - this.k) * (1.0D - this.h));
/*      */       
/* 4915 */       f2 = (float)(f7 * this.j * (1.0D - this.h) + f10 * this.j * this.h + f11 * (1.0D - this.j) * this.h + f12 * (1.0D - this.j) * (1.0D - this.h));
/*      */       
/* 4917 */       f3 = (float)(f7 * this.j * (1.0D - this.i) + f10 * this.j * this.i + f11 * (1.0D - this.j) * this.i + f12 * (1.0D - this.j) * (1.0D - this.i));
/*      */       
/* 4919 */       f4 = (float)(f7 * this.k * (1.0D - this.i) + f10 * this.k * this.i + f11 * (1.0D - this.k) * this.i + f12 * (1.0D - this.k) * (1.0D - this.i));
/*      */       
/*      */ 
/* 4922 */       i4 = a(this.aj, this.ab, this.af, i3);
/* 4923 */       i5 = a(this.af, this.ak, this.ag, i3);
/* 4924 */       i6 = a(this.V, this.Y, this.ak, i3);
/* 4925 */       i7 = a(this.T, this.aj, this.V, i3);
/* 4926 */       this.al = a(i4, i7, i6, i5, this.k * (1.0D - this.h), (1.0D - this.k) * (1.0D - this.h), (1.0D - this.k) * this.h, this.k * this.h);
/* 4927 */       this.am = a(i4, i7, i6, i5, this.j * (1.0D - this.h), (1.0D - this.j) * (1.0D - this.h), (1.0D - this.j) * this.h, this.j * this.h);
/* 4928 */       this.an = a(i4, i7, i6, i5, this.j * (1.0D - this.i), (1.0D - this.j) * (1.0D - this.i), (1.0D - this.j) * this.i, this.j * this.i);
/* 4929 */       this.ao = a(i4, i7, i6, i5, this.k * (1.0D - this.i), (1.0D - this.k) * (1.0D - this.i), (1.0D - this.k) * this.i, this.k * this.i);
/* 4931 */       if (i1 != 0)
/*      */       {
/* 4932 */         this.ap = (this.aq = this.ar = this.as = paramFloat1 * 0.8F);
/* 4933 */         this.at = (this.au = this.av = this.aw = paramFloat2 * 0.8F);
/* 4934 */         this.ax = (this.ay = this.az = this.aA = paramFloat3 * 0.8F);
/*      */       }
/*      */       else
/*      */       {
/* 4936 */         this.ap = (this.aq = this.ar = this.as = 0.8F);
/* 4937 */         this.at = (this.au = this.av = this.aw = 0.8F);
/* 4938 */         this.ax = (this.ay = this.az = this.aA = 0.8F);
/*      */       }
/* 4940 */       this.ap *= f1;
/* 4941 */       this.at *= f1;
/* 4942 */       this.ax *= f1;
/* 4943 */       this.aq *= f2;
/* 4944 */       this.au *= f2;
/* 4945 */       this.ay *= f2;
/* 4946 */       this.ar *= f3;
/* 4947 */       this.av *= f3;
/* 4948 */       this.az *= f3;
/* 4949 */       this.as *= f4;
/* 4950 */       this.aw *= f4;
/* 4951 */       this.aA *= f4;
/* 4952 */       ps localps2 = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 3);
/* 4953 */       d(paramahu, paramInt1, paramInt2, paramInt3, localps2);
/* 4954 */       if ((b) && (localps2.g().equals("grass_side")) && (!b()))
/*      */       {
/* 4955 */         this.ap *= paramFloat1;
/* 4956 */         this.aq *= paramFloat1;
/* 4957 */         this.ar *= paramFloat1;
/* 4958 */         this.as *= paramFloat1;
/* 4959 */         this.at *= paramFloat2;
/* 4960 */         this.au *= paramFloat2;
/* 4961 */         this.av *= paramFloat2;
/* 4962 */         this.aw *= paramFloat2;
/* 4963 */         this.ax *= paramFloat3;
/* 4964 */         this.ay *= paramFloat3;
/* 4965 */         this.az *= paramFloat3;
/* 4966 */         this.aA *= paramFloat3;
/* 4967 */         d(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 4969 */       bool1 = true;
/*      */     }
/* 4971 */     if ((this.f) || (paramahu.a(this.a, paramInt1 - 1, paramInt2, paramInt3, 4)))
/*      */     {
/* 4972 */       if (this.h <= 0.0D) {
/* 4972 */         paramInt1--;
/*      */       }
/* 4973 */       this.y = this.a.a(paramInt1, paramInt2 - 1, paramInt3).I();
/* 4974 */       this.N = this.a.a(paramInt1, paramInt2, paramInt3 - 1).I();
/* 4975 */       this.P = this.a.a(paramInt1, paramInt2, paramInt3 + 1).I();
/* 4976 */       this.G = this.a.a(paramInt1, paramInt2 + 1, paramInt3).I();
/*      */       
/* 4978 */       this.S = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3);
/* 4979 */       this.ah = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1);
/* 4980 */       this.aj = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1);
/* 4981 */       this.aa = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3);
/*      */       
/* 4983 */       bool2 = this.a.a(paramInt1 - 1, paramInt2 + 1, paramInt3).l();
/* 4984 */       bool3 = this.a.a(paramInt1 - 1, paramInt2 - 1, paramInt3).l();
/* 4985 */       bool4 = this.a.a(paramInt1 - 1, paramInt2, paramInt3 - 1).l();
/* 4986 */       bool5 = this.a.a(paramInt1 - 1, paramInt2, paramInt3 + 1).l();
/* 4988 */       if ((bool4) || (bool3))
/*      */       {
/* 4989 */         this.x = this.a.a(paramInt1, paramInt2 - 1, paramInt3 - 1).I();
/* 4990 */         this.R = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 4992 */         this.x = this.N;
/* 4993 */         this.R = this.ah;
/*      */       }
/* 4995 */       if ((bool5) || (bool3))
/*      */       {
/* 4996 */         this.z = this.a.a(paramInt1, paramInt2 - 1, paramInt3 + 1).I();
/* 4997 */         this.T = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 4999 */         this.z = this.P;
/* 5000 */         this.T = this.aj;
/*      */       }
/* 5002 */       if ((bool4) || (bool2))
/*      */       {
/* 5003 */         this.F = this.a.a(paramInt1, paramInt2 + 1, paramInt3 - 1).I();
/* 5004 */         this.Z = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 5006 */         this.F = this.N;
/* 5007 */         this.Z = this.ah;
/*      */       }
/* 5009 */       if ((bool5) || (bool2))
/*      */       {
/* 5010 */         this.H = this.a.a(paramInt1, paramInt2 + 1, paramInt3 + 1).I();
/* 5011 */         this.ab = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 5013 */         this.H = this.P;
/* 5014 */         this.ab = this.aj;
/*      */       }
/* 5016 */       if (this.h <= 0.0D) {
/* 5016 */         paramInt1++;
/*      */       }
/* 5017 */       i3 = i2;
/* 5018 */       if ((this.h <= 0.0D) || (!this.a.a(paramInt1 - 1, paramInt2, paramInt3).c())) {
/* 5018 */         i3 = paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3);
/*      */       }
/* 5019 */       f5 = this.a.a(paramInt1 - 1, paramInt2, paramInt3).I();
/*      */       
/*      */ 
/* 5022 */       float f8 = (this.y + this.z + f5 + this.P) / 4.0F;
/* 5023 */       f10 = (f5 + this.P + this.G + this.H) / 4.0F;
/* 5024 */       f11 = (this.N + f5 + this.F + this.G) / 4.0F;
/* 5025 */       f12 = (this.x + this.y + this.N + f5) / 4.0F;
/* 5026 */       f1 = (float)(f10 * this.k * this.m + f11 * this.k * (1.0D - this.m) + f12 * (1.0D - this.k) * (1.0D - this.m) + f8 * (1.0D - this.k) * this.m);
/*      */       
/*      */ 
/* 5029 */       f2 = (float)(f10 * this.k * this.l + f11 * this.k * (1.0D - this.l) + f12 * (1.0D - this.k) * (1.0D - this.l) + f8 * (1.0D - this.k) * this.l);
/*      */       
/*      */ 
/* 5032 */       f3 = (float)(f10 * this.j * this.l + f11 * this.j * (1.0D - this.l) + f12 * (1.0D - this.j) * (1.0D - this.l) + f8 * (1.0D - this.j) * this.l);
/*      */       
/*      */ 
/* 5035 */       f4 = (float)(f10 * this.j * this.m + f11 * this.j * (1.0D - this.m) + f12 * (1.0D - this.j) * (1.0D - this.m) + f8 * (1.0D - this.j) * this.m);
/*      */       
/*      */ 
/*      */ 
/* 5039 */       i4 = a(this.S, this.T, this.aj, i3);
/* 5040 */       i5 = a(this.aj, this.aa, this.ab, i3);
/* 5041 */       i6 = a(this.ah, this.Z, this.aa, i3);
/* 5042 */       i7 = a(this.R, this.S, this.ah, i3);
/* 5043 */       this.al = a(i5, i6, i7, i4, this.k * this.m, this.k * (1.0D - this.m), (1.0D - this.k) * (1.0D - this.m), (1.0D - this.k) * this.m);
/* 5044 */       this.am = a(i5, i6, i7, i4, this.k * this.l, this.k * (1.0D - this.l), (1.0D - this.k) * (1.0D - this.l), (1.0D - this.k) * this.l);
/* 5045 */       this.an = a(i5, i6, i7, i4, this.j * this.l, this.j * (1.0D - this.l), (1.0D - this.j) * (1.0D - this.l), (1.0D - this.j) * this.l);
/* 5046 */       this.ao = a(i5, i6, i7, i4, this.j * this.m, this.j * (1.0D - this.m), (1.0D - this.j) * (1.0D - this.m), (1.0D - this.j) * this.m);
/* 5049 */       if (i1 != 0)
/*      */       {
/* 5050 */         this.ap = (this.aq = this.ar = this.as = paramFloat1 * 0.6F);
/* 5051 */         this.at = (this.au = this.av = this.aw = paramFloat2 * 0.6F);
/* 5052 */         this.ax = (this.ay = this.az = this.aA = paramFloat3 * 0.6F);
/*      */       }
/*      */       else
/*      */       {
/* 5054 */         this.ap = (this.aq = this.ar = this.as = 0.6F);
/* 5055 */         this.at = (this.au = this.av = this.aw = 0.6F);
/* 5056 */         this.ax = (this.ay = this.az = this.aA = 0.6F);
/*      */       }
/* 5058 */       this.ap *= f1;
/* 5059 */       this.at *= f1;
/* 5060 */       this.ax *= f1;
/* 5061 */       this.aq *= f2;
/* 5062 */       this.au *= f2;
/* 5063 */       this.ay *= f2;
/* 5064 */       this.ar *= f3;
/* 5065 */       this.av *= f3;
/* 5066 */       this.az *= f3;
/* 5067 */       this.as *= f4;
/* 5068 */       this.aw *= f4;
/* 5069 */       this.aA *= f4;
/* 5070 */       ps localps3 = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 4);
/* 5071 */       e(paramahu, paramInt1, paramInt2, paramInt3, localps3);
/* 5072 */       if ((b) && (localps3.g().equals("grass_side")) && (!b()))
/*      */       {
/* 5073 */         this.ap *= paramFloat1;
/* 5074 */         this.aq *= paramFloat1;
/* 5075 */         this.ar *= paramFloat1;
/* 5076 */         this.as *= paramFloat1;
/* 5077 */         this.at *= paramFloat2;
/* 5078 */         this.au *= paramFloat2;
/* 5079 */         this.av *= paramFloat2;
/* 5080 */         this.aw *= paramFloat2;
/* 5081 */         this.ax *= paramFloat3;
/* 5082 */         this.ay *= paramFloat3;
/* 5083 */         this.az *= paramFloat3;
/* 5084 */         this.aA *= paramFloat3;
/* 5085 */         e(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 5087 */       bool1 = true;
/*      */     }
/* 5089 */     if ((this.f) || (paramahu.a(this.a, paramInt1 + 1, paramInt2, paramInt3, 5)))
/*      */     {
/* 5090 */       if (this.i >= 1.0D) {
/* 5090 */         paramInt1++;
/*      */       }
/* 5091 */       this.D = this.a.a(paramInt1, paramInt2 - 1, paramInt3).I();
/* 5092 */       this.O = this.a.a(paramInt1, paramInt2, paramInt3 - 1).I();
/* 5093 */       this.Q = this.a.a(paramInt1, paramInt2, paramInt3 + 1).I();
/* 5094 */       this.K = this.a.a(paramInt1, paramInt2 + 1, paramInt3).I();
/*      */       
/* 5096 */       this.X = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3);
/* 5097 */       this.ai = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1);
/* 5098 */       this.ak = paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1);
/* 5099 */       this.ae = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3);
/*      */       
/* 5101 */       bool2 = this.a.a(paramInt1 + 1, paramInt2 + 1, paramInt3).l();
/* 5102 */       bool3 = this.a.a(paramInt1 + 1, paramInt2 - 1, paramInt3).l();
/* 5103 */       bool4 = this.a.a(paramInt1 + 1, paramInt2, paramInt3 + 1).l();
/* 5104 */       bool5 = this.a.a(paramInt1 + 1, paramInt2, paramInt3 - 1).l();
/* 5107 */       if ((bool3) || (bool5))
/*      */       {
/* 5108 */         this.C = this.a.a(paramInt1, paramInt2 - 1, paramInt3 - 1).I();
/* 5109 */         this.W = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 5111 */         this.C = this.O;
/* 5112 */         this.W = this.ai;
/*      */       }
/* 5114 */       if ((bool3) || (bool4))
/*      */       {
/* 5115 */         this.E = this.a.a(paramInt1, paramInt2 - 1, paramInt3 + 1).I();
/* 5116 */         this.Y = paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 5118 */         this.E = this.Q;
/* 5119 */         this.Y = this.ak;
/*      */       }
/* 5121 */       if ((bool2) || (bool5))
/*      */       {
/* 5122 */         this.J = this.a.a(paramInt1, paramInt2 + 1, paramInt3 - 1).I();
/* 5123 */         this.ad = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3 - 1);
/*      */       }
/*      */       else
/*      */       {
/* 5125 */         this.J = this.O;
/* 5126 */         this.ad = this.ai;
/*      */       }
/* 5128 */       if ((bool2) || (bool4))
/*      */       {
/* 5129 */         this.M = this.a.a(paramInt1, paramInt2 + 1, paramInt3 + 1).I();
/* 5130 */         this.ag = paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3 + 1);
/*      */       }
/*      */       else
/*      */       {
/* 5132 */         this.M = this.Q;
/* 5133 */         this.ag = this.ak;
/*      */       }
/* 5135 */       if (this.i >= 1.0D) {
/* 5135 */         paramInt1--;
/*      */       }
/* 5136 */       i3 = i2;
/* 5137 */       if ((this.i >= 1.0D) || (!this.a.a(paramInt1 + 1, paramInt2, paramInt3).c())) {
/* 5137 */         i3 = paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3);
/*      */       }
/* 5138 */       f5 = this.a.a(paramInt1 + 1, paramInt2, paramInt3).I();
/*      */       
/*      */ 
/* 5141 */       float f9 = (this.D + this.E + f5 + this.Q) / 4.0F;
/* 5142 */       f10 = (this.C + this.D + this.O + f5) / 4.0F;
/* 5143 */       f11 = (this.O + f5 + this.J + this.K) / 4.0F;
/* 5144 */       f12 = (f5 + this.Q + this.K + this.M) / 4.0F;
/* 5145 */       f1 = (float)(f9 * (1.0D - this.j) * this.m + f10 * (1.0D - this.j) * (1.0D - this.m) + f11 * this.j * (1.0D - this.m) + f12 * this.j * this.m);
/*      */       
/*      */ 
/* 5148 */       f2 = (float)(f9 * (1.0D - this.j) * this.l + f10 * (1.0D - this.j) * (1.0D - this.l) + f11 * this.j * (1.0D - this.l) + f12 * this.j * this.l);
/*      */       
/*      */ 
/* 5151 */       f3 = (float)(f9 * (1.0D - this.k) * this.l + f10 * (1.0D - this.k) * (1.0D - this.l) + f11 * this.k * (1.0D - this.l) + f12 * this.k * this.l);
/*      */       
/*      */ 
/* 5154 */       f4 = (float)(f9 * (1.0D - this.k) * this.m + f10 * (1.0D - this.k) * (1.0D - this.m) + f11 * this.k * (1.0D - this.m) + f12 * this.k * this.m);
/*      */       
/*      */ 
/*      */ 
/* 5158 */       i4 = a(this.X, this.Y, this.ak, i3);
/* 5159 */       i5 = a(this.ak, this.ae, this.ag, i3);
/* 5160 */       i6 = a(this.ai, this.ad, this.ae, i3);
/* 5161 */       i7 = a(this.W, this.X, this.ai, i3);
/* 5162 */       this.al = a(i4, i7, i6, i5, (1.0D - this.j) * this.m, (1.0D - this.j) * (1.0D - this.m), this.j * (1.0D - this.m), this.j * this.m);
/* 5163 */       this.am = a(i4, i7, i6, i5, (1.0D - this.j) * this.l, (1.0D - this.j) * (1.0D - this.l), this.j * (1.0D - this.l), this.j * this.l);
/* 5164 */       this.an = a(i4, i7, i6, i5, (1.0D - this.k) * this.l, (1.0D - this.k) * (1.0D - this.l), this.k * (1.0D - this.l), this.k * this.l);
/* 5165 */       this.ao = a(i4, i7, i6, i5, (1.0D - this.k) * this.m, (1.0D - this.k) * (1.0D - this.m), this.k * (1.0D - this.m), this.k * this.m);
/* 5167 */       if (i1 != 0)
/*      */       {
/* 5168 */         this.ap = (this.aq = this.ar = this.as = paramFloat1 * 0.6F);
/* 5169 */         this.at = (this.au = this.av = this.aw = paramFloat2 * 0.6F);
/* 5170 */         this.ax = (this.ay = this.az = this.aA = paramFloat3 * 0.6F);
/*      */       }
/*      */       else
/*      */       {
/* 5172 */         this.ap = (this.aq = this.ar = this.as = 0.6F);
/* 5173 */         this.at = (this.au = this.av = this.aw = 0.6F);
/* 5174 */         this.ax = (this.ay = this.az = this.aA = 0.6F);
/*      */       }
/* 5176 */       this.ap *= f1;
/* 5177 */       this.at *= f1;
/* 5178 */       this.ax *= f1;
/* 5179 */       this.aq *= f2;
/* 5180 */       this.au *= f2;
/* 5181 */       this.ay *= f2;
/* 5182 */       this.ar *= f3;
/* 5183 */       this.av *= f3;
/* 5184 */       this.az *= f3;
/* 5185 */       this.as *= f4;
/* 5186 */       this.aw *= f4;
/* 5187 */       this.aA *= f4;
/*      */       
/* 5189 */       ps localps4 = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 5);
/* 5190 */       f(paramahu, paramInt1, paramInt2, paramInt3, localps4);
/* 5191 */       if ((b) && (localps4.g().equals("grass_side")) && (!b()))
/*      */       {
/* 5192 */         this.ap *= paramFloat1;
/* 5193 */         this.aq *= paramFloat1;
/* 5194 */         this.ar *= paramFloat1;
/* 5195 */         this.as *= paramFloat1;
/* 5196 */         this.at *= paramFloat2;
/* 5197 */         this.au *= paramFloat2;
/* 5198 */         this.av *= paramFloat2;
/* 5199 */         this.aw *= paramFloat2;
/* 5200 */         this.ax *= paramFloat3;
/* 5201 */         this.ay *= paramFloat3;
/* 5202 */         this.az *= paramFloat3;
/* 5203 */         this.aA *= paramFloat3;
/* 5204 */         f(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 5206 */       bool1 = true;
/*      */     }
/* 5208 */     this.w = false;
/* 5209 */     return bool1;
/*      */   }
/*      */   
/*      */   private int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
/*      */   {
/* 5217 */     if (paramInt1 == 0) {
/* 5217 */       paramInt1 = paramInt4;
/*      */     }
/* 5218 */     if (paramInt2 == 0) {
/* 5218 */       paramInt2 = paramInt4;
/*      */     }
/* 5219 */     if (paramInt3 == 0) {
/* 5219 */       paramInt3 = paramInt4;
/*      */     }
/* 5220 */     return paramInt1 + paramInt2 + paramInt3 + paramInt4 >> 2 & 0xFF00FF;
/*      */   }
/*      */   
/*      */   private int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
/*      */   {
/* 5225 */     int i1 = (int)((paramInt1 >> 16 & 0xFF) * paramDouble1 + (paramInt2 >> 16 & 0xFF) * paramDouble2 + (paramInt3 >> 16 & 0xFF) * paramDouble3 + (paramInt4 >> 16 & 0xFF) * paramDouble4) & 0xFF;
/* 5226 */     int i2 = (int)((paramInt1 & 0xFF) * paramDouble1 + (paramInt2 & 0xFF) * paramDouble2 + (paramInt3 & 0xFF) * paramDouble3 + (paramInt4 & 0xFF) * paramDouble4) & 0xFF;
/* 5227 */     return i1 << 16 | i2;
/*      */   }
/*      */   
/*      */   public boolean d(ahu paramahu, int paramInt1, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, float paramFloat3)
/*      */   {
/* 5231 */     this.w = false;
/*      */     
/* 5233 */     blz localblz = blz.a;
/*      */     
/* 5235 */     boolean bool = false;
/* 5236 */     float f1 = 0.5F;
/* 5237 */     float f2 = 1.0F;
/* 5238 */     float f3 = 0.8F;
/* 5239 */     float f4 = 0.6F;
/*      */     
/* 5241 */     float f5 = f2 * paramFloat1;
/* 5242 */     float f6 = f2 * paramFloat2;
/* 5243 */     float f7 = f2 * paramFloat3;
/*      */     
/* 5245 */     float f8 = f1;
/* 5246 */     float f9 = f3;
/* 5247 */     float f10 = f4;
/*      */     
/* 5249 */     float f11 = f1;
/* 5250 */     float f12 = f3;
/* 5251 */     float f13 = f4;
/*      */     
/* 5253 */     float f14 = f1;
/* 5254 */     float f15 = f3;
/* 5255 */     float f16 = f4;
/* 5257 */     if (paramahu != ahz.c)
/*      */     {
/* 5258 */       f8 *= paramFloat1;
/* 5259 */       f9 *= paramFloat1;
/* 5260 */       f10 *= paramFloat1;
/*      */       
/* 5262 */       f11 *= paramFloat2;
/* 5263 */       f12 *= paramFloat2;
/* 5264 */       f13 *= paramFloat2;
/*      */       
/* 5266 */       f14 *= paramFloat3;
/* 5267 */       f15 *= paramFloat3;
/* 5268 */       f16 *= paramFloat3;
/*      */     }
/* 5272 */     int i1 = paramahu.c(this.a, paramInt1, paramInt2, paramInt3);
/* 5274 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2 - 1, paramInt3, 0)))
/*      */     {
/* 5275 */       localblz.b(this.j > 0.0D ? i1 : paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3));
/* 5276 */       localblz.b(f8, f11, f14);
/*      */       
/* 5278 */       a(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 0));
/* 5279 */       bool = true;
/*      */     }
/* 5282 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2 + 1, paramInt3, 1)))
/*      */     {
/* 5283 */       localblz.b(this.k < 1.0D ? i1 : paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3));
/* 5284 */       localblz.b(f5, f6, f7);
/*      */       
/* 5286 */       b(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 1));
/* 5287 */       bool = true;
/*      */     }
/*      */     ps localps;
/* 5290 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2, paramInt3 - 1, 2)))
/*      */     {
/* 5291 */       localblz.b(this.l > 0.0D ? i1 : paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1));
/* 5292 */       localblz.b(f9, f12, f15);
/*      */       
/* 5294 */       localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 2);
/* 5295 */       c(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 5296 */       if ((b) && (localps.g().equals("grass_side")) && (!b()))
/*      */       {
/* 5297 */         localblz.b(f9 * paramFloat1, f12 * paramFloat2, f15 * paramFloat3);
/* 5298 */         c(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 5300 */       bool = true;
/*      */     }
/* 5303 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2, paramInt3 + 1, 3)))
/*      */     {
/* 5304 */       localblz.b(this.m < 1.0D ? i1 : paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1));
/* 5305 */       localblz.b(f9, f12, f15);
/*      */       
/* 5307 */       localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 3);
/* 5308 */       d(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 5309 */       if ((b) && (localps.g().equals("grass_side")) && (!b()))
/*      */       {
/* 5310 */         localblz.b(f9 * paramFloat1, f12 * paramFloat2, f15 * paramFloat3);
/* 5311 */         d(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 5313 */       bool = true;
/*      */     }
/* 5316 */     if ((this.f) || (paramahu.a(this.a, paramInt1 - 1, paramInt2, paramInt3, 4)))
/*      */     {
/* 5317 */       localblz.b(this.h > 0.0D ? i1 : paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3));
/* 5318 */       localblz.b(f10, f13, f16);
/*      */       
/* 5320 */       localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 4);
/* 5321 */       e(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 5322 */       if ((b) && (localps.g().equals("grass_side")) && (!b()))
/*      */       {
/* 5323 */         localblz.b(f10 * paramFloat1, f13 * paramFloat2, f16 * paramFloat3);
/* 5324 */         e(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 5326 */       bool = true;
/*      */     }
/* 5329 */     if ((this.f) || (paramahu.a(this.a, paramInt1 + 1, paramInt2, paramInt3, 5)))
/*      */     {
/* 5330 */       localblz.b(this.i < 1.0D ? i1 : paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3));
/* 5331 */       localblz.b(f10, f13, f16);
/*      */       
/* 5333 */       localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 5);
/* 5334 */       f(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 5335 */       if ((b) && (localps.g().equals("grass_side")) && (!b()))
/*      */       {
/* 5336 */         localblz.b(f10 * paramFloat1, f13 * paramFloat2, f16 * paramFloat3);
/* 5337 */         f(paramahu, paramInt1, paramInt2, paramInt3, ajt.e());
/*      */       }
/* 5339 */       bool = true;
/*      */     }
/* 5342 */     return bool;
/*      */   }
/*      */   
/*      */   private boolean a(ail paramail, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 5346 */     blz localblz = blz.a;
/*      */     
/* 5348 */     localblz.b(paramail.c(this.a, paramInt1, paramInt2, paramInt3));
/* 5349 */     localblz.b(1.0F, 1.0F, 1.0F);
/*      */     
/* 5351 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 5352 */     int i2 = aiw.l(i1);
/* 5353 */     int i3 = ail.c(i1);
/* 5354 */     ps localps = paramail.b(i3);
/*      */     
/* 5356 */     int i4 = 4 + i3 * 2;
/* 5357 */     int i5 = 5 + i3 * 2;
/*      */     
/* 5359 */     double d1 = 15.0D - i4;
/* 5360 */     double d2 = 15.0D;
/* 5361 */     double d3 = 4.0D;
/* 5362 */     double d4 = 4.0D + i5;
/* 5363 */     double d5 = localps.a(d1);
/* 5364 */     double d6 = localps.a(d2);
/* 5365 */     double d7 = localps.b(d3);
/* 5366 */     double d8 = localps.b(d4);
/*      */     
/*      */ 
/* 5369 */     double d9 = 0.0D;
/* 5370 */     double d10 = 0.0D;
/* 5372 */     switch (i2)
/*      */     {
/*      */     case 2: 
/* 5374 */       d9 = 8.0D - i4 / 2;
/* 5375 */       d10 = 1.0D;
/* 5376 */       break;
/*      */     case 0: 
/* 5378 */       d9 = 8.0D - i4 / 2;
/* 5379 */       d10 = 15.0D - i4;
/* 5380 */       break;
/*      */     case 3: 
/* 5382 */       d9 = 15.0D - i4;
/* 5383 */       d10 = 8.0D - i4 / 2;
/* 5384 */       break;
/*      */     case 1: 
/* 5386 */       d9 = 1.0D;
/* 5387 */       d10 = 8.0D - i4 / 2;
/*      */     }
/* 5391 */     double d11 = paramInt1 + d9 / 16.0D;
/* 5392 */     double d12 = paramInt1 + (d9 + i4) / 16.0D;
/* 5393 */     double d13 = paramInt2 + (12.0D - i5) / 16.0D;
/* 5394 */     double d14 = paramInt2 + 0.75D;
/* 5395 */     double d15 = paramInt3 + d10 / 16.0D;
/* 5396 */     double d16 = paramInt3 + (d10 + i4) / 16.0D;
/*      */     
/*      */ 
/*      */ 
/* 5400 */     localblz.a(d11, d13, d15, d5, d8);
/* 5401 */     localblz.a(d11, d13, d16, d6, d8);
/* 5402 */     localblz.a(d11, d14, d16, d6, d7);
/* 5403 */     localblz.a(d11, d14, d15, d5, d7);
/*      */     
/*      */ 
/*      */ 
/* 5407 */     localblz.a(d12, d13, d16, d5, d8);
/* 5408 */     localblz.a(d12, d13, d15, d6, d8);
/* 5409 */     localblz.a(d12, d14, d15, d6, d7);
/* 5410 */     localblz.a(d12, d14, d16, d5, d7);
/*      */     
/*      */ 
/*      */ 
/* 5414 */     localblz.a(d12, d13, d15, d5, d8);
/* 5415 */     localblz.a(d11, d13, d15, d6, d8);
/* 5416 */     localblz.a(d11, d14, d15, d6, d7);
/* 5417 */     localblz.a(d12, d14, d15, d5, d7);
/*      */     
/*      */ 
/*      */ 
/* 5421 */     localblz.a(d11, d13, d16, d5, d8);
/* 5422 */     localblz.a(d12, d13, d16, d6, d8);
/* 5423 */     localblz.a(d12, d14, d16, d6, d7);
/* 5424 */     localblz.a(d11, d14, d16, d5, d7);
/*      */     
/*      */ 
/* 5427 */     int i6 = i4;
/* 5428 */     if (i3 >= 2) {
/* 5430 */       i6--;
/*      */     }
/* 5433 */     d5 = localps.c();
/* 5434 */     d6 = localps.a(i6);
/* 5435 */     d7 = localps.e();
/* 5436 */     d8 = localps.b(i6);
/*      */     
/*      */ 
/*      */ 
/* 5440 */     localblz.a(d11, d14, d16, d5, d8);
/* 5441 */     localblz.a(d12, d14, d16, d6, d8);
/* 5442 */     localblz.a(d12, d14, d15, d6, d7);
/* 5443 */     localblz.a(d11, d14, d15, d5, d7);
/*      */     
/*      */ 
/*      */ 
/* 5447 */     localblz.a(d11, d13, d15, d5, d7);
/* 5448 */     localblz.a(d12, d13, d15, d6, d7);
/* 5449 */     localblz.a(d12, d13, d16, d6, d8);
/* 5450 */     localblz.a(d11, d13, d16, d5, d8);
/*      */     
/*      */ 
/*      */ 
/* 5454 */     d5 = localps.a(12.0D);
/* 5455 */     d6 = localps.d();
/* 5456 */     d7 = localps.e();
/* 5457 */     d8 = localps.b(4.0D);
/*      */     
/* 5459 */     d9 = 8.0D;
/* 5460 */     d10 = 0.0D;
/*      */     double d17;
/* 5462 */     switch (i2)
/*      */     {
/*      */     case 2: 
/* 5464 */       d9 = 8.0D;
/* 5465 */       d10 = 0.0D;
/* 5466 */       break;
/*      */     case 0: 
/* 5468 */       d9 = 8.0D;
/* 5469 */       d10 = 12.0D;
/*      */       
/* 5471 */       d17 = d5;
/* 5472 */       d5 = d6;
/* 5473 */       d6 = d17;
/*      */       
/* 5475 */       break;
/*      */     case 3: 
/* 5477 */       d9 = 12.0D;
/* 5478 */       d10 = 8.0D;
/*      */       
/* 5480 */       d17 = d5;
/* 5481 */       d5 = d6;
/* 5482 */       d6 = d17;
/*      */       
/* 5484 */       break;
/*      */     case 1: 
/* 5486 */       d9 = 0.0D;
/* 5487 */       d10 = 8.0D;
/*      */     }
/* 5491 */     d11 = paramInt1 + d9 / 16.0D;
/* 5492 */     d12 = paramInt1 + (d9 + 4.0D) / 16.0D;
/* 5493 */     d13 = paramInt2 + 0.75D;
/* 5494 */     d14 = paramInt2 + 1.0D;
/* 5495 */     d15 = paramInt3 + d10 / 16.0D;
/* 5496 */     d16 = paramInt3 + (d10 + 4.0D) / 16.0D;
/* 5497 */     if ((i2 == 2) || (i2 == 0))
/*      */     {
/* 5500 */       localblz.a(d11, d13, d15, d6, d8);
/* 5501 */       localblz.a(d11, d13, d16, d5, d8);
/* 5502 */       localblz.a(d11, d14, d16, d5, d7);
/* 5503 */       localblz.a(d11, d14, d15, d6, d7);
/*      */       
/*      */ 
/*      */ 
/* 5507 */       localblz.a(d11, d13, d16, d5, d8);
/* 5508 */       localblz.a(d11, d13, d15, d6, d8);
/* 5509 */       localblz.a(d11, d14, d15, d6, d7);
/* 5510 */       localblz.a(d11, d14, d16, d5, d7);
/*      */     }
/* 5512 */     else if ((i2 == 1) || (i2 == 3))
/*      */     {
/* 5515 */       localblz.a(d12, d13, d15, d5, d8);
/* 5516 */       localblz.a(d11, d13, d15, d6, d8);
/* 5517 */       localblz.a(d11, d14, d15, d6, d7);
/* 5518 */       localblz.a(d12, d14, d15, d5, d7);
/*      */       
/*      */ 
/*      */ 
/* 5522 */       localblz.a(d11, d13, d15, d6, d8);
/* 5523 */       localblz.a(d12, d13, d15, d5, d8);
/* 5524 */       localblz.a(d12, d14, d15, d5, d7);
/* 5525 */       localblz.a(d11, d14, d15, d6, d7);
/*      */     }
/* 5530 */     return true;
/*      */   }
/*      */   
/*      */   private boolean a(ahs paramahs, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 5535 */     float f1 = 0.1875F;
/*      */     
/* 5537 */     a(b(ahz.w));
/* 5538 */     a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/* 5539 */     q(paramahs, paramInt1, paramInt2, paramInt3);
/*      */     
/*      */ 
/* 5542 */     this.f = true;
/* 5543 */     a(b(ahz.Z));
/* 5544 */     a(0.125D, 0.006250000093132258D, 0.125D, 0.875D, f1, 0.875D);
/* 5545 */     q(paramahs, paramInt1, paramInt2, paramInt3);
/*      */     
/* 5547 */     a(b(ahz.bJ));
/* 5548 */     a(0.1875D, f1, 0.1875D, 0.8125D, 0.875D, 0.8125D);
/* 5549 */     q(paramahs, paramInt1, paramInt2, paramInt3);
/* 5550 */     this.f = false;
/*      */     
/* 5552 */     a();
/*      */     
/* 5554 */     return true;
/*      */   }
/*      */   
/*      */   public boolean t(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 5558 */     int i1 = paramahu.d(this.a, paramInt1, paramInt2, paramInt3);
/* 5559 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/* 5560 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/* 5561 */     float f3 = (i1 & 0xFF) / 255.0F;
/* 5563 */     if (bll.a)
/*      */     {
/* 5564 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/* 5565 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/* 5566 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/* 5568 */       f1 = f4;
/* 5569 */       f2 = f5;
/* 5570 */       f3 = f6;
/*      */     }
/* 5573 */     return e(paramahu, paramInt1, paramInt2, paramInt3, f1, f2, f3);
/*      */   }
/*      */   
/*      */   public boolean e(ahu paramahu, int paramInt1, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, float paramFloat3)
/*      */   {
/* 5577 */     blz localblz = blz.a;
/*      */     
/* 5579 */     int i1 = 0;
/* 5580 */     float f1 = 0.5F;
/* 5581 */     float f2 = 1.0F;
/* 5582 */     float f3 = 0.8F;
/* 5583 */     float f4 = 0.6F;
/*      */     
/* 5585 */     float f5 = f1 * paramFloat1;
/* 5586 */     float f6 = f2 * paramFloat1;
/* 5587 */     float f7 = f3 * paramFloat1;
/* 5588 */     float f8 = f4 * paramFloat1;
/*      */     
/* 5590 */     float f9 = f1 * paramFloat2;
/* 5591 */     float f10 = f2 * paramFloat2;
/* 5592 */     float f11 = f3 * paramFloat2;
/* 5593 */     float f12 = f4 * paramFloat2;
/*      */     
/* 5595 */     float f13 = f1 * paramFloat3;
/* 5596 */     float f14 = f2 * paramFloat3;
/* 5597 */     float f15 = f3 * paramFloat3;
/* 5598 */     float f16 = f4 * paramFloat3;
/*      */     
/* 5600 */     float f17 = 0.0625F;
/*      */     
/* 5602 */     int i2 = paramahu.c(this.a, paramInt1, paramInt2, paramInt3);
/* 5604 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2 - 1, paramInt3, 0)))
/*      */     {
/* 5605 */       localblz.b(this.j > 0.0D ? i2 : paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3));
/* 5606 */       localblz.b(f5, f9, f13);
/* 5607 */       a(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 0));
/*      */     }
/* 5610 */     if ((this.f) || (paramahu.a(this.a, paramInt1, paramInt2 + 1, paramInt3, 1)))
/*      */     {
/* 5611 */       localblz.b(this.k < 1.0D ? i2 : paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3));
/* 5612 */       localblz.b(f6, f10, f14);
/* 5613 */       b(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 1));
/*      */     }
/* 5617 */     localblz.b(i2);
/* 5618 */     localblz.b(f7, f11, f15);
/* 5619 */     localblz.d(0.0F, 0.0F, f17);
/* 5620 */     c(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 2));
/* 5621 */     localblz.d(0.0F, 0.0F, -f17);
/*      */     
/* 5623 */     localblz.d(0.0F, 0.0F, -f17);
/* 5624 */     d(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 3));
/* 5625 */     localblz.d(0.0F, 0.0F, f17);
/*      */     
/*      */ 
/* 5628 */     localblz.b(f8, f12, f16);
/* 5629 */     localblz.d(f17, 0.0F, 0.0F);
/* 5630 */     e(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 4));
/* 5631 */     localblz.d(-f17, 0.0F, 0.0F);
/*      */     
/* 5633 */     localblz.d(-f17, 0.0F, 0.0F);
/* 5634 */     f(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 5));
/* 5635 */     localblz.d(f17, 0.0F, 0.0F);
/*      */     
/* 5637 */     return true;
/*      */   }
/*      */   
/*      */   public boolean a(ajl paramajl, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 5641 */     boolean bool1 = false;
/*      */     
/* 5643 */     float f1 = 0.375F;
/* 5644 */     float f2 = 0.625F;
/* 5645 */     a(f1, 0.0D, f1, f2, 1.0D, f2);
/* 5646 */     q(paramajl, paramInt1, paramInt2, paramInt3);
/* 5647 */     bool1 = true;
/*      */     
/* 5649 */     int i1 = 0;
/* 5650 */     int i2 = 0;
/* 5652 */     if ((paramajl.e(this.a, paramInt1 - 1, paramInt2, paramInt3)) || (paramajl.e(this.a, paramInt1 + 1, paramInt2, paramInt3))) {
/* 5652 */       i1 = 1;
/*      */     }
/* 5653 */     if ((paramajl.e(this.a, paramInt1, paramInt2, paramInt3 - 1)) || (paramajl.e(this.a, paramInt1, paramInt2, paramInt3 + 1))) {
/* 5653 */       i2 = 1;
/*      */     }
/* 5655 */     boolean bool2 = paramajl.e(this.a, paramInt1 - 1, paramInt2, paramInt3);
/* 5656 */     boolean bool3 = paramajl.e(this.a, paramInt1 + 1, paramInt2, paramInt3);
/* 5657 */     boolean bool4 = paramajl.e(this.a, paramInt1, paramInt2, paramInt3 - 1);
/* 5658 */     boolean bool5 = paramajl.e(this.a, paramInt1, paramInt2, paramInt3 + 1);
/* 5660 */     if ((i1 == 0) && (i2 == 0)) {
/* 5660 */       i1 = 1;
/*      */     }
/* 5662 */     f1 = 0.4375F;
/* 5663 */     f2 = 0.5625F;
/* 5664 */     float f3 = 0.75F;
/* 5665 */     float f4 = 0.9375F;
/*      */     
/* 5667 */     float f5 = bool2 ? 0.0F : f1;
/* 5668 */     float f6 = bool3 ? 1.0F : f2;
/* 5669 */     float f7 = bool4 ? 0.0F : f1;
/* 5670 */     float f8 = bool5 ? 1.0F : f2;
/* 5671 */     if (i1 != 0)
/*      */     {
/* 5672 */       a(f5, f3, f1, f6, f4, f2);
/* 5673 */       q(paramajl, paramInt1, paramInt2, paramInt3);
/* 5674 */       bool1 = true;
/*      */     }
/* 5676 */     if (i2 != 0)
/*      */     {
/* 5677 */       a(f1, f3, f7, f2, f4, f8);
/* 5678 */       q(paramajl, paramInt1, paramInt2, paramInt3);
/* 5679 */       bool1 = true;
/*      */     }
/* 5682 */     f3 = 0.375F;
/* 5683 */     f4 = 0.5625F;
/* 5684 */     if (i1 != 0)
/*      */     {
/* 5685 */       a(f5, f3, f1, f6, f4, f2);
/* 5686 */       q(paramajl, paramInt1, paramInt2, paramInt3);
/* 5687 */       bool1 = true;
/*      */     }
/* 5689 */     if (i2 != 0)
/*      */     {
/* 5690 */       a(f1, f3, f7, f2, f4, f8);
/* 5691 */       q(paramajl, paramInt1, paramInt2, paramInt3);
/* 5692 */       bool1 = true;
/*      */     }
/* 5695 */     paramajl.a(this.a, paramInt1, paramInt2, paramInt3);
/* 5696 */     return bool1;
/*      */   }
/*      */   
/*      */   public boolean a(amu paramamu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 5701 */     boolean bool1 = paramamu.e(this.a, paramInt1 - 1, paramInt2, paramInt3);
/* 5702 */     boolean bool2 = paramamu.e(this.a, paramInt1 + 1, paramInt2, paramInt3);
/* 5703 */     boolean bool3 = paramamu.e(this.a, paramInt1, paramInt2, paramInt3 - 1);
/* 5704 */     boolean bool4 = paramamu.e(this.a, paramInt1, paramInt2, paramInt3 + 1);
/*      */     
/* 5706 */     int i1 = (bool3) && (bool4) && (!bool1) && (!bool2) ? 1 : 0;
/* 5707 */     int i2 = (!bool3) && (!bool4) && (bool1) && (bool2) ? 1 : 0;
/* 5708 */     boolean bool5 = this.a.c(paramInt1, paramInt2 + 1, paramInt3);
/* 5710 */     if (((i1 == 0) && (i2 == 0)) || (!bool5))
/*      */     {
/* 5712 */       a(0.25D, 0.0D, 0.25D, 0.75D, 1.0D, 0.75D);
/* 5713 */       q(paramamu, paramInt1, paramInt2, paramInt3);
/* 5715 */       if (bool1)
/*      */       {
/* 5716 */         a(0.0D, 0.0D, 0.3125D, 0.25D, 0.8125D, 0.6875D);
/* 5717 */         q(paramamu, paramInt1, paramInt2, paramInt3);
/*      */       }
/* 5719 */       if (bool2)
/*      */       {
/* 5720 */         a(0.75D, 0.0D, 0.3125D, 1.0D, 0.8125D, 0.6875D);
/* 5721 */         q(paramamu, paramInt1, paramInt2, paramInt3);
/*      */       }
/* 5723 */       if (bool3)
/*      */       {
/* 5724 */         a(0.3125D, 0.0D, 0.0D, 0.6875D, 0.8125D, 0.25D);
/* 5725 */         q(paramamu, paramInt1, paramInt2, paramInt3);
/*      */       }
/* 5727 */       if (bool4)
/*      */       {
/* 5728 */         a(0.3125D, 0.0D, 0.75D, 0.6875D, 0.8125D, 1.0D);
/* 5729 */         q(paramamu, paramInt1, paramInt2, paramInt3);
/*      */       }
/*      */     }
/* 5731 */     else if (i1 != 0)
/*      */     {
/* 5733 */       a(0.3125D, 0.0D, 0.0D, 0.6875D, 0.8125D, 1.0D);
/* 5734 */       q(paramamu, paramInt1, paramInt2, paramInt3);
/*      */     }
/*      */     else
/*      */     {
/* 5737 */       a(0.0D, 0.0D, 0.3125D, 1.0D, 0.8125D, 0.6875D);
/* 5738 */       q(paramamu, paramInt1, paramInt2, paramInt3);
/*      */     }
/* 5741 */     paramamu.a(this.a, paramInt1, paramInt2, paramInt3);
/* 5742 */     return true;
/*      */   }
/*      */   
/*      */   public boolean a(ajb paramajb, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 5746 */     boolean bool = false;
/*      */     
/* 5748 */     int i1 = 0;
/* 5749 */     for (int i2 = 0; i2 < 8; i2++)
/*      */     {
/* 5750 */       int i3 = 0;
/* 5751 */       int i4 = 1;
/* 5752 */       if (i2 == 0) {
/* 5752 */         i3 = 2;
/*      */       }
/* 5753 */       if (i2 == 1) {
/* 5753 */         i3 = 3;
/*      */       }
/* 5754 */       if (i2 == 2) {
/* 5754 */         i3 = 4;
/*      */       }
/* 5755 */       if (i2 == 3)
/*      */       {
/* 5756 */         i3 = 5;
/* 5757 */         i4 = 2;
/*      */       }
/* 5759 */       if (i2 == 4)
/*      */       {
/* 5760 */         i3 = 6;
/* 5761 */         i4 = 3;
/*      */       }
/* 5763 */       if (i2 == 5)
/*      */       {
/* 5764 */         i3 = 7;
/* 5765 */         i4 = 5;
/*      */       }
/* 5767 */       if (i2 == 6)
/*      */       {
/* 5768 */         i3 = 6;
/* 5769 */         i4 = 2;
/*      */       }
/* 5771 */       if (i2 == 7) {
/* 5771 */         i3 = 3;
/*      */       }
/* 5772 */       float f1 = i3 / 16.0F;
/* 5773 */       float f2 = 1.0F - i1 / 16.0F;
/* 5774 */       float f3 = 1.0F - (i1 + i4) / 16.0F;
/* 5775 */       i1 += i4;
/* 5776 */       a(0.5F - f1, f3, 0.5F - f1, 0.5F + f1, f2, 0.5F + f1);
/* 5777 */       q(paramajb, paramInt1, paramInt2, paramInt3);
/*      */     }
/* 5779 */     bool = true;
/*      */     
/* 5781 */     a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/* 5782 */     return bool;
/*      */   }
/*      */   
/*      */   public boolean a(ajm paramajm, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 5786 */     boolean bool1 = true;
/*      */     
/* 5788 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 5789 */     boolean bool2 = ajm.b(i1);
/* 5790 */     int i2 = aiw.l(i1);
/*      */     
/* 5792 */     float f1 = 0.375F;
/* 5793 */     float f2 = 0.5625F;
/* 5794 */     float f3 = 0.75F;
/* 5795 */     float f4 = 0.9375F;
/* 5796 */     float f5 = 0.3125F;
/* 5797 */     float f6 = 1.0F;
/* 5799 */     if (((i2 != 2) && (i2 != 0)) || (((this.a.a(paramInt1 - 1, paramInt2, paramInt3) == ahz.bK) && (this.a.a(paramInt1 + 1, paramInt2, paramInt3) == ahz.bK)) || (((i2 == 3) || (i2 == 1)) && (this.a.a(paramInt1, paramInt2, paramInt3 - 1) == ahz.bK) && (this.a.a(paramInt1, paramInt2, paramInt3 + 1) == ahz.bK))))
/*      */     {
/* 5801 */       f1 -= 0.1875F;
/* 5802 */       f2 -= 0.1875F;
/* 5803 */       f3 -= 0.1875F;
/* 5804 */       f4 -= 0.1875F;
/* 5805 */       f5 -= 0.1875F;
/* 5806 */       f6 -= 0.1875F;
/*      */     }
/* 5809 */     this.f = true;
/*      */     float f7;
/*      */     float f8;
/*      */     float f9;
/*      */     float f10;
/* 5811 */     if ((i2 == 3) || (i2 == 1))
/*      */     {
/* 5812 */       this.u = 1;
/* 5813 */       f7 = 0.4375F;
/* 5814 */       f8 = 0.5625F;
/* 5815 */       f9 = 0.0F;
/* 5816 */       f10 = 0.125F;
/* 5817 */       a(f7, f5, f9, f8, f6, f10);
/* 5818 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */       
/* 5820 */       f9 = 0.875F;
/* 5821 */       f10 = 1.0F;
/* 5822 */       a(f7, f5, f9, f8, f6, f10);
/* 5823 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5824 */       this.u = 0;
/*      */     }
/*      */     else
/*      */     {
/* 5826 */       f7 = 0.0F;
/* 5827 */       f8 = 0.125F;
/* 5828 */       f9 = 0.4375F;
/* 5829 */       f10 = 0.5625F;
/* 5830 */       a(f7, f5, f9, f8, f6, f10);
/* 5831 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */       
/* 5833 */       f7 = 0.875F;
/* 5834 */       f8 = 1.0F;
/* 5835 */       a(f7, f5, f9, f8, f6, f10);
/* 5836 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */     }
/* 5838 */     if (bool2)
/*      */     {
/* 5839 */       if ((i2 == 2) || (i2 == 0)) {
/* 5840 */         this.u = 1;
/*      */       }
/*      */       float f11;
/*      */       float f12;
/*      */       float f13;
/* 5843 */       if (i2 == 3)
/*      */       {
/* 5844 */         f7 = 0.0F;
/* 5845 */         f8 = 0.125F;
/* 5846 */         f9 = 0.875F;
/* 5847 */         f10 = 1.0F;
/*      */         
/* 5849 */         f11 = 0.5625F;
/* 5850 */         f12 = 0.8125F;
/* 5851 */         f13 = 0.9375F;
/*      */         
/* 5853 */         a(0.8125D, f1, 0.0D, 0.9375D, f4, 0.125D);
/* 5854 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5855 */         a(0.8125D, f1, 0.875D, 0.9375D, f4, 1.0D);
/* 5856 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */         
/* 5858 */         a(0.5625D, f1, 0.0D, 0.8125D, f2, 0.125D);
/* 5859 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5860 */         a(0.5625D, f1, 0.875D, 0.8125D, f2, 1.0D);
/* 5861 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */         
/* 5863 */         a(0.5625D, f3, 0.0D, 0.8125D, f4, 0.125D);
/* 5864 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5865 */         a(0.5625D, f3, 0.875D, 0.8125D, f4, 1.0D);
/* 5866 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */       }
/* 5867 */       else if (i2 == 1)
/*      */       {
/* 5868 */         f7 = 0.0F;
/* 5869 */         f8 = 0.125F;
/* 5870 */         f9 = 0.875F;
/* 5871 */         f10 = 1.0F;
/*      */         
/* 5873 */         f11 = 0.0625F;
/* 5874 */         f12 = 0.1875F;
/* 5875 */         f13 = 0.4375F;
/*      */         
/* 5877 */         a(0.0625D, f1, 0.0D, 0.1875D, f4, 0.125D);
/* 5878 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5879 */         a(0.0625D, f1, 0.875D, 0.1875D, f4, 1.0D);
/* 5880 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */         
/* 5882 */         a(0.1875D, f1, 0.0D, 0.4375D, f2, 0.125D);
/* 5883 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5884 */         a(0.1875D, f1, 0.875D, 0.4375D, f2, 1.0D);
/* 5885 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */         
/* 5887 */         a(0.1875D, f3, 0.0D, 0.4375D, f4, 0.125D);
/* 5888 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5889 */         a(0.1875D, f3, 0.875D, 0.4375D, f4, 1.0D);
/* 5890 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */       }
/* 5891 */       else if (i2 == 0)
/*      */       {
/* 5893 */         f7 = 0.0F;
/* 5894 */         f8 = 0.125F;
/* 5895 */         f9 = 0.875F;
/* 5896 */         f10 = 1.0F;
/*      */         
/* 5898 */         f11 = 0.5625F;
/* 5899 */         f12 = 0.8125F;
/* 5900 */         f13 = 0.9375F;
/*      */         
/* 5902 */         a(0.0D, f1, 0.8125D, 0.125D, f4, 0.9375D);
/* 5903 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5904 */         a(0.875D, f1, 0.8125D, 1.0D, f4, 0.9375D);
/* 5905 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */         
/* 5907 */         a(0.0D, f1, 0.5625D, 0.125D, f2, 0.8125D);
/* 5908 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5909 */         a(0.875D, f1, 0.5625D, 1.0D, f2, 0.8125D);
/* 5910 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */         
/* 5912 */         a(0.0D, f3, 0.5625D, 0.125D, f4, 0.8125D);
/* 5913 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5914 */         a(0.875D, f3, 0.5625D, 1.0D, f4, 0.8125D);
/* 5915 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */       }
/* 5916 */       else if (i2 == 2)
/*      */       {
/* 5917 */         f7 = 0.0F;
/* 5918 */         f8 = 0.125F;
/* 5919 */         f9 = 0.875F;
/* 5920 */         f10 = 1.0F;
/*      */         
/* 5922 */         f11 = 0.0625F;
/* 5923 */         f12 = 0.1875F;
/* 5924 */         f13 = 0.4375F;
/*      */         
/* 5926 */         a(0.0D, f1, 0.0625D, 0.125D, f4, 0.1875D);
/* 5927 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5928 */         a(0.875D, f1, 0.0625D, 1.0D, f4, 0.1875D);
/* 5929 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */         
/* 5931 */         a(0.0D, f1, 0.1875D, 0.125D, f2, 0.4375D);
/* 5932 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5933 */         a(0.875D, f1, 0.1875D, 1.0D, f2, 0.4375D);
/* 5934 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */         
/* 5936 */         a(0.0D, f3, 0.1875D, 0.125D, f4, 0.4375D);
/* 5937 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5938 */         a(0.875D, f3, 0.1875D, 1.0D, f4, 0.4375D);
/* 5939 */         q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */       }
/*      */     }
/* 5942 */     else if ((i2 == 3) || (i2 == 1))
/*      */     {
/* 5943 */       this.u = 1;
/* 5944 */       f7 = 0.4375F;
/* 5945 */       f8 = 0.5625F;
/* 5946 */       f9 = 0.375F;
/* 5947 */       f10 = 0.5F;
/* 5948 */       a(f7, f1, f9, f8, f4, f10);
/* 5949 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5950 */       f9 = 0.5F;
/* 5951 */       f10 = 0.625F;
/* 5952 */       a(f7, f1, f9, f8, f4, f10);
/* 5953 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5954 */       f9 = 0.625F;
/* 5955 */       f10 = 0.875F;
/* 5956 */       a(f7, f1, f9, f8, f2, f10);
/* 5957 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5958 */       a(f7, f3, f9, f8, f4, f10);
/* 5959 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5960 */       f9 = 0.125F;
/* 5961 */       f10 = 0.375F;
/* 5962 */       a(f7, f1, f9, f8, f2, f10);
/* 5963 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5964 */       a(f7, f3, f9, f8, f4, f10);
/* 5965 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */     }
/*      */     else
/*      */     {
/* 5967 */       f7 = 0.375F;
/* 5968 */       f8 = 0.5F;
/* 5969 */       f9 = 0.4375F;
/* 5970 */       f10 = 0.5625F;
/* 5971 */       a(f7, f1, f9, f8, f4, f10);
/* 5972 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5973 */       f7 = 0.5F;
/* 5974 */       f8 = 0.625F;
/* 5975 */       a(f7, f1, f9, f8, f4, f10);
/* 5976 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5977 */       f7 = 0.625F;
/* 5978 */       f8 = 0.875F;
/* 5979 */       a(f7, f1, f9, f8, f2, f10);
/* 5980 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5981 */       a(f7, f3, f9, f8, f4, f10);
/* 5982 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5983 */       f7 = 0.125F;
/* 5984 */       f8 = 0.375F;
/* 5985 */       a(f7, f1, f9, f8, f2, f10);
/* 5986 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/* 5987 */       a(f7, f3, f9, f8, f4, f10);
/* 5988 */       q(paramajm, paramInt1, paramInt2, paramInt3);
/*      */     }
/* 5992 */     this.f = false;
/* 5993 */     this.u = 0;
/*      */     
/* 5995 */     a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/* 5996 */     return bool1;
/*      */   }
/*      */   
/*      */   private boolean a(ajz paramajz, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 6000 */     blz localblz = blz.a;
/*      */     
/* 6002 */     localblz.b(paramajz.c(this.a, paramInt1, paramInt2, paramInt3));
/* 6003 */     int i1 = paramajz.d(this.a, paramInt1, paramInt2, paramInt3);
/* 6004 */     float f1 = (i1 >> 16 & 0xFF) / 255.0F;
/* 6005 */     float f2 = (i1 >> 8 & 0xFF) / 255.0F;
/* 6006 */     float f3 = (i1 & 0xFF) / 255.0F;
/* 6008 */     if (bll.a)
/*      */     {
/* 6009 */       float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/* 6010 */       float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/* 6011 */       float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */       
/* 6013 */       f1 = f4;
/* 6014 */       f2 = f5;
/* 6015 */       f3 = f6;
/*      */     }
/* 6017 */     localblz.b(f1, f2, f3);
/*      */     
/* 6019 */     return a(paramajz, paramInt1, paramInt2, paramInt3, this.a.e(paramInt1, paramInt2, paramInt3), false);
/*      */   }
/*      */   
/*      */   private boolean a(ajz paramajz, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
/*      */   {
/* 6023 */     blz localblz = blz.a;
/* 6024 */     int i1 = ajz.b(paramInt4);
/*      */     
/*      */ 
/* 6027 */     double d1 = 0.625D;
/* 6028 */     a(0.0D, d1, 0.0D, 1.0D, 1.0D, 1.0D);
/* 6030 */     if (paramBoolean)
/*      */     {
/* 6031 */       localblz.b();
/* 6032 */       localblz.c(0.0F, -1.0F, 0.0F);
/* 6033 */       a(paramajz, 0.0D, 0.0D, 0.0D, a(paramajz, 0, paramInt4));
/* 6034 */       localblz.a();
/*      */       
/* 6036 */       localblz.b();
/* 6037 */       localblz.c(0.0F, 1.0F, 0.0F);
/* 6038 */       b(paramajz, 0.0D, 0.0D, 0.0D, a(paramajz, 1, paramInt4));
/* 6039 */       localblz.a();
/*      */       
/* 6041 */       localblz.b();
/* 6042 */       localblz.c(0.0F, 0.0F, -1.0F);
/* 6043 */       c(paramajz, 0.0D, 0.0D, 0.0D, a(paramajz, 2, paramInt4));
/* 6044 */       localblz.a();
/*      */       
/* 6046 */       localblz.b();
/* 6047 */       localblz.c(0.0F, 0.0F, 1.0F);
/* 6048 */       d(paramajz, 0.0D, 0.0D, 0.0D, a(paramajz, 3, paramInt4));
/* 6049 */       localblz.a();
/*      */       
/* 6051 */       localblz.b();
/* 6052 */       localblz.c(-1.0F, 0.0F, 0.0F);
/* 6053 */       e(paramajz, 0.0D, 0.0D, 0.0D, a(paramajz, 4, paramInt4));
/* 6054 */       localblz.a();
/*      */       
/* 6056 */       localblz.b();
/* 6057 */       localblz.c(1.0F, 0.0F, 0.0F);
/* 6058 */       f(paramajz, 0.0D, 0.0D, 0.0D, a(paramajz, 5, paramInt4));
/* 6059 */       localblz.a();
/*      */     }
/*      */     else
/*      */     {
/* 6061 */       q(paramajz, paramInt1, paramInt2, paramInt3);
/*      */     }
/* 6064 */     if (!paramBoolean)
/*      */     {
/* 6065 */       localblz.b(paramajz.c(this.a, paramInt1, paramInt2, paramInt3));
/* 6066 */       int i2 = paramajz.d(this.a, paramInt1, paramInt2, paramInt3);
/* 6067 */       float f1 = (i2 >> 16 & 0xFF) / 255.0F;
/* 6068 */       f2 = (i2 >> 8 & 0xFF) / 255.0F;
/* 6069 */       float f3 = (i2 & 0xFF) / 255.0F;
/* 6071 */       if (bll.a)
/*      */       {
/* 6072 */         float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
/* 6073 */         float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
/* 6074 */         float f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
/*      */         
/* 6076 */         f1 = f4;
/* 6077 */         f2 = f5;
/* 6078 */         f3 = f6;
/*      */       }
/* 6080 */       localblz.b(f1, f2, f3);
/*      */     }
/* 6084 */     ps localps1 = ajz.e("hopper_outside");
/* 6085 */     ps localps2 = ajz.e("hopper_inside");
/* 6086 */     float f2 = 0.125F;
/* 6088 */     if (paramBoolean)
/*      */     {
/* 6089 */       localblz.b();
/* 6090 */       localblz.c(1.0F, 0.0F, 0.0F);
/* 6091 */       f(paramajz, -1.0F + f2, 0.0D, 0.0D, localps1);
/* 6092 */       localblz.a();
/*      */       
/* 6094 */       localblz.b();
/* 6095 */       localblz.c(-1.0F, 0.0F, 0.0F);
/* 6096 */       e(paramajz, 1.0F - f2, 0.0D, 0.0D, localps1);
/* 6097 */       localblz.a();
/*      */       
/* 6099 */       localblz.b();
/* 6100 */       localblz.c(0.0F, 0.0F, 1.0F);
/* 6101 */       d(paramajz, 0.0D, 0.0D, -1.0F + f2, localps1);
/* 6102 */       localblz.a();
/*      */       
/* 6104 */       localblz.b();
/* 6105 */       localblz.c(0.0F, 0.0F, -1.0F);
/* 6106 */       c(paramajz, 0.0D, 0.0D, 1.0F - f2, localps1);
/* 6107 */       localblz.a();
/*      */       
/* 6109 */       localblz.b();
/* 6110 */       localblz.c(0.0F, 1.0F, 0.0F);
/* 6111 */       b(paramajz, 0.0D, -1.0D + d1, 0.0D, localps2);
/* 6112 */       localblz.a();
/*      */     }
/*      */     else
/*      */     {
/* 6114 */       f(paramajz, paramInt1 - 1.0F + f2, paramInt2, paramInt3, localps1);
/* 6115 */       e(paramajz, paramInt1 + 1.0F - f2, paramInt2, paramInt3, localps1);
/* 6116 */       d(paramajz, paramInt1, paramInt2, paramInt3 - 1.0F + f2, localps1);
/* 6117 */       c(paramajz, paramInt1, paramInt2, paramInt3 + 1.0F - f2, localps1);
/* 6118 */       b(paramajz, paramInt1, paramInt2 - 1.0F + d1, paramInt3, localps2);
/*      */     }
/* 6122 */     a(localps1);
/* 6123 */     double d2 = 0.25D;
/* 6124 */     double d3 = 0.25D;
/* 6125 */     double d4 = d1;
/* 6126 */     a(d2, d3, d2, 1.0D - d2, d4 - 0.002D, 1.0D - d2);
/* 6128 */     if (paramBoolean)
/*      */     {
/* 6129 */       localblz.b();
/* 6130 */       localblz.c(1.0F, 0.0F, 0.0F);
/* 6131 */       f(paramajz, 0.0D, 0.0D, 0.0D, localps1);
/* 6132 */       localblz.a();
/*      */       
/* 6134 */       localblz.b();
/* 6135 */       localblz.c(-1.0F, 0.0F, 0.0F);
/* 6136 */       e(paramajz, 0.0D, 0.0D, 0.0D, localps1);
/* 6137 */       localblz.a();
/*      */       
/* 6139 */       localblz.b();
/* 6140 */       localblz.c(0.0F, 0.0F, 1.0F);
/* 6141 */       d(paramajz, 0.0D, 0.0D, 0.0D, localps1);
/* 6142 */       localblz.a();
/*      */       
/* 6144 */       localblz.b();
/* 6145 */       localblz.c(0.0F, 0.0F, -1.0F);
/* 6146 */       c(paramajz, 0.0D, 0.0D, 0.0D, localps1);
/* 6147 */       localblz.a();
/*      */       
/* 6149 */       localblz.b();
/* 6150 */       localblz.c(0.0F, 1.0F, 0.0F);
/* 6151 */       b(paramajz, 0.0D, 0.0D, 0.0D, localps1);
/* 6152 */       localblz.a();
/*      */       
/* 6154 */       localblz.b();
/* 6155 */       localblz.c(0.0F, -1.0F, 0.0F);
/* 6156 */       a(paramajz, 0.0D, 0.0D, 0.0D, localps1);
/* 6157 */       localblz.a();
/*      */     }
/*      */     else
/*      */     {
/* 6159 */       q(paramajz, paramInt1, paramInt2, paramInt3);
/*      */     }
/* 6162 */     if (!paramBoolean)
/*      */     {
/* 6164 */       double d5 = 0.375D;
/* 6165 */       double d6 = 0.25D;
/* 6166 */       a(localps1);
/* 6169 */       if (i1 == 0)
/*      */       {
/* 6170 */         a(d5, 0.0D, d5, 1.0D - d5, 0.25D, 1.0D - d5);
/* 6171 */         q(paramajz, paramInt1, paramInt2, paramInt3);
/*      */       }
/* 6174 */       if (i1 == 2)
/*      */       {
/* 6175 */         a(d5, d3, 0.0D, 1.0D - d5, d3 + d6, d2);
/* 6176 */         q(paramajz, paramInt1, paramInt2, paramInt3);
/*      */       }
/* 6179 */       if (i1 == 3)
/*      */       {
/* 6180 */         a(d5, d3, 1.0D - d2, 1.0D - d5, d3 + d6, 1.0D);
/* 6181 */         q(paramajz, paramInt1, paramInt2, paramInt3);
/*      */       }
/* 6184 */       if (i1 == 4)
/*      */       {
/* 6185 */         a(0.0D, d3, d5, d2, d3 + d6, 1.0D - d5);
/* 6186 */         q(paramajz, paramInt1, paramInt2, paramInt3);
/*      */       }
/* 6189 */       if (i1 == 5)
/*      */       {
/* 6190 */         a(1.0D - d2, d3, d5, 1.0D, d3 + d6, 1.0D - d5);
/* 6191 */         q(paramajz, paramInt1, paramInt2, paramInt3);
/*      */       }
/*      */     }
/* 6195 */     a();
/*      */     
/* 6197 */     return true;
/*      */   }
/*      */   
/*      */   public boolean a(ame paramame, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 6204 */     paramame.e(this.a, paramInt1, paramInt2, paramInt3);
/* 6205 */     a(paramame);
/* 6206 */     q(paramame, paramInt1, paramInt2, paramInt3);
/*      */     
/* 6208 */     boolean bool = paramame.f(this.a, paramInt1, paramInt2, paramInt3);
/* 6209 */     a(paramame);
/* 6210 */     q(paramame, paramInt1, paramInt2, paramInt3);
/* 6212 */     if ((bool) && 
/* 6213 */       (paramame.g(this.a, paramInt1, paramInt2, paramInt3)))
/*      */     {
/* 6214 */       a(paramame);
/* 6215 */       q(paramame, paramInt1, paramInt2, paramInt3);
/*      */     }
/* 6219 */     return true;
/*      */   }
/*      */   
/*      */   public boolean u(ahu paramahu, int paramInt1, int paramInt2, int paramInt3)
/*      */   {
/* 6223 */     blz localblz = blz.a;
/*      */     
/*      */ 
/* 6226 */     int i1 = this.a.e(paramInt1, paramInt2, paramInt3);
/* 6227 */     if ((i1 & 0x8) != 0)
/*      */     {
/* 6228 */       if (this.a.a(paramInt1, paramInt2 - 1, paramInt3) != paramahu) {
/* 6229 */         return false;
/*      */       }
/*      */     }
/* 6232 */     else if (this.a.a(paramInt1, paramInt2 + 1, paramInt3) != paramahu) {
/* 6233 */       return false;
/*      */     }
/* 6237 */     boolean bool = false;
/* 6238 */     float f1 = 0.5F;
/* 6239 */     float f2 = 1.0F;
/* 6240 */     float f3 = 0.8F;
/* 6241 */     float f4 = 0.6F;
/*      */     
/*      */ 
/*      */ 
/* 6245 */     int i2 = paramahu.c(this.a, paramInt1, paramInt2, paramInt3);
/*      */     
/* 6247 */     localblz.b(this.j > 0.0D ? i2 : paramahu.c(this.a, paramInt1, paramInt2 - 1, paramInt3));
/* 6248 */     localblz.b(f1, f1, f1);
/* 6249 */     a(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 0));
/* 6250 */     bool = true;
/*      */     
/* 6252 */     localblz.b(this.k < 1.0D ? i2 : paramahu.c(this.a, paramInt1, paramInt2 + 1, paramInt3));
/* 6253 */     localblz.b(f2, f2, f2);
/* 6254 */     b(paramahu, paramInt1, paramInt2, paramInt3, a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 1));
/* 6255 */     bool = true;
/*      */     
/*      */ 
/* 6258 */     localblz.b(this.l > 0.0D ? i2 : paramahu.c(this.a, paramInt1, paramInt2, paramInt3 - 1));
/* 6259 */     localblz.b(f3, f3, f3);
/* 6260 */     ps localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 2);
/* 6261 */     c(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 6262 */     bool = true;
/* 6263 */     this.e = false;
/*      */     
/*      */ 
/*      */ 
/* 6267 */     localblz.b(this.m < 1.0D ? i2 : paramahu.c(this.a, paramInt1, paramInt2, paramInt3 + 1));
/* 6268 */     localblz.b(f3, f3, f3);
/* 6269 */     localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 3);
/* 6270 */     d(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 6271 */     bool = true;
/* 6272 */     this.e = false;
/*      */     
/*      */ 
/*      */ 
/* 6276 */     localblz.b(this.h > 0.0D ? i2 : paramahu.c(this.a, paramInt1 - 1, paramInt2, paramInt3));
/* 6277 */     localblz.b(f4, f4, f4);
/* 6278 */     localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 4);
/* 6279 */     e(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 6280 */     bool = true;
/* 6281 */     this.e = false;
/*      */     
/*      */ 
/* 6284 */     localblz.b(this.i < 1.0D ? i2 : paramahu.c(this.a, paramInt1 + 1, paramInt2, paramInt3));
/* 6285 */     localblz.b(f4, f4, f4);
/* 6286 */     localps = a(paramahu, this.a, paramInt1, paramInt2, paramInt3, 5);
/* 6287 */     f(paramahu, paramInt1, paramInt2, paramInt3, localps);
/* 6288 */     bool = true;
/* 6289 */     this.e = false;
/*      */     
/* 6291 */     return bool;
/*      */   }
/*      */   
/*      */   public void a(ahu paramahu, double paramDouble1, double paramDouble2, double paramDouble3, ps paramps)
/*      */   {
/* 6295 */     blz localblz = blz.a;
/* 6297 */     if (b()) {
/* 6297 */       paramps = this.d;
/*      */     }
/* 6298 */     double d1 = paramps.a(this.h * 16.0D);
/* 6299 */     double d2 = paramps.a(this.i * 16.0D);
/* 6300 */     double d3 = paramps.b(this.l * 16.0D);
/* 6301 */     double d4 = paramps.b(this.m * 16.0D);
/* 6303 */     if ((this.h < 0.0D) || (this.i > 1.0D))
/*      */     {
/* 6304 */       d1 = paramps.c();
/* 6305 */       d2 = paramps.d();
/*      */     }
/* 6307 */     if ((this.l < 0.0D) || (this.m > 1.0D))
/*      */     {
/* 6308 */       d3 = paramps.e();
/* 6309 */       d4 = paramps.f();
/*      */     }
/* 6312 */     double d5 = d2;double d6 = d1;double d7 = d3;double d8 = d4;
/* 6314 */     if (this.v == 2)
/*      */     {
/* 6315 */       d1 = paramps.a(this.l * 16.0D);
/* 6316 */       d3 = paramps.b(16.0D - this.i * 16.0D);
/* 6317 */       d2 = paramps.a(this.m * 16.0D);
/* 6318 */       d4 = paramps.b(16.0D - this.h * 16.0D);
/*      */       
/* 6320 */       d5 = d2;
/* 6321 */       d6 = d1;
/* 6322 */       d7 = d3;
/* 6323 */       d8 = d4;
/* 6324 */       d5 = d1;
/* 6325 */       d6 = d2;
/* 6326 */       d3 = d4;
/* 6327 */       d4 = d7;
/*      */     }
/* 6328 */     else if (this.v == 1)
/*      */     {
/* 6330 */       d1 = paramps.a(16.0D - this.m * 16.0D);
/* 6331 */       d3 = paramps.b(this.h * 16.0D);
/* 6332 */       d2 = paramps.a(16.0D - this.l * 16.0D);
/* 6333 */       d4 = paramps.b(this.i * 16.0D);
/*      */       
/*      */ 
/* 6336 */       d5 = d2;
/* 6337 */       d6 = d1;
/* 6338 */       d7 = d3;
/* 6339 */       d8 = d4;
/* 6340 */       d1 = d5;
/* 6341 */       d2 = d6;
/* 6342 */       d7 = d4;
/* 6343 */       d8 = d3;
/*      */     }
/* 6344 */     else if (this.v == 3)
/*      */     {
/* 6345 */       d1 = paramps.a(16.0D - this.h * 16.0D);
/* 6346 */       d2 = paramps.a(16.0D - this.i * 16.0D);
/* 6347 */       d3 = paramps.b(16.0D - this.l * 16.0D);
/* 6348 */       d4 = paramps.b(16.0D - this.m * 16.0D);
/*      */       
/* 6350 */       d5 = d2;
/* 6351 */       d6 = d1;
/* 6352 */       d7 = d3;
/* 6353 */       d8 = d4;
/*      */     }
/* 6356 */     double d9 = paramDouble1 + this.h;
/* 6357 */     double d10 = paramDouble1 + this.i;
/* 6358 */     double d11 = paramDouble2 + this.j;
/* 6359 */     double d12 = paramDouble3 + this.l;
/* 6360 */     double d13 = paramDouble3 + this.m;
/* 6362 */     if (this.g)
/*      */     {
/* 6363 */       d9 = paramDouble1 + this.i;
/* 6364 */       d10 = paramDouble1 + this.h;
/*      */     }
/* 6367 */     if (this.w)
/*      */     {
/* 6368 */       localblz.b(this.ap, this.at, this.ax);
/* 6369 */       localblz.b(this.al);
/* 6370 */       localblz.a(d9, d11, d13, d6, d8);
/*      */       
/* 6372 */       localblz.b(this.aq, this.au, this.ay);
/* 6373 */       localblz.b(this.am);
/* 6374 */       localblz.a(d9, d11, d12, d1, d3);
/*      */       
/* 6376 */       localblz.b(this.ar, this.av, this.az);
/* 6377 */       localblz.b(this.an);
/* 6378 */       localblz.a(d10, d11, d12, d5, d7);
/*      */       
/* 6380 */       localblz.b(this.as, this.aw, this.aA);
/* 6381 */       localblz.b(this.ao);
/* 6382 */       localblz.a(d10, d11, d13, d2, d4);
/*      */     }
/*      */     else
/*      */     {
/* 6384 */       localblz.a(d9, d11, d13, d6, d8);
/* 6385 */       localblz.a(d9, d11, d12, d1, d3);
/* 6386 */       localblz.a(d10, d11, d12, d5, d7);
/* 6387 */       localblz.a(d10, d11, d13, d2, d4);
/*      */     }
/*      */   }
/*      */   
/*      */   public void b(ahu paramahu, double paramDouble1, double paramDouble2, double paramDouble3, ps paramps)
/*      */   {
/* 6392 */     blz localblz = blz.a;
/* 6394 */     if (b()) {
/* 6394 */       paramps = this.d;
/*      */     }
/* 6395 */     double d1 = paramps.a(this.h * 16.0D);
/* 6396 */     double d2 = paramps.a(this.i * 16.0D);
/* 6397 */     double d3 = paramps.b(this.l * 16.0D);
/* 6398 */     double d4 = paramps.b(this.m * 16.0D);
/* 6400 */     if ((this.h < 0.0D) || (this.i > 1.0D))
/*      */     {
/* 6401 */       d1 = paramps.c();
/* 6402 */       d2 = paramps.d();
/*      */     }
/* 6404 */     if ((this.l < 0.0D) || (this.m > 1.0D))
/*      */     {
/* 6405 */       d3 = paramps.e();
/* 6406 */       d4 = paramps.f();
/*      */     }
/* 6409 */     double d5 = d2;double d6 = d1;double d7 = d3;double d8 = d4;
/* 6411 */     if (this.u == 1)
/*      */     {
/* 6412 */       d1 = paramps.a(this.l * 16.0D);
/* 6413 */       d3 = paramps.b(16.0D - this.i * 16.0D);
/* 6414 */       d2 = paramps.a(this.m * 16.0D);
/* 6415 */       d4 = paramps.b(16.0D - this.h * 16.0D);
/*      */       
/* 6417 */       d5 = d2;
/* 6418 */       d6 = d1;
/* 6419 */       d7 = d3;
/* 6420 */       d8 = d4;
/* 6421 */       d5 = d1;
/* 6422 */       d6 = d2;
/* 6423 */       d3 = d4;
/* 6424 */       d4 = d7;
/*      */     }
/* 6425 */     else if (this.u == 2)
/*      */     {
/* 6427 */       d1 = paramps.a(16.0D - this.m * 16.0D);
/* 6428 */       d3 = paramps.b(this.h * 16.0D);
/* 6429 */       d2 = paramps.a(16.0D - this.l * 16.0D);
/* 6430 */       d4 = paramps.b(this.i * 16.0D);
/*      */       
/*      */ 
/* 6433 */       d5 = d2;
/* 6434 */       d6 = d1;
/* 6435 */       d7 = d3;
/* 6436 */       d8 = d4;
/* 6437 */       d1 = d5;
/* 6438 */       d2 = d6;
/* 6439 */       d7 = d4;
/* 6440 */       d8 = d3;
/*      */     }
/* 6441 */     else if (this.u == 3)
/*      */     {
/* 6442 */       d1 = paramps.a(16.0D - this.h * 16.0D);
/* 6443 */       d2 = paramps.a(16.0D - this.i * 16.0D);
/* 6444 */       d3 = paramps.b(16.0D - this.l * 16.0D);
/* 6445 */       d4 = paramps.b(16.0D - this.m * 16.0D);
/*      */       
/* 6447 */       d5 = d2;
/* 6448 */       d6 = d1;
/* 6449 */       d7 = d3;
/* 6450 */       d8 = d4;
/*      */     }
/* 6453 */     double d9 = paramDouble1 + this.h;
/* 6454 */     double d10 = paramDouble1 + this.i;
/* 6455 */     double d11 = paramDouble2 + this.k;
/* 6456 */     double d12 = paramDouble3 + this.l;
/* 6457 */     double d13 = paramDouble3 + this.m;
/* 6459 */     if (this.g)
/*      */     {
/* 6460 */       d9 = paramDouble1 + this.i;
/* 6461 */       d10 = paramDouble1 + this.h;
/*      */     }
/* 6464 */     if (this.w)
/*      */     {
/* 6465 */       localblz.b(this.ap, this.at, this.ax);
/* 6466 */       localblz.b(this.al);
/* 6467 */       localblz.a(d10, d11, d13, d2, d4);
/*      */       
/* 6469 */       localblz.b(this.aq, this.au, this.ay);
/* 6470 */       localblz.b(this.am);
/* 6471 */       localblz.a(d10, d11, d12, d5, d7);
/*      */       
/* 6473 */       localblz.b(this.ar, this.av, this.az);
/* 6474 */       localblz.b(this.an);
/* 6475 */       localblz.a(d9, d11, d12, d1, d3);
/*      */       
/* 6477 */       localblz.b(this.as, this.aw, this.aA);
/* 6478 */       localblz.b(this.ao);
/* 6479 */       localblz.a(d9, d11, d13, d6, d8);
/*      */     }
/*      */     else
/*      */     {
/* 6481 */       localblz.a(d10, d11, d13, d2, d4);
/* 6482 */       localblz.a(d10, d11, d12, d5, d7);
/* 6483 */       localblz.a(d9, d11, d12, d1, d3);
/* 6484 */       localblz.a(d9, d11, d13, d6, d8);
/*      */     }
/*      */   }
/*      */   
/*      */   public void c(ahu paramahu, double paramDouble1, double paramDouble2, double paramDouble3, ps paramps)
/*      */   {
/* 6489 */     blz localblz = blz.a;
/* 6491 */     if (b()) {
/* 6491 */       paramps = this.d;
/*      */     }
/* 6492 */     double d1 = paramps.a(this.i * 16.0D);
/* 6493 */     double d2 = paramps.a(this.h * 16.0D);
/* 6494 */     double d3 = paramps.b(16.0D - this.k * 16.0D);
/* 6495 */     double d4 = paramps.b(16.0D - this.j * 16.0D);
/* 6497 */     if (this.e)
/*      */     {
/* 6498 */       d5 = d1;
/* 6499 */       d1 = d2;
/* 6500 */       d2 = d5;
/*      */     }
/* 6503 */     if ((this.h < 0.0D) || (this.i > 1.0D))
/*      */     {
/* 6504 */       d1 = paramps.c();
/* 6505 */       d2 = paramps.d();
/*      */     }
/* 6507 */     if ((this.j < 0.0D) || (this.k > 1.0D))
/*      */     {
/* 6508 */       d3 = paramps.e();
/* 6509 */       d4 = paramps.f();
/*      */     }
/* 6512 */     double d5 = d2;double d6 = d1;double d7 = d3;double d8 = d4;
/* 6514 */     if (this.q == 2)
/*      */     {
/* 6515 */       d1 = paramps.a(this.j * 16.0D);
/* 6516 */       d3 = paramps.b(16.0D - this.h * 16.0D);
/* 6517 */       d2 = paramps.a(this.k * 16.0D);
/* 6518 */       d4 = paramps.b(16.0D - this.i * 16.0D);
/*      */       
/* 6520 */       d5 = d2;
/* 6521 */       d6 = d1;
/* 6522 */       d7 = d3;
/* 6523 */       d8 = d4;
/* 6524 */       d5 = d1;
/* 6525 */       d6 = d2;
/* 6526 */       d3 = d4;
/* 6527 */       d4 = d7;
/*      */     }
/* 6528 */     else if (this.q == 1)
/*      */     {
/* 6530 */       d1 = paramps.a(16.0D - this.k * 16.0D);
/* 6531 */       d3 = paramps.b(this.i * 16.0D);
/* 6532 */       d2 = paramps.a(16.0D - this.j * 16.0D);
/* 6533 */       d4 = paramps.b(this.h * 16.0D);
/*      */       
/*      */ 
/* 6536 */       d5 = d2;
/* 6537 */       d6 = d1;
/* 6538 */       d7 = d3;
/* 6539 */       d8 = d4;
/* 6540 */       d1 = d5;
/* 6541 */       d2 = d6;
/* 6542 */       d7 = d4;
/* 6543 */       d8 = d3;
/*      */     }
/* 6544 */     else if (this.q == 3)
/*      */     {
/* 6545 */       d1 = paramps.a(16.0D - this.h * 16.0D);
/* 6546 */       d2 = paramps.a(16.0D - this.i * 16.0D);
/* 6547 */       d3 = paramps.b(this.k * 16.0D);
/* 6548 */       d4 = paramps.b(this.j * 16.0D);
/*      */       
/* 6550 */       d5 = d2;
/* 6551 */       d6 = d1;
/* 6552 */       d7 = d3;
/* 6553 */       d8 = d4;
/*      */     }
/* 6556 */     double d9 = paramDouble1 + this.h;
/* 6557 */     double d10 = paramDouble1 + this.i;
/* 6558 */     double d11 = paramDouble2 + this.j;
/* 6559 */     double d12 = paramDouble2 + this.k;
/* 6560 */     double d13 = paramDouble3 + this.l;
/* 6562 */     if (this.g)
/*      */     {
/* 6563 */       d9 = paramDouble1 + this.i;
/* 6564 */       d10 = paramDouble1 + this.h;
/*      */     }
/* 6567 */     if (this.w)
/*      */     {
/* 6568 */       localblz.b(this.ap, this.at, this.ax);
/* 6569 */       localblz.b(this.al);
/* 6570 */       localblz.a(d9, d12, d13, d5, d7);
/*      */       
/* 6572 */       localblz.b(this.aq, this.au, this.ay);
/* 6573 */       localblz.b(this.am);
/* 6574 */       localblz.a(d10, d12, d13, d1, d3);
/*      */       
/* 6576 */       localblz.b(this.ar, this.av, this.az);
/* 6577 */       localblz.b(this.an);
/* 6578 */       localblz.a(d10, d11, d13, d6, d8);
/*      */       
/* 6580 */       localblz.b(this.as, this.aw, this.aA);
/* 6581 */       localblz.b(this.ao);
/* 6582 */       localblz.a(d9, d11, d13, d2, d4);
/*      */     }
/*      */     else
/*      */     {
/* 6584 */       localblz.a(d9, d12, d13, d5, d7);
/* 6585 */       localblz.a(d10, d12, d13, d1, d3);
/* 6586 */       localblz.a(d10, d11, d13, d6, d8);
/* 6587 */       localblz.a(d9, d11, d13, d2, d4);
/*      */     }
/*      */   }
/*      */   
/*      */   public void d(ahu paramahu, double paramDouble1, double paramDouble2, double paramDouble3, ps paramps)
/*      */   {
/* 6592 */     blz localblz = blz.a;
/* 6594 */     if (b()) {
/* 6594 */       paramps = this.d;
/*      */     }
/* 6595 */     double d1 = paramps.a(this.h * 16.0D);
/* 6596 */     double d2 = paramps.a(this.i * 16.0D);
/* 6597 */     double d3 = paramps.b(16.0D - this.k * 16.0D);
/* 6598 */     double d4 = paramps.b(16.0D - this.j * 16.0D);
/* 6600 */     if (this.e)
/*      */     {
/* 6601 */       d5 = d1;
/* 6602 */       d1 = d2;
/* 6603 */       d2 = d5;
/*      */     }
/* 6606 */     if ((this.h < 0.0D) || (this.i > 1.0D))
/*      */     {
/* 6607 */       d1 = paramps.c();
/* 6608 */       d2 = paramps.d();
/*      */     }
/* 6610 */     if ((this.j < 0.0D) || (this.k > 1.0D))
/*      */     {
/* 6611 */       d3 = paramps.e();
/* 6612 */       d4 = paramps.f();
/*      */     }
/* 6615 */     double d5 = d2;double d6 = d1;double d7 = d3;double d8 = d4;
/* 6617 */     if (this.r == 1)
/*      */     {
/* 6618 */       d1 = paramps.a(this.j * 16.0D);
/* 6619 */       d4 = paramps.b(16.0D - this.h * 16.0D);
/* 6620 */       d2 = paramps.a(this.k * 16.0D);
/* 6621 */       d3 = paramps.b(16.0D - this.i * 16.0D);
/*      */       
/* 6623 */       d5 = d2;
/* 6624 */       d6 = d1;
/* 6625 */       d7 = d3;
/* 6626 */       d8 = d4;
/* 6627 */       d5 = d1;
/* 6628 */       d6 = d2;
/* 6629 */       d3 = d4;
/* 6630 */       d4 = d7;
/*      */     }
/* 6631 */     else if (this.r == 2)
/*      */     {
/* 6633 */       d1 = paramps.a(16.0D - this.k * 16.0D);
/* 6634 */       d3 = paramps.b(this.h * 16.0D);
/* 6635 */       d2 = paramps.a(16.0D - this.j * 16.0D);
/* 6636 */       d4 = paramps.b(this.i * 16.0D);
/*      */       
/*      */ 
/* 6639 */       d5 = d2;
/* 6640 */       d6 = d1;
/* 6641 */       d7 = d3;
/* 6642 */       d8 = d4;
/* 6643 */       d1 = d5;
/* 6644 */       d2 = d6;
/* 6645 */       d7 = d4;
/* 6646 */       d8 = d3;
/*      */     }
/* 6647 */     else if (this.r == 3)
/*      */     {
/* 6648 */       d1 = paramps.a(16.0D - this.h * 16.0D);
/* 6649 */       d2 = paramps.a(16.0D - this.i * 16.0D);
/* 6650 */       d3 = paramps.b(this.k * 16.0D);
/* 6651 */       d4 = paramps.b(this.j * 16.0D);
/*      */       
/* 6653 */       d5 = d2;
/* 6654 */       d6 = d1;
/* 6655 */       d7 = d3;
/* 6656 */       d8 = d4;
/*      */     }
/* 6659 */     double d9 = paramDouble1 + this.h;
/* 6660 */     double d10 = paramDouble1 + this.i;
/* 6661 */     double d11 = paramDouble2 + this.j;
/* 6662 */     double d12 = paramDouble2 + this.k;
/* 6663 */     double d13 = paramDouble3 + this.m;
/* 6665 */     if (this.g)
/*      */     {
/* 6666 */       d9 = paramDouble1 + this.i;
/* 6667 */       d10 = paramDouble1 + this.h;
/*      */     }
/* 6670 */     if (this.w)
/*      */     {
/* 6671 */       localblz.b(this.ap, this.at, this.ax);
/* 6672 */       localblz.b(this.al);
/* 6673 */       localblz.a(d9, d12, d13, d1, d3);
/*      */       
/* 6675 */       localblz.b(this.aq, this.au, this.ay);
/* 6676 */       localblz.b(this.am);
/* 6677 */       localblz.a(d9, d11, d13, d6, d8);
/*      */       
/* 6679 */       localblz.b(this.ar, this.av, this.az);
/* 6680 */       localblz.b(this.an);
/* 6681 */       localblz.a(d10, d11, d13, d2, d4);
/*      */       
/* 6683 */       localblz.b(this.as, this.aw, this.aA);
/* 6684 */       localblz.b(this.ao);
/* 6685 */       localblz.a(d10, d12, d13, d5, d7);
/*      */     }
/*      */     else
/*      */     {
/* 6687 */       localblz.a(d9, d12, d13, d1, d3);
/* 6688 */       localblz.a(d9, d11, d13, d6, d8);
/* 6689 */       localblz.a(d10, d11, d13, d2, d4);
/* 6690 */       localblz.a(d10, d12, d13, d5, d7);
/*      */     }
/*      */   }
/*      */   
/*      */   public void e(ahu paramahu, double paramDouble1, double paramDouble2, double paramDouble3, ps paramps)
/*      */   {
/* 6695 */     blz localblz = blz.a;
/* 6697 */     if (b()) {
/* 6697 */       paramps = this.d;
/*      */     }
/* 6698 */     double d1 = paramps.a(this.l * 16.0D);
/* 6699 */     double d2 = paramps.a(this.m * 16.0D);
/* 6700 */     double d3 = paramps.b(16.0D - this.k * 16.0D);
/* 6701 */     double d4 = paramps.b(16.0D - this.j * 16.0D);
/* 6703 */     if (this.e)
/*      */     {
/* 6704 */       d5 = d1;
/* 6705 */       d1 = d2;
/* 6706 */       d2 = d5;
/*      */     }
/* 6709 */     if ((this.l < 0.0D) || (this.m > 1.0D))
/*      */     {
/* 6710 */       d1 = paramps.c();
/* 6711 */       d2 = paramps.d();
/*      */     }
/* 6713 */     if ((this.j < 0.0D) || (this.k > 1.0D))
/*      */     {
/* 6714 */       d3 = paramps.e();
/* 6715 */       d4 = paramps.f();
/*      */     }
/* 6718 */     double d5 = d2;double d6 = d1;double d7 = d3;double d8 = d4;
/* 6720 */     if (this.t == 1)
/*      */     {
/* 6721 */       d1 = paramps.a(this.j * 16.0D);
/* 6722 */       d3 = paramps.b(16.0D - this.m * 16.0D);
/* 6723 */       d2 = paramps.a(this.k * 16.0D);
/* 6724 */       d4 = paramps.b(16.0D - this.l * 16.0D);
/*      */       
/* 6726 */       d5 = d2;
/* 6727 */       d6 = d1;
/* 6728 */       d7 = d3;
/* 6729 */       d8 = d4;
/* 6730 */       d5 = d1;
/* 6731 */       d6 = d2;
/* 6732 */       d3 = d4;
/* 6733 */       d4 = d7;
/*      */     }
/* 6734 */     else if (this.t == 2)
/*      */     {
/* 6736 */       d1 = paramps.a(16.0D - this.k * 16.0D);
/* 6737 */       d3 = paramps.b(this.l * 16.0D);
/* 6738 */       d2 = paramps.a(16.0D - this.j * 16.0D);
/* 6739 */       d4 = paramps.b(this.m * 16.0D);
/*      */       
/*      */ 
/* 6742 */       d5 = d2;
/* 6743 */       d6 = d1;
/* 6744 */       d7 = d3;
/* 6745 */       d8 = d4;
/* 6746 */       d1 = d5;
/* 6747 */       d2 = d6;
/* 6748 */       d7 = d4;
/* 6749 */       d8 = d3;
/*      */     }
/* 6750 */     else if (this.t == 3)
/*      */     {
/* 6751 */       d1 = paramps.a(16.0D - this.l * 16.0D);
/* 6752 */       d2 = paramps.a(16.0D - this.m * 16.0D);
/* 6753 */       d3 = paramps.b(this.k * 16.0D);
/* 6754 */       d4 = paramps.b(this.j * 16.0D);
/*      */       
/* 6756 */       d5 = d2;
/* 6757 */       d6 = d1;
/* 6758 */       d7 = d3;
/* 6759 */       d8 = d4;
/*      */     }
/* 6762 */     double d9 = paramDouble1 + this.h;
/* 6763 */     double d10 = paramDouble2 + this.j;
/* 6764 */     double d11 = paramDouble2 + this.k;
/* 6765 */     double d12 = paramDouble3 + this.l;
/* 6766 */     double d13 = paramDouble3 + this.m;
/* 6768 */     if (this.g)
/*      */     {
/* 6769 */       d12 = paramDouble3 + this.m;
/* 6770 */       d13 = paramDouble3 + this.l;
/*      */     }
/* 6773 */     if (this.w)
/*      */     {
/* 6774 */       localblz.b(this.ap, this.at, this.ax);
/* 6775 */       localblz.b(this.al);
/* 6776 */       localblz.a(d9, d11, d13, d5, d7);
/*      */       
/* 6778 */       localblz.b(this.aq, this.au, this.ay);
/* 6779 */       localblz.b(this.am);
/* 6780 */       localblz.a(d9, d11, d12, d1, d3);
/*      */       
/* 6782 */       localblz.b(this.ar, this.av, this.az);
/* 6783 */       localblz.b(this.an);
/* 6784 */       localblz.a(d9, d10, d12, d6, d8);
/*      */       
/* 6786 */       localblz.b(this.as, this.aw, this.aA);
/* 6787 */       localblz.b(this.ao);
/* 6788 */       localblz.a(d9, d10, d13, d2, d4);
/*      */     }
/*      */     else
/*      */     {
/* 6790 */       localblz.a(d9, d11, d13, d5, d7);
/* 6791 */       localblz.a(d9, d11, d12, d1, d3);
/* 6792 */       localblz.a(d9, d10, d12, d6, d8);
/* 6793 */       localblz.a(d9, d10, d13, d2, d4);
/*      */     }
/*      */   }
/*      */   
/*      */   public void f(ahu paramahu, double paramDouble1, double paramDouble2, double paramDouble3, ps paramps)
/*      */   {
/* 6798 */     blz localblz = blz.a;
/* 6800 */     if (b()) {
/* 6800 */       paramps = this.d;
/*      */     }
/* 6801 */     double d1 = paramps.a(this.m * 16.0D);
/* 6802 */     double d2 = paramps.a(this.l * 16.0D);
/* 6803 */     double d3 = paramps.b(16.0D - this.k * 16.0D);
/* 6804 */     double d4 = paramps.b(16.0D - this.j * 16.0D);
/* 6806 */     if (this.e)
/*      */     {
/* 6807 */       d5 = d1;
/* 6808 */       d1 = d2;
/* 6809 */       d2 = d5;
/*      */     }
/* 6812 */     if ((this.l < 0.0D) || (this.m > 1.0D))
/*      */     {
/* 6813 */       d1 = paramps.c();
/* 6814 */       d2 = paramps.d();
/*      */     }
/* 6816 */     if ((this.j < 0.0D) || (this.k > 1.0D))
/*      */     {
/* 6817 */       d3 = paramps.e();
/* 6818 */       d4 = paramps.f();
/*      */     }
/* 6821 */     double d5 = d2;double d6 = d1;double d7 = d3;double d8 = d4;
/* 6823 */     if (this.s == 2)
/*      */     {
/* 6824 */       d1 = paramps.a(this.j * 16.0D);
/* 6825 */       d3 = paramps.b(16.0D - this.l * 16.0D);
/* 6826 */       d2 = paramps.a(this.k * 16.0D);
/* 6827 */       d4 = paramps.b(16.0D - this.m * 16.0D);
/*      */       
/* 6829 */       d5 = d2;
/* 6830 */       d6 = d1;
/* 6831 */       d7 = d3;
/* 6832 */       d8 = d4;
/* 6833 */       d5 = d1;
/* 6834 */       d6 = d2;
/* 6835 */       d3 = d4;
/* 6836 */       d4 = d7;
/*      */     }
/* 6837 */     else if (this.s == 1)
/*      */     {
/* 6839 */       d1 = paramps.a(16.0D - this.k * 16.0D);
/* 6840 */       d3 = paramps.b(this.m * 16.0D);
/* 6841 */       d2 = paramps.a(16.0D - this.j * 16.0D);
/* 6842 */       d4 = paramps.b(this.l * 16.0D);
/*      */       
/*      */ 
/* 6845 */       d5 = d2;
/* 6846 */       d6 = d1;
/* 6847 */       d7 = d3;
/* 6848 */       d8 = d4;
/* 6849 */       d1 = d5;
/* 6850 */       d2 = d6;
/* 6851 */       d7 = d4;
/* 6852 */       d8 = d3;
/*      */     }
/* 6853 */     else if (this.s == 3)
/*      */     {
/* 6854 */       d1 = paramps.a(16.0D - this.l * 16.0D);
/* 6855 */       d2 = paramps.a(16.0D - this.m * 16.0D);
/* 6856 */       d3 = paramps.b(this.k * 16.0D);
/* 6857 */       d4 = paramps.b(this.j * 16.0D);
/*      */       
/* 6859 */       d5 = d2;
/* 6860 */       d6 = d1;
/* 6861 */       d7 = d3;
/* 6862 */       d8 = d4;
/*      */     }
/* 6865 */     double d9 = paramDouble1 + this.i;
/* 6866 */     double d10 = paramDouble2 + this.j;
/* 6867 */     double d11 = paramDouble2 + this.k;
/* 6868 */     double d12 = paramDouble3 + this.l;
/* 6869 */     double d13 = paramDouble3 + this.m;
/* 6871 */     if (this.g)
/*      */     {
/* 6872 */       d12 = paramDouble3 + this.m;
/* 6873 */       d13 = paramDouble3 + this.l;
/*      */     }
/* 6876 */     if (this.w)
/*      */     {
/* 6877 */       localblz.b(this.ap, this.at, this.ax);
/* 6878 */       localblz.b(this.al);
/* 6879 */       localblz.a(d9, d10, d13, d6, d8);
/*      */       
/* 6881 */       localblz.b(this.aq, this.au, this.ay);
/* 6882 */       localblz.b(this.am);
/* 6883 */       localblz.a(d9, d10, d12, d2, d4);
/*      */       
/* 6885 */       localblz.b(this.ar, this.av, this.az);
/* 6886 */       localblz.b(this.an);
/* 6887 */       localblz.a(d9, d11, d12, d5, d7);
/*      */       
/* 6889 */       localblz.b(this.as, this.aw, this.aA);
/* 6890 */       localblz.b(this.ao);
/* 6891 */       localblz.a(d9, d11, d13, d1, d3);
/*      */     }
/*      */     else
/*      */     {
/* 6893 */       localblz.a(d9, d10, d13, d6, d8);
/* 6894 */       localblz.a(d9, d10, d12, d2, d4);
/* 6895 */       localblz.a(d9, d11, d12, d5, d7);
/* 6896 */       localblz.a(d9, d11, d13, d1, d3);
/*      */     }
/*      */   }
/*      */   
/*      */   public void a(ahu paramahu, int paramInt, float paramFloat)
/*      */   {
/* 6932 */     blz localblz = blz.a;
/*      */     
/* 6934 */     int i1 = paramahu == ahz.c ? 1 : 0;
/* 6936 */     if ((paramahu == ahz.z) || (paramahu == ahz.cd) || (paramahu == ahz.al)) {
/* 6937 */       paramInt = 3;
/*      */     }
/*      */     float f3;
/*      */     float f5;
/* 6940 */     if (this.c)
/*      */     {
/* 6941 */       i2 = paramahu.i(paramInt);
/* 6942 */       if (i1 != 0) {
/* 6943 */         i2 = 16777215;
/*      */       }
/* 6945 */       float f1 = (i2 >> 16 & 0xFF) / 255.0F;
/* 6946 */       f3 = (i2 >> 8 & 0xFF) / 255.0F;
/* 6947 */       f5 = (i2 & 0xFF) / 255.0F;
/*      */       
/* 6949 */       GL11.glColor4f(f1 * paramFloat, f3 * paramFloat, f5 * paramFloat, 1.0F);
/*      */     }
/* 6952 */     int i2 = paramahu.b();
/* 6953 */     a(paramahu);
/* 6955 */     if ((i2 == 0) || (i2 == 31) || (i2 == 39) || (i2 == 16) || (i2 == 26))
/*      */     {
/* 6956 */       if (i2 == 16) {
/* 6957 */         paramInt = 1;
/*      */       }
/* 6959 */       paramahu.g();
/* 6960 */       a(paramahu);
/* 6961 */       GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
/* 6962 */       GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 6963 */       localblz.b();
/* 6964 */       localblz.c(0.0F, -1.0F, 0.0F);
/* 6965 */       a(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 0, paramInt));
/* 6966 */       localblz.a();
/* 6968 */       if ((i1 != 0) && (this.c))
/*      */       {
/* 6969 */         int i3 = paramahu.i(paramInt);
/* 6970 */         f3 = (i3 >> 16 & 0xFF) / 255.0F;
/* 6971 */         f5 = (i3 >> 8 & 0xFF) / 255.0F;
/* 6972 */         float f6 = (i3 & 0xFF) / 255.0F;
/*      */         
/* 6974 */         GL11.glColor4f(f3 * paramFloat, f5 * paramFloat, f6 * paramFloat, 1.0F);
/*      */       }
/* 6977 */       localblz.b();
/* 6978 */       localblz.c(0.0F, 1.0F, 0.0F);
/* 6979 */       b(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 1, paramInt));
/* 6980 */       localblz.a();
/* 6982 */       if ((i1 != 0) && (this.c)) {
/* 6983 */         GL11.glColor4f(paramFloat, paramFloat, paramFloat, 1.0F);
/*      */       }
/* 6986 */       localblz.b();
/* 6987 */       localblz.c(0.0F, 0.0F, -1.0F);
/* 6988 */       c(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 2, paramInt));
/* 6989 */       localblz.a();
/*      */       
/* 6991 */       localblz.b();
/* 6992 */       localblz.c(0.0F, 0.0F, 1.0F);
/* 6993 */       d(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 3, paramInt));
/* 6994 */       localblz.a();
/*      */       
/* 6996 */       localblz.b();
/* 6997 */       localblz.c(-1.0F, 0.0F, 0.0F);
/* 6998 */       e(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 4, paramInt));
/* 6999 */       localblz.a();
/*      */       
/* 7001 */       localblz.b();
/* 7002 */       localblz.c(1.0F, 0.0F, 0.0F);
/* 7003 */       f(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 5, paramInt));
/* 7004 */       localblz.a();
/*      */       
/* 7006 */       GL11.glTranslatef(0.5F, 0.5F, 0.5F);
/*      */     }
/* 7007 */     else if (i2 == 1)
/*      */     {
/* 7008 */       localblz.b();
/* 7009 */       localblz.c(0.0F, -1.0F, 0.0F);
/* 7010 */       ps localps = a(paramahu, 0, paramInt);
/* 7011 */       a(localps, -0.5D, -0.5D, -0.5D, 1.0F);
/* 7012 */       localblz.a();
/*      */     }
/* 7013 */     else if (i2 == 19)
/*      */     {
/* 7014 */       localblz.b();
/* 7015 */       localblz.c(0.0F, -1.0F, 0.0F);
/* 7016 */       paramahu.g();
/* 7017 */       a(paramahu, paramInt, this.k, -0.5D, -0.5D, -0.5D);
/* 7018 */       localblz.a();
/*      */     }
/* 7019 */     else if (i2 == 23)
/*      */     {
/* 7020 */       localblz.b();
/* 7021 */       localblz.c(0.0F, -1.0F, 0.0F);
/* 7022 */       paramahu.g();
/* 7023 */       localblz.a();
/*      */     }
/* 7024 */     else if (i2 == 13)
/*      */     {
/* 7025 */       paramahu.g();
/* 7026 */       GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 7027 */       float f2 = 0.0625F;
/* 7028 */       localblz.b();
/* 7029 */       localblz.c(0.0F, -1.0F, 0.0F);
/* 7030 */       a(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 0));
/* 7031 */       localblz.a();
/*      */       
/* 7033 */       localblz.b();
/* 7034 */       localblz.c(0.0F, 1.0F, 0.0F);
/* 7035 */       b(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 1));
/* 7036 */       localblz.a();
/*      */       
/* 7038 */       localblz.b();
/* 7039 */       localblz.c(0.0F, 0.0F, -1.0F);
/* 7040 */       localblz.d(0.0F, 0.0F, f2);
/* 7041 */       c(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 2));
/* 7042 */       localblz.d(0.0F, 0.0F, -f2);
/* 7043 */       localblz.a();
/*      */       
/* 7045 */       localblz.b();
/* 7046 */       localblz.c(0.0F, 0.0F, 1.0F);
/* 7047 */       localblz.d(0.0F, 0.0F, -f2);
/* 7048 */       d(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 3));
/* 7049 */       localblz.d(0.0F, 0.0F, f2);
/* 7050 */       localblz.a();
/*      */       
/* 7052 */       localblz.b();
/* 7053 */       localblz.c(-1.0F, 0.0F, 0.0F);
/* 7054 */       localblz.d(f2, 0.0F, 0.0F);
/* 7055 */       e(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 4));
/* 7056 */       localblz.d(-f2, 0.0F, 0.0F);
/* 7057 */       localblz.a();
/*      */       
/* 7059 */       localblz.b();
/* 7060 */       localblz.c(1.0F, 0.0F, 0.0F);
/* 7061 */       localblz.d(-f2, 0.0F, 0.0F);
/* 7062 */       f(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 5));
/* 7063 */       localblz.d(f2, 0.0F, 0.0F);
/* 7064 */       localblz.a();
/*      */       
/* 7066 */       GL11.glTranslatef(0.5F, 0.5F, 0.5F);
/*      */     }
/* 7067 */     else if (i2 == 22)
/*      */     {
/* 7068 */       GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
/* 7069 */       GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 7070 */       blk.a.a(paramahu, paramInt, paramFloat);
/* 7071 */       GL11.glEnable(32826);
/*      */     }
/* 7072 */     else if (i2 == 6)
/*      */     {
/* 7073 */       localblz.b();
/* 7074 */       localblz.c(0.0F, -1.0F, 0.0F);
/* 7075 */       a(paramahu, paramInt, -0.5D, -0.5D, -0.5D);
/* 7076 */       localblz.a();
/*      */     }
/* 7077 */     else if (i2 == 2)
/*      */     {
/* 7078 */       localblz.b();
/* 7079 */       localblz.c(0.0F, -1.0F, 0.0F);
/* 7080 */       a(paramahu, -0.5D, -0.5D, -0.5D, 0.0D, 0.0D, 0);
/* 7081 */       localblz.a();
/*      */     }
/*      */     else
/*      */     {
/*      */       int i4;
/* 7082 */       if (i2 == 10)
/*      */       {
/* 7083 */         for (i4 = 0; i4 < 2; i4++)
/*      */         {
/* 7084 */           if (i4 == 0) {
/* 7084 */             a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.5D);
/*      */           }
/* 7085 */           if (i4 == 1) {
/* 7085 */             a(0.0D, 0.0D, 0.5D, 1.0D, 0.5D, 1.0D);
/*      */           }
/* 7087 */           GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 7088 */           localblz.b();
/* 7089 */           localblz.c(0.0F, -1.0F, 0.0F);
/* 7090 */           a(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 0));
/* 7091 */           localblz.a();
/*      */           
/* 7093 */           localblz.b();
/* 7094 */           localblz.c(0.0F, 1.0F, 0.0F);
/* 7095 */           b(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 1));
/* 7096 */           localblz.a();
/*      */           
/* 7098 */           localblz.b();
/* 7099 */           localblz.c(0.0F, 0.0F, -1.0F);
/* 7100 */           c(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 2));
/* 7101 */           localblz.a();
/*      */           
/* 7103 */           localblz.b();
/* 7104 */           localblz.c(0.0F, 0.0F, 1.0F);
/* 7105 */           d(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 3));
/* 7106 */           localblz.a();
/*      */           
/* 7108 */           localblz.b();
/* 7109 */           localblz.c(-1.0F, 0.0F, 0.0F);
/* 7110 */           e(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 4));
/* 7111 */           localblz.a();
/*      */           
/* 7113 */           localblz.b();
/* 7114 */           localblz.c(1.0F, 0.0F, 0.0F);
/* 7115 */           f(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 5));
/* 7116 */           localblz.a();
/*      */           
/* 7118 */           GL11.glTranslatef(0.5F, 0.5F, 0.5F);
/*      */         }
/*      */       }
/* 7120 */       else if (i2 == 27)
/*      */       {
/* 7121 */         i4 = 0;
/* 7122 */         GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 7123 */         localblz.b();
/* 7124 */         for (int i5 = 0; i5 < 8; i5++)
/*      */         {
/* 7125 */           int i6 = 0;
/* 7126 */           int i7 = 1;
/* 7127 */           if (i5 == 0) {
/* 7127 */             i6 = 2;
/*      */           }
/* 7128 */           if (i5 == 1) {
/* 7128 */             i6 = 3;
/*      */           }
/* 7129 */           if (i5 == 2) {
/* 7129 */             i6 = 4;
/*      */           }
/* 7130 */           if (i5 == 3)
/*      */           {
/* 7131 */             i6 = 5;
/* 7132 */             i7 = 2;
/*      */           }
/* 7134 */           if (i5 == 4)
/*      */           {
/* 7135 */             i6 = 6;
/* 7136 */             i7 = 3;
/*      */           }
/* 7138 */           if (i5 == 5)
/*      */           {
/* 7139 */             i6 = 7;
/* 7140 */             i7 = 5;
/*      */           }
/* 7142 */           if (i5 == 6)
/*      */           {
/* 7143 */             i6 = 6;
/* 7144 */             i7 = 2;
/*      */           }
/* 7146 */           if (i5 == 7) {
/* 7146 */             i6 = 3;
/*      */           }
/* 7147 */           float f7 = i6 / 16.0F;
/* 7148 */           float f8 = 1.0F - i4 / 16.0F;
/* 7149 */           float f9 = 1.0F - (i4 + i7) / 16.0F;
/* 7150 */           i4 += i7;
/* 7151 */           a(0.5F - f7, f9, 0.5F - f7, 0.5F + f7, f8, 0.5F + f7);
/* 7152 */           localblz.c(0.0F, -1.0F, 0.0F);
/* 7153 */           a(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 0));
/* 7154 */           localblz.c(0.0F, 1.0F, 0.0F);
/* 7155 */           b(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 1));
/* 7156 */           localblz.c(0.0F, 0.0F, -1.0F);
/* 7157 */           c(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 2));
/* 7158 */           localblz.c(0.0F, 0.0F, 1.0F);
/* 7159 */           d(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 3));
/* 7160 */           localblz.c(-1.0F, 0.0F, 0.0F);
/* 7161 */           e(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 4));
/* 7162 */           localblz.c(1.0F, 0.0F, 0.0F);
/* 7163 */           f(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 5));
/*      */         }
/* 7165 */         localblz.a();
/* 7166 */         GL11.glTranslatef(0.5F, 0.5F, 0.5F);
/* 7167 */         a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/*      */       }
/*      */       else
/*      */       {
/*      */         float f4;
/* 7168 */         if (i2 == 11)
/*      */         {
/* 7169 */           for (i4 = 0; i4 < 4; i4++)
/*      */           {
/* 7170 */             f4 = 0.125F;
/* 7171 */             if (i4 == 0) {
/* 7171 */               a(0.5F - f4, 0.0D, 0.0D, 0.5F + f4, 1.0D, f4 * 2.0F);
/*      */             }
/* 7172 */             if (i4 == 1) {
/* 7172 */               a(0.5F - f4, 0.0D, 1.0F - f4 * 2.0F, 0.5F + f4, 1.0D, 1.0D);
/*      */             }
/* 7173 */             f4 = 0.0625F;
/* 7174 */             if (i4 == 2) {
/* 7174 */               a(0.5F - f4, 1.0F - f4 * 3.0F, -f4 * 2.0F, 0.5F + f4, 1.0F - f4, 1.0F + f4 * 2.0F);
/*      */             }
/* 7175 */             if (i4 == 3) {
/* 7175 */               a(0.5F - f4, 0.5F - f4 * 3.0F, -f4 * 2.0F, 0.5F + f4, 0.5F - f4, 1.0F + f4 * 2.0F);
/*      */             }
/* 7177 */             GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 7178 */             localblz.b();
/* 7179 */             localblz.c(0.0F, -1.0F, 0.0F);
/* 7180 */             a(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 0));
/* 7181 */             localblz.a();
/*      */             
/* 7183 */             localblz.b();
/* 7184 */             localblz.c(0.0F, 1.0F, 0.0F);
/* 7185 */             b(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 1));
/* 7186 */             localblz.a();
/*      */             
/* 7188 */             localblz.b();
/* 7189 */             localblz.c(0.0F, 0.0F, -1.0F);
/* 7190 */             c(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 2));
/* 7191 */             localblz.a();
/*      */             
/* 7193 */             localblz.b();
/* 7194 */             localblz.c(0.0F, 0.0F, 1.0F);
/* 7195 */             d(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 3));
/* 7196 */             localblz.a();
/*      */             
/* 7198 */             localblz.b();
/* 7199 */             localblz.c(-1.0F, 0.0F, 0.0F);
/* 7200 */             e(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 4));
/* 7201 */             localblz.a();
/*      */             
/* 7203 */             localblz.b();
/* 7204 */             localblz.c(1.0F, 0.0F, 0.0F);
/* 7205 */             f(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 5));
/* 7206 */             localblz.a();
/*      */             
/* 7208 */             GL11.glTranslatef(0.5F, 0.5F, 0.5F);
/*      */           }
/* 7210 */           a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/*      */         }
/* 7211 */         else if (i2 == 21)
/*      */         {
/* 7212 */           for (i4 = 0; i4 < 3; i4++)
/*      */           {
/* 7213 */             f4 = 0.0625F;
/* 7214 */             if (i4 == 0) {
/* 7214 */               a(0.5F - f4, 0.300000011920929D, 0.0D, 0.5F + f4, 1.0D, f4 * 2.0F);
/*      */             }
/* 7215 */             if (i4 == 1) {
/* 7215 */               a(0.5F - f4, 0.300000011920929D, 1.0F - f4 * 2.0F, 0.5F + f4, 1.0D, 1.0D);
/*      */             }
/* 7216 */             f4 = 0.0625F;
/* 7217 */             if (i4 == 2) {
/* 7217 */               a(0.5F - f4, 0.5D, 0.0D, 0.5F + f4, 1.0F - f4, 1.0D);
/*      */             }
/* 7219 */             GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 7220 */             localblz.b();
/* 7221 */             localblz.c(0.0F, -1.0F, 0.0F);
/* 7222 */             a(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 0));
/* 7223 */             localblz.a();
/*      */             
/* 7225 */             localblz.b();
/* 7226 */             localblz.c(0.0F, 1.0F, 0.0F);
/* 7227 */             b(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 1));
/* 7228 */             localblz.a();
/*      */             
/* 7230 */             localblz.b();
/* 7231 */             localblz.c(0.0F, 0.0F, -1.0F);
/* 7232 */             c(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 2));
/* 7233 */             localblz.a();
/*      */             
/* 7235 */             localblz.b();
/* 7236 */             localblz.c(0.0F, 0.0F, 1.0F);
/* 7237 */             d(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 3));
/* 7238 */             localblz.a();
/*      */             
/* 7240 */             localblz.b();
/* 7241 */             localblz.c(-1.0F, 0.0F, 0.0F);
/* 7242 */             e(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 4));
/* 7243 */             localblz.a();
/*      */             
/* 7245 */             localblz.b();
/* 7246 */             localblz.c(1.0F, 0.0F, 0.0F);
/* 7247 */             f(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 5));
/* 7248 */             localblz.a();
/*      */             
/* 7250 */             GL11.glTranslatef(0.5F, 0.5F, 0.5F);
/*      */           }
/*      */         }
/* 7252 */         else if (i2 == 32)
/*      */         {
/* 7253 */           for (i4 = 0; i4 < 2; i4++)
/*      */           {
/* 7254 */             if (i4 == 0) {
/* 7254 */               a(0.0D, 0.0D, 0.3125D, 1.0D, 0.8125D, 0.6875D);
/*      */             }
/* 7255 */             if (i4 == 1) {
/* 7255 */               a(0.25D, 0.0D, 0.25D, 0.75D, 1.0D, 0.75D);
/*      */             }
/* 7257 */             GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 7258 */             localblz.b();
/* 7259 */             localblz.c(0.0F, -1.0F, 0.0F);
/* 7260 */             a(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 0, paramInt));
/* 7261 */             localblz.a();
/*      */             
/* 7263 */             localblz.b();
/* 7264 */             localblz.c(0.0F, 1.0F, 0.0F);
/* 7265 */             b(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 1, paramInt));
/* 7266 */             localblz.a();
/*      */             
/* 7268 */             localblz.b();
/* 7269 */             localblz.c(0.0F, 0.0F, -1.0F);
/* 7270 */             c(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 2, paramInt));
/* 7271 */             localblz.a();
/*      */             
/* 7273 */             localblz.b();
/* 7274 */             localblz.c(0.0F, 0.0F, 1.0F);
/* 7275 */             d(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 3, paramInt));
/* 7276 */             localblz.a();
/*      */             
/* 7278 */             localblz.b();
/* 7279 */             localblz.c(-1.0F, 0.0F, 0.0F);
/* 7280 */             e(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 4, paramInt));
/* 7281 */             localblz.a();
/*      */             
/* 7283 */             localblz.b();
/* 7284 */             localblz.c(1.0F, 0.0F, 0.0F);
/* 7285 */             f(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 5, paramInt));
/* 7286 */             localblz.a();
/*      */             
/* 7288 */             GL11.glTranslatef(0.5F, 0.5F, 0.5F);
/*      */           }
/* 7290 */           a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/*      */         }
/* 7291 */         else if (i2 == 35)
/*      */         {
/* 7292 */           GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 7293 */           a((ahn)paramahu, 0, 0, 0, paramInt << 2, true);
/* 7294 */           GL11.glTranslatef(0.5F, 0.5F, 0.5F);
/*      */         }
/* 7295 */         else if (i2 == 34)
/*      */         {
/* 7296 */           for (i4 = 0; i4 < 3; i4++)
/*      */           {
/* 7297 */             if (i4 == 0)
/*      */             {
/* 7298 */               a(0.125D, 0.0D, 0.125D, 0.875D, 0.1875D, 0.875D);
/* 7299 */               a(b(ahz.Z));
/*      */             }
/* 7300 */             else if (i4 == 1)
/*      */             {
/* 7301 */               a(0.1875D, 0.1875D, 0.1875D, 0.8125D, 0.875D, 0.8125D);
/* 7302 */               a(b(ahz.bJ));
/*      */             }
/* 7303 */             else if (i4 == 2)
/*      */             {
/* 7304 */               a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/* 7305 */               a(b(ahz.w));
/*      */             }
/* 7308 */             GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 7309 */             localblz.b();
/* 7310 */             localblz.c(0.0F, -1.0F, 0.0F);
/* 7311 */             a(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 0, paramInt));
/* 7312 */             localblz.a();
/*      */             
/* 7314 */             localblz.b();
/* 7315 */             localblz.c(0.0F, 1.0F, 0.0F);
/* 7316 */             b(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 1, paramInt));
/* 7317 */             localblz.a();
/*      */             
/* 7319 */             localblz.b();
/* 7320 */             localblz.c(0.0F, 0.0F, -1.0F);
/* 7321 */             c(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 2, paramInt));
/* 7322 */             localblz.a();
/*      */             
/* 7324 */             localblz.b();
/* 7325 */             localblz.c(0.0F, 0.0F, 1.0F);
/* 7326 */             d(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 3, paramInt));
/* 7327 */             localblz.a();
/*      */             
/* 7329 */             localblz.b();
/* 7330 */             localblz.c(-1.0F, 0.0F, 0.0F);
/* 7331 */             e(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 4, paramInt));
/* 7332 */             localblz.a();
/*      */             
/* 7334 */             localblz.b();
/* 7335 */             localblz.c(1.0F, 0.0F, 0.0F);
/* 7336 */             f(paramahu, 0.0D, 0.0D, 0.0D, a(paramahu, 5, paramInt));
/* 7337 */             localblz.a();
/*      */             
/* 7339 */             GL11.glTranslatef(0.5F, 0.5F, 0.5F);
/*      */           }
/* 7341 */           a(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
/* 7342 */           a();
/*      */         }
/* 7343 */         else if (i2 == 38)
/*      */         {
/* 7344 */           GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
/* 7345 */           a((ajz)paramahu, 0, 0, 0, 0, true);
/* 7346 */           GL11.glTranslatef(0.5F, 0.5F, 0.5F);
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public static boolean a(int paramInt)
/*      */   {
/* 7352 */     if (paramInt == 0) {
/* 7352 */       return true;
/*      */     }
/* 7353 */     if (paramInt == 31) {
/* 7353 */       return true;
/*      */     }
/* 7354 */     if (paramInt == 39) {
/* 7354 */       return true;
/*      */     }
/* 7355 */     if (paramInt == 13) {
/* 7355 */       return true;
/*      */     }
/* 7356 */     if (paramInt == 10) {
/* 7356 */       return true;
/*      */     }
/* 7357 */     if (paramInt == 11) {
/* 7357 */       return true;
/*      */     }
/* 7358 */     if (paramInt == 27) {
/* 7358 */       return true;
/*      */     }
/* 7359 */     if (paramInt == 22) {
/* 7359 */       return true;
/*      */     }
/* 7360 */     if (paramInt == 21) {
/* 7360 */       return true;
/*      */     }
/* 7361 */     if (paramInt == 16) {
/* 7361 */       return true;
/*      */     }
/* 7362 */     if (paramInt == 26) {
/* 7362 */       return true;
/*      */     }
/* 7363 */     if (paramInt == 32) {
/* 7363 */       return true;
/*      */     }
/* 7364 */     if (paramInt == 34) {
/* 7364 */       return true;
/*      */     }
/* 7365 */     if (paramInt == 35) {
/* 7365 */       return true;
/*      */     }
/* 7366 */     if (paramInt == -1) {
/* 7366 */       return false;
/*      */     }
/* 7367 */     return false;
/*      */   }
/*      */   
/*      */   public ps a(ahu paramahu, afx paramafx, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
/*      */   {
/* 7371 */     return b(paramahu.e(paramafx, paramInt1, paramInt2, paramInt3, paramInt4));
/*      */   }
/*      */   
/*      */   public ps a(ahu paramahu, int paramInt1, int paramInt2)
/*      */   {
/* 7375 */     return b(paramahu.a(paramInt1, paramInt2));
/*      */   }
/*      */   
/*      */   public ps a(ahu paramahu, int paramInt)
/*      */   {
/* 7379 */     return b(paramahu.h(paramInt));
/*      */   }
/*      */   
/*      */   public ps b(ahu paramahu)
/*      */   {
/* 7383 */     return b(paramahu.h(1));
/*      */   }
/*      */   
/*      */   public ps b(ps paramps)
/*      */   {
/* 7387 */     if (paramps == null) {
/* 7388 */       paramps = ((bpr)azd.A().N().b(bpr.b)).b("missingno");
/*      */     }
/* 7390 */     return paramps;
/*      */   }
/*      */ }


/* Location:           C:\Users\Andrew.Billings\AppData\Roaming\.minecraft\versions\1.7.2\1.7.2\
 * Qualified Name:     ble
 * JD-Core Version:    0.7.0.1
 */