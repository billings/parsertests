﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Test
{
    public abstract class ParseTest
    {
        [NotNull] public readonly string Name;

        public ParseTest([NotNull] string name)
        {
            Name = name;
        }

        public virtual bool Test([NotNull] string str, int iterations)
        {
            string result = ParseString(str);
            if (result != null)
            {
                Console.WriteLine(result);
                return false;
            }

            Stopwatch sw = new Stopwatch();

            sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                ParseString(str);
            }
            sw.Stop();

            Console.WriteLine("{1}:\t {0:F2} ms", sw.Elapsed.TotalMilliseconds / iterations, Name);

            return true;
        }

        [CanBeNull]
        public abstract string ParseFile([NotNull] string path);

        [CanBeNull]
        public abstract string ParseString([NotNull] string str);

        [CanBeNull]
        public abstract string ParseReader([NotNull] TextReader reader);
    }
}
