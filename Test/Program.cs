﻿using bsn.GoldParser.Grammar;
using bsn.GoldParser.Parser;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Test
{
    class Program
    {
        public static void Main([NotNull] string[] args)
        {
            ParseTest test;

            test = new JSON.NpegTest();
            test.Test(JSON.Resources.Test, 3);

            test = new JSON.AntlrTest();
            test.Test(JSON.Resources.Test, 3);

            test = new JSON.AntlrTest2();
            test.Test(JSON.Resources.Test, 3);
            
            test = new JSON.GoldTest();
            test.Test(JSON.Resources.Test, 3);

            Console.WriteLine();
            
            test = new XML.AntlrTest();
            test.Test(XML.Resources.Test, 5);

            test = new XML.GoldTest();
            test.Test(XML.Resources.Test, 5);

            Console.WriteLine();
            
            test = new Java.AntlrTest();
            test.Test(Java.Resources.Test, 20);

            test = new Java.GoldTest();
            test.Test(Java.Resources.Test, 20);
        }

        #region OLD
        //#define SHOW_VALID_FAILS
        //#define SHOW_INVALID_FAILS

        private static readonly Dictionary<string, string> _validFails = new Dictionary<string, string>();
        private static readonly Dictionary<string, string> _invalidFails = new Dictionary<string, string>();

        public static void Main2([NotNull]string[] args)
        {
            string line;
            using (var reader = new StringReader(Resources.valid))
                while ((line = reader.ReadLine()) != null)
                {
                    var split = line.Split(new[] { '\t' }, 2);
                    _validFails.Add(split[0], split[1]);
                }
            using (var reader = new StringReader(Resources.invalid))
                while ((line = reader.ReadLine()) != null)
                {
                    var split = line.Split(new[] { '\t' }, 2);
                    _invalidFails.Add(split[0], split[1]);
                }

            string xmlFiles = Path.Combine(Directory.GetCurrentDirectory(), @"..\..\xmlconf");
            CompiledGrammar grammar = CompiledGrammar.Load(new BinaryReader(typeof(Program).Assembly.GetManifestResourceStream("Test.XMLbsn.egt")));

            int s = xmlFiles.Length + 1;
            int fails = 0;
            int passes = 0;
            foreach (string xmlFile in Directory.GetFiles(xmlFiles, "*.xml", SearchOption.AllDirectories))
            {
                string result = ParseXML(grammar, xmlFile);
                if (result == null)
                {
                    passes++;
                    continue;
                }
                string fn = xmlFile.Substring(s, xmlFile.Length - (s + 4));

                string expected;
                if (_validFails.TryGetValue(fn, out expected))
                {
#if SHOW_VALID_FAILS
                        Console.WriteLine(fn + " ... " + expected + result);
#endif
                    continue;
                }

                if (_invalidFails.TryGetValue(fn, out expected))
                {
#if SHOW_INVALID_FAILS
                        Console.WriteLine(fn + " ... " + expected + result);
#endif
                    fails++;
                    continue;
                }
                
                Console.WriteLine(fn + " ... " + result);
                fails++;
            }

            Console.WriteLine("{0} passes.", passes);
            Console.WriteLine("{0} failures.", fails);
        }

        public static void TestGsn()
        {
            //    var grammar = CompiledGrammar.Load(new BinaryReader(typeof(Program).Assembly.GetManifestResourceStream("Test.XMLbsn.egt")));

            //    Stopwatch sw = new Stopwatch();

            //    ParseXML(grammar);

            //    sw.Start();
            //    for (int i = 0; i < 10; i++)
            //        ParseXML(grammar);
            //    sw.Stop();

            //    Console.WriteLine("{0:F2} ms", sw.Elapsed.TotalMilliseconds / 10);
        }

        private static string ParseXML(CompiledGrammar grammar, string fileName)
        {
            using (var fr = File.OpenRead(fileName))
            using (var reader = new StreamReader(fr, true))
            {
                var tokenizer = new Tokenizer(reader, grammar);
                var processor = new LalrProcessor(tokenizer, true);
                ParseMessage parseMessage = processor.ParseAll();
                if (parseMessage == ParseMessage.Accept)
                    return null;
                Token token = processor.CurrentToken;
                if (token == null)
                    return string.Format("{0} - NO TOKEN!", parseMessage);

                int col = token.Position.Column;
                int line = token.Position.Line;
                return string.Format("{0}{1} at line {2}, column {3}.{0}{4}{0}",
                    Environment.NewLine,
                        parseMessage,
                        line,
                        col,
                        token.Text);
            }
        }
        #endregion
    }
}
