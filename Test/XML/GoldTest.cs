﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bsn.GoldParser.Grammar;

namespace Test.XML
{
    public class GoldTest : GoldParseTest
    {
        private readonly CompiledGrammar _grammar;

        public GoldTest() : base("Gold XML")
        {
            // ReSharper disable AssignNullToNotNullAttribute
            _grammar = CompiledGrammar.Load(new BinaryReader(typeof(GoldTest).Assembly.GetManifestResourceStream("Test.XML.XML.egt")));
            // ReSharper restore AssignNullToNotNullAttribute
        }

        public override CompiledGrammar Grammar
        {
            get { return _grammar; }
        }
    }
}
