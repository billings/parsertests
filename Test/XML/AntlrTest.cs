﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr4.Runtime;
using JetBrains.Annotations;

namespace Test.XML
{
    public class AntlrTest : ParseTest
    {
        private readonly StringBuilder _errorBuilder = new StringBuilder();
        private readonly XMLParser _parser = new XMLParser(null);

        public AntlrTest() : base("Antlr XML")
        {
            _parser.RemoveErrorListeners(); 
            _parser.AddErrorListener(new ErrorListener(s => _errorBuilder.AppendLine(s)));
        }
        
        public override string ParseFile(string path)
        {
            return ParseString(File.ReadAllText(path));
        }

        public override string ParseString(string str)
        {
            AntlrInputStream input = new AntlrInputStream(str);

            Lexer lexer = new XMLLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);

            _errorBuilder.Clear();
            _parser.SetInputStream(tokens);
            _parser.document();

            if (_errorBuilder.Length <= 0)
                return null;
            
            string error = _errorBuilder.ToString();
            _errorBuilder.Clear();
            
            return error;
        }

        public override string ParseReader(TextReader reader)
        {
            return ParseString(reader.ReadToEnd());
        }
    }
}
